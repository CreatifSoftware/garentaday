import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { hp, wp } from '../styles/Dimens'

export const filterIcon = require('./icons/ic_filter.png')
export const sortIcon = require('./icons/ic_filter.png')

export const CampaignFlag = (props) => {
    return (
        <View style={[styles.container, props.style]}>
            <View style={styles.triangleCorner} />
            <View style={styles.square} />
            <Text style={styles.title}>Kampanya</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        alignSelf:'flex-end',
    },
    triangleCorner: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderRightWidth: hp(35),
        borderTopWidth: hp(35),
        borderRightColor: 'transparent',
        borderTopColor: '#f6a609',
        transform: [{ rotate: '90deg' }]
    },
    square: {
        width: wp(81),
        height: hp(35),
        backgroundColor: '#f6a609'
    },
    title:{
        position:'absolute',
        right:wp(10),
        alignSelf:'center',
        color:'white',
        fontWeight:'700',
        fontSize: hp(14)
    }
})