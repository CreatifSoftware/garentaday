import React from 'react';
import { FilledButton } from '../components/buttons/FilledButton';
import renderer from 'react-test-renderer';
import { ActivityIndicator, FlatList, Text, TextInput } from 'react-native';

jest.setTimeout(35000);
it('Renders snapshot as expected', () => {
    const tree = renderer.create(<FilledButton />).toJSON();
    expect(tree).toMatchSnapshot();
});
// describe('Button', () => {
//     it('Renders snapshot as expected', () => {
//         const tree = renderer.create(<FilledButton />).toJSON();
//         expect(tree).toMatchSnapshot();
//     });
// });