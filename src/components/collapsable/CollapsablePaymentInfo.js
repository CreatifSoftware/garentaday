import React, { useState } from "react"
import { StyleSheet, Text, View, Image } from "react-native"
import { Colors, TextColors } from "../../styles/Colors"
import { hp, wp } from "../../styles/Dimens"
import { PAYMENT_TYPES } from "../../utilities/constants"
import { calculateCarGroupDifferentAmount, getGroupAmountByPaymentType, getOldGroupAmountByPaymentType, isUpdate } from "../../utilities/helpers"
import { HorizontalSeperator } from "../HorizontalSeperator"
import { CollabsableCard } from "../views/CollapsableCard"

export const CollapsablePaymentInfo = (props) => {
    const { reservation } = props
    const [expand, setExpand] = useState(false)

    const calculateTotalAmount = () => {
        let servicesTotalAmount = 0
        let productsTotalAmount = 0

        if (reservation.selectedProducts && reservation.selectedProducts.length > 0) {
            productsTotalAmount = reservation.selectedProducts.reduce(function (prev, current) {
                return prev + (Number(current.value) * Number(current.totalAmount)) 
            }, 0);
        }

        let sum = Number(servicesTotalAmount) + Number(productsTotalAmount) + (reservation.paymentType === PAYMENT_TYPES.PAY_NOW ?
            Number(reservation.selectedGroup.amountItem.payNowAmount) :
            Number(reservation.selectedGroup.amountItem.payLaterAmount))

        return sum.toFixed(2)
    }

    const calculatePaidAmount = () => {
        let servicesTotalAmount = 0
        let productsTotalAmount = 0

        if (reservation.oldSelectedProducts && reservation.oldSelectedProducts.length > 0) {
            productsTotalAmount = reservation.oldSelectedProducts.reduce(function (prev, current) {
                return prev + Number(current.totalAmount)
            }, 0);
        }

        let sum = Number(servicesTotalAmount) + Number(productsTotalAmount) + Number(getOldGroupAmountByPaymentType(reservation))

        return sum.toFixed(2)
    }

    const renderPaymentInfo = () => {
        return (
            <View style={{ width: wp(295) }}>
                <View style={styles.paymentInfo}>
                    <Text style={styles.paymentTitle}>Kiralama Ücreti</Text>
                    <Text style={styles.paymentValue}>
                        {reservation.paymentType === PAYMENT_TYPES.PAY_NOW ?
                            reservation.selectedGroup.amountItem.payNowAmount :
                            reservation.selectedGroup.amountItem.payLaterAmount}TL</Text>
                </View>

                <Text style={styles.paymentTitle}>Ek Hizmet Ücreti</Text>
                {
                    reservation.selectedProducts && reservation.selectedProducts.map(item => {
                        return (
                            <View style={styles.productInfo}>
                                <Text style={styles.paymentItemTitle}>{item.productName}</Text>
                                <Text style={styles.paymentValue}>{(Number(item.value) * Number(item.totalAmount)).toFixed(2)} TL</Text>
                            </View>
                        )
                    })
                }

                {/* {
                    reservation.couponCode &&
                    <View style={styles.paymentInfo}>
                        <Text style={styles.paymentTitle}>Kupon İndirimi</Text>
                        <Text style={styles.paymentValue}>
                            {reservation.paymentType === PAYMENT_TYPES.PAY_NOW ?
                                reservation.selectedGroup.amountItem.payNowAmount :
                                reservation.selectedGroup.amountItem.payLaterAmount}TL</Text>
                    </View>
                } */}
                
                <HorizontalSeperator style={styles.horizontalSeperator} />

                <View style={styles.paymentInfo}>
                    <Text style={styles.totalValue}>Toplam</Text>
                    <Text style={styles.totalValue}>{calculateTotalAmount()} TL</Text>
                </View>

                {
                    isUpdate(reservation) && reservation.isPaid &&

                    <>
                        <View style={styles.paymentInfo}>
                            <Text style={styles.paidValue}>Ödenmiş</Text>
                            <Text style={styles.paidValue}>{calculatePaidAmount()} TL</Text>
                        </View>

                        <View style={styles.paymentInfo}>
                            <Text style={[styles.totalValue, { color: calculateTotalAmount() - calculatePaidAmount() > 0 ? '#2ac769' : '#39a9db' }]}>{calculateTotalAmount() - calculatePaidAmount() > 0 ? 'Tahsil Edilecek' : 'İade Edilecek'}</Text>
                            <Text style={[styles.totalValue, { color: calculateTotalAmount() - calculatePaidAmount() > 0 ? '#2ac769' : '#39a9db' }]}>
                                {calculateTotalAmount() - calculatePaidAmount()} TL</Text>
                        </View>

                    </>
                }
            </View>
        )
    }

    const renderPayNowInfo = () => {
        return (
            <View style={styles.payNowContainer}>
                <Image style={{ tintColor: '#2ac769', marginEnd: wp(8) }} source={require('../../assets/icons/ic_info.png')} />
                <Text>Ödeme tahsil edilmiştir</Text>
            </View>
        )
    }

    const renderPayLaterInfo = () => {
        return (
            <View style={styles.payLaterContainer}>
                <Image source={require('../../assets/icons/ic_info.png')} style={{ tintColor: '#f6a609' }} />
                <Text style={styles.payLaterInfo}>Ödeme, aracı kiralayan şahıs tarafından aracı şubeden teslim alırken kredi kartı ile tahsil edilecektir. </Text>
            </View>
        )
    }

    return (
        <CollabsableCard
            title={'Ödeme Bilgileri'}
            showDetail={expand}
            onPress={() => setExpand(!expand)}>
            <View style={styles.container}>
                {
                    (isUpdate(reservation) && reservation.isPaid) &&
                    <>{renderPayNowInfo()}</>
                }
                {
                    (!reservation.isPaid && reservation.paymentType === PAYMENT_TYPES.PAY_LATER) &&
                    <>{renderPayLaterInfo()}</>
                }
                {renderPaymentInfo()}

            </View>


        </CollabsableCard>
    )
}

const styles = StyleSheet.create({
    container: {
        width: wp(295),
        marginBottom: hp(20)
    },
    paymentInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: hp(10)
    },
    paymentTitle: {
        fontSize: hp(14),
        color: '#999999',
        fontFamily: 'NunitoSans-SemiBold',
    },
    paymentItemTitle: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-Bold',
        marginEnd:40
    },
    paymentValue: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-Bold',
    },
    horizontalSeperator: {
        marginTop: hp(20)
    },
    totalValue: {
        fontSize: hp(18),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-Bold',
    },
    paidValue: {
        fontSize: hp(18),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',
    },
    productInfo: {
        marginStart: wp(20),
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: hp(8)
    },
    payLaterContainer: {
        width: wp(295),
        marginTop: hp(16),
        flexDirection: 'row',
        backgroundColor: '#ffeac1',
        borderRadius: hp(10),
        paddingVertical: hp(10),
        paddingHorizontal: wp(10)
    },
    payNowContainer: {
        width: wp(295),
        flexDirection: 'row',
        height: hp(44),
        backgroundColor: '#d5fae4',
        borderRadius: hp(10),
        marginTop: hp(16),
        marginBottom: hp(10),
        paddingStart: wp(10),
        alignItems: 'center'
    },
    payLaterInfo: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-SemiBold',
        marginStart: wp(10),
        marginEnd: wp(20)
    },
})