import React, { useState } from "react"
import { StyleSheet, Text, View } from "react-native"
import { Colors, TextColors } from "../../styles/Colors"
import { hp, wp } from "../../styles/Dimens"
import { CollabsableCard } from "../views/CollapsableCard"

export const CollapsableGroupCodeInfo = (props) => {

    const { groupInfo } = props
    
    const [expand, setExpand] = useState(false)

    const renderItem = (title, value) => {
        return (
            <View style={styles.itemContainer}>
                <View style={styles.circle} />
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.value}>{value}</Text>
            </View>
        )
    }

    return (
        <CollabsableCard
            title={'Kiralama Koşulları'}
            showDetail={expand}
            onPress={() => setExpand(!expand)}>
            <View style={styles.container}>
                {renderItem('Min. sürücü yaşı : ', Number(groupInfo.minAge))}
                {renderItem('Min. genç sürücü yaşı : ', Number(groupInfo.minYoungAge))}
                {renderItem('Min. ehliyet yılı : ', Number(groupInfo.minLicense))}
                {renderItem('Min. genç sürücü ehliyet yılı : ', Number(groupInfo.minYoungLicense))}
                {
                    Number(groupInfo.amountItem.dailyDepositAmount).toFixed(2) > 0 &&
                    renderItem('Günlük teminat tutarı : ', Number(groupInfo.amountItem.dailyDepositAmount) + ' TL')
                }
                {
                    Number(groupInfo.amountItem.monhthlyDepositAmount).toFixed(2) > 0 &&
                    renderItem('Aylık teminat tutarı : ', Number(groupInfo.amountItem.monhthlyDepositAmount) + ' TL')
                }
                {
                    Number(groupInfo.dailyKm) > 0 &&
                    renderItem('Günlük Km. : ', Number(groupInfo.dailyKm))
                }
                {
                    Number(groupInfo.monthlyKm)> 0 &&
                    renderItem('Aylık Km. : ', Number(groupInfo.monthlyKm))  
                }
                
                {renderItem('Çift kredi kartı : ', groupInfo.doubleCreditCard ? 'Evet' : 'Hayır')}
            </View>

        </CollabsableCard>
    )
}

const styles = StyleSheet.create({
    container:{
        width:wp(295),
        marginBottom:hp(20)
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: hp(17)
    },
    circle: {
        width: wp(6),
        height: wp(6),
        borderRadius: wp(3),
        backgroundColor: Colors.primaryBrand
    },
    title: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-SemiBold',
        color: TextColors.primaryTextV7,
        marginStart: wp(10)
    },
    value: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        color: TextColors.primaryText,
        marginStart: wp(10)
    }
})