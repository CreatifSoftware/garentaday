import React, { useState } from "react"
import { StyleSheet, Text, View, Image } from "react-native"
import { Colors, TextColors } from "../../styles/Colors"
import { hp, wp } from "../../styles/Dimens"
import { getFormatedDate } from "../../utilities/helpers"
import { HorizontalSeperator } from "../HorizontalSeperator"
import { CollabsableCard } from "../views/CollapsableCard"

export const CollapsableReservationInfo = (props) => {

    const { reservation } = props

    const [expand, setExpand] = useState(false)

    const renderReservationInfo = (title, branchName, date) => {
        return (
            <View style={{ marginBottom: hp(10), width: '100%' }}>
                <Text style={styles.branchTitle}>{title}</Text>
                <Text style={styles.branchName}>{branchName}</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp(8) }}>
                    <Image style={{ tintColor: Colors.primaryBrand }} source={require('../../assets/icons/ic_date.png')} />
                    <Text style={styles.date}>{getFormatedDate(date, 'DD MMMM YYYY')}</Text>
                    <Image style={{ tintColor: Colors.primaryBrand }} source={require('../../assets/icons/ic_alarm.png')} />
                    <Text style={styles.date}>{getFormatedDate(date, 'HH:mm')}</Text>
                </View>
            </View>
        )
    }

    const expandedReservationInfo = () => {
        return (
            <View style={{ marginBottom: hp(20), width: wp(295) }}>

                <View style={{
                    width: wp(295),
                    flexDirection: 'row',
                    height: hp(44),
                    backgroundColor: '#ffeae0',
                    borderRadius: hp(10),
                    marginTop: hp(20),
                    marginBottom: hp(10),
                    paddingStart: wp(20),
                    alignItems: 'center'
                }}>
                    <Image style={{ tintColor: Colors.primaryBrand, marginEnd: wp(8) }} source={require('../../assets/icons/ic_info.png')} />
                    <Text>Araç Kiralama süresi {reservation.duration} gün</Text>
                </View>
                {renderReservationInfo('Alış Bilgisi', reservation.pickupBranch.branchName, reservation.pickupDateTime)}
                <HorizontalSeperator style={{ marginVertical: 16 }} />
                {renderReservationInfo('Teslim Bilgisi', reservation.dropoffBranch.branchName, reservation.dropoffDateTime)}
            </View>
        )
    }

    return (
        <CollabsableCard
            title={'Rezervasyon Bilgileri'}
            showDetail={expand}
            onPress={() => setExpand(!expand)}>
            <View style={styles.container}>
                {expandedReservationInfo()}
            </View>


        </CollabsableCard>
    )
}

const styles = StyleSheet.create({
    container:{
        width:wp(295),
        marginBottom:hp(10)
    },
    branchTitle: {
        fontSize: hp(14),
        color: '#999999',
        fontFamily: 'NunitoSans-SemiBold'
    },
    branchName: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontWeight: '700',
        marginTop: hp(8)
    },
    date: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginHorizontal: wp(8)
    },
})