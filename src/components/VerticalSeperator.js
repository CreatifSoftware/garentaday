import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Colors } from '../styles/Colors';

export const VerticalSeperator = (props) => {

    return (
        <View
            style={[styles.container, props.style]} />
    )
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: Colors.seperatorColor, 
        width: 1
    }
})