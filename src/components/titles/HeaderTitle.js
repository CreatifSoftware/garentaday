import React from 'react'
import { Platform, StyleSheet, Text, View } from "react-native"
import { Colors } from "../../styles/Colors"
import { hp } from "../../styles/Dimens"
import { getFormatedDate } from "../../utilities/helpers"

export const HeaderTitle = (props) => {
    return (
        <View style={[styles.container,Platform.OS === 'ios' && {marginTop:hp(20)}]}>
            <Text style={styles.headerTitle}>{props.title}</Text>
        </View>

    )

}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: hp(17),
        color: Colors.white,
        textAlign: 'center',
        fontWeight: '700',
        marginTop: hp(3)
    },
})