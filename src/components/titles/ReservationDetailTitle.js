import React from 'react'
import { StyleSheet, Text, View } from "react-native"
import { Colors } from "../../styles/Colors"
import { hp } from "../../styles/Dimens"
import { getFormatedDate } from "../../utilities/helpers"

export const ReservationDetailTitle = (props) => {

    const { reservation } = props

    return (
        <View style={{ justifyContent: 'center' }}>
            <Text style={styles.headerTitle}>Rezervasyon No: {reservation.contractPnrNo ? reservation.contractPnrNo : reservation.pnrNo}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    headerTitle: {
        fontSize: hp(17),
        color: Colors.white,
        textAlign: 'center',
        fontFamily: 'NunitoSans-Bold'
    },
})