import React from 'react'
import { StyleSheet, Text, View } from "react-native"
import { Colors } from "../../styles/Colors"
import { hp } from "../../styles/Dimens"
import { getFormatedDate } from "../../utilities/helpers"

export const CarGroupTitle = (props) => {

    const { reservation } = props

    return (
        <View style={{justifyContent:'center'}}>
            <Text style={styles.headerTitle}>{reservation.pickupBranch.branchName} - {reservation.dropoffBranch.branchName}</Text>
            <Text style={styles.headerSubtitle}>{getFormatedDate(reservation.pickupDateTime, 'DD MMMM HH:mm')} - {getFormatedDate(reservation.dropoffDateTime, 'DD MMMM HH:mm')}</Text>
        </View>

    )

}

const styles = StyleSheet.create({
    headerTitle: {
        fontSize: hp(12),
        color: Colors.white,
        textAlign: 'center',
        fontFamily:'NunitoSans-Bold'
    },
    headerSubtitle: {
        fontSize: hp(12),
        color: Colors.white,
        textAlign: 'center',
        fontFamily:'NunitoSans-SemiBold'
    },
})