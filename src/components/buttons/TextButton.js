import React from 'react'
import { StyleSheet, TouchableOpacity, Text, Image, View } from "react-native"
import { Colors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';

export const TextButton = (props) => {
    const { title = '', style = {}, onPress } = props;

    return (
        <TouchableOpacity
            disabled={props.disabled}
            style={props.buttonStyle}
            onPress={onPress} >
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems:'center' }}>
                {
                    props.source &&
                    <Image style={{ marginEnd: wp(8) }} source={props.source} />
                }
                <Text style={[styles.text, style]}>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        alignSelf: 'center',
        color: Colors.white,
    },
})