import { useNavigation } from '@react-navigation/native';
import React from 'react'
import { StyleSheet, TouchableOpacity, Text, Image, Platform } from "react-native"
import { Colors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';

export const BackButton = (props) => {
    const navigation = useNavigation()
    return (
        <TouchableOpacity
            style={styles.container}
            onPress={() => {
                props.override ?
                    props.onPress :
                    navigation.goBack()
            }}>
            <Image
                style={styles.image}
                source={require('../../assets/icons/ic_arrow_left.png')}
            />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: wp(40),
        height: wp(40),
        marginStart: wp(20),
        borderRadius: wp(40) / 2,
        backgroundColor: Colors.primaryBrandV3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerAndroid: {
        width: wp(35),
        height: wp(35),
        marginStart: wp(20),
        borderRadius: wp(40) / 2,
        backgroundColor: Colors.primaryBrandV3,
        justifyContent: 'center',
        alignItems: 'center',
    },

    image: {
        tintColor: Colors.white,
    }
})



