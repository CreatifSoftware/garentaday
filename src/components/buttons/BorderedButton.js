import React from 'react'
import { StyleSheet, TouchableOpacity, Text, Image, View } from "react-native"
import { Colors } from "../../styles/Colors";
import { wp, hp } from '../../styles/Dimens';

export const BorderedButton = (props) => {
    const { title = '', style = {}, textStyle = {}, onPress } = props;

    return (
        <TouchableOpacity
            disabled={props.disabled}
            activeOpacity={0.6}
            onPress={onPress}
            style={[styles.button, style]}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                {
                    props.source &&
                    <Image style={{ tintColor: Colors.primaryBrand, marginEnd: wp(8) }} source={props.source} />
                }
                <Text style={[styles.text, textStyle]}>{props.title}</Text>
            </View>

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        display: 'flex',
        height: hp(50),
        borderRadius: hp(50) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: Colors.primaryBrand,
        borderWidth: 1
    },
    text: {
        fontSize: hp(16),
        fontFamily: 'NunitoSans-Bold',
        color: Colors.primaryBrand,
    },
})



