import React from 'react'
import { StyleSheet, TouchableOpacity, Text, Image } from "react-native"
import { Colors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';

export const ImageButton = (props) => {

    return (
        <TouchableOpacity
            {...props}
            style={[styles.container,props.style]}
            onPress={props.onPress}>
            <Image
                style={props.imageStyle}
                source={props.source}/>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
})



