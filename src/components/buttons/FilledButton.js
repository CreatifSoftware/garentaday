import React from 'react'
import { StyleSheet, TouchableOpacity, Text, TouchableHighlight, Image } from "react-native"
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { Colors, TextColors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';

export const FilledButton = (props) => {
    const { title = '', style = {}, textStyle = {}, onPress, iconStyle = {} } = props;

    return (
        <TouchableOpacity
            {...props}
            onPress={onPress}
            activeOpacity={0.6}
            style={[props.disabled ? styles.disabledButton : styles.button, style]}>
                {
                    props.source &&
                    <Image style={[styles.imageStyle,iconStyle]} source={props.source}/>
                }
            <Text style={[props.disabled ? styles.disabledText : styles.text, textStyle]}>{props.title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        height: hp(50),
        borderRadius: hp(50) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        backgroundColor: Colors.primaryBrand,
    },
    disabledButton: {
        height: hp(50),
        borderRadius: hp(50) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        backgroundColor: Colors.disabledBackground,
    },
    imageStyle:{
        marginEnd:wp(8),
        tintColor:Colors.white
    },
    text: {
        fontSize: hp(16),
        fontFamily:'NunitoSans-SemiBold',
        color: Colors.white
    },
    disabledText: {
        fontSize: hp(16),
        fontFamily:'NunitoSans-SemiBold',
        color: TextColors.primaryTextV7
    },
})



