import React from 'react'
import { StyleSheet, TouchableOpacity, Text, View } from "react-native"
import { Colors, TextColors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';

export const RadioButton = (props) => {
    return (
        <View style={{
            height: hp(18),
            width: hp(18),
            borderRadius: hp(18) / 2,
            borderWidth: 2,
            borderColor: props.selected ? props.activeColor : Colors.checkboxBorder,
            alignItems: 'center',
            justifyContent: 'center',
        }}>
            {
                props.selected ?
                    <View style={{
                        height: hp(9),
                        width: hp(9),
                        borderRadius: hp(9) / 2,
                        backgroundColor: props.activeColor,
                    }} />
                    : null
            }
        </View>
    )
}

export const RadioButtonItem = (props) => {
    return (
        <TouchableOpacity
            {...props}
            onPress={props.onPress}
            style={styles.itemContainer}>
            <Text style={props.selected ? styles.selectedItemText : styles.text}>{props.label}</Text>
            <RadioButton {...props} activeColor={Colors.primaryBrand} />

        </TouchableOpacity>
    )
}

export const RadioButtonView = (props) => {
    return (
        <TouchableOpacity
            disabled={props.disabled}
            onPress={props.onPress}
            style={props.selected ? [styles.selectedContainer, props.style] : [styles.container, props.style]}>
            <RadioButton {...props} activeColor={Colors.white} />
            <Text style={props.selected ? [styles.selectedText, { opacity: props.disabled && 0.5 }] : [styles.text, { opacity: props.disabled && 0.5 }]}>{props.label}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        height: hp(52),
        width: wp(375),
        borderRadius: hp(10),
        paddingHorizontal: wp(20),
        backgroundColor: Colors.white,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    disabledButton: {
        height: hp(50),
        borderRadius: hp(50) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: Colors.disabledBackground,
    },
    container: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: Colors.seperatorColor,
        height: hp(48),
        width: wp(163),
        borderRadius: hp(10),
        paddingHorizontal: wp(13),
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
    selectedContainer: {
        flexDirection: 'row',
        height: hp(48),
        width: wp(163),
        borderRadius: hp(10),
        paddingHorizontal: wp(13),
        backgroundColor: Colors.primaryBrand,
        alignItems: 'center',
    },
    text: {
        fontSize: hp(16),
        fontFamily: 'NunitoSans-SemiBold',
        paddingHorizontal: wp(13),
        color: TextColors.primaryTextV2
    },
    selectedItemText: {
        fontSize: hp(16),
        fontFamily: 'NunitoSans-SemiBold',
        paddingHorizontal: wp(13),
        color: Colors.primaryBrand
    },
    selectedText: {
        fontSize: hp(16),
        // marginStart:wp(11),
        fontFamily: 'NunitoSans-SemiBold',
        paddingHorizontal: wp(13),
        color: Colors.white
    },
    checkbox: {
        width: hp(24),
        height: hp(24)
    },
})



