import React from 'react'
import { StyleSheet, TouchableOpacity, Text, Image, Alert } from "react-native"
import { Colors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';

export const HeaderCancelButton = (props) => {

    const cancelProcessConfirmation = () => {
        Alert.alert(
            "",
            props.title,
            [
                {
                    text: "Vazgeç",
                    style: 'cancel'
                },
                {
                    text: "Çıkış Yap", onPress: () => {
                        // console.warn(props.route)
                        props.navigation.popToTop()
                    },
                    style: 'destructive'
                }
            ],
            { cancelable: false }
        );


    }
    return (
        <TouchableOpacity
            style={styles.container}
            onPress={() => cancelProcessConfirmation()}>
            <Image
                style={styles.image}
                source={require('../../assets/icons/ic_close.png')}
            />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: wp(40),
        height: wp(40),
        marginEnd: wp(20),
        borderRadius: wp(40) / 2,
        backgroundColor: Colors.primaryBrandV3,
        justifyContent: 'center',
        alignItems: 'center',
    },

    image: {
        tintColor: Colors.white,
    }
})

