import React from 'react'
import { Image, StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { Styles } from '../../styles/Styles'
import { FilledButton } from '../../components/buttons/FilledButton'
import { getCityItem, getCountryItem, getDistrictItem } from '../../utilities/helpers'
import { useSelector } from 'react-redux'

export const AddressItem = (props) => {

    const { item, handleOnEditAddress } = props
    const countries = useSelector(state => state.masterReducer.countries)
    const cities = useSelector(state => state.masterReducer.cities)
    const districts = useSelector(state => state.masterReducer.districts)

    const countryItem = getCountryItem(countries, item.country)
    const cityItem = getCityItem(cities, item.country, item.city)
    const districtItem = getDistrictItem(districts, item.district)

    const district = districtItem && districtItem.districtName

    return (
        <View style={localStyles.container}>
            <Text style={[Styles.title, { marginBottom: hp(12) }]}>{item.addressTitle}</Text>
            {
                district ?
                    <Text style={Styles.content}>{district} - {cityItem.cityName} / {countryItem.countryName}</Text>
                    :
                    <Text style={Styles.content}>{cityItem.cityName} / {countryItem.countryName}</Text>
            }
            <Text style={[Styles.content, { marginBottom: hp(20) }]}>{item.address}</Text>
            <FilledButton
                onPress={handleOnEditAddress}
                title={'Düzenle'}
                style={{ width: wp(120), height: hp(45) }} />
        </View>
    )
}

const localStyles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        marginHorizontal: wp(20),
        paddingVertical: hp(20),
        paddingHorizontal: wp(20),
        borderRadius: hp(10),
        marginTop: hp(16),
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
    },
    addressTitle: {
        fontSize: hp(16),
        fontWeight: '700',
        color: TextColors.primaryText,
    },
    address: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginTop: hp(5)
    },
    cardType: {
        alignSelf: 'center',
        marginHorizontal: wp(20),
    }
})