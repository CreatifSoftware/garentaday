import React from 'react'
import { Image, StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { KEY_VISA } from '../../utilities/constants'
import { getCreditCardType } from '../../utilities/helpers'

export const CreditCardInfo = (props) => {
    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row' }}>
                {
                    getCreditCardType(props.item.cardNo) === KEY_VISA
                        ?
                        <Image style={styles.cardType} source={require('../../assets/icons/ic_visa.png')} />
                        :
                        <Image style={styles.cardType} source={require('../../assets/icons/ic_master_card.png')} />
                }
                {/* <Text style={styles.cardName}>{props.item.title}</Text> */}
                <Text style={styles.cardNumber}>{props.item.cardNo}</Text>
            </View>
            {/* <Text style={styles.cardNumber}>{props.item.cardNo}</Text> */}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: wp(20),
    },
    cardName: {
        fontSize: hp(16),
        fontWeight: '700',
        color: TextColors.primaryText,
    },
    cardNumber: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginTop: hp(5)
    },
    cardType: {
        alignSelf: 'center',
        marginEnd: wp(13),
    }
})