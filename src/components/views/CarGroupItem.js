import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import FastImage from 'react-native-fast-image'
import { CampaignFlag, campaignFlag } from '../../assets/Icon'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { HorizontalSeperator } from '../HorizontalSeperator'
import { VerticalSeperator } from '../VerticalSeperator'

export const CarGroupItem = (props) => {
    const { item, payNowAmount, payLaterAmount, hasCampaign } = props

    return (
        <>
            <TouchableOpacity
                onPress={() => props.onPress(item)}
                style={styles.container}>
                {
                    hasCampaign &&
                    <CampaignFlag style={{ marginVertical: hp(10) }} />
                }

                <View style={{ flexDirection: 'row', paddingTop: hp(14) }}>
                    <FastImage
                        style={styles.image}
                        source={{
                            uri: item.imageUrl,
                            priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                    />


                    <View style={{ flex: 1 }}>
                        <View style={styles.segmentContainer}>
                            <Text style={styles.segmentText}>{item.segmentDesc}</Text>
                            <VerticalSeperator style={{ marginHorizontal: wp(8), width: 2 }} />
                            <Text style={styles.segmentText}>{item.segmentId} Grubu</Text>
                        </View>

                        <Text style={styles.displayText}>{item.displayText}</Text>

                        <View style={styles.infoContainer}>
                            <Text style={styles.infoText}>{item.transmissionDesc}</Text>
                            <VerticalSeperator style={{ marginHorizontal: wp(8), width: 2 }} />
                            <Text style={styles.infoText}>{item.fuelTypeDesc}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginBottom: hp(20) }}>
                            <View style={styles.amountContainer}>
                                <Text style={styles.payNowTitle}>ŞİMDİ ÖDE</Text>
                                <Text style={styles.payNow}>{payNowAmount} TL</Text>
                            </View>

                            <View style={styles.amountContainer}>
                                <Text style={styles.payLaterTitle}>SONRA ÖDE</Text>
                                <Text style={styles.payLater}>{payLaterAmount} TL</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
            <HorizontalSeperator style={{ height: 4, backgroundColor: Colors.seperatorColorV2 }} />
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: hp(176),
        backgroundColor: Colors.white,
        // paddingTop: hp(14),
    },
    image: {
        width: wp(112),
        height: hp(70),
        marginHorizontal: wp(10)
    },
    displayText: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryText,
        marginTop: hp(9),
        marginEnd: wp(20)
    },
    segmentContainer: {
        flexDirection: 'row',
    },
    segmentText: {
        fontSize: hp(12),
        fontWeight: '700',
        color: TextColors.primaryTextV3,
    },
    infoContainer: {
        flexDirection: 'row',
        marginTop: hp(9),
    },
    infoText: {
        fontSize: hp(12),
        fontWeight: '700',
        color: TextColors.primaryTextV2,
    },
    amountContainer: {
        marginTop: hp(20),
        marginEnd: wp(30)
    },
    payNowTitle: {
        fontSize: hp(12),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',

    },
    payNow: {
        fontSize: hp(18),
        fontFamily: 'NunitoSans-Bold',
        color: Colors.primaryBrand,
    },
    payLaterTitle: {
        fontSize: hp(12),
        fontFamily: 'NunitoSans-Bold',
        color: TextColors.primaryTextV3,
    },
    payLater: {
        fontSize: hp(18),
        fontFamily: 'NunitoSans-Bold',
        color: TextColors.primaryTextV3,
    }
})