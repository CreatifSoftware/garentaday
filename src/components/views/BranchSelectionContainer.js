import React from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native';
import { Colors, TextColors } from '../../styles/Colors';
import { hp, wp } from '../../styles/Dimens';
import { VerticalSeperator } from '../VerticalSeperator';

export const BranchSelectionContainer = (props) => {

    return (
        <View style={styles.container}>
            <TouchableOpacity
                disabled={props.isDisabled}
                style={[styles.pickup, { opacity: props.isDisabled ? 0.5 : 1 }]}
                activeOpacity={0.6}
                onPress={props.handleOnPickupBranch}>
                <Text style={styles.pickupTitle}>Alış Şube</Text>
                <Text style={styles.pickupBranch}>{props.pickupBranch ? props.pickupBranch.branchName : 'ŞUBE SEÇİN'}</Text>
            </TouchableOpacity>

            <View style={{ alignItems: 'center' }}>
                <VerticalSeperator style={{ flex: 1, backgroundColor: Colors.primaryBrand, width: 2 }} />
                <View style={{ width: hp(44), height: hp(44), borderRadius: hp(44) / 2, borderWidth: 2, borderColor: Colors.primaryBrand, justifyContent: 'center', alignItems: 'center' }}>
                    <Image style={{ tintColor: Colors.primaryBrand }} resizeMode={'contain'} resizeMethod={'auto'} source={require('../../assets/icons/ic_location.png')} />
                </View>
                <VerticalSeperator style={{ flex: 1, backgroundColor: Colors.primaryBrand, width: 2 }} />
            </View>

            <TouchableOpacity
                style={styles.dropoff}
                activeOpacity={0.6}
                onPress={props.handleOnDropoffBranch}>
                <Text style={styles.dropoffTitle}>Teslim Şube</Text>
                <Text style={styles.dropoffBranch}>{props.dropoffBranch ? props.dropoffBranch.branchName : 'ŞUBE SEÇİN'}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: hp(101),
        marginHorizontal: wp(20),
        borderRadius: hp(10),
        backgroundColor: Colors.white,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: Colors.borderColor,
        borderWidth: 1,
        marginBottom: hp(10)
    },
    pickup: {
        marginVertical: hp(16),
        marginStart: wp(16),
        flex: 1
    },
    pickupTitle: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryTextV7,
        marginBottom: hp(8)
    },
    pickupBranch: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryText,
        marginEnd:wp(16)
    },
    dropoff: {
        marginTop: hp(16),
        marginEnd: wp(16),
        flex: 1
    },
    dropoffTitle: {
        textAlign: 'right',
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryTextV7,
        marginBottom: hp(8)
    },
    dropoffBranch: {
        textAlign: 'right',
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryText,
        marginStart:wp(16)
    }
})


