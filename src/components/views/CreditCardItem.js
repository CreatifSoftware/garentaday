import React from 'react'
import { Image, StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { KEY_VISA } from '../../utilities/constants'
import { getCreditCardType } from '../../utilities/helpers'

export const CreditCardItem = (props) => {

    return (
        <TouchableOpacity style={styles.container} onPress={props.onPress}>
            <View style={{ flexDirection: 'row' }}>
                {
                    getCreditCardType(props.item.cardNo) === KEY_VISA
                        ?
                        <Image style={styles.cardType} source={require('../../assets/icons/ic_visa.png')} />
                        :
                        <Image style={styles.cardType} source={require('../../assets/icons/ic_master_card.png')} />
                }

                <View>
                    <Text style={styles.cardName}>{props.item.title}</Text>
                    <Text style={styles.cardNumber}>{props.item.cardNo}</Text>
                </View>
            </View>

            <Image style={{marginEnd:wp(20), tintColor: Colors.primaryBrand }} source={require('../../assets/icons/ic_edit.png')} />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor:Colors.white,
        flexDirection: 'row',
        marginHorizontal: wp(20),
        height: hp(74),
        borderWidth: 1,
        borderColor: Colors.borderColor,
        borderRadius: hp(10),
        justifyContent: 'space-between',
        alignItems:'center'
    },
    cardName: {
        fontSize: hp(16),
        fontWeight:'700',
        color: TextColors.primaryText,
    },
    cardNumber: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginTop:hp(5)
    },
    cardType:{
        alignSelf:'center',
        marginHorizontal:wp(20),
    }
})