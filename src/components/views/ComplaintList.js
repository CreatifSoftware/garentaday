import React from 'react'
import { FlatList, StyleSheet, View, TouchableOpacity, Text, SafeAreaView, Image } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { HorizontalSeperator } from '../HorizontalSeperator'


export const ComplaintList = (props) => {
    const { complaintList, onPress } = props

    const renderItem = ({ item }) => {
        return (
            <View style={styles.itemContainer}>
                <View style={{ flexDirection: 'row', marginVertical: hp(12), marginHorizontal: wp(20), alignItems: 'center' }}>
                    <Text style={{ fontSize: wp(14), color: TextColors.primaryTextV7, fontFamily: 'NunitoSans-SemiBold' }}>Dosya No</Text>
                    <Text style={{ marginStart: wp(16), fontSize: wp(18), color: Colors.primaryBrand, fontFamily: 'NunitoSans-Bold' }}>{item.complaintId}</Text>
                </View>

                <HorizontalSeperator style={{ marginBottom: hp(16) }} />

                <View style={{ flexDirection: 'row', marginBottom: hp(12), marginHorizontal: wp(20), }}>
                    <Text style={{ fontSize: wp(14), color: TextColors.primaryTextV7, fontFamily: 'NunitoSans-SemiBold' }}>Konu</Text>
                    <Text style={{ marginStart: wp(16), fontSize: wp(14), color: TextColors.primaryText, fontFamily: 'NunitoSans-SemiBold' }}>{item.status}</Text>
                </View>

                <View style={{ flexDirection: 'row', marginBottom: hp(12), marginHorizontal: wp(20), alignItems: 'center' }}>
                    <Text style={{ fontSize: wp(14), color: TextColors.primaryTextV7, fontFamily: 'NunitoSans-SemiBold' }}>Başlık</Text>
                    <Text style={{ marginStart: wp(16), fontSize: wp(14), color: TextColors.primaryText, fontFamily: 'NunitoSans-SemiBold' }}>{item.title}</Text>
                </View>

                <View style={{ flexDirection: 'row', marginBottom: hp(12), marginHorizontal: wp(20), alignItems: 'center' }}>
                    <Text style={{ fontSize: wp(14), color: TextColors.primaryTextV7, fontFamily: 'NunitoSans-SemiBold' }}>Açıklama</Text>
                    <Text style={{ marginStart: wp(16), fontSize: wp(14), color: TextColors.primaryText, fontFamily: 'NunitoSans-SemiBold' }}>{item.message}</Text>
                </View>

            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={complaintList}
                renderItem={renderItem}
                keyExtractor={item => item.complaintId}
            />
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: Colors.mainBackground,
    },
    itemContainer: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginHorizontal: wp(24),
        marginVertical: hp(10),
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.15,
        elevation: 4,
    }
})