import React from 'react'
import { StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { Header } from '@react-navigation/stack'
import { Colors } from '../../styles/Colors';

export const GradientHeader = props => {
    return (
        <LinearGradient
            start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
            colors={[Colors.primaryBrand, Colors.primaryBrandV2]}
            style={[StyleSheet.absoluteFill, { height: Header.HEIGHT }]}>
        </LinearGradient>
    )
}