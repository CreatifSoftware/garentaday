import React, { useLayoutEffect, useState } from 'react'
import { View, StyleSheet, Text, Switch } from 'react-native'
import { PreviousNextView } from 'react-native-keyboard-manager';
import { FloatingTextInput } from '../inputs/FloatingTextInput';
import { hp, wp } from '../../styles/Dimens';
import { Colors, TextColors } from '../../styles/Colors';
import moment from 'moment';

export const AddCardInformation = (props) => {

    const [expDateError, setExpDateError] = useState(null)

    const expDateOnChange = (text) => {
        
        if (text.length == 5) {
            let cardInfo = {
                month: text.split('/')[0],
                year: "20" + text.split('/')[1],
            }

            let formattedExpDate = `${cardInfo.year}-${cardInfo.month}-28`
            let today = new Date();
            let someday = new Date();
            someday.setFullYear(cardInfo.year, Number(cardInfo.month) - 1, 1);

            var date = moment(formattedExpDate);

            if (Number(cardInfo.month) > 12) {
                setExpDateError('Geçerli tarih girin')
            }
            else if (someday < today) {
                setExpDateError('Geçerli tarih girin')
            }
            else if (!date.isValid()) {
                setExpDateError('Geçerli tarih girin')
            } else {
                setExpDateError(null)
            }
        }

        props.expDateChange(text)
    }

    return (
        <PreviousNextView >
            <Text style={{ fontSize: hp(16), color: TextColors.primaryText, fontWeight: '700', marginTop: hp(30), marginStart: wp(20) }}>Kart Bilgileri</Text>
            <FloatingTextInput
                disabled={props.disabled}
                label={'Kart Üzerindeki İsim'}
                placeholder={'Kart Üzerindeki İsim'}
                onChangeText={props.cardHolderNameChange}
                value={props.cardHolderName}
            />
            <FloatingTextInput
                disabled={props.disabled}
                label={'Kart Numarası'}
                placeholder={'Kart Numarası'}
                onChangeText={props.cardNumberChange}
                value={props.cardNumber}
                type={!props.disabled && 'credit-card'}
                keyboardType={'number-pad'}
            />
            <Text style={{ fontSize: hp(16), color: TextColors.primaryText, fontWeight: '700', marginTop: hp(30), marginStart: wp(20) }}>Son Kullanma Tarihi</Text>

            <View style={{ flexDirection: 'row' }}>
                <FloatingTextInput
                    disabled={props.disabled}
                    style={{ marginTop: props.cvv ? props.expDate ? 0 : 16 : 0, width: wp(140) }}
                    label={'Ay/Yıl'}
                    type={!props.disabled && 'datetime'}
                    placeholder={'Ay/Yıl'}
                    onChangeText={(text) => expDateOnChange(text)}
                    value={props.expDate}
                    error={expDateError}
                    dateFormat={'MM/YY'}
                    keyboardType={'number-pad'}
                />
                <FloatingTextInput
                    disabled={props.disabled}
                    style={{ marginTop: props.expDate ? props.cvv ? 0 : 16 : 0, width: wp(140) }}
                    label={'CVV'}
                    placeholder={'CVV'}
                    onChangeText={props.cvvChange}
                    value={props.cvv}
                    maxLength={4}
                    keyboardType={'number-pad'}
                // iconSource={require('../../assets/icons/ic_question.png')}
                />
            </View>
            {
                props.showSwitch &&
                <>
                    <View style={{
                        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: hp(30),
                        marginHorizontal: wp(20),
                    }}>
                        <Text style={styles.title}>Kredi kartını kaydet</Text>
                        <Switch
                            style={styles.switchStyle}
                            trackColor={{ false: Colors.white, true: Colors.primaryBrand }}
                            thumbColor={Colors.white}
                            onValueChange={props.toggleSwitch}
                            value={props.saveCreditCard}
                        />
                    </View>
                    {
                        props.saveCreditCard &&
                        <FloatingTextInput
                            label={'Kart Tanımı'}
                            placeholder={'Kart Tanımı'}
                            onChangeText={props.cardDescriptionChange}
                            value={props.cardDescription}
                        />
                    }

                </>
            }

        </PreviousNextView >

    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white
    },
    nationalityGroup: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between',
        marginTop: hp(24)
    },
    genderTitle: {
        fontFamily: 'NunitoSans-Bold',
        fontSize: hp(18),
        color: TextColors.primaryText,
        // marginTop: hp(36),
        marginTop: 36,
        marginStart: wp(20)
    },
    genderGroup: {
        marginHorizontal: wp(20),
    },
    genderRadio: {
        width: wp(335),
        marginTop: hp(10)
    },
    continueButton: {
        position: 'absolute',
        bottom: hp(44),
        width: wp(335),
        marginHorizontal: wp(20)
    }
})