import React from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { FilledButton } from '../buttons/FilledButton'

export const NearbyBranchItem = (props) => {

    const { item, onPress } = props

    return (
        <TouchableOpacity style={[styles.container, props.style]} onPress={() => onPress(item)}>
            <Text style={styles.branchName}>{item.branchName}</Text>
            <Text style={styles.address}>{item.address}</Text>

            <FilledButton
                title={'Rezervasyon Yap'}
                style={styles.button}
                source={require('../../assets/icons/ic_date.png')}
                iconStyle={{ tintColor: Colors.primaryBrand }}
                textStyle={{ color: Colors.primaryBrand, fontSize: wp(14), fontFamily:'NunitoSans-SemiBold' }}
                onPress={() => onPress(item)} />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        borderRadius: hp(10),
        marginStart: wp(20),
        backgroundColor: 'white',
        width: wp(325),
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.1,
        elevation: 4,
        marginBottom: hp(16),
        paddingHorizontal:wp(16)
    },
    branchName: {
        color: TextColors.primaryText,
        fontSize: hp(16),
        marginTop: hp(20),
        fontFamily: 'NunitoSans-SemiBold',
    },
    address: {
        color: TextColors.primaryTextV3,
        fontSize: hp(14),
        marginTop: hp(6),
        fontFamily: 'NunitoSans-SemiBold',
    },
    button: {
        width: wp(173),
        height:hp(36),
        marginTop: hp(10),
        backgroundColor: Colors.primaryBrandV5,
        marginBottom: hp(24)
    },
})