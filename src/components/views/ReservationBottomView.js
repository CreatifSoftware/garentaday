import React, { useRef } from 'react'
import { Dimensions, Image, Platform, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import RBSheet from 'react-native-raw-bottom-sheet'
import { ReservationSummaryModal } from '../../containers/modals/ReservationSummaryModal'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { Styles } from '../../styles/Styles'
import { PAYMENT_TYPES } from '../../utilities/constants'
import { getOldGroupAmountByPaymentType, isUpdate } from '../../utilities/helpers'
import { FilledButton } from '../buttons/FilledButton'

export const ReservationBottomView = (props) => {
    const { onPress, title, buttonStyle, showReservationDetail, reservation } = props
    const isPayNow = reservation.paymentType === PAYMENT_TYPES.PAY_NOW
    const refRBSheet = useRef();



    const calculateTotalAmount = () => {
        let servicesTotalAmount = 0
        let productsTotalAmount = 0

        if (reservation.selectedProducts && reservation.selectedProducts.length > 0) {
            productsTotalAmount = reservation.selectedProducts.reduce(function (prev, current) {
                return prev + (Number(current.value) * Number(current.totalAmount))
            }, 0);
        }

        let sum = Number(servicesTotalAmount) + Number(productsTotalAmount) + (reservation.paymentType === PAYMENT_TYPES.PAY_NOW ?
            Number(reservation.selectedGroup.amountItem.payNowAmount) :
            Number(reservation.selectedGroup.amountItem.payLaterAmount))

        return sum.toFixed(2)
    }

    const calculatePaidAmount = () => {
        let servicesTotalAmount = 0
        let productsTotalAmount = 0

        if (reservation.oldSelectedProducts && reservation.oldSelectedProducts.length > 0) {
            productsTotalAmount = reservation.oldSelectedProducts.reduce(function (prev, current) {
                return prev + Number(current.totalAmount)
            }, 0);
        }

        let sum = Number(servicesTotalAmount) + Number(productsTotalAmount) + Number(getOldGroupAmountByPaymentType(reservation))

        return sum.toFixed(2)
    }

    let totalAmount = 0
    if (isUpdate(reservation) && reservation.isPaid){
        totalAmount = calculateTotalAmount() - calculatePaidAmount()
    } else {
        totalAmount = calculateTotalAmount()
    }

    // const totalAmount = isUpdate(reservation) ? reservation.isPaid ? (calculateTotalAmount() - calculatePaidAmount()) : calculateTotalAmount()

    const getTitle = () => {
        if (isUpdate(reservation)) {
            if (!isPayNow) {
                return 'Ofiste Ödenecek'
            } else if (totalAmount > 0 && isPayNow) {
                return 'Tahsil Edilecek' 
            } else if (totalAmount == 0) {
                return 'Toplam'
            } else {
                return 'İade Edilecek'
            }
        }else{
            return 'Toplam'
        }
    }
        
    return (
        <View style={[Styles.shadowBox, styles.container]}>
            <TouchableOpacity
                onPress={() => refRBSheet.current.open()} activeOpacity={0.8} >
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.totalTitle}>{getTitle()}</Text>
                    <View style={styles.arrowContainer}>
                        <Image
                            style={styles.arrowIcon}
                            source={require('../../assets/icons/ic_small_arrow_up.png')} />
                    </View>
                </View>
                <Text style={styles.amount}>{Number(totalAmount).toFixed(2)} TL</Text>
            </TouchableOpacity>

            <FilledButton
                disabled={props.disabled}
                onPress={onPress}
                title={title}
                style={buttonStyle} />

            <RBSheet
                height={hp(Dimensions.get('screen').height * 0.8)}
                ref={refRBSheet}
                closeOnDragDown={false}
                closeOnPressMask={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <ReservationSummaryModal 
                    onPress={() => refRBSheet.current.close()}/>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingHorizontal: wp(20),
        paddingTop: hp(16),
        justifyContent: 'space-between'
    },
    totalTitle: {
        color: TextColors.primaryTextV7,
        fontSize: hp(14),
        fontWeight: '700',
        marginEnd: wp(8)
    },
    arrowContainer: {
        width: hp(24),
        height: hp(24),
        borderRadius: hp(12),
        backgroundColor: '#ffeae0',
        justifyContent: 'center',
        alignItems: 'center'
    },
    arrowIcon: {
        width: wp(12),
        height: hp(7),
        tintColor: Colors.primaryBrand
    },
    amount: {
        color: TextColors.primaryText,
        fontSize: hp(18),
        fontWeight: '700',
        marginTop: hp(5)
    }
})