import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { Styles } from '../../styles/Styles'
import { hp, wp } from '../../styles/Dimens'
import { HorizontalSeperator } from '../HorizontalSeperator'
import { getFormatedDate } from '../../utilities/helpers'
import { useSelector } from 'react-redux'
import moment from 'moment'

export const ReservationListItem = (props) => {
    const { item, branchs } = props

    const renderReservationInfo = (title, branchId, date, time) => {
        let filteredBranch = branchs.filter(arg => arg.branchId === branchId)
        
        const branchName = filteredBranch.length > 0 ? filteredBranch[0].branchName : ""
        return (
            <View
                style={{ flexDirection: 'row', alignItems: 'center'}}>
                <View style={styles.flag}>
                    <Image style={{ tintColor: Colors.primaryBrandV3 }} source={require('../../assets/icons/ic_location.png')} />
                </View>
                <View style={{ marginVertical: hp(10), width: '100%' }}>
                    <Text style={styles.branchTitle}>{title}</Text>
                    <Text style={styles.branchName}>{branchName}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp(8) }}>
                        <Image style={{ tintColor: Colors.primaryBrandV3 }} source={require('../../assets/icons/ic_date.png')} />
                        <Text style={styles.date}>{getFormatedDate(date, 'DD MMMM YYYY')}</Text>
                        <Image style={{ tintColor: Colors.primaryBrandV3 }} source={require('../../assets/icons/ic_alarm.png')} />
                        <Text style={styles.date}>{moment(time, 'HH:mm').format('HH:mm')}</Text>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <TouchableOpacity
            disabled={props.disabled}
            onPress={() => props.onPress(item)}
            style={styles.container}>
            {/* Title */}
            <View style={styles.titleContainer}>
                <Text style={styles.pnrTitle}>PNR No</Text>
                <Text style={styles.pnrNo}>{item.contractPnrNo ? item.contractPnrNo : item.pnrNo}</Text>
            </View>

            <HorizontalSeperator />

            <View>
                {renderReservationInfo('Alış Bilgisi', item.pickupBranchId, item.pickupDate, item.pickupTime)}
                <HorizontalSeperator style={{ marginStart: wp(50), marginVertical: hp(10), marginEnd: wp(20) }} />
                {renderReservationInfo('Teslim Bilgisi', item.dropoffBranchId, item.dropoffDate, item.dropoffTime)}
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        borderRadius: hp(10),
        backgroundColor: 'white',
        marginHorizontal: wp(20),
        marginTop: hp(20),
        paddingBottom:10,
        marginBottom:10,
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.15,
        elevation: 4,
    },
    titleContainer: {
        flexDirection: 'row',
        paddingStart: wp(20),
        alignItems: 'center',
        marginVertical: hp(12)
    },
    pnrTitle: {
        color: TextColors.primaryTextV7,
        fontSize: hp(14),
        fontFamily: 'NunitoSans-SemiBold',
    },
    pnrNo: {
        color: Colors.primaryBrand,
        fontSize: hp(18),
        fontFamily: 'NunitoSans-Bold',
        marginStart: wp(10)
    },
    branchTitle: {
        fontSize: hp(14),
        color: TextColors.primaryTextV7,
        fontFamily: 'NunitoSans-SemiBold'
    },
    branchName: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontWeight: '700',
        marginTop: hp(8)
    },
    date: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginHorizontal: wp(8)
    },
    flag: {
        width: wp(40),
        height: hp(32),
        backgroundColor: '#ffeae0',
        marginEnd: wp(10),
        justifyContent: 'center',
        alignItems: 'center',
        borderTopEndRadius: hp(70),
        borderBottomEndRadius: hp(70)
    }
})