import React from 'react'
import { FlatList, StyleSheet, View, TouchableOpacity, Text, SafeAreaView, Image } from 'react-native'
import { AddressItem } from './AddressItem'
import { Colors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'

export const AddressList = (props) => {
    const { addressList, handleOnAddAddress, handleOnEditAddress } = props

    const renderItem = ({ item }) => {
        return (
            <AddressItem
                item={item}
                handleOnEditAddress={() => handleOnEditAddress(item)} />
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                contentContainerStyle={{ paddingBottom: 20 }}
                data={addressList}
                renderItem={renderItem}
                keyExtractor={item => item.addressId}
            />
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: Colors.mainBackground
    },
    addButton: {
        width: 64,
        height: 64,
        borderRadius: 64 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: hp(45),
        end: wp(20),
        backgroundColor: Colors.primaryBrand
    }
})