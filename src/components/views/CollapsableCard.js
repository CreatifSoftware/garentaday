import React from 'react'
import { View, TouchableOpacity, Text, Image, StyleSheet } from 'react-native'
import { TextColors, Colors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'

export const CollabsableCard = (props) => {

    const { showDetail, title } = props

    return (
        <View style={styles.collapsedContainer}>
            <TouchableOpacity
                style={styles.collapsableContainer}
                onPress={props.onPress}>
                <Text style={styles.collapsableTitle}>{title}</Text>
                <Image style={styles.collapsableIcon} source={require('../../assets/icons/ic_small_arrow_right.png')} />
            </TouchableOpacity>
            {

                showDetail &&
                props.children

            }
        </View>
    )
}

const styles = StyleSheet.create({
    collapsableContainer: {
        backgroundColor: 'white',
        width: wp(335),
        height: hp(56),
        borderRadius: hp(20),
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.05,
        elevation: 4,
        justifyContent: 'center',
        paddingHorizontal: wp(20)
    },
    collapsableTitle: {
        fontSize: hp(18),
        color: TextColors.primaryText,
        fontWeight: '700'
    },
    collapsableIcon: {
        position: 'absolute',
        tintColor: Colors.primaryBrand,
        end: wp(20)
    },
    collapsedContainer: {
        marginHorizontal: wp(20),
        backgroundColor: 'white',
        borderRadius: hp(20),
        marginTop: hp(20),
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },
})