import React from 'react'
import { FlatList, StyleSheet, View, TouchableOpacity, Text, SafeAreaView } from 'react-native'
import { ReservationListItem } from './ReservationListItem'
import { Colors } from '../../styles/Colors'
import { useSelector } from 'react-redux'


export const ReservationList = (props) => {
    const { reservationList, onPress } = props
    const branchs = useSelector(state => state.masterReducer.branchs)

    const renderItem = ({ item }) => {
        return (
            <ReservationListItem
                disabled={props.disabled}
                branchs={branchs}
                item={item}
                onPress={() => onPress(item)} />
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={reservationList}
                renderItem={renderItem}
                keyExtractor={item => item.reservationId}
            />
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: Colors.mainBackground
    },
})