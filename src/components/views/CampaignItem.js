import React from 'react'
import { TouchableOpacity,Text,StyleSheet } from 'react-native'
import FastImage from 'react-native-fast-image'
import { IMAGE_URL } from '../../api/constants'
import { TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'

export const CampaignItem = (props) => {

    const { item, onPress } = props

    return (
        <TouchableOpacity style={[styles.campaignContainer, props.style]} onPress={() => onPress(item)}>
            <FastImage
                style={{ width: wp(162), height: hp(87),marginTop:16 }}
                source={{
                    uri: IMAGE_URL + item.imageUrl,
                    priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={styles.campaignTitle}>{item.title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    campaignContainer: {
        borderRadius: hp(10),
        marginStart: wp(20),
        backgroundColor: 'white',
        width: wp(325),
        alignItems: 'center',
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.1,
        elevation: 4,
        marginBottom:hp(16)
    },
    campaignTitle: {
        textAlign: 'center',
        color: TextColors.primaryText,
        fontSize: hp(14),
        marginBottom:16,
        // fontWeight:'700',
        fontWeight:'700',
        paddingHorizontal: wp(30)
    }
})