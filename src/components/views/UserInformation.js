import React, { useEffect, useRef } from 'react'
import { View, StyleSheet, Text } from "react-native"
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { FloatingSelectionInput } from '../../components/inputs/FloatingSelectionInput';
import { RadioButtonView } from '../../components/buttons/RadioButtonView';
import { PreviousNextView } from 'react-native-keyboard-manager';

export const UserInformation = (props) => {

    const setGender = (gender) => {
        props.setGender(gender)
    }

    return (
        <PreviousNextView>
            {
                props.isTurkishCitizen ?

                    props.showSecureGovermentId ?

                        <FloatingTextInput
                            disabled={props.disabled}
                            label={'T.C. Kimlik Numarası'}
                            placeholder={'T.C. Kimlik Numarası'}
                            maxLength={11}
                            value={props.govermentId}
                        />

                        :

                        <FloatingTextInput
                            label={'T.C. Kimlik Numarası'}
                            placeholder={'T.C. Kimlik Numarası'}
                            maxLength={11}
                            type={'only-numbers'}
                            keyboardType={'number-pad'}
                            onChangeText={props.setGovermentId}
                            value={props.govermentId}
                            error={props.govermentError}
                        />

                    :

                    <>
                        <FloatingSelectionInput
                            disabled={props.disabled}
                            label={'Uyruk'}
                            placeholder={'Uyruk'}
                            onFocus={props.showCountryModal}
                            value={props.nationality} />

                        <FloatingTextInput
                            disabled={props.disabled}
                            label={'Pasaport Numarası'}
                            placeholder={'Pasaport Numarası'}
                            onChangeText={props.setPassport}
                            value={props.passport}
                        />
                    </>
            }

            <FloatingTextInput
                disabled={props.disabled}
                label={'Adınız'}
                placeholder={'Adınız'}
                onChangeText={props.setFirstName}
                value={props.firstName}
            />

            <FloatingTextInput
                disabled={props.disabled}
                label={'Soyadınız'}
                placeholder={'Soyadınız'}
                onChangeText={props.setLastName}
                value={props.lastName}
            />

            <FloatingTextInput
                disabled={props.disabled}
                label={'Doğum Tarihiniz'}
                type={'datetime'}
                placeholder={'DD/MM/YYYY'}
                maxLength={10}
                keyboardType={'number-pad'}
                returnKeyType={'done'}
                error={props.dateOfBirthError}
                onChangeText={props.setDateOfBirth}
                value={props.dateOfBirth}

            />

            <Text style={styles.genderTitle}>Cinsiyet</Text>
            <View style={styles.genderGroup}>
                <RadioButtonView disabled={props.disabled} label='Erkek' selected={props.gender == 'M'} onPress={() => setGender('M')} style={styles.genderRadio} />
                <RadioButtonView disabled={props.disabled} label='Kadın' selected={props.gender == 'F'} onPress={() => setGender('F')} style={styles.genderRadio} />
                <RadioButtonView disabled={props.disabled} label='Belirtmek İstemiyorum' selected={props.gender == 'U'} onPress={() => setGender('U')} style={styles.genderRadio} />
            </View>

        </PreviousNextView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white
    },
    nationalityGroup: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between',
        marginTop: hp(24)
    },
    genderTitle: {
        fontFamily: 'NunitoSans-Bold',
        fontSize: hp(18),
        color: TextColors.primaryText,
        // marginTop: hp(36),
        marginTop: 36,
        marginStart: wp(20)
    },
    genderGroup: {
        marginHorizontal: wp(20),
    },
    genderRadio: {
        width: wp(335),
        marginTop: hp(10)
    },
    continueButton: {
        width: wp(335),
        marginVertical: hp(40),
        marginHorizontal: wp(20)
    }
})