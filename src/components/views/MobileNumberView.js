import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Colors, TextColors } from '../../styles/Colors';
import { hp, wp } from '../../styles/Dimens';
import { FloatingSelectionInput } from '../inputs/FloatingSelectionInput';
import { FloatingTextInput } from '../inputs/FloatingTextInput';

export const MobileNumberView = (props) => {

    return (
        <View style={styles.mobileContainer}>
            <FloatingSelectionInput
                style={styles.mobileCode}
                label={props.mobileCode && 'Ülke Kodu'}
                placeholder={'Ülke Kodu'}
                onFocus={props.onFocus}
                stickIcon={props.error}
                value={props.mobileCode} />

            <FloatingTextInput
                autoFocus={props.autoFocus}
                label={props.mobilePhone && 'Cep Telefonu Numarası'}
                placeholder={'Cep Telefonu Numarası'}
                placeholderTextColor={TextColors.primaryTextV7}
                style={[styles.mobilePhone, { marginTop : props.mobilePhone ? 0 : 16}]}
                maxLength={15}
                onChangeText={props.onChangeText}
                keyboardType='number-pad'
                value={props.mobilePhone}
                type={'custom'}
                error={props.error} />
        </View>
    )
}

const styles = StyleSheet.create({
    mobileContainer: {
        flexDirection: 'row',
    },
    mobileCode: {
        width: wp(103),
    },
    mobilePhone: {
        // marginEnd: wp(20),
        width: wp(198),
        backgroundColor: 'transparent'
    },
})


