import React, { useState } from 'react'
import { FlatList, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native'
import FastImage from 'react-native-fast-image'
import { useSelector } from 'react-redux'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { PAYMENT_TYPES } from '../../utilities/constants'
import { getFormatedDate } from '../../utilities/helpers'
import { CollapsablePaymentInfo } from '../collapsable/CollapsablePaymentInfo'
import { CollapsableReservationInfo } from '../collapsable/CollapsableReservationInfo'
import { HorizontalSeperator } from '../HorizontalSeperator'
import { VerticalSeperator } from '../VerticalSeperator'
// import CollapsableCard from './CollapsableCard'

export const ReservationSummaryInfo = (props) => {

    const reservation = useSelector(state => state.reservationReducer.reservation)
    const [showReservationInfo, setShowReservationInfo] = useState(false)
    const [showPaymentInfo, setShowPaymentInfo] = useState(false)

    // const calculateTotalAmount = () => {
    //     let servicesTotalAmount = 0
    //     let productsTotalAmount = 0

    //     if (reservation.selectedProducts && reservation.selectedProducts.length > 0) {
    //         productsTotalAmount = reservation.selectedProducts.reduce(function (prev, current) {
    //             return prev + Number(current.totalAmount)
    //         }, 0);
    //     }

    //     let sum = servicesTotalAmount + productsTotalAmount + (reservation.paymentType === PAYMENT_TYPES.PAY_NOW ?
    //         Number(reservation.selectedGroup.amountItem.payNowAmount) :
    //         Number(reservation.selectedGroup.amountItem.payLaterAmount))

    //     return sum.toFixed(2)
    // }

    const renderGroupInfo = () => {
        return (
            <View style={styles.groupInfo}>
                <FastImage
                    style={styles.image}
                    source={{
                        uri: reservation.selectedGroup.imageUrl,
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                />

                <View style={{ flex: 1 }}>
                    <View style={styles.segmentContainer}>
                        <Text style={styles.segmentText}>{reservation.selectedGroup.segmentDesc}</Text>
                        <VerticalSeperator style={{ marginHorizontal: wp(8), width: 2 }} />
                        <Text style={styles.segmentText}>{reservation.selectedGroup.segmentId} Grubu</Text>
                    </View>

                    <Text style={styles.displayText}>{reservation.selectedGroup.displayText}</Text>
                </View>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            {renderGroupInfo()}
            <HorizontalSeperator style={styles.horizontalSeperator} />

            {/* REZERVASYON BİLGİLERİ */}
            <CollapsableReservationInfo
                reservation={reservation} />

            {/* ÖDEME BİLGİLERİ */}
            <HorizontalSeperator style={{ marginTop: hp(16) }} />
            <CollapsablePaymentInfo
                reservation={reservation} />
            {/* {renderPaymentInfo()} */}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    collapsableContainer: {
        backgroundColor: 'white',
        width: '100%',
        height: hp(56),
        borderRadius: hp(20),
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
        justifyContent: 'center',
        paddingHorizontal: wp(20)
    },
    collapsableTitle: {
        fontSize: hp(18),
        color: TextColors.primaryText,
        fontWeight: '700'
    },
    collapsableIcon: {
        position: 'absolute',
        tintColor: Colors.primaryBrand,
        end: wp(20)
    },
    collapsedContainer: {
        marginHorizontal: wp(20),
        backgroundColor: 'white',
        borderRadius: hp(20),
        marginTop: hp(20),
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },
    groupInfo: {
        marginVertical: hp(20),
        height: hp(70),
        flexDirection: 'row'
    },
    paymentInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: hp(10)
    },
    horizontalSeperator: {
        marginTop: hp(20)
    },
    image: {
        width: wp(112),
        height: hp(70),
        marginHorizontal: wp(10)
    },
    displayText: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryText,
        marginTop: hp(9),
        marginEnd: wp(20)
    },
    segmentContainer: {
        flexDirection: 'row',
    },
    segmentText: {
        fontSize: hp(12),
        fontWeight: '700',
        color: TextColors.primaryTextV3,
    },
    branchTitle: {
        fontSize: hp(14),
        color: '#999999',
        fontFamily: 'NunitoSans-SemiBold'
    },
    branchName: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontWeight: '700',
        marginTop: hp(8)
    },
    date: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginHorizontal: wp(8)
    },
    paymentTitle: {
        fontSize: hp(14),
        color: '#999999',
        fontFamily: 'NunitoSans-SemiBold'
    },
    paymentValue: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontWeight: '700',
    },
    totalValue: {
        fontSize: hp(18),
        color: TextColors.primaryText,
        fontWeight: '700',
    },
    productInfo: {
        marginStart: wp(20),
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: hp(8)
    }

})