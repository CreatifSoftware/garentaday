import { useNavigation } from '@react-navigation/native'
import React, { useState, useLayoutEffect, useRef } from 'react'
import { StyleSheet, Alert, ScrollView, View, SafeAreaView, Switch, Text } from "react-native"
import { FilledButton } from '../buttons/FilledButton'
import { Colors, TextColors } from '../../styles/Colors'
import { getStatusBarHeight, hp, wp } from '../../styles/Dimens'
import { FloatingTextInput } from '../inputs/FloatingTextInput';
import { useDispatch, useSelector } from 'react-redux';
import { setUserAction } from '../../redux/actions/AuthActions';
import { PreviousNextView } from 'react-native-keyboard-manager';
import RBSheet from "react-native-raw-bottom-sheet";
import { CountrySelectionModal } from '../../containers/modals/CountrySelectionModal';
import { CitySelectionModal } from '../../containers/modals/CitySelectionModal';
import { DistrictSelectionModal } from '../../containers/modals/DistrictSelectionModal';
import { filterDistrictsByCityCode, getCityItem, getCountryItem, getDistrictItem } from '../../utilities/helpers'
import { FloatingSelectionInput } from '../inputs/FloatingSelectionInput'
import { INVOICE_ADDRESS, PERSONAL_ADDRESS } from '../../utilities/constants'
import { RadioButtonView } from '../buttons/RadioButtonView'
import { HorizontalSeperator } from '../HorizontalSeperator'
import { callCreateUpdateAddressApi, callGetProfileInfoApi } from '../../api/user/userService'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { getErrorMessage, isSuccess } from '../../api/apiHelpers'
import { BorderedButton } from '../buttons/BorderedButton'
import { Styles } from '../../styles/Styles'
import { GetProfileResponse } from '../..//api/models/GetProfileResponse';

export const AddressInformation = ({ route }) => {
    const { addressItem, type, hideDeleteButton } = route.params;
    const user = useSelector(state => state.authReducer.user)
    const countries = useSelector(state => state.masterReducer.countries)
    const cities = useSelector(state => state.masterReducer.cities)
    const districts = useSelector(state => state.masterReducer.districts)

    const [corporateAddress, setCorporateAddress] = useState(addressItem && addressItem.taxNo.length > 0)
    const [name, setName] = useState(addressItem && addressItem.name)
    const [surname, setSurname] = useState(addressItem && addressItem.surname)
    const [govermentId, setGovermentId] = useState(addressItem && addressItem.govermentId)
    const [taxNo, setTaxNo] = useState(addressItem && addressItem.taxNo)
    const [taxPlace, setTaxPlace] = useState(addressItem && addressItem.taxPlace)
    const [companyName, setCompanyName] = useState(addressItem && addressItem.companyName)

    const [addressTitle, setAddressTitle] = useState(addressItem && addressItem.addressTitle)
    const [address, setAddress] = useState(addressItem && addressItem.address)
    const [country, setCountry] = useState(addressItem && getCountryItem(countries, addressItem.country))
    const [city, setCity] = useState(addressItem && getCityItem(cities, addressItem.country, addressItem.city))
    const [district, setDistrict] = useState(addressItem && getDistrictItem(districts, addressItem.district))

    const [useAsInvoice, setUseAsInvoice] = useState(false)
    const [invoiceAddressTitle, setInvoiceAddressTitle] = useState(null)

    const [modalType, setModalType] = useState(null)
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const navigation = useNavigation()
    const dispatch = useDispatch()
    const refRBSheet = useRef();

    let isCountrySelected = false
    let isCitySelected = false
    let isDistrictSelected = false
    let isCityHasDistricts = false

    useLayoutEffect(() => {
        setContinueButtonState()
    }, [addressTitle, address, country, city, district]);

    const setContinueButtonState = () => {
        setButtonDisabled(false)
        if (addressTitle && addressTitle.length > 0 &&
            address && address.length > 0 &&
            country &&
            city) {
            let filtered = filterDistrictsByCityCode(city.city, country.country, districts)
            if (filtered.length > 0 && district) {
                setButtonDisabled(false)
            } else if (filtered.length == 0) {
                setButtonDisabled(false)
            } else {
                setButtonDisabled(true)
            }
        }
        else {
            setButtonDisabled(true)
        }
    }

    function showCountryModal() {
        setModalType('country')
        refRBSheet.current.open()
    }

    function showCityModal() {
        setModalType('city')
        refRBSheet.current.open()
    }

    function showDistrictModal() {
        if (city){
            setModalType('district')
            refRBSheet.current.open()
        } else {
            Alert.alert('Uyarı', 'Önce şehir seçilmelidir.')
        }
        
    }

    const handleOnCountrySelection = (item) => {
        setCountry(item)
        setCity(undefined)
        setDistrict(undefined)
        // isCountrySelected = true
        // isCitySelected = false
        // isDistrictSelected = false
        // setContinueButtonState()
        refRBSheet.current.close()
    }

    const handleOnCitySelection = (item) => {
        setCity(item)
        setDistrict(undefined)
        // setContinueButtonState()
        refRBSheet.current.close()
    }

    const handleOnDistrictSelection = (item) => {
        setDistrict(item)
        // isDistrictSelected = true
        // setContinueButtonState()
        refRBSheet.current.close()
    }

    function getAddressListForApiCall(isDelete) {
        let addressList = []
        let addressInfo = {}

        addressInfo.addressId = addressItem && addressItem.addressId
        addressInfo.type = type
        addressInfo.addressTitle = addressTitle
        addressInfo.address = address
        addressInfo.country = country.country
        addressInfo.city = city.city
        addressInfo.district = district && district.district

        if (type === INVOICE_ADDRESS && corporateAddress) {
            addressInfo.companyName = companyName
            addressInfo.taxNo = taxNo
            addressInfo.taxPlace = taxPlace
            addressInfo.name = ""
            addressInfo.surname = ""
            addressInfo.govermentId = ""
        }

        if (type === INVOICE_ADDRESS && !corporateAddress) {
            addressInfo.name = name
            addressInfo.surname = surname
            addressInfo.govermentId = govermentId
            addressInfo.companyName = ""
            addressInfo.taxNo = ""
            addressInfo.taxPlace = ""
        }

        addressInfo.isDelete = isDelete
        addressList.push(addressInfo)

        // if useAsInvoice checked, add invoice item
        if (useAsInvoice) {
            let invoiceAddress = {}
            invoiceAddress.type = INVOICE_ADDRESS
            invoiceAddress.addressTitle = invoiceAddressTitle
            invoiceAddress.address = address
            invoiceAddress.country = country.country
            invoiceAddress.city = city.city
            invoiceAddress.district = district && district.district
            invoiceAddress.name = user.personalInfo.firstName
            invoiceAddress.surname = user.personalInfo.lastName
            invoiceAddress.govermentId = user.personalInfo.isTurkishCitizen ? user.personalInfo.govermentId : user.personalInfo.passport
            invoiceAddress.companyName = ""
            invoiceAddress.taxNo = ""
            invoiceAddress.taxPlace = ""

            addressList.push(invoiceAddress)
        }

        return addressList
    }

    const getProfileInfo = () => {
        callGetProfileInfoApi(user.userId)
            .then(response => {
                dispatch(setLoadingAction(false))
                dispatch(setUserAction(GetProfileResponse(response.data.EXPORT, user)))
                Alert.alert(
                    "Başarılı",
                    "İşlem başarılı",
                    [
                        { text: "Tamam", onPress: () => navigation.goBack() }
                    ],
                    { cancelable: false }
                );
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    const handleOnContinueButton = () => {
        dispatch(setLoadingAction(true))
        let addressList = getAddressListForApiCall(false)
        callCreateUpdateAddressApi(addressList, user.userId)
            .then(response => {
                if (isSuccess(response)) {
                    getProfileInfo()
                } else {
                    dispatch(setLoadingAction(false))
                    Alert.alert('Hata', getErrorMessage(response))
                }
                // dispatch(setLoadingAction(false))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    const handleOnDeleteButton = () => {
        dispatch(setLoadingAction(true))
        let addressList = getAddressListForApiCall(true)
        callCreateUpdateAddressApi(addressList, user.userId)
            .then(response => {
                if (isSuccess(response)) {
                    getProfileInfo()
                } else {
                    dispatch(setLoadingAction(false))
                    Alert.alert('Hata', getErrorMessage(response))
                }
                // dispatch(setLoadingAction(false))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    return (
        <View style={{
            flex: 1,
            position: 'relative'
        }}>
            <ScrollView
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={styles.container}
                style={{ backgroundColor: 'white' }}>

                <PreviousNextView >

                    {
                        type === INVOICE_ADDRESS &&

                        <View style={styles.nationalityGroup}>
                            <RadioButtonView label='Bireysel' selected={!corporateAddress} onPress={() => setCorporateAddress(false)} />
                            <RadioButtonView label='Kurumsal' selected={corporateAddress} onPress={() => setCorporateAddress(true)} />
                        </View>
                    }
                    {
                        type === INVOICE_ADDRESS && corporateAddress ?
                            <>

                                <FloatingTextInput
                                    label={'Şirket Unvanı'}
                                    placeholder={'Şirket Unvanı'}
                                    onChangeText={text => setCompanyName(text)}
                                    value={companyName} />

                                <FloatingTextInput
                                    label={'Vergi Dairesi'}
                                    placeholder={'Vergi Dairesi'}
                                    onChangeText={text => setTaxPlace(text)}
                                    value={taxPlace} />

                                <FloatingTextInput
                                    label={'Vergi Numarası'}
                                    placeholder={'Vergi Numarası'}
                                    maxLength={10}
                                    type={'only-numbers'}
                                    keyboardType={'number-pad'}
                                    onChangeText={text => setTaxNo(text)}
                                    value={taxNo} />

                                <HorizontalSeperator style={{ marginTop: hp(30), height: hp(10), backgroundColor: Colors.mainBackground }} />
                            </>

                            :

                            type === INVOICE_ADDRESS && !corporateAddress ?

                                <>

                                    <FloatingTextInput
                                        label={'T.C. Kimlik Numarası/Pasaport Numarası'}
                                        placeholder={'T.C. Kimlik Numarası/Pasaport Numarası'}
                                        maxLength={11}
                                        type={'only-numbers'}
                                        keyboardType={'number-pad'}
                                        onChangeText={text => setGovermentId(text)}
                                        value={govermentId} />

                                    <FloatingTextInput
                                        label={'Ad'}
                                        placeholder={'Ad'}
                                        onChangeText={text => setName(text)}
                                        value={name} />

                                    <FloatingTextInput
                                        label={'Soyad'}
                                        placeholder={'Soyad'}
                                        onChangeText={text => setSurname(text)}
                                        value={surname} />

                                    <HorizontalSeperator style={{ marginTop: hp(30), height: hp(10), backgroundColor: Colors.mainBackground }} />
                                </>

                                :

                                null
                    }
                    <FloatingTextInput
                        label={'Adres Tanımı'}
                        placeholder={'Adres Tanımı'}
                        onChangeText={text => setAddressTitle(text)}
                        value={addressTitle} />

                    <FloatingTextInput
                        label={'Adres'}
                        placeholder={'Adres'}
                        onChangeText={text => setAddress(text)}
                        value={address} />

                    <FloatingSelectionInput
                        label={'Ülke'}
                        placeholder={'Ülke'}
                        onFocus={showCountryModal}
                        value={country && country.countryName} />

                    <FloatingSelectionInput
                        disabled={country === null}
                        label={'Şehir'}
                        placeholder={'Şehir'}
                        onFocus={showCityModal}
                        value={city && city.cityName} />

                    <FloatingSelectionInput
                        disabled={city === null || (city && filterDistrictsByCityCode(city.city, country.country, districts).length == 0)}
                        label={'İlçe'}
                        placeholder={'İlçe'}
                        onFocus={showDistrictModal}
                        value={district && district.districtName} />
                </PreviousNextView>

                {
                    type === PERSONAL_ADDRESS && !addressItem &&
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: wp(20), marginTop: hp(30) }}>
                        <Text style={styles.title}>Fatura adresi olarak kullan</Text>
                        <Switch
                            style={styles.switchStyle}
                            trackColor={{ false: Colors.white, true: Colors.primaryBrand }}
                            thumbColor={Colors.white}
                            onValueChange={() => setUseAsInvoice(!useAsInvoice)}
                            value={useAsInvoice}
                        />
                    </View>
                }
                {
                    useAsInvoice &&
                    <FloatingTextInput
                        style={{ marginTop: 0 }}
                        label={'Fatura Adresi Tanımı'}
                        placeholder={'Fatura Adresi Tanımı'}
                        onChangeText={text => setInvoiceAddressTitle(text)}
                        value={invoiceAddressTitle} />
                }
            </ScrollView>

            <View style={Styles.shadowBox}>
                <FilledButton
                    disabled={buttonDisabled}
                    style={Styles.continueButton}
                    onPress={handleOnContinueButton}
                    title={'Kaydet'} />

                {
                    !hideDeleteButton && addressItem &&
                    <BorderedButton
                        disabled={buttonDisabled}
                        style={styles.deleteButton}
                        onPress={handleOnDeleteButton}
                        title={'Sil'} />
                }

            </View>

            <RBSheet
                height={hp(718)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                {
                    modalType == 'country' ?
                        <CountrySelectionModal onPress={handleOnCountrySelection} current={country && country.countryName} />

                        :

                        modalType == 'city' ?
                            <CitySelectionModal onPress={handleOnCitySelection} countryCode={country.country} current={city && city.cityName} />

                            :

                            modalType == 'district' ?
                                <DistrictSelectionModal onPress={handleOnDistrictSelection} countryCode={country.country} cityCode={city.city} current={district && district.district} />

                                :

                                null
                }
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        paddingBottom: hp(106),
    },
    box1: {
        width: wp(375),
        height: hp(62) + getStatusBarHeight(),
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        // add shadows for iOS only
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
    },
    nationalityGroup: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between',
        marginTop: hp(24)
    },
    genderTitle: {
        fontFamily: 'NunitoSans-Bold',
        fontSize: hp(18),
        color: TextColors.primaryText,
        // marginTop: hp(36),
        marginTop: 36,
        marginStart: wp(20)
    },
    genderGroup: {
        marginHorizontal: wp(20),
    },
    genderRadio: {
        width: wp(335),
        marginTop: hp(10)
    },
    saveButton: {
        width: wp(335),
        marginVertical: hp(40),
        marginHorizontal: wp(20)
    },
    deleteButton: {
        width: wp(335),
        marginBottom: hp(40),
        marginTop: hp(16),
        marginHorizontal: wp(20)
    },
    title: {
        fontSize: hp(16),
        color: TextColors.primaryText,
    },
    subtitle: {
        fontSize: hp(14),
        color: TextColors.primaryTextV3,
        marginTop: hp(8)
    },
    switchStyle: {
        marginStart: wp(20)
    }
})