import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from 'react-native'
import { useSelector } from 'react-redux'
import { TextColors, Colors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'

export const FilterGroupList = (props) => {
    const { fuelTypes, transmissionTypes } = useSelector(state => state.masterReducer)
    const { selectedTransmissionTypes, selectedFuelTypes } = props

    const renderFuelTypeItem = ({ item }) => {
        let isSelected = selectedFuelTypes.filter(arg => arg.YAKIT_TIP === item.YAKIT_TIP).length > 0
        return (
            <TouchableOpacity
                onPress={() => props.handleFuelTypeSelection(item)}
                style={isSelected ? styles.advancedFilterContainerSelected : styles.advancedFilterContainer}>
                <Text style={isSelected ? styles.advancedFilterItemSelected : styles.advancedFilterItem}>{item.YAKIT_TIP_TEXT}</Text>
            </TouchableOpacity>
        )
    }

    const renderTransmissionTypeItem = ({ item }) => {
        let isSelected = selectedTransmissionTypes.filter(arg => arg.SANZIMAN === item.SANZIMAN).length > 0
        return (
            <TouchableOpacity
                onPress={() => props.handleTransmissionTypeSelection(item)}
                style={isSelected ? styles.advancedFilterContainerSelected : styles.advancedFilterContainer}>
                <Text style={isSelected ? styles.advancedFilterItemSelected : styles.advancedFilterItem}>{item.SANZIMAN_TEXT}</Text>
            </TouchableOpacity>
        )
    }

    const renderFuelTypeList = () => {
        return (
            <View style={{ paddingStart: wp(20), marginTop: hp(20) }}>
                <Text style={styles.advancedFilterTitle}>Yakıt Tipi</Text>
                <FlatList
                    horizontal={true}
                    contentContainerStyle={{ marginTop: hp(16) }}
                    data={fuelTypes}
                    renderItem={renderFuelTypeItem}
                    keyExtractor={item => item.YAKIT_TIP}
                />
            </View>
        )
    }

    const renderTransmissionTypeList = () => {
        return (
            <View style={{ paddingStart: wp(20), marginTop: hp(20) }}>
                <Text style={styles.advancedFilterTitle}>Vites Tipi</Text>
                <FlatList
                    horizontal={true}
                    contentContainerStyle={{ marginTop: hp(16) }}
                    extraData={transmissionTypes}
                    data={transmissionTypes}
                    renderItem={renderTransmissionTypeItem}
                    keyExtractor={item => item.SANZIMAN}
                />
            </View>
        )
    }

    return (
        <View >
            {renderFuelTypeList()}
            {renderTransmissionTypeList()}
        </View>
    )
}

const styles = StyleSheet.create({
    advancedFilterTitle: {
        fontSize: hp(18),
        color: TextColors.primaryTextV4,
        fontWeight: '700',
    },
    advancedFilterContainerSelected: {
        backgroundColor: Colors.primaryBrand,
        height: hp(44),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: hp(22),
        marginEnd: wp(10)
    },
    advancedFilterItemSelected: {
        fontSize: hp(16),
        color: 'white',
        fontWeight: '700',
        paddingHorizontal: wp(12),
    },
    advancedFilterContainer: {
        backgroundColor: Colors.white,
        height: hp(44),
        borderWidth: 1,
        borderColor: Colors.borderColor,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: hp(22),
        marginEnd: wp(10)
    },
    advancedFilterItem: {
        fontSize: hp(16),
        color: TextColors.primaryText,
        fontWeight: '700',
        paddingHorizontal: wp(12),
    }
})