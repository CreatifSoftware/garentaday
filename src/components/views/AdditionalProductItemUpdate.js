import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { ImageButton } from '../buttons/ImageButton'
import { HorizontalSeperator } from '../HorizontalSeperator'
import moment from 'moment'
import { PAYMENT_TYPES } from '../../utilities/constants'

export const AdditionalProductItemUpdate = (props) => {
    const { item, onPress, reservation } = props

    const isPassive = item.required
    const isMinusPassive = item.value === 0
    const isPlusPassive = item.value === item.maxValue
    const isSelected = item.value > 0
    const isPayNow = reservation.paymentType == PAYMENT_TYPES.PAY_NOW

    const calculateDifferentAmount = () => {
        return item.totalAmount * item.value - item.paidAmount
    }

    return (
        <>
            <TouchableOpacity
                disabled={isPassive || item.maxValue > 1}
                onPress={onPress}
                style={isSelected ? styles.selectedContainer : styles.container}>
                {
                    isSelected &&
                    <Image style={styles.checkMark} source={require('../../assets/icons/ic_product_selected.png')} />
                }
                <Image
                    style={styles.icon}
                    source={require('../../assets/icons/ic_sample_product.png')}
                    resizeMode={'contain'} />
                <View style={{ flex: 1 }}>
                    <Text style={styles.productName}>{item.productName}</Text>
                    <TouchableOpacity
                        onPress={props.onInfoPress}
                        style={styles.dailyAmountContainer}>
                        <Text style={styles.dailyAmount}>{(Number(item.totalAmount) / Number(props.duration)).toFixed(2)} TL</Text>
                        <Text style={styles.daily}> / günlük</Text>
                        <Image
                            style={styles.infoIcon}
                            source={require('../../assets/icons/ic_info.png')} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between',alignItems:'center', marginVertical:hp(16) }}>
                        <View style={styles.amountContainer}>
                            <Text style={styles.totalAmount}>{Number(item.totalAmount).toFixed(2)} TL</Text>
                            <Text style={styles.totalDays}> / {props.duration} gün</Text>
                        </View>
                        {
                            item.maxValue > 1 ?

                                <View style={styles.plusMinusContainer}>
                                    <ImageButton
                                        disabled={isMinusPassive}
                                        imageStyle={isMinusPassive ? styles.minusImagePassive : styles.minusImage}
                                        source={require('../../assets/icons/ic_minus.png')}
                                        onPress={props.onMinusPress} />
                                    <Text style={styles.value}>{Number(item.value)}</Text>
                                    <ImageButton
                                        disabled={isPlusPassive}
                                        imageStyle={isPlusPassive ? styles.plusImagePassive : styles.plusImage}
                                        source={require('../../assets/icons/ic_plus.png')}
                                        onPress={props.onPlusPress} />
                                </View>

                                :

                                <Image style={isPassive ? styles.selectionPassive : styles.selection} source={
                                    isSelected ?
                                        require('../../assets/icons/ic_selected.png') :
                                        require('../../assets/icons/ic_unselected.png')
                                } />
                        }
                    </View>

                    {
                        reservation.isPaid &&
                        <View style={{ flexDirection: 'row', marginBottom: hp(20) }}>
                            <View style={styles.paidAmountContainer}>
                                <Text style={styles.paidAmountTitle}>ÖDENEN</Text>
                                <Text style={styles.paidAmount}>{reservation.isPaid ? item.paidAmount : 0.00} TL</Text>
                            </View>

                            <View style={styles.paidAmountContainer}>
                                <Text style={styles.paidAmountTitle}>{calculateDifferentAmount() > 0 ? 'ÖDENECEK' : 'İADE EDİLECEK'} </Text>
                                <Text style={[styles.amountToBePaid, { color: calculateDifferentAmount() > 0 ? '#2ac769' : '#39a9db' }]}>{calculateDifferentAmount()} TL</Text>
                            </View>
                        </View>
                    }
                </View>


            </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: Colors.white,
        minHeight: hp(125),
        paddingTop: hp(20),
        paddingHorizontal: wp(20),
        borderBottomWidth: 1,
        borderBottomColor: Colors.seperatorColor
    },
    selectedContainer: {
        flexDirection: 'row',
        backgroundColor: Colors.white,
        minHeight: hp(125),
        paddingTop: hp(20),
        paddingHorizontal: wp(20),
        borderTopColor: '#2ac769',
        borderBottomColor: '#2ac769',
        backgroundColor: '#f7fffa',
        borderBottomWidth: 1,
        borderTopWidth: 1
    },
    amountContainer: {
        // position: 'absolute',
        width: wp(120),
        // marginBottom: hp(20),
        // marginTop: hp(10),
        flexDirection: 'row',
        backgroundColor: Colors.seperatorColorV2,
        height: hp(32),
        borderRadius: hp(10),
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: wp(10),
    },
    paidAmountContainer: {
        marginEnd: wp(30)
    },
    dailyAmountContainer: {
        width: wp(120),
        marginTop: hp(6),
        flexDirection: 'row',
        alignItems: 'center',
    },
    productName: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontWeight: '700'
    },
    daily: {
        fontSize: hp(12),
        color: TextColors.primaryTextV3,
        fontFamily: 'NunitoSans-SemiBold',

    },
    dailyAmount: {
        fontSize: hp(12),
        color: Colors.primaryBrandV3,
        fontFamily: 'NunitoSans-Bold',
    },
    totalDays: {
        fontSize: hp(12),
        color: TextColors.primaryText,
        fontWeight: '700',

    },
    totalAmount: {
        fontSize: hp(14),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',

    },
    icon: {
        width: wp(64),
        height: wp(64),
        marginEnd: wp(8),
        tintColor: Colors.primaryBrand
    },
    infoIcon: {
        width: wp(18),
        height: wp(18),
        marginStart: wp(8),
        tintColor: Colors.primaryBrand
    },
    selection: {
        width: hp(32),
        height: hp(32),
        // tintColor: Colors.primaryBrand
    },
    selectionPassive: {
        width: hp(32),
        height: hp(32),
        // tintColor: Colors.checkboxBorder
    },
    plusMinusContainer: {
        flexDirection: 'row',
        height: hp(32),
        alignItems: 'center',
    },
    checkMark: {
        position: 'absolute',
        top: 0,
        left: 0
    },
    value: {
        fontSize: hp(20),
        color: TextColors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',
        marginHorizontal: wp(14)
    },
    plusImage: {
        width: hp(32),
        height: hp(32),
        tintColor: Colors.primaryBrand
    },
    minusImage: {
        width: hp(32),
        height: hp(32),
        tintColor: Colors.primaryBrand
    },
    plusImagePassive: {
        width: hp(32),
        height: hp(32),
        tintColor: Colors.checkboxBorder
    },
    minusImagePassive: {
        width: hp(32),
        height: hp(32),
        tintColor: Colors.checkboxBorder
    }
})