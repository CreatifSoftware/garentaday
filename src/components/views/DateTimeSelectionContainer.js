import React from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native';
import { Colors, TextColors } from '../../styles/Colors';
import { hp, wp } from '../../styles/Dimens';
import { getFormatedDate } from '../../utilities/helpers';
import { VerticalSeperator } from '../VerticalSeperator';

export const DateTimeSelectionContainer = (props) => {

    return (
        <View style={[styles.container, { opacity: props.isDisabled ? 0.5 : 1 }]}>
            <TouchableOpacity
                disabled={props.isDisabled}
                style={styles.pickup}
                activeOpacity={0.6}
                onPress={props.handleOnPickupDate}>
                <Text style={styles.pickupTitle}>Alış Tarihi</Text>
                <Text style={styles.pickupDate}>{props.pickupDate ? getFormatedDate(props.pickupDate, 'DD MMMM yyyy') : 'TARİH VE SAAT'}</Text>
                <Text style={styles.pickupDate}>{props.pickupDate ? `${getFormatedDate(props.pickupDate, 'HH:mm')} - ${getFormatedDate(props.pickupDate, 'ddd')}` : 'SEÇİN'} </Text>
            </TouchableOpacity>

            <View style={{ alignItems: 'center' }}>
                <VerticalSeperator style={{ flex: 1, backgroundColor: Colors.primaryBrand, width: 2 }} />
                <View style={{ width: hp(44), height: hp(44), borderRadius: hp(44) / 2, borderWidth: 2, borderColor: Colors.primaryBrand, justifyContent: 'center', alignItems: 'center' }}>
                    <Image style={{ tintColor: Colors.primaryBrand }} resizeMode={'contain'} resizeMethod={'auto'} source={require('../../assets/icons/ic_date.png')} />
                </View>
                <VerticalSeperator style={{ flex: 1, backgroundColor: Colors.primaryBrand, width: 2 }} />
            </View>

            <TouchableOpacity
                disabled={props.isDisabled}
                style={styles.dropoff}
                activeOpacity={0.6}
                onPress={props.handleOnDropoffDate}>
                <Text style={styles.dropoffTitle}>Teslim Tarihi</Text>
                <Text style={styles.dropoffDate}>{props.dropoffDate ? getFormatedDate(props.dropoffDate, 'DD MMMM yyyy') : 'TARİH VE SAAT'}</Text>
                <Text style={styles.dropoffDate}>{props.dropoffDate ? `${getFormatedDate(props.dropoffDate, 'HH:mm')} - ${getFormatedDate(props.dropoffDate, 'ddd')}` : 'SEÇİN'} </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: hp(101),
        marginHorizontal: wp(20),
        borderRadius: hp(10),
        backgroundColor: Colors.white,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: Colors.borderColor,
        borderWidth: 1,
        opacity: 0.5,
    },
    pickup: {
        marginVertical: hp(16),
        marginStart: wp(16),
        flex: 1,
    },
    pickupTitle: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryTextV7,
        marginBottom: hp(8)
    },
    pickupDate: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryText
    },
    dropoff: {
        marginTop: hp(16),
        marginEnd: wp(16),
        flex: 1,
    },
    dropoffTitle: {
        textAlign: 'right',
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryTextV7,
        marginBottom: hp(8)
    },
    dropoffDate: {
        textAlign: 'right',
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryText
    }
})


