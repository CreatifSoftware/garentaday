import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import FastImage from 'react-native-fast-image'
import { CampaignFlag, campaignFlag } from '../../assets/Icon'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { PAYMENT_TYPES } from '../../utilities/constants'
import { calculateCarGroupDifferentAmount } from '../../utilities/helpers'
import { HorizontalSeperator } from '../HorizontalSeperator'
import { VerticalSeperator } from '../VerticalSeperator'

export const CarGroupItemUpdate = (props) => {
    const { item, payNowAmount, payLaterAmount, hasCampaign, reservation } = props
    // Rezervasyon yaratılırken seçilen ödeme tipi
    const isPayNow = reservation.paymentType == PAYMENT_TYPES.PAY_NOW
    const itemPrice = isPayNow ? payNowAmount : payLaterAmount
    const differentAmount = calculateCarGroupDifferentAmount(reservation, itemPrice)
    // const calculateDifferentAmount = () => {
    //     if (reservation.isPaid) {
    //         let reservationCarPrice = isPayNow ? reservation.selectedGroup.amountItem.payNowAmount : reservation.selectedGroup.amountItem.payLaterAmount
    //         let difference = Number(itemPrice) - Number(reservationCarPrice)

    //         return difference.toFixed(2)
    //     }

    //     return 0.00
    // }

    return (
        <>
            <TouchableOpacity
                onPress={() => props.onPress(item)}
                style={[reservation.selectedGroup.groupCode === item.groupCode ? styles.selectedContainer : styles.container]}>
                {
                    hasCampaign &&
                    <CampaignFlag style={{ marginTop: 10 }} />
                }

                <View style={{ flexDirection: 'row', marginTop: hp(16) }}>
                    <FastImage
                        style={styles.image}
                        source={{
                            uri: item.imageUrl,
                            priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                    />


                    <View style={{ flex: 1 }}>
                        <View style={styles.segmentContainer}>
                            <Text style={styles.segmentText}>{item.segmentDesc}</Text>
                            <VerticalSeperator style={{ marginHorizontal: wp(8), width: 2 }} />
                            <Text style={styles.segmentText}>{item.segmentId} Grubu</Text>
                        </View>

                        <Text style={styles.displayText}>{item.displayText}</Text>

                        <View style={styles.infoContainer}>
                            <Text style={styles.infoText}>{item.transmissionDesc}</Text>
                            <VerticalSeperator style={{ marginHorizontal: wp(8), width: 2 }} />
                            <Text style={styles.infoText}>{item.fuelTypeDesc}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginBottom: hp(20) }}>
                            <View style={styles.amountContainer}>
                                <Text style={styles.payNowTitle}>TOPLAM TUTAR</Text>
                                <Text style={styles.payNow}>{itemPrice} TL</Text>
                            </View>

                            <View style={styles.amountContainer}>
                                <Text style={[styles.payLaterTitle, { color: differentAmount > 0 ? '#2ac769' : '#39a9db' }]}>{differentAmount > 0 ? 'TAHSİL EDİLECEK' : 'İADE EDİLECEK'} </Text>
                                <Text style={[styles.payLater, { color: differentAmount > 0 ? '#2ac769' : '#39a9db' }]}>{differentAmount} TL</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
            <HorizontalSeperator style={{ height: 4, backgroundColor: Colors.seperatorColorV2 }} />
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: hp(176),
        backgroundColor: Colors.white,
        // paddingTop: hp(14),
    },
    selectedContainer: {
        minHeight: hp(176),
        backgroundColor: Colors.white,
        // paddingTop: hp(14),
        borderWidth: 1,
        borderColor: Colors.primaryBrand
    },
    image: {
        width: wp(112),
        height: hp(70),
        marginHorizontal: wp(10)
    },
    displayText: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryText,
        marginTop: hp(9),
        marginEnd: wp(20)
    },
    segmentContainer: {
        flexDirection: 'row',
    },
    segmentText: {
        fontSize: hp(12),
        fontWeight: '700',
        color: TextColors.primaryTextV3,
    },
    infoContainer: {
        flexDirection: 'row',
        marginTop: hp(9),
    },
    infoText: {
        fontSize: hp(12),
        fontWeight: '700',
        color: TextColors.primaryTextV2,
    },
    amountContainer: {
        marginTop: hp(20),
        marginEnd: wp(30)
    },
    payNowTitle: {
        fontSize: hp(12),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Black',

    },
    payNow: {
        fontSize: hp(18),
        fontFamily: 'NunitoSans-Bold',
        color: Colors.primaryBrand,
    },
    payLaterTitle: {
        fontSize: hp(12),
        fontFamily: 'NunitoSans-Black',
        color: TextColors.primaryTextV3,
    },
    payLater: {
        fontSize: hp(18),
        fontFamily: 'NunitoSans-Bold',
        color: TextColors.primaryTextV3,
    }
})