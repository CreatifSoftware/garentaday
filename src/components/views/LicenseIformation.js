import React from 'react'
import { StyleSheet, Text, ScrollView, View, SafeAreaView } from "react-native"
import { FilledButton } from '../../components/buttons/FilledButton'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { FloatingSelectionInput } from '../../components/inputs/FloatingSelectionInput'
import { PreviousNextView } from 'react-native-keyboard-manager';
import { Styles } from '../../styles/Styles'

export const LicenseInformation = (props) => {

    return (
        <View style={{ flex: 1 }}>
            <ScrollView
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={styles.container}
                style={{ backgroundColor: 'white' }}>
                <PreviousNextView>
                    <FloatingTextInput
                        type={'only-numbers'}
                        label={'Ehliyet Numarası'}
                        placeholder={'Ehliyet Numarası'}
                        keyboardType={'number-pad'}
                        autoFocus={props.autoFocus}
                        onChangeText={props.setLicenseNumber}
                        value={props.licenseNumber}
                    />
                    <FloatingSelectionInput
                        label={'Ehliyet Sınıfı'}
                        placeholder={'Ehliyet Sınıfı'}
                        onFocus={props.showLicenseClassModal}
                        value={props.licenseClass}
                    />
                    <FloatingTextInput
                        label={'Ehliyet Alış Yeri'}
                        placeholder={'Ehliyet Alış Yeri'}
                        onChangeText={props.setLicensePlace}
                        value={props.licensePlace}
                    />

                    <Text style={styles.genderTitle}>Ehliyet Veriliş Tarihi</Text>
                    <FloatingTextInput
                        label={'Ehliyet Veriliş Tarihi'}
                        placeholder={'DD/MM/YYYY'}
                        maxLength={10}
                        keyboardType={'number-pad'}
                        returnKeyType={'done'}
                        type={'datetime'}
                        error={props.licenseDateError}
                        onChangeText={props.handleOnDateOfLicenseChange}
                        value={props.licenseDate}
                        disabled={props.disabled}
                    />

                </PreviousNextView>
            </ScrollView >

            <View style={[Styles.shadowBox]} >
                <FilledButton
                    disabled={props.buttonDisabled}
                    style={Styles.continueButton}
                    onPress={props.handleOnContinueButton}
                    title={props.buttonTitle} />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        paddingBottom:40
    },
    nationalityGroup: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between',
        marginTop: hp(24)
    },
    genderTitle: {
        fontFamily: 'NunitoSans-Bold',
        fontSize: hp(18),
        color: TextColors.primaryText,
        // marginTop: hp(36),
        marginTop: hp(36),
        marginStart: wp(20)
    },
    genderGroup: {
        marginHorizontal: wp(20),
    },
    genderRadio: {
        width: wp(335),
        marginTop: hp(10)
    },
    continueButton: {
        width: wp(335),
        marginTop: hp(20),
        marginHorizontal: wp(20)
    }
})