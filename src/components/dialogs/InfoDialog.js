import React from 'react'
import { StyleSheet, TouchableOpacity,Text } from "react-native"
import { Colors, TextColors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';

export const InfoDialog = (props) => {
    const { title = '', style = {}, textStyle = {}, onPress } = props;

    return (
        <View
            style={[styles.button, style]}>
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.content}>{props.content}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        height: wp(50),
        borderRadius: wp(50)/2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primaryBrand,
    },
    title: {
        fontSize: hp(20),
        fontFamily: 'NunitoSans-Bold',
        color: TextColors.primaryText,
    },
    content: {
        fontSize: hp(18),
        color: TextColors.primaryTextV2,
    },
    button: {
        display: 'flex',
        height: wp(50),
        borderRadius: wp(50)/2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primaryBrand,
    },
})



