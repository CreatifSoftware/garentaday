import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import {Colors, TextColors} from '../../styles/Colors';
import {hp, wp} from '../../styles/Dimens';
import {HorizontalSeperator} from '../HorizontalSeperator';

export const FloatingTextInput = props => {
  const [focused, setFocused] = useState(false);

  const onFocus = () => {
    setFocused(!focused);
    props.onFocus;
  };

  return (
    <View style={styles.container}>
      {props.value ? (
        <Text
          style={focused ? styles.activeFloatingLabel : styles.floatingLabel}>
          {props.label}
        </Text>
      ) : null}
      {props.type ? (
        <TextInputMask
          {...props}
          editable={!props.disabled}
          placeholderTextColor={TextColors.primaryTextV7}
          style={[
            props.disabled ? styles.disabledTextInput : styles.textInput,
            props.style,
          ]}
          autoCorrect={false}
          spellCheck={false}
          onFocus={onFocus}
          onBlur={onFocus}
          returnKeyType={'next'}
          onChangeText={props.onChangeText}
          value={props.value}
          type={props.type}
          options={{
            mask: '(999) 999 99 99',
            format: props.dateFormat ? props.dateFormat : 'DD/MM/YYYY',
            issuer: 'visa-or-mastercard',
          }}
        />
      ) : (
        <TextInput
          {...props}
          editable={!props.disabled}
          placeholderTextColor={TextColors.primaryTextV7}
          style={[
            props.disabled ? styles.disabledTextInput : styles.textInput,
            props.style,
          ]}
          autoCorrect={false}
          spellCheck={false}
          onFocus={onFocus}
          onBlur={onFocus}
          returnKeyType={'next'}
          onChangeText={props.onChangeText}
          value={props.value}
        />
      )}

      {props.iconSource && (
        <TouchableOpacity
          onPress={props.iconOnPress}
          style={[styles.iconButton, {bottom: props.error ? 30 : 8}]}>
          <Image source={props.iconSource} />
        </TouchableOpacity>
      )}

      <HorizontalSeperator
        style={{
          backgroundColor: focused
            ? Colors.primaryBrand
            : Colors.seperatorColor,
        }}
      />
      {props.error && <Text style={styles.errorText}>{props.error}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: hp(30),
    backgroundColor: Colors.white,
    marginHorizontal: wp(20),
  },
  textInput: {
    height: hp(50),
    fontSize: hp(16),
    backgroundColor: 'transparent',
    color: TextColors.primaryText,
  },
  disabledTextInput: {
    height: hp(50),
    fontSize: hp(16),
    backgroundColor: 'transparent',
    color: TextColors.primaryTextV7,
  },
  iconButton: {
    width: hp(24),
    height: hp(24),
    position: 'absolute',
    end: wp(12),
    bottom: hp(8),
    justifyContent: 'center',
    alignItems: 'center',
  },
  floatingLabel: {
    fontSize: hp(12),
    color: TextColors.primaryTextV7,
    fontWeight: '700',
  },
  activeFloatingLabel: {
    fontSize: hp(12),
    color: Colors.primaryBrand,
    fontWeight: '700',
  },
  errorText: {
    marginTop: hp(4),
    fontSize: hp(12),
    color: TextColors.errorText,
    fontWeight: '700',
  },
});
