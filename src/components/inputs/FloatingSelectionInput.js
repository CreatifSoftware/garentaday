import React, { useRef, useState } from 'react'
import { StyleSheet, TouchableOpacity, View, Image, TextInput, Text, Keyboard } from 'react-native';
import { Colors, TextColors } from '../../styles/Colors';
import { hp, wp } from '../../styles/Dimens';
import { HorizontalSeperator } from '../HorizontalSeperator';


export const FloatingSelectionInput = (props) => {
    const textInputRef = useRef(null);
    const [focused, setFocused] = useState(false)

    const onFocus = () => {
        Keyboard.dismiss
        textInputRef.current?.blur()
        props.onFocus()
    }

    return (
        <TouchableOpacity 
            onPress={props.onFocus}
            style={styles.container}>
            {
                props.value
                    ?
                    <Text style={focused ? styles.activeFloatingLabel : styles.floatingLabel}>{props.label}</Text>
                    :
                    null
            }
            <TextInput
                {...props}
                pointerEvents="none"
                ref={textInputRef}
                underlineColorAndroid="transparent"
                editable={false}
                placeholderTextColor={TextColors.primaryTextV7}
                style={[props.disabled ? styles.disabledTextInput : styles.textInput, props.style]}
                autoCorrect={false}
                spellCheck={false}
                onFocus={onFocus}
                returnKeyType={'next'}
                // onChangeText={props.onChangeText}
                value={props.value}
            />

            <Image style={[styles.icon,  props.disabled ? { tintColor: Colors.seperatorColor } : {bottom: props.stickIcon ? hp(40.5) : hp(20)}]} source={require('../../assets/icons/ic_small_arrow_down.png')} />

            <HorizontalSeperator style={{ backgroundColor: focused ? Colors.primaryBrand : Colors.seperatorColor }} />
            {
                props.error &&
                <Text style={styles.errorText}>{props.error}</Text>
            }
        </TouchableOpacity>

    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: hp(30),
        backgroundColor: Colors.white,
        marginHorizontal: wp(20),
    },
    inputContainer: {
        height: hp(50),
    },
    textInput: {
        height: hp(50),
        fontSize: hp(16),
        backgroundColor: 'transparent',
        color: TextColors.primaryText
    },
    icon: {
        tintColor: Colors.primaryBrand,
        position: 'absolute',
        end: wp(12),
        bottom: hp(20),
        justifyContent: 'center',
        alignItems: 'center'
    },

    disabledTextInput: {
        height: hp(50),
        fontSize: hp(16),
        backgroundColor: 'transparent',
        color: TextColors.primaryTextV7
    },
    floatingLabel: {
        fontSize: hp(12),
        color: TextColors.primaryTextV7,
        fontWeight: '700'
    },
    activeFloatingLabel: {
        fontSize: hp(12),
        color: Colors.primaryBrand,
        fontWeight: '700'
    },
    errorText: {
        marginTop: hp(4),
        fontSize: hp(12),
        color: TextColors.errorText,
        fontWeight: '700'
    }
})