import React from 'react'
import { Image, StyleSheet, View } from "react-native"
import LinearGradient from 'react-native-linear-gradient'
import { Colors } from "../../styles/Colors"
import { hp, wp } from "../../styles/Dimens"

const image = require('../../assets/icons/bg_home_navbar.png')

export const PaymentSelectionHeader = (props) => {
    const { reservation, navigation } = props
    const image = require('../../assets/icons/bg_home_navbar.png')
    const images = [
        require('../../assets/icons/bg_home_navbar.png')
    ];

    const uris = images.map(image => ({
        uri: Image.resolveAssetSource(image).uri
    }));

    return (
        <View style={styles.container}>
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                colors={[Colors.primaryBrand, Colors.primaryBrandV2]}
                style={[StyleSheet.absoluteFill,{borderBottomEndRadius: 40,
                    borderBottomStartRadius: 40}]}></LinearGradient>
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.primaryBrand,
        position: 'absolute',
        top: 0,
        // height: hp(192) + getStatusBarHeight(),
        height: hp(98),
        width: wp(375),
        alignItems: 'center',
        borderBottomEndRadius: 40,
        borderBottomStartRadius: 40
    },
}) 