import React from 'react'
import { Image, SafeAreaView, StyleSheet, View, Text } from "react-native"
import { Colors } from "../../styles/Colors"
import { getStatusBarHeight, hp, wp } from "../../styles/Dimens"
import { capitalizeFirstLetter } from '../../utilities/helpers'
import { BorderedButton } from '../buttons/BorderedButton'
import { FilledButton } from '../buttons/FilledButton'

const image = require('../../assets/icons/bg_home_navbar.png')

export const AccountHeader = (props) => {
    return (
        <View style={props.user ? styles.container2 : styles.container}>
            <Image style={{
                height: props.user ? styles.container2.height : styles.container.height,
                width: wp(375)
            }}
                // transition={false}
                resizeMethod='resize'
                resizeMode='cover'
                source={image} />
            <View style={props.user ? styles.children : styles.children2}>
                <Text style={styles.title}>{props.user ? (capitalizeFirstLetter(props.user.personalInfo.firstName) + ' ' + capitalizeFirstLetter(props.user.personalInfo.lastName)) : 'Misafir Kullanıcı'}</Text>
                <Text style={styles.subtitle}>{props.user ? props.user.contactInfo ? props.user.contactInfo.email : 'Garenta\'ya hoş geldiniz!' : 'Garenta\'ya hoş geldiniz!'}</Text>

                {
                    !props.user &&
                    <View style={styles.buttonGroup}>
                        <FilledButton
                            style={styles.loginButton}
                            textStyle={styles.loginTextStyle}
                            onPress={props.onLoginPress}
                            title={'Giriş Yap'} />

                        <BorderedButton
                            style={styles.registerButton}
                            textStyle={styles.registerTextStyle}
                            onPress={props.onRegisterPress}
                            title={'Üye Ol'} />
                    </View>
                }

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: hp(192) + getStatusBarHeight(),
        width: wp(375),
    },
    container2: {
        height: hp(145) + getStatusBarHeight(),
        width: wp(375),
    },
    children: {
        position: 'absolute',
        bottom: hp(0),
        marginStart:wp(32)
    },
    children2: {
        position: 'absolute',
        bottom: hp(20),
        marginStart:wp(32)
    },
    buttonGroup: {
        flexDirection: 'row',
    },
    button: {
        backgroundColor: '#ffc148',
        width: wp(335),
    },
    title: {
        color: Colors.white,
        fontSize: hp(24),
        fontFamily:'NunitoSans-Bold',
        marginBottom: hp(5)
    },
    subtitle: {
        color: Colors.white,
        fontSize: hp(16),
        fontFamily:'NunitoSans-SemiBold',
        marginBottom: hp(24)
    },
    image: {
        height: hp(34),
        width: wp(204),
    },
    loginButton: {
        backgroundColor: Colors.white,
        width:wp(121),
        height:hp(40),
        marginEnd:wp(10)
    },
    loginTextStyle:{
        fontSize: hp(14),
        color:Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',
    },
    registerButton: {
        borderColor:Colors.white,
        width:wp(121),
        height:hp(40),
    },
    registerTextStyle:{
        fontSize: hp(14),
        color:Colors.white,
        fontFamily: 'NunitoSans-Bold',
    },
}) 