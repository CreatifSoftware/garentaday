import React from 'react'
import { Image, SafeAreaView, StyleSheet, View, Text } from "react-native"
import { Colors } from "../../styles/Colors"
import { getStatusBarHeight, hp, wp } from "../../styles/Dimens"
import { capitalizeFirstLetter } from '../../utilities/helpers'
import { FilledButton } from '../buttons/FilledButton'

const image = require('../../assets/icons/bg_home_navbar.png')

export const MainHeader = (props) => {
    return (
        <View style={styles.container}>
            <Image style={{
                height: styles.container.height,
                width: styles.container.width
            }}
                transition={false}
                borderBottomEndRadius={hp(40)}
                borderBottomStartRadius={hp(40)}
                source={image} />
            <View style={styles.children}>
                <Text style={styles.title}>{props.user ? `Hoş geldin ${capitalizeFirstLetter(props.user.personalInfo.firstName)}` : 'Hoş geldiniz'}</Text>
                <FilledButton style={styles.button} onPress={props.onPress} title={'Araç Bul & Araç Kirala'} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: hp(192) + getStatusBarHeight(),
        width: wp(375),
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomEndRadius: 40,
        borderBottomStartRadius: 40
    },
    children: {
        position: 'absolute',
        bottom: hp(30),
        marginHorizontal: wp(20)
    },
    button: {
        backgroundColor: '#ffc148',
        width: wp(335),
    },
    title: {
        color: Colors.white,
        fontSize: hp(24),
        fontFamily: 'NunitoSans-Bold',
        marginBottom: hp(16)
    },
    image: {
        height: hp(34),
        width: wp(204),
    }
}) 