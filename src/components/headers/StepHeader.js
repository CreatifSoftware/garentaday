import React from 'react'
import { StyleSheet, Text, View } from "react-native"
import { Colors } from "../../styles/Colors"
import { hp, wp } from "../../styles/Dimens"
import LinearGradient from 'react-native-linear-gradient'

export const StepHeader = (props) => {
    const steps = []
    for (let index = 0; index <= props.stepCount; index++) {
        steps.push[index]
    }

    const renderStepView = () => {
        return (
            <View style={{ flex: 1 }}>
                <View style={{
                    alignSelf: 'center',
                    // width: wp(67, 5),
                    height: 8,
                    marginEnd: wp(5),
                    backgroundColor: props.step >= 2 ? 'white' : Colors.primaryBrandV3,
                    borderRadius: 3.5
                }} />
            </View>

        )
    }

    return (
        <View style={styles.container}>
            {/* <Image style={styles.image} source={require('../../assets/icons/garenta_logo_white.png')} /> */}
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                colors={[Colors.primaryBrand, Colors.primaryBrandV2]}
                style={StyleSheet.absoluteFill}>
                <Text style={styles.title}>{props.title}</Text>
                {/* <View style={styles.stepContainer}>
                    {

                        steps.map(item => {
                            return (
                                renderStepView(item)
                            )
                        })

                    }
                    <Text style={styles.stepCounter}>{props.step} / {props.stepCount}</Text>
                </View> */}

                <View style={styles.stepContainer}>
                    <View style={{
                        alignSelf: 'center',
                        flex: 1,
                        height: 8,
                        marginEnd: wp(5),
                        backgroundColor: props.step >= 1 ? 'white' : Colors.primaryBrandV3,
                        borderRadius: 3.5
                    }} />
                    <View style={{
                        alignSelf: 'center',
                        flex: 1,
                        height: 8,
                        marginEnd: wp(5),
                        backgroundColor: props.step >= 2 ? 'white' : Colors.primaryBrandV3,
                        borderRadius: 3.5
                    }} />
                    <View style={{
                        alignSelf: 'center',
                        flex: 1,
                        height: 8,
                        marginEnd: wp(5),
                        backgroundColor: props.step >= 3 ? 'white' : Colors.primaryBrandV3,
                        borderRadius: 3.5
                    }} />
                    {
                        props.stepCount > 3 &&
                        <View style={{
                            alignSelf: 'center',
                            flex: 1,
                            height: 8,
                            marginEnd: wp(5),
                            backgroundColor: props.step >= 4 ? 'white' : Colors.primaryBrandV3,
                            borderRadius: 3.5
                        }} />
                    }

                    <Text style={styles.stepCounter}>{props.step} / {props.stepCount}</Text>
                </View>
            </LinearGradient>

        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        height: hp(85),
        width: wp(375),
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primaryBrand,
    },
    title: {
        fontSize: hp(28),
        fontFamily: 'NunitoSans-Bold',
        marginStart: wp(20),
        color: Colors.white,
        marginBottom: hp(8)
    },
    image: {
        height: hp(34),
        width: wp(204),
    },
    stepContainer: {
        // flex: 1,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'space-between',
        marginStart: wp(20),
    },
    stepCounter: {
        alignSelf: 'center',
        marginEnd: wp(20),
        marginStart: wp(15),
        fontSize: hp(18),
        fontFamily: 'NunitoSans-Bold',
        color: Colors.white
    }
}) 