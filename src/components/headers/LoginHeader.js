import React from 'react'
import { Image, SafeAreaView, StatusBar, StyleSheet, View } from "react-native"
import { Colors } from "../../styles/Colors"
import { getStatusBarHeight, hp, wp } from "../../styles/Dimens"

export const LoginHeader = () => {
    return (
        <SafeAreaView style={styles.container}>
            <Image resizeMode={'contain'} style={styles.image} source={require('../../assets/icons/garenta_logo_white.png')}/>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        height:hp(146) + getStatusBarHeight(),
        width:wp(375),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:Colors.primaryBrand,
        borderBottomEndRadius:hp(40),
        borderBottomStartRadius:hp(40)
    },
    image:{
        height:hp(34),
        width:wp(204),
    }
}) 