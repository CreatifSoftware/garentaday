import React, { useEffect } from 'react'
import { Text, View } from 'react-native'
import { connect, useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import {initialDataAction,checkVersionAction} from '../redux/actions/AuthActions'

export const Test = () => {
    //const dispatch = useDispatch()
    //const data = useSelector(state => state.authReducer.data);
    const initialData = useSelector(state => state.authReducer.initialData)
    //const loading = useSelector(state => state.authReducer.loading);

    // useEffect(() => {
    //     dispatch(initialDataAction())
    // }, [dispatch])

    return (
        <Text style={{alignItems:'center',margin:50}}>
            {
                JSON.stringify(initialData)
            }
        </Text>
    )
}



