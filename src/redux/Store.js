
// import { createStore, applyMiddleware } from 'redux';
// import thunk from 'redux-thunk';
// import reducers from './reducers';
// import { createLogger } from 'redux-logger';

// const rootReducer = (state, action) => reducers(state, action);
// const logger = createLogger();

// export const store = createStore(rootReducer, applyMiddleware(thunk));


import { createStore, applyMiddleware } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import { allReducer } from "./reducers";
import AsyncStorage from '@react-native-async-storage/async-storage';
import thunk from 'redux-thunk';
import logger from 'redux-logger';


const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    //  whitelist: []
};

//AsyncStorage.removeItem('persist:root')
const persistedReducer = persistReducer(persistConfig, allReducer);
// const middleware = applyMiddleware(thunk, logger)
const middleware = applyMiddleware(thunk)
export const store = createStore(persistedReducer, middleware)
export const persistor = persistStore(store);