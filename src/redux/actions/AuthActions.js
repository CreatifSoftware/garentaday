import { callCheckVersionApi, callInitialDataApi, callSignInApi, callSignUpApi } from '../../api/auth/authServices';
import { userMapper } from '../../api/auth/mapper';

/**
 |--------------------------------------------------
 | Types
 |--------------------------------------------------
 */
export const API_PENDING = 'API_PENDING';
export const API_SUCCESS = 'API_SUCCESS';
export const API_ERROR = 'API_ERROR';
export const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';
export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';
export const SET_INITIAL_STATE = 'SET_INITIAL_STATE';
export const USER_ACTION = 'USER_ACTION';
export const TEMP_USER_ACTION = 'TEMP_USER_ACTION';
export const LOGIN_ACTION = 'LOGIN_ACTION';
export const LOGOUT_ACTION = 'LOGOUT_ACTION';
/**
|--------------------------------------------------
| Actions
|--------------------------------------------------
*/
export const setUserAction = (user) => dispatch => {
    dispatch({ type: USER_ACTION, payload: user});
};

export const setTempUserAction = (user) => dispatch => {
    dispatch({ type: TEMP_USER_ACTION, payload: user});
};

export const setLoginAction = (loginInfo) => dispatch => {
    dispatch({ type: LOGIN_ACTION, payload: loginInfo });
};

export const setLogoutAction = (logoutInfo) => dispatch => {
    dispatch({ type: LOGOUT_ACTION, payload: logoutInfo });
};

export const signInUserAction = ({ uname, password }) => (dispatch) => {
    dispatch({ type: SIGN_IN_REQUEST })
    callSignInApi(uname, password)
        .then((user) => {
            dispatch({ type: SIGN_IN_SUCCESS, payload: user });
        })
        .catch((error) => { 
            dispatch({ type: SIGN_IN_FAILURE, payload: authFailMessage(error.code) });
        })
};

export const signUpUser = (user) => (dispatch) => {
    dispatch({ type: SIGN_UP_REQUEST });

    callSignUpApi(user)
        .then((response) => {
            dispatch({ type: SIGN_UP_SUCCESS, payload: response });
        })
        .catch((error) => { 
            dispatch({ type: SIGN_UP_FAILURE, payload: authFailMessage(error.code) }); 
        });
};

export const signOutUser = () => (dispatch) => {
    dispatch({ type: SET_INITIAL_STATE });
};

export const authFailMessage = (errorCode) => {
    switch (errorCode) {
        case 'auth/invalid-email':
            return 'Email is invalid.';
        case 'auth/user-disabled':
            return 'User is disabled.';
        case 'auth/user-not-found':
            return 'User not found.';
        case 'auth/wrong-password':
            return 'Password is invalid.';
        case 'auth/email-already-in-use':
            return 'Email address is already in use.';
        case 'auth/weak-password':
            return 'Password is not strong enough.';
        default:
            return 'Authentication failed.';
    }
};
