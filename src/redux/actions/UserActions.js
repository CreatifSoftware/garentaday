import { callCheckVersionApi, callInitialDataApi, callSignInApi, callSignUpApi } from '../../api/auth/authServices';
import { userMapper } from '../../api/auth/mapper';
import { callGetFavoriteBranchApi } from '../../api/user/userService';

/**
 |--------------------------------------------------
 | Types
 |--------------------------------------------------
 */
export const GET_FAV_BRANCHS_REQUEST = 'GET_FAV_BRANCHS_REQUEST';
export const GET_FAV_BRANCHS_SUCCESS = 'GET_FAV_BRANCHS_SUCCESS';
export const GET_FAV_BRANCHS_ERROR = 'GET_FAV_BRANCHS_ERROR';
export const SET_INITIAL_STATE = 'SET_INITIAL_STATE';
export const USER_ACTION = 'USER_ACTION';

/**
|--------------------------------------------------
| Actions
|--------------------------------------------------
*/
export const setUserAction = (user) => dispatch => {
    dispatch({ type: USER_ACTION, payload: user});
};

export const setFavoriteBranchsAction = (response) => dispatch => {
    dispatch({ type: GET_FAV_BRANCHS_SUCCESS, payload: response});
};

export const getFavoriteBranchsAction = (userId) => (dispatch) => {
    dispatch({ type: GET_FAV_BRANCHS_REQUEST })
    callGetFavoriteBranchApi(userId)
        .then((response) => {
            dispatch({ type: GET_FAV_BRANCHS_SUCCESS, payload: response.data.EXPORT});
        })
        .catch((error) => { 
            dispatch({ type: GET_FAV_BRANCHS_ERROR, payload: null });
        })
};