import { callInitialDataApi } from '../../api/auth/authServices';

/**
 |--------------------------------------------------
 | Types
 |--------------------------------------------------
 */
export const LOADER_STATE = 'LOADER_STATE';
export const INITIAL_DATA_SUCCESS = 'INITIAL_DATA_SUCCESS';
export const INITIAL_DATA_FAILURE = 'INITIAL_DATA_FAILURE';
export const CHECK_VERSION_SUCCESS = 'CHECK_VERSION_SUCCESS';
export const CHECK_VERSION_FAILURE = 'CHECK_VERSION_FAILURE';
export const SET_LANGUGAGE = 'SET_LANGUGAGE';
/**
|--------------------------------------------------
| Actions
|--------------------------------------------------
*/

export const setLoadingAction = (loading) => dispatch => {
    dispatch({ type: LOADER_STATE, payload: loading })
};

export const setLanguageAction = (langId) => dispatch => {
    dispatch({ type: SET_LANGUGAGE,payload: langId })
};

export const setInitialDataAction = (response) => dispatch => {
    dispatch({ type: INITIAL_DATA_SUCCESS, payload: response })
};

export const checkVersionAction = () => dispatch => {
    callCheckVersionApi()
        .then((response) => {
            dispatch({ type: CHECK_VERSION_SUCCESS, payload: response });
            return Promise.resolve();
        })
        .catch((error) => { 
            dispatch({ type: CHECK_VERSION_FAILURE, payload: error.message });
            return Promise.reject();
        })
};

export const initialDataAction = () => dispatch => {
    callInitialDataApi()
        .then((response) => {
            dispatch({ type: INITIAL_DATA_SUCCESS, payload: response });
            return Promise.resolve();
        })
        .catch((error) => { 
            dispatch({ type: INITIAL_DATA_FAILURE, payload: error.message });
            return Promise.reject();
        })
};
