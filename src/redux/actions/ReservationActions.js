import { callCheckVersionApi, callInitialDataApi, callSignInApi, callSignUpApi } from '../../api/auth/authServices';
import { userMapper } from '../../api/auth/mapper';
import { callGetReservationsListApi } from '../../api/reservation/reservationService';
import { INITIAL_STATE } from '../reducers/ReservationReducer';

/**
 |--------------------------------------------------
 | Types
 |--------------------------------------------------
 */
export const API_PENDING = 'API_PENDING';
export const API_SUCCESS = 'API_SUCCESS';
export const API_ERROR = 'API_ERROR';
export const GET_RESERVATION_SUCCESS = 'GET_RESERVATION_SUCCESS';
export const SET_RESERVATION_DATA = 'SET_RESERVATION_DATA';
export const SET_CAMPAIGN_LIST = 'SET_CAMPAIGN_LIST';
export const SET_RESERVATION_LIST = 'SET_RESERVATION_LIST';
export const CLEAR_RESERVATION_DATA = 'CLEAR_RESERVATION_DATA';
/**
|--------------------------------------------------
| Actions
|--------------------------------------------------
*/

export const setCampaignListAction = (campaigns) => dispatch => {
    dispatch({ type: SET_CAMPAIGN_LIST, payload: campaigns });
};

export const setReservationDataAction = (reservation) => dispatch => {
    dispatch({ type: SET_RESERVATION_DATA, payload: reservation });
};

export const clearReservationDataAction = () => dispatch => {
    dispatch({ type: CLEAR_RESERVATION_DATA, payload: { ...INITIAL_STATE } });
};

export const getReservationAction = (reservationList) => (dispatch) => {
    dispatch({ type: SET_RESERVATION_LIST, payload: reservationList });
};