export const TEST_ACTION_KEY = "TEST_ACTION_KEY"

export const setTestAction = (response) => dispatch => {
    dispatch({ type: TEST_ACTION_KEY, payload: response});
};