import { INITIAL_DATA_SUCCESS, API_PENDING, API_SUCCESS, API_ERROR, MODAL_STATE, USER_ACTION } from "../actions/AuthActions";

/**
 |--------------------------------------------------
 | Reducer
 |--------------------------------------------------
 */
const INITIAL_STATE = {
    showLoading:false,
    user:null,
    masterData:null
};

const testReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case API_PENDING:
      return { ...state, loading: true };
    case TEST_ACTION_KEY:
      return { ...state, user: action.payload }
    default:
      return state;
  }
};

export default testReducer



