import { BranchResponse } from "../../api/models/BranchResponse";
import { InitResponse } from "../../api/models/InitResponse";
import { INITIAL_DATA_SUCCESS, INITIAL_DATA_FAILURE, INITIAL_DATA_REQUEST, LOADER_STATE, SET_LANGUGAGE } from "../actions/MasterActions";

/**
 |--------------------------------------------------
 | Reducer
 |--------------------------------------------------
 */
const INITIAL_STATE = {
  error: null,
  loading: false,
  countries: [],
  cities: [],
  districts: [],
  branchs: [],
  favoriteBranchs:[],
  holidays: [],
  workingHours: [],
  mobileCodes: [],
  timeLimit: {},
  language:'TR'
};

const masterReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOADER_STATE:
      return { ...state, loading: action.payload };
    case SET_LANGUGAGE:
        return { ...state, language: action.payload };
    case INITIAL_DATA_REQUEST:
      return { ...state, ...INITIAL_STATE };
    case INITIAL_DATA_FAILURE:
      return { ...state, ...INITIAL_STATE, error: action.payload };
    case INITIAL_DATA_SUCCESS:
      return {
        ...state,
        timeLimit: action.payload.EXPORT.ES_TIME_LIMIT,
        ... InitResponse(action.payload.EXPORT),
        branchs: BranchResponse(action.payload.EXPORT.ET_SUBE_INFO),
        holidays: action.payload.EXPORT.ET_TATIL_ZAMANI,
        workingHours: action.payload.EXPORT.ET_CALISMA_SAAT,
        mobileCodes: action.payload.EXPORT.ET_TELNO,
        fuelTypes: action.payload.EXPORT.ET_YAKIT_TIP,
        segments: action.payload.EXPORT.ET_SEGMENT,
        transmissionTypes: action.payload.EXPORT.ET_SANZIMAN,
      }
    default:
      return state;
  }
};

export default masterReducer