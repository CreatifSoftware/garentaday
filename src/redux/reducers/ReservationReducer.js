import { INITIAL_DATA_SUCCESS, INITIAL_DATA_FAILURE, INITIAL_DATA_REQUEST, LOADER_STATE } from "../actions/MasterActions";
import { CLEAR_RESERVATION_DATA, SET_CAMPAIGN_LIST, SET_RESERVATION_DATA, SET_RESERVATION_LIST } from "../actions/ReservationActions";

/**
 |--------------------------------------------------
 | Reducer
 |--------------------------------------------------
 */
export const INITIAL_STATE = {
  reservation: {
    pickupBranch: null,
    dropoffBranch: null,
    pickupDateTime: null,
    dropoffDateTime: null,
    selectedAdditionalServices: [],
    selectedAdditionalProducts: [],
    campaignId: null,
    transactionId: null,
    individualAddressId: null,
    invoiceAddressId: null
  },
  reservationList: {
    activeReservations: [],
    cancelledReservations: [],
    completedReservations: []
  },
  campaignList: []
};

const reservationReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOADER_STATE:
      return { ...state, loading: action.payload };
    case CLEAR_RESERVATION_DATA:
      return { ...state, reservation: action.payload };
    case SET_RESERVATION_DATA:
      return { ...state, reservation: action.payload };
    case SET_RESERVATION_LIST:
      return { ...state, reservationList: action.payload };
    case SET_CAMPAIGN_LIST:
      return { ...state, campaignList: action.payload };
    default:
      return state;
  }
};

export default reservationReducer