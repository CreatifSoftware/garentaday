import { INITIAL_DATA_SUCCESS, INITIAL_DATA_FAILURE, INITIAL_DATA_REQUEST, LOADER_STATE } from "../actions/MasterActions";
import { CLEAR_RESERVATION_DATA, SET_RESERVATION_DATA } from "../actions/ReservationActions";
import { GET_FAV_BRANCHS_ERROR, GET_FAV_BRANCHS_SUCCESS } from "../actions/UserActions";

/**
 |--------------------------------------------------
 | Reducer
 |--------------------------------------------------
 */
const INITIAL_STATE = {
  favoriteBranchs:[]
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOADER_STATE:
      return { ...state, loading: action.payload };
    case GET_FAV_BRANCHS_SUCCESS:
      return { ...state, favoriteBranchs:action.payload.ET_FAV_BRANCH_LIST};
    case GET_FAV_BRANCHS_ERROR:
      return { ...state, favoriteBranchs:[] };
    default:
      return state;
  }
};

export default userReducer