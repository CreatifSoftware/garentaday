import { View } from 'react-native';
import { combineReducers } from 'redux'
import authReducer from './AuthReducer'
import masterReducer from './MasterReducer'
import reservationReducer from './ReservationReducer'
import userReducer from './UserReducer'

export const allReducer = combineReducers({
    authReducer,
    masterReducer,
    reservationReducer,
    userReducer
});