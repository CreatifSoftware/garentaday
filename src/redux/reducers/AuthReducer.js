import { INITIAL_DATA_SUCCESS, API_PENDING, API_SUCCESS, API_ERROR, MODAL_STATE, USER_ACTION,TEMP_USER_ACTION } from "../actions/AuthActions";

/**
 |--------------------------------------------------
 | Reducer
 |--------------------------------------------------
 */
const initialUser = {
    personalInfo: {},
    licenseInfo: {},
    contactInfo:{},
    permissionInfo:{},
    addressList: [],
    creditCards: []
}

const INITIAL_STATE = {
  error: null,
  loading: false,
  isLoggedIn:false,
  user:initialUser,
  tempUser: initialUser,
  username: null,
  password: null,
  modalType: null
};

const authReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case API_PENDING:
      return { ...state, loading: true };
    case API_ERROR:
      return { ...state, ...INITIAL_STATE, error: action.payload };
    case API_SUCCESS:
      return { ...state, data: action.payload, loading: false };
    case MODAL_STATE:
      return { ...state, modalType: action.payload }
    case USER_ACTION:
      return { ...state, user: action.payload, loading: false }
    case TEMP_USER_ACTION:
        return { ...state, tempUser: action.payload, loading: false }
    default:
      return state;
  }
};

export default authReducer