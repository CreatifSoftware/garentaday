import { useSelector } from 'react-redux';
import { KEY_MASTER_CARD, PAYMENT_TYPES } from './constants';
import moment from 'moment'
import AsyncStorage from '@react-native-async-storage/async-storage';
import localization from 'moment/locale/tr'
import { locale } from 'moment';
moment.updateLocale('tr', localization)
export const generateCode = () => {
  return Math.floor(100000 + Math.random() * 900000)
}

export const encodeUTF16LE = (str) => {
  var out, i, len, c;
  var char2, char3;
  var binaryToBase64 = require('./binaryToBase64');

  out = "";
  len = str.length;
  i = 0;
  while (i < len) {
    c = str.charCodeAt(i++);
    switch (c >> 4) {
      case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
        // 0xxxxxxx
        out += str.charAt(i - 1);
        break;
      case 12: case 13:
        // 110x xxxx   10xx xxxx
        char2 = str.charCodeAt(i++);
        out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
        out += str.charAt(i - 1);
        break;
      case 14:
        // 1110 xxxx  10xx xxxx  10xx xxxx
        char2 = str.charCodeAt(i++);
        char3 = str.charCodeAt(i++);
        out += String.fromCharCode(((c & 0x0F) << 12) | ((char2 & 0x3F) << 6) | ((char3 & 0x3F) << 0));
        break;
    }
  }

  var byteArray = new Uint16Array(out.length * 2);
  for (var i = 0; i < out.length; i++) {
    byteArray[i * 2] = out.charCodeAt(i); // & 0xff;
    byteArray[i * 2 + 1] = out.charCodeAt(i) >> 8; // & 0xff;
  }


  return binaryToBase64(byteArray)
  // return String.fromCharCode.apply( String, byteArray );
}

export const getBranchesCityList = (branchs) => {
  let uniqueList = branchs.filter((v, i, a) => a.findIndex(t => (t.city === v.city)) === i)
  return uniqueList.sort((a, b) => a.cityName.localeCompare(b.cityName))
}

export const getFavoriteBranchesCityList = (branchs, favoriteBranchs) => {
  let uniqueList = branchs.filter((v, i, a) => a.findIndex(t => (t.city === v.city)) === i)
  return uniqueList.sort((a, b) => a.cityName.localeCompare(b.cityName))
}

export const getMainPageCampaign = (campaigns) => {
  return campaigns.filter(function (arg) {
    return arg.isCollaboration == "";
  })
}

export const getCollaborationCampaigns = (campaigns) => {
  return campaigns.filter(function (arg) {
    return arg.isCollaboration == "X";
  })
}

export const getCreditCardType = (cardNumber) => {
  let visa = new RegExp('^4[0-9]{12}(?:[0-9]{3})?$');
  let mastercard = new RegExp('^5[1-5][0-9]{14}$');
  let mastercard2 = new RegExp('^2[2-7][0-9]{14}$');

  if (visa.test(cardNumber)) {
    return KEY_VISA;
  }
  if (mastercard.test(cardNumber) || mastercard2.test(cardNumber)) {
    return KEY_MASTER_CARD;
  }
}

export const convertTime = (time) => {
  return time.substring(0, 2) + ":" + time.substring(2, 4);
}

export const convertDate = (date) => {
  return date.substring(6, 8) + "/" + date.substring(4, 6) + "/" + date.substring(0, 4);
}

export const isFavoriteBranch = (favoriteBranchs, branchId) => {
  if (favoriteBranchs) {
    const isFavorite = favoriteBranchs.filter(function (arg) {
      return arg.BRANCH == branchId;
    }).length > 0

    return isFavorite
  }

  return false
}

export const getCountryItem = (countries, countryCode) => {
  // const countries = useSelector(state => state.masterReducer.countries)
  let filtered = countries.filter(item => item.country == countryCode)
  if (filtered.length > 0) {
    return filtered[0]
  }

  return null
}

export const getCityItem = (cities, countryCode, cityCode) => {
  // const cities = useSelector(state => state.masterReducer.cities)
  let filtered = cities.filter(item => item.country == countryCode && item.city == cityCode)
  if (filtered.length > 0) {
    return filtered[0]
  }

  return null
}

export const getDistrictItem = (districts, districtCode) => {
  // const districts = useSelector(state => state.masterReducer.districts)
  let filtered = districts.filter(item => item.district == districtCode)
  if (filtered.length > 0) {
    return filtered[0]
  }

  return null
}

export const filterCitiesByCountryCode = (countryCode) => {
  const cities = useSelector(state => state.masterReducer.cities)
  return cities.filter(item => item.country == countryCode)
}

export const filterDistrictsByCityCode = (cityCode, countryCode, districts) => {
  return districts.filter(item => item.country == countryCode && item.city == cityCode)
}

export const getFormatedDate = (date, formatStr) => {
  return moment(date).format(formatStr)
}

export const getDateErrorText = (date) => {
  if (date.length == 10) {
    let day = date.split('/')[0]
    let month = date.split('/')[1]
    let year = date.split('/')[2]

    if (parseInt(day) > 31 || parseInt(month) > 12 || parseInt(year) > new Date().getFullYear() | parseInt(year) < 1901) {
      return 'Geçerli bir tarih girin'
    }

    return null
  }
}

export const saveJsonData = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value)
    await AsyncStorage.setItem(key, jsonValue)
  } catch (e) {
    // saving error
  }
}

export const getJsonData = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key)
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    // error reading value
  }
}

export const generateArrayOfTimes = (start, end, interval = 15) => {
  const hours = []
  for (let hour = 0; hour < 24; hour++) {
    hours.push(moment({
      hour
    }).format('HH:mm'))

    hours.push(moment({
      hour,
      minute: interval
    }).format('HH:mm'))
  }

  return hours
}

export const getCarGroupPrice = (reservation) => {
  return (reservation.paymentType === PAYMENT_TYPES.PAY_NOW ?
    Number(reservation.selectedGroup.amountItem.payNowAmount) :
    Number(reservation.selectedGroup.amountItem.payLaterAmount))
}

export const calculateReservationProductsPrice = (products) => {
  let productSum = 0
  for (let index = 0; index < products.length; index++) {
    const element = products[index];
    debugger
    if (element.value > 0) {
      productSum = productSum + (Number(element.value) * Number(element.totalAmount) - element.paidAmount)
    }
  }

  debugger
  return productSum
}

export const calculateReservationProductsPriceUpdate = (products) => {
  let productSum = 0
  for (let index = 0; index < products.length; index++) {
    const element = products[index];
    if (element.value > 0) {
      productSum = productSum + (element.value * Number(element.totalAmount) - element.paidAmount)
      // productSum = productSum + element.paidAmount
    }
  }

  return productSum
}


export const isUpdate = (reservation) => {
  return reservation.reservationId !== undefined
}

export const calculateCarGroupDifferentAmount = (reservation, newPrice) => {
  if (reservation.isPaid) {
    let reservationCarPrice = getOldGroupAmountByPaymentType(reservation)
    let difference = Number(newPrice) - Number(reservationCarPrice)

    return difference.toFixed(2)
  }

  return 0.00
}

export const getGroupAmountByPaymentType = (reservation) => {
  return reservation.paymentType === PAYMENT_TYPES.PAY_NOW ? reservation.selectedGroup.amountItem.payNowAmount : reservation.selectedGroup.amountItem.payLaterAmount
}

export const getOldGroupAmountByPaymentType = (reservation) => {
  return reservation.paymentType === PAYMENT_TYPES.PAY_NOW ?
    reservation.oldSelectedGroup.amountItem.payNowAmount :
    reservation.oldSelectedGroup.amountItem.payLaterAmount
}

export const calculateTotalAmount = (reservation, products) => {
  let productSum = calculateReservationProductsPrice(products)
  let sum = productSum + getCarGroupPrice(reservation)

  if (isUpdate(reservation)) {
    let oldProductsSum = calculateReservationProductsPriceUpdate(reservation.oldSelectedProducts)
    let oldSum = Number(oldProductsSum) + Number(getOldGroupAmountByPaymentType(reservation))

    return Number(sum) -  oldSum
  } else {
    return sum
  }
}

export const hasGarentaXpressAdded = (products) => {
  return products.filter(function (arg) {
    return arg.productId == "HZM0034";
  }).length > 0
}

export function capitalizeFirstLetter(string){
  return string.charAt(0).toLocaleUpperCase('tr-TR') + string.slice(1).toLocaleLowerCase('tr-TR');
}
