export const KEY_TURKISH_LANG = 'Türkçe'
export const KEY_ENGLISH_LANG = 'İngilizce'
export const KEY_VISA = 'VISA'
export const KEY_MASTER_CARD = 'MASTER_CARD'
export const PERSONAL_ADDRESS = 'B'
export const INVOICE_ADDRESS = 'F'

export const PERSONAL_ADDRESS_LIST = 'Kayıtlı Adreslerim'
export const INVOICEL_ADDRESS_LIST = 'Fatura Adreslerim'

export const ACTIVE_RESERVATIONS = 'Aktif'
export const CANCELLED_RESERVATIONS = 'İptal'
export const COMPLETED_RESERVATIONS = 'Biten'

export const ACTIVE_COMPLAINTS = "Açık"
export const COMPLETED_COMPLAINTS = "Kapalı"

export const SORT_TYPES = {
    SORT_LOW_TO_HIGH_PRICE: "Artan Fiyata Göre",
	SORT_HIGHT_TO_LOW_PRICE: "Azalan Fiyata Göre",
	SORT_BY_CAMPAIGN: "Kampanya Öncelikli",
}

export const PAYMENT_TYPES = {
    PAY_NOW: "pay_now",
    PAY_LATER: "pay_later"
}

export const COMPLAINT_RESPONSE_CHANNEL = {
    PHONE: "Telefon ile",
    MAIL: "E-Posta ile",
    SMS: "SMS ile"
}

export const COMPLAINT_SUBJECT = {
    SIKAYET: "Şikayet",
    TESEKKUR: "Teşekkür",
    ONERI: "Öneri-Eleştiri",
    TALEP: "Talep"
}
