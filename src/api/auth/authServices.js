import axios from "axios"
import { API_HEADERS, API_URL } from "../constants"
import { checkGeneralOtpCodeRequest, checkLoginOtpRequest, checkMernisRequest, checkVersionRequest, createPermissionRequest, forgotPasswordRequest, forgotPasswordSendSMSRequest, initialDataRequest, permissionSMSRequest, signInRequest, signUpRequest, validateSMSRequest } from "./authRequest"

export const callCheckVersionApi = () => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            checkVersionRequest(),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callCheckMernisApi = (user) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            checkMernisRequest(user),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callInitialDataApi = () => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            initialDataRequest(),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response.data)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callCheckLoginOtpApi = (username,password) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            checkLoginOtpRequest(username,password),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callCheckGeneralOtpApi = (username,password,code) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            checkGeneralOtpCodeRequest(username,password,code),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callSignInApi = (username,password, confirmationCode) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            signInRequest(username,password,confirmationCode),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callSignUpApi = (user) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            signUpRequest(user),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callForgotPasswordSendSMSApi = (mobileCode,mobilePhone) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            forgotPasswordSendSMSRequest(mobileCode,mobilePhone),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)            
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callForgotPasswordApi = (mobileCode,mobilePhone,email,password) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            forgotPasswordRequest(mobileCode,mobilePhone,email,password),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)            
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callSendPermissionSMS = (user) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            permissionSMSRequest(user),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)            
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callValidateSMS = (user, type, code) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            validateSMSRequest(user, type, code),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)            
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callCreateUserPermission = (user, origin) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            createPermissionRequest(user,origin),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)            
        })
        .catch(error => {
            reject(error)
        })
    })
}