import { Platform } from "react-native"
import { LANG_ID } from "../constants"
import { checkVersionEndpoint, initialDataEndpoint, signInEndpoint, signUpEndpoint, forgotPasswordSmsEndpoint, forgotPasswordEndpoint, checkMernisEndpoint, CHECK_LOGIN_OTP_ENDPOINT, checkGeneralOtpCodeEndpoint } from "../endpoints"
import { prepareSapProps } from "../apiHelpers"
import { encodeUTF16LE } from "../../utilities/helpers"
import { getVersion } from "react-native-device-info"
var binaryToBase64 = require('../../utilities/binaryToBase64');

export const checkVersionRequest = () => {
    let appVersion = getVersion()
    let version = appVersion.replace('.','').replace('.','')
    const jsonBody = {
        ...prepareSapProps(checkVersionEndpoint),
        import: {
            IV_APP_NAME: Platform.OS === "ios" ? "REZ_IOS" : "REZ_ADR",
            IV_LANGU: LANG_ID,
            IV_APPVERSION: version
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const checkMernisRequest = (user) => {
    const jsonBody = {
        ...prepareSapProps(checkMernisEndpoint),
        import: {
            IS_INPUT: {
                TCKIMLIK_NO: user.personalInfo.govermentId,
                AD: user.personalInfo.firstName,
                SOYAD: user.personalInfo.lastName,
                DOGUM_YILI: user.personalInfo.dateOfBirth
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const initialDataRequest = () => {
    const jsonBody = {
        ...prepareSapProps(initialDataEndpoint),
        import: {
            IS_TOKEN: {
                DEVICE_TOKEN: "",
                IS_IOS: Platform.OS === 'ios' ? "X" : "",
                IS_ANDROID: Platform.OS === 'ios' ? "" : "X",
            },
            IV_KANAL: "40",
            IV_LANGU: LANG_ID,
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const checkLoginOtpRequest = (uname, password) => {

    const jsonBody = {
        ...prepareSapProps(CHECK_LOGIN_OTP_ENDPOINT),
        import: {
            IS_SEARCH: {
                FREE_TEXT: uname,
                PASSWORD: encodeUTF16LE(password)
            },
            IV_LANGU: LANG_ID,
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const signInRequest = (uname, password, confirmationCode) => {

    const jsonBody = {
        ...prepareSapProps(signInEndpoint),
        import: {
            IS_SEARCH: {
                FREE_TEXT: uname,
                PASSWORD: encodeUTF16LE(password),
                CODE: confirmationCode
            },
            IS_TOKEN: {
                DEVICE_TOKEN: "",
                IS_IOS: Platform.OS === 'ios' ? "X" : "",
                IS_ANDROID: Platform.OS === 'ios' ? "" : "X",
            },
            IV_LANGU: LANG_ID,
        }
    }
    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const signUpRequest = (user) => {
    const jsonBody = {
        ...prepareSapProps(signUpEndpoint),
        import: {
            IS_HEADER: {
                BP_NUMBER: "",
                FIRSTNAME: user.personalInfo.firstName,
                LASTNAME: user.personalInfo.lastName,
                NATIONALITY: user.personalInfo.isTurkishCitizen ? 'TR' : user.personalInfo.nationality.country,
                GENDER: user.personalInfo.gender,
                BIRTHPLACE: "",
                BIRTHDATE: user.personalInfo.dateOfBirth,
                TCKN: user.personalInfo.isTurkishCitizen ? user.personalInfo.govermentId : "",
                PASAPORTNO: user.personalInfo.isTurkishCitizen ? "" : user.personalInfo.passport,
                EMAILDOGRFLAG: "",
                SMSDOGRFLAG: "X",
                EHLIYETNO: user.licenseInfo.licenseNumber,
                EHLYTALISTAR: user.licenseInfo.licenseDate,
                EHLALISYERI: user.licenseInfo.licensePlace,
                EHLYTSINIFI: user.licenseInfo.licenseClass,
                ODEMEBICIMI: "",
                PASAPORTVERTAR: "",
                SOZLESME_ONAY: "",
                TEL_CODE: user.contactInfo.mobileCode,
                MOB_TEL: user.contactInfo.mobilePhone.replace('(', '').replace(')', '').replace(' ', '').replace(' ', '').replace(' ', ''),
                E_MAIL: user.contactInfo.email,
                ALLOW_SMS: "",
                ALLOW_EMAIL: "",
                ALLOW_PHONE: "",
                ALLOW_LETTER: "",
                COMPANY_NO: "",
                COMPANY_EMAIL: "",
                NICKNAME: "",
                PASSWORD: encodeUTF16LE(user.password),
                ALLOW_MARKETING: "",
                ALLOW_PRIVATE: "",
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const forgotPasswordSendSMSRequest = (mobileCode, mobilePhone) => {
    let trimmedMobile = mobilePhone.replace('(', '').replace(')', '').replace(' ', '').replace(' ', '').replace(' ', '')
    const jsonBody = {
        ...prepareSapProps(forgotPasswordSmsEndpoint),
        import: {
            IV_TEL_CODE: mobileCode,
            IV_TEL_NUMBER: trimmedMobile
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const checkGeneralOtpCodeRequest = (mobileCode, mobilePhone, code) => {
    let trimmedMobile = mobilePhone.replace('(', '').replace(')', '').replace(' ', '').replace(' ', '').replace(' ', '')
    const jsonBody = {
        ...prepareSapProps(checkGeneralOtpCodeEndpoint),
        import: {
            IV_ACT_CODE: code,
            IV_TEL_CODE: mobileCode,
            IV_TEL_NUMBER: trimmedMobile
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const permissionSMSRequest = (user) => {
    let trimmedMobile = user.contactInfo.mobilePhone.replace('(', '').replace(')', '').replace(' ', '').replace(' ', '').replace(' ', '')
    const jsonBody = {
        sap_props: {
            rfcName: "Z_CRM_KDK_MOBD_PERMISSION_SMS",
            language: LANG_ID,
            PERM_SOURCE: 40,
            is_mobile: 'X'
        },
        import: {
            IS_INPUT: {
                FIRST_NAME: user.personalInfo.firstName,
                LAST_NAME: user.personalInfo.lastName,
                EMAIL: user.contactInfo.email,
                MSISDN: `+${user.contactInfo.mobileCode}${trimmedMobile}`,
                PERM_STAT: user.permissionState,
                BIRTHDATE: user.personalInfo.dateOfBirth,
                TCKN: user.personalInfo.isTurkishCitizen ? user.personalInfo.govermentId : "",
                PASAPORTNO: user.personalInfo.isTurkishCitizen ? "" : user.personalInfo.passport,
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const validateSMSRequest = (user, type, code) => {
    let trimmedMobile = user.contactInfo.mobilePhone.replace('(', '').replace(')', '').replace(' ', '').replace(' ', '').replace(' ', '')
    const jsonBody = {
        sap_props: {
            rfcName: "Z_CRM_KDK_MOBD_VALIDATE_CODE",
            language: LANG_ID,
            PERM_SOURCE: 40,
            is_mobile: 'X'
        },
        import: {
            IS_INPUT: {
                TYPE: type,
                MSISDN: `+${user.contactInfo.mobileCode}${trimmedMobile}`,
                CODE: code
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const createPermissionRequest = (user, origin) => {
    let trimmedMobile = user.contactInfo.mobilePhone.replace('(', '').replace(')', '').replace(' ', '').replace(' ', '').replace(' ', '')
    const jsonBody = {
        sap_props: {
            rfcName: "Z_CRM_KDK_MOBD_CREATE_PERM",
            language: LANG_ID,
            is_mobile: 'X'
        },
        import: {
            IS_INPUT: {
                PARTNER: user.userId,
                MSISDN: `+${user.contactInfo.mobileCode}${trimmedMobile}`,
                ORIGIN: origin
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const forgotPasswordRequest = (mobileCode, mobilePhone, email, password) => {
    const jsonBody = {
        ...prepareSapProps(forgotPasswordEndpoint),
        import: {
            IS_INPUT: {
                PASSWORD: encodeUTF16LE(password),
                TEL_CODE: mobileCode ? mobileCode : "",
                MOB_TEL: mobilePhone ? mobilePhone.replace('(', '').replace(')', '').replace(' ', '').replace(' ', '').replace(' ', '') : "",
                EMAIL: email ? email : ""
            }

        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}
