export const userMapper = (response) => {
    if (response === null) {
        return null
    }
    
    let user = {
        userId:response.MUSTERI_NO,
        notifCount: response.NOTIFICATION_COUNT && response.NOTIFICATION_COUNT,
        allowMarketing: response.ALLOW_MARKETING && response.ALLOW_MARKETING === 'X',
        confirmContract: response.SOZLESME_ONAY && response.SOZLESME_ONAY === 'X',
        personalInfo: {
            firstName: response.MUSTERI_ADI && response.MUSTERI_ADI,
            lastName: response.SOZLESME_ONAY && response.SOZLESME_ONAY,
            fullName: response.MUSTERI_ADI && response.MUSTERI_ADI,
            dateOfBirth: response.DOGUM_TARIHI && response.DOGUM_TARIHI,
            gender: response.SOZLESME_ONAY && response.SOZLESME_ONAY,
            nationality: response.SOZLESME_ONAY && response.SOZLESME_ONAY,
            isTurkishCitizen: response.SOZLESME_ONAY && response.SOZLESME_ONAY,
            govermentId: response.SOZLESME_ONAY && response.SOZLESME_ONAY,
            passport: response.SOZLESME_ONAY && response.SOZLESME_ONAY,
        },
        licenseInfo: {
            licenseNumber:response.SOZLESME_ONAY && response.SOZLESME_ONAY,
            licenseClass:response.SOZLESME_ONAY && response.SOZLESME_ONAY,
            licensePlace:response.SOZLESME_ONAY && response.SOZLESME_ONAY,
            licenseDate:response.SOZLESME_ONAY && response.SOZLESME_ONAY,
        },
        addressDetail: [],
        creditCards: []
    }
    return user
}
