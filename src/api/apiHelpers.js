import { LANG_ID } from "./constants"

export const isSuccess = (response) => {
    debugger
    if (response.data && response.data.EXPORT && response.data.EXPORT.EV_SUCCESS === "X") {
        return true
    } else if (response.data.EXPORT && response.data.EXPORT.EV_PROCESS === "X") {
        return true
    } else if (response.data.EXPORT.ES_RETURN && response.data.EXPORT.ES_RETURN.SUCCESS === "X") {
        return true
    } else {
        return false
    }

    // return response.data.EXPORT.EV_SUCCESS === "X" || response.data.EXPORT.ES_RETURN.SUCCESS === "X"
}

export const getErrorMessage = (response) => {
    let message = null
    if (response.data.EXPORT.ET_MESSAGE) {
        response.data.EXPORT.ET_MESSAGE.forEach(item => {
            if (message) {
                message = message + "\n" + item.MESSAGE
            } else {
                message = item.MESSAGE
            }
        });
    } else if (response.data.EXPORT.ES_RETURN && response.data.EXPORT.ES_RETURN.MESSAGE_TAB) {
        response.data.EXPORT.ES_RETURN.MESSAGE_TAB.forEach(item => {
            if (message) {
                message = message + "\n" + item.MESSAGE
            } else {
                message = item.MESSAGE
            }
        });
    } else if (response.data.EXPORT.ES_RETURN && response.data.EXPORT.ES_RETURN.MESSAGE) {
        message = response.data.EXPORT.ES_RETURN.MESSAGE
        if (!message) {
            message = "Lütfen daha sonra tekrar deneyin"
        }
    }

    return message
}

export const prepareSapProps = (endpoint) => {
    return {
        sap_props: {
            rfcName: "Z_CRM_KDK_MOB_" + endpoint,
            language: LANG_ID,
            is_mobile: 'X'
        }
    }
}