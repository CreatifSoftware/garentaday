import { prepareSapProps } from "../apiHelpers"
import { LANG_ID } from "../constants"
import { CHECK_CAMP_CODE_ENDPOINT } from "../endpoints"

export const checkCampaignCodeRequest = (campaignId, phoneNumber, code) => {
    const jsonBody = {
        ...prepareSapProps(CHECK_CAMP_CODE_ENDPOINT),
        import: {
            IT_PARAM: [
                {
                    "NAME": "CAMPAIGN_ID",
                    "VALUE": campaignId
                },
                {
                    "NAME": "CODE",
                    "VALUE": code
                },
                {
                    "NAME": "TELEPHONE",
                    "VALUE": phoneNumber.replace('(','').replace(')','').replace(' ','').replace(' ','').replace(' ','')
                },
            ]
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const filterCampaignRequest = (branchs, periods) => {
    const branchList = []
    for (let index = 0; index < branchs.length; index++) {
        const item = branchs[index];
        branchList.push(item.branchId)
    }

    const periodList = []
    for (let index = 0; index < periods.length; index++) {
        const item = periods[index];
        periodList.push(item.code)
    }

    const jsonBody = {
        sap_props: {
            rfcName: "z_crm_kdk_web_special_cmp_list".toUpperCase(),
            language: LANG_ID,
            is_mobile: 'X'
        },
        import: {
            IT_SUBE: branchList,
            IT_PERIOD: periodList
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}