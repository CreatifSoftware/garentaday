import axios from "axios"
import { API_HEADERS, API_URL } from "../constants"
import { checkCampaignCodeRequest, filterCampaignRequest } from "./campaignRequest"

export const getFullCampaignList = () => {
    return new Promise((resolve,reject) => {
        axios.get(
            'https://wsport.garenta.com.tr/wsport/mobile/getCampaigns/tr',
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callCheckCampaignCodeApi = (campaignId, phoneNumber, code) => {
    return new Promise((resolve,reject) => {
        axios.post(
            "https://wsport.garenta.com.tr/wsport/mobile",
            checkCampaignCodeRequest(campaignId, phoneNumber, code),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callFilterCampaignsApi = (branchs, periods) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            filterCampaignRequest(branchs, periods),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}