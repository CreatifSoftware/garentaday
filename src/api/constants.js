import { Platform } from "react-native"
import { AdjustConfig } from "react-native-adjust"

export const APP_VERSION = "1.2"

export const LANG_ID = "T"
export const TOKEN = "bW9iaWxlOkdhcmVudGEhISwsLi5A"

export const IMAGE_URL = "http://images.garenta.com.tr"
export const API_HEADERS = {
    'Accept': 'application/json; charset=utf-8',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': `Basic ${TOKEN}` 
}

// TEST
// export const ADJUST_APP_TOKEN = "xaydffyfi8e8"
// export const API_URL = "https://wsport.garenta.com.tr/wsportqa/mobile"
// export const ADJUST_ENVIRONMENT = AdjustConfig.EnvironmentSandbox

// LIVE
export const API_URL = "https://wsport.garenta.com.tr/wsport/mobile"
export const ADJUST_APP_TOKEN = "xaydffyfi8e8"
export const ADJUST_ENVIRONMENT = AdjustConfig.EnvironmentProduction