
import {
    ADD_NAD_GET_CREDIT_CARD,
    CHANGE_PASSWORD_ENDPOINT,
    CREATE_UPDATE_ADDRESS_ENDPOINT,
    getProfileInfoEndpoint,
    GET_FAV_BRANCH_ENDPOINT,
    SET_FAV_BRANCH_ENDPOINT,
    signUpEndpoint,
    GET_INVOICE_LIST_ENDPOINT,
    GET_ACTIVE_COMPLAINT_LIST_ENDPOINT,
    GET_COMPLETED_COMPLAINT_LIST_ENDPOINT,
    CREATE_COMPLAINT_ENDPOINT
} from "../endpoints"
import { prepareSapProps } from "../apiHelpers"
import { encodeUTF16LE } from "../../utilities/helpers"
import { LANG_ID } from "../constants"

export const getProfileInfoRequest = (userId) => {
    const jsonBody = {
        ...prepareSapProps(getProfileInfoEndpoint),
        import: {
            IV_BP_NUMBER: userId
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const changePasswordRequest = (userId, oldPassword, newPassword) => {
    const jsonBody = {
        ...prepareSapProps(CHANGE_PASSWORD_ENDPOINT),
        import: {
            IV_LANGU: LANG_ID,
            IS_INPUT: {
                BP_NUMBER: userId,
                OLD_PASSWORD: encodeUTF16LE(oldPassword),
                NEW_PASSWORD: encodeUTF16LE(newPassword)
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const setFavoriteBranchRequest = (userId, branchId, process) => {
    const jsonBody = {
        ...prepareSapProps(SET_FAV_BRANCH_ENDPOINT),
        import: {
            IV_BRANCH: branchId,
            IV_PARTNER: userId,
            IV_PROCESS: process
        }
    }
    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const getFavoriteBranchRequest = (userId) => {

    const jsonBody = {
        ...prepareSapProps(GET_FAV_BRANCH_ENDPOINT),
        import: {
            IV_PARTNER: userId,
        }
    }
    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const updateUserRequest = (user) => {
    const jsonBody = {
        ...prepareSapProps(signUpEndpoint),
        import: {
            IS_HEADER: {
                BP_NUMBER: user.userId,
                FIRSTNAME: user.personalInfo.firstName,
                LASTNAME: user.personalInfo.lastName,
                NATIONALITY: user.personalInfo.isTurkishCitizen ? 'TR' : user.personalInfo.nationality,
                GENDER: user.personalInfo.gender,
                BIRTHPLACE: "",
                BIRTHDATE: user.personalInfo.dateOfBirth,
                TCKN: user.personalInfo.isTurkishCitizen ? user.personalInfo.govermentId : "",
                PASAPORTNO: user.personalInfo.isTurkishCitizen ? "" : user.personalInfo.passport,
                EMAILDOGRFLAG: "",
                SMSDOGRFLAG: "X",
                EHLIYETNO: user.licenseInfo.licenseNumber,
                EHLYTALISTAR: user.licenseInfo.licenseDate,
                EHLALISYERI: user.licenseInfo.licensePlace,
                EHLYTSINIFI: user.licenseInfo.licenseClass,
                ODEMEBICIMI: "",
                PASAPORTVERTAR: "",
                SOZLESME_ONAY: user.permissionInfo.agreementChecked,
                TEL_CODE: user.contactInfo.mobileCode,
                MOB_TEL: user.contactInfo.mobilePhone,
                E_MAIL: user.contactInfo.email,
                ALLOW_SMS: user.permissionInfo.allowSms,
                ALLOW_EMAIL: user.permissionInfo.allowEmail,
                ALLOW_PHONE: user.permissionInfo.allowPhone,
                ALLOW_LETTER: user.permissionInfo.allowLetter,
                COMPANY_NO: "",
                COMPANY_EMAIL: "",
                NICKNAME: "",
                PASSWORD: user.password,
                ALLOW_MARKETING: user.permissionInfo.allowMarketing,
                ALLOW_PRIVATE: user.permissionInfo.allowPrivate,
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const createUpdateAddressRequest = (addressList, userId) => {

    const jsonArray = []
    for (let index = 0; index < addressList.length; index++) {
        const item = addressList[index];
        jsonArray.push({
            ADDRESS_NUMBER: item.addressId,
            ADDRESS_DEFINITION: item.addressTitle,
            ADDRESS_TYPE: item.type,
            ADDRESS_DEL_FLAG: item.isDelete ? "X" : "",
            ADDRESS: item.address,
            COUNTRY: item.country,
            REGION: item.city,
            CITY_NO: item.district,
            NAME: item.name,
            SURNAME: item.surname,
            TCKN: item.govermentId,
            COMPANY_NAME: item.companyName,
            VERGI_NO: item.taxNo,
            VERGI_DAIRESI: item.taxPlace
        })
    }

    const jsonBody = {
        ...prepareSapProps(CREATE_UPDATE_ADDRESS_ENDPOINT),
        import: {
            IV_CUSTOMER_NO: userId,
            IT_ADDRESS: jsonArray
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const addCreditCardRequest = (userId, cardInfo) => {
    const jsonBody = {
        ...prepareSapProps(ADD_NAD_GET_CREDIT_CARD),
        import: {
            IS_INPUT: {
                STAMP_NAME: cardInfo.cardHolderName,
                CARD_NO: cardInfo.cardNo,
                CARD_DESC: cardInfo.cardDesc,
                VALID_MONTH: cardInfo.month,
                VALID_YEAR: cardInfo.year,
                CVV_NUM: cardInfo.cvv,
                PARTNER: userId,
                BRANCH: "3063"
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const getCreditCardRequest = (userId) => {
    const jsonBody = {
        ...prepareSapProps(ADD_NAD_GET_CREDIT_CARD),
        import: {
            IS_INPUT: {
                PARTNER: userId,
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const getInvoiceListRequest = (user) => {
    const jsonBody = {
        ...prepareSapProps(GET_INVOICE_LIST_ENDPOINT),
        import: {
            IV_BP_NUMBER: user.userId,
            IV_CUSTOMER_TYPE: 'B',
            IV_COMPANY_NO: "",
            IV_LANGU: LANG_ID
        }
    }
    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const getActiveComplaintListRequest = (userId) => {
    const jsonBody = {
        ...prepareSapProps(GET_ACTIVE_COMPLAINT_LIST_ENDPOINT),
        import: {
            IV_PARTNER: userId,
        }
    }
    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const getCompletedComplaintListRequest = (userId) => {
    const jsonBody = {
        ...prepareSapProps(GET_COMPLETED_COMPLAINT_LIST_ENDPOINT),
        import: {
            IV_PARTNER: userId,
        }
    }
    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const createComplaintRequest = (complaintObj) => {
    // const assetsArray = []
    // for (let index = 0; index < complaintObj.assets.length; index++) {
    //     const item = complaintObj.assets[index];
    //     assetsArray.push({
    //         GUID: null,
    //         FILE_NAME: item.fileName,
    //         FILE_CONTENT: item.base64,
    //         DESCRIPTION: `Şikayet Görseli_${index + 1}`,
    //         USERNAME: ""
    //     })
    // }

    const jsonBody = {
        ...prepareSapProps(CREATE_COMPLAINT_ENDPOINT),
        import: {
            IS_INFO: {
                BP_NUMBER: complaintObj.userId,
                AD: complaintObj.firstName,
                SOYAD: complaintObj.lastName,
                // NATIONALITY: complaintObj.nationality.country,
                // TCKN: complaintObj.nationality.country === "TR" ? complaintObj.govermentId : "",
                // PASAPORTNO: complaintObj.nationality.country !== "TR" ? complaintObj.passportNo : "",
                CEP_TEL: complaintObj.mobilePhone.replace('(', '').replace(')', '').replace(' ', '').replace(' ', '').replace(' ', ''),
                E_MAIL: complaintObj.email,
                METIN: complaintObj.description,
                CATEGORY: complaintObj.subject,
                BASLIK: complaintObj.title,
                ILGILI: complaintObj.userId.length > 0 ? "client" : "other",
                DONUS_KANALI: complaintObj.responseChannel
            },
            // IT_ATTACHMENTS: assetsArray
        }
    }

    console.log(JSON.stringify(jsonBody).length);
    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const getComplaintCategoryRequest = () => {
    const jsonBody = {
        ...prepareSapProps("GET_KONU"),
        import: {
            IV_LANGU: "T"
        }
    }
    return {
        parameter: JSON.stringify(jsonBody),
    }
}


