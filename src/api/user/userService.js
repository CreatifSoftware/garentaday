import axios from "axios"
import { API_HEADERS, API_URL } from "../constants"
import { addCreditCardRequest, 
    changePasswordRequest, 
    createUpdateAddressRequest, 
    getCreditCardRequest, 
    getFavoriteBranchRequest, 
    getProfileInfoRequest, 
    setFavoriteBranchRequest, 
    updateUserRequest,
    getInvoiceListRequest, 
    getActiveComplaintListRequest,
    getCompletedComplaintListRequest,
    createComplaintRequest,
    getComplaintCategoryRequest} from "./userRequest"

export const callGetProfileInfoApi = (userId) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            getProfileInfoRequest(userId),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callChangePasswordApi = (userId, oldPassword, newPassword) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            changePasswordRequest(userId, oldPassword, newPassword),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callUpdateUserApi = (user) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            updateUserRequest(user),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callSetFavoriteBranchApi = (userId, branchId, process) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            setFavoriteBranchRequest(userId, branchId, process),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callGetFavoriteBranchApi = (userId) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            getFavoriteBranchRequest(userId),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callCreateUpdateAddressApi = (addressList,userId) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            createUpdateAddressRequest(addressList,userId),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callAddCreditCardApi = (userId,cardInfo) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            addCreditCardRequest(userId,cardInfo),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callGetCreditCardApi = (userId) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            getCreditCardRequest(userId),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callGetInvoiceListApi = (user) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            getInvoiceListRequest(user),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callGetActiveComplaintListApi = (userId) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            getActiveComplaintListRequest(userId),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callGetCompletedComplaintListApi = (userId) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            getCompletedComplaintListRequest(userId),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callCreateComplaintApi = (complaintObj) => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            createComplaintRequest(complaintObj),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const callGetcomplaintCategoriesApi = () => {
    return new Promise((resolve,reject) => {
        axios.post(
            API_URL,
            getComplaintCategoryRequest(),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
}