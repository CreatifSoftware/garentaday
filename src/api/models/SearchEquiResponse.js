export const SearchEquiResponse = (response) => {
    if (response === null) {
        return null
    }
    
    let pricingList = []
    for (let index = 0; index < response.ET_PRICING.length; index++) {
        const item = response.ET_PRICING[index];

        pricingList.push({
            guid:item.GUID,
            groupCode:item.GRUP_KODU,
            campaignId:item.KAMPANYA_ID,
            kapsam: item.KAPSAM,
            pricingGuid:item.PRICING_GUID
        })
    }

    let amountList = []
    for (let index = 0; index < response.ET_TUTAR.length; index++) {
        const item = response.ET_TUTAR[index];

        amountList.push({
            guid:item.REF_GUID,
            payLaterAmount:item.TOPLAM_TUTAR,
            payNowAmount:item.INDIRIMLI_TUTAR,
            currency: item.PARA_BIRIMI,
            dailyDepositAmount:item.GUNLUK_TEMINAT_TUTAR,
            monhthlyDepositAmount:item.AYLIK_TEMINAT_TUTAR
        })
    }

    
    let carGroupList = []
    for (let index = 0; index < response.ET_GRUP_DETAY.length; index++) {
        const item = response.ET_GRUP_DETAY[index];

        let pricingItem = pricingList.filter(arg => arg.groupCode === item.GRUP_KODU)[0]
        if (pricingItem !== undefined) {
            let amountItem = amountList.filter(arg => arg.guid === pricingItem.guid)[0]

            carGroupList.push({
                groupCode:item.GRUP_KODU,
                groupCodeDesc:item.GRUP_DESC,
                segmentId:item.SEGMENT_ID,
                segmentDesc: item.SEGMENT_DESC,
                fuelType:item.YAKIT_TIP,
                fuelTypeDesc:item.YAKIT_DESC,
                transmisionId:item.SANZ_ID,
                transmissionDesc:item.SANZ_DESC,
                bodyTypeId:item.GOVDE_TIPI,
                bodyTypeDesc:item.GOVDE_TIPI_DESC,
                doorNumber:item.KAPI_SAYISI,
                personNumber:item.YOLCU_SAYISI,
                minLicense:item.MIN_EHLIYET,
                minAge:item.MIN_YAS,
                minYoungLicense:item.GENC_SRC_EHL,
                minYoungAge:item.GENC_SRC_YAS,
                displayText:item.DISPLAY_TEXT,
                imageUrl:item.RESIM_LINK,
                doubleCreditCard:item.CIFT_KK == 'X',
                findeks:item.FINDEKS_PUANI,
                dailyKm:item.GUNLUK_KM,
                monthlyKm:item.AYLIK_KM,
                pricingItem: pricingItem,
                amountItem: amountItem
            })
        }

    }

    let duration = response.EV_DAY
    
    return { carGroupList, duration }
}