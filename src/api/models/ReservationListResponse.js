import { getFormatedDate } from "../../utilities/helpers"

export const ReservationListResponse = (response) => {
    if (response === null) {
        return null
    }

    // Credit cards
    let activeReservations = []
    let completedReservations = []
    let cancelledReservations = []

    for (let index = 0; index < response.ET_OPEN.length; index++) {
        let item = response.ET_OPEN[index];
        activeReservations.push({
            reservationId: item.RESERV_ID,
            pnrNo: item.RESERV_CODE,
            contractId: item.SOZLESME_ID,
            contractPnrNo: item.SOZLESME_CODE,
            couponCode: item.COUPON_CODE,
            pickupBranchId: item.SUBE,
            dropoffBranchId: item.HDFSUBE,
            pickupDate: item.BEG_DATE,
            pickupTime: item.BEG_TIME,
            dropoffDate: item.END_DATE,
            dropoffTime: item.END_TIME,
            statusText: item.STATUS_TEXT

        })
    }

    for (let index = 0; index < response.ET_COMPLETED.length; index++) {
        let item = response.ET_COMPLETED[index];

        completedReservations.push({
            reservationId: item.RESERV_ID,
            pnrNo: item.RESERV_CODE,
            contractId: item.SOZLESME_ID,
            contractPnrNo: item.SOZLESME_CODE,
            couponCode: item.COUPON_CODE,
            pickupBranchId: item.SUBE,
            dropoffBranchId: item.HDFSUBE,
            pickupDate: item.BEG_DATE,
            pickupTime: item.BEG_TIME,
            dropoffDate: item.END_DATE,
            dropoffTime: item.END_TIME,
            statusText: item.STATUS_TEXT

        })
    }

    for (let index = 0; index < response.ET_CANCELED.length; index++) {
        let item = response.ET_CANCELED[index];

        cancelledReservations.push({
            reservationId: item.RESERV_ID,
            pnrNo: item.RESERV_CODE,
            contractId: item.SOZLESME_ID,
            contractPnrNo: item.SOZLESME_CODE,
            couponCode: item.COUPON_CODE,
            pickupBranchId: item.SUBE,
            dropoffBranchId: item.HDFSUBE,
            pickupDate: item.BEG_DATE,
            pickupTime: item.BEG_TIME,
            dropoffDate: item.END_DATE,
            dropoffTime: item.END_TIME,
            statusText: item.STATUS_TEXT

        })
    }

    return { activeReservations, completedReservations, cancelledReservations }
}