import { PAYMENT_TYPES } from "../../utilities/constants"
import { getFormatedDate } from "../../utilities/helpers"

export const ReservationDetailResponse = (response, reservation) => {
    if (response === null) {
        return null
    }

    reservation.individualAddressId = response.ES_RETURN.ADDR_NUMBER_B
    reservation.individualAddressId = response.ES_RETURN.ADDR_NUMBER_F
    reservation.totalAmount = response.ES_RETURN.TOPLAM_TUTAR
    reservation.isEditable = response.ES_RETURN.EDITABLE == 'X'
    reservation.isCancelable = response.ES_RETURN.CANCELABLE == 'X'
    reservation.isPaid = response.ES_RETURN.ODENMIS == 'X'
    reservation.isCorporateReservationApproved = response.ES_RETURN.KURUMSAL_ONAY == 'X'
    reservation.paymentType = response.ES_RETURN.SIMDI_ODE == 'X' ? PAYMENT_TYPES.PAY_NOW : PAYMENT_TYPES.PAY_LATER


    //CREDIT CARD
    reservation.creditCardInfo = {}
    reservation.creditCardInfo.cardNo = response.ES_RETURN.DISPLAY_TEXT
    reservation.creditCardInfo.merkey = response.ES_RETURN.MERKEY
    reservation.creditCardInfo.instalment = "1"

    //CAR GROUP
    for (let index = 0; index < response.ET_GRUP.length; index++) {
        const item = response.ET_GRUP[index];
        
        reservation.selectedGroup = {}
        reservation.selectedGroup.amountItem = {}
        reservation.selectedGroup.groupCode = item.GRUP_KODU
        reservation.selectedGroup.groupCodeDesc = item.GRUP_DESC
        reservation.selectedGroup.segmentId = item.SEGMENT_ID
        reservation.selectedGroup.segmentDesc = item.SEGMENT_DESC
        reservation.selectedGroup.fuelType = item.YAKIT_TIP
        reservation.selectedGroup.fuelTypeDesc = item.YAKIT_DESC
        reservation.selectedGroup.transmisionId = item.SANZ_ID
        reservation.selectedGroup.transmissionDesc = item.SANZ_DESC
        reservation.selectedGroup.bodyTypeId = item.GOVDE_TIPI
        reservation.selectedGroup.bodyTypeDesc = item.GOVDE_TIPI_DESC
        reservation.selectedGroup.doorNumber = item.KAPI_SAYISI
        reservation.selectedGroup.personNumber = item.YOLCU_SAYISI
        reservation.selectedGroup.minLicense = item.MIN_EHLIYET
        reservation.selectedGroup.minAge = item.MIN_YAS
        reservation.selectedGroup.minYoungLicense = item.GENC_SRC_EHL
        reservation.selectedGroup.minYoungAge = item.GENC_SRC_YAS
        reservation.selectedGroup.displayText = item.DISPLAY_TEXT
        reservation.selectedGroup.imageUrl = item.RESIM_LINK
        reservation.selectedGroup.doubleCreditCard = item.CIFT_KK == 'X'
        reservation.selectedGroup.findeks = item.FINDEKS_PUANI
        reservation.selectedGroup.dailyKm = item.GUNLUK_KM
        reservation.selectedGroup.monthlyKm = item.AYLIK_KM

        reservation.selectedGroup.amountItem = {
            guid: response.ES_TUTAR.REF_GUID,
            payLaterAmount: response.ES_TUTAR.TOPLAM_TUTAR,
            payNowAmount: response.ES_TUTAR.INDIRIMLI_TUTAR,
            currency: response.ES_TUTAR.PARA_BIRIMI,
            dailyDepositAmount: item.GUNLUK_TEMINAT_TUTARI,
            monhthlyDepositAmount: item.AYLIK_TEMINAT_TUTARI
        }
    }



    // Eski araç grubunu saklıyorum
    reservation.oldSelectedGroup = {...reservation.selectedGroup}

    let products = []
    for (let index = 0; index < response.ET_EKURUN.length; index++) {
        const item = response.ET_EKURUN[index];

        products.push({
            productId: item.PRODUCT_ID,
            productName: item.PRODUCT_DESC,
            pricingGuid: item.PRICING_GUID,
            parentId: item.PARENT_ID,
            maxValue: Number(item.MAX_ADET),
            infoText: item.INFO_TEXT,
            required: item.ZORUNLU === 'X',
            productType: item.EKURUN_TIPI,
            billingType: item.FATURA_TIPI,
            value: Number(item.ADET),
            totalAmount: item.TOPLAM_TUTAR,
            paidAmount: Number(item.ODENMIS_TUTAR),
            amountToBePaid: Number(item.ODENECEK_TUTAR)
        })
    }

    reservation.oldSelectedProducts = products
    reservation.selectedProducts = products


    return reservation
}