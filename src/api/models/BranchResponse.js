export const BranchResponse = (branchList) => {
    let branchs = []
    for (let index = 0; index < branchList.length; index++) {
        const item = branchList[index];

        if (item.AKTIFSUBE === 'X') {
            branchs.push({
                branchId:item.ALT_SUBE,
                branchName:item.ALT_SUBETX,
                city:item.SEHIR,
                cityName:item.SEHIRTX,
                latitude:item.XKORD,
                longitude:item.YKORD,
                address:item.ADRES,
                telephone:item.TEL,
                email:item.EMAIL,
            })
        }
    }
    
    return branchs
}