import { useSelector } from "react-redux";

export const GetProfileResponse = (response, user) => {
    if (response === null) {
        return null
    }

    let addressList = []
    for (let index = 0; index < response.ET_ADDRESS.length; index++) {
        const item = response.ET_ADDRESS[index];

        addressList.push({
            addressId: item.ADDRESS_NUMBER,
            address: item.ADDRESS,
            addressTitle: item.ADDRESS_DEFINITION,
            isDelete: item.ADDRESS_DEL_FLAG,
            type: item.ADDRESS_TYPE,
            district: item.CITY_NO,
            companyName: item.COMPANY_NAME,
            country: item.COUNTRY,
            city: item.REGION,
            govermentId: item.TCKN,
            taxPlace: item.VERGI_DAIRESI,
            taxNo: item.VERGI_NO,
            name: item.NAME,
            surname: item.SURNAME
        })
    }

    let userObj = {
        userId: response.ES_CUSTOMER.BP_NUMBER,
        notifCount: user.notifCount,
        personalInfo: {
            ...user.personalInfo,
            firstName: response.ES_CUSTOMER.FIRSTNAME,
            lastName: response.ES_CUSTOMER.LASTNAME,
            dateOfBirth: response.ES_CUSTOMER.BIRTHDATE,
            gender: response.ES_CUSTOMER.GENDER,
            nationality: response.ES_CUSTOMER.NATIONALITY,
            isTurkishCitizen: response.ES_CUSTOMER.NATIONALITY == "TR",
            govermentId: response.ES_CUSTOMER.TCKN,
            passport: response.ES_CUSTOMER.PASAPORTNO
        },
        contactInfo: {
            email: response.ES_CUSTOMER.E_MAIL,
            mobileCode: response.ES_CUSTOMER.TEL_CODE,
            mobilePhone: response.ES_CUSTOMER.MOB_TEL
        },
        permissionInfo: {
            ...user.permissionInfo,
            allowEmail: response.ES_CUSTOMER.ALLOW_EMAIL === 'X',
            allowLetter: response.ES_CUSTOMER.ALLOW_LETTER === 'X',
            allowMarketing: response.ES_CUSTOMER.ALLOW_MARKETING === 'X',
            allowPhone: response.ES_CUSTOMER.ALLOW_PHONE === 'X',
            allowPrivate: response.ES_CUSTOMER.ALLOW_PRIVATE === 'X',
            allowSms: response.ES_CUSTOMER.ALLOW_SMS === 'X',
        },
        licenseInfo: {
            licenseNumber: response.ES_CUSTOMER.EHLIYETNO,
            licenseClass: response.ES_CUSTOMER.EHLYTSINIFI,
            licensePlace: response.ES_CUSTOMER.EHLALISYERI,
            licenseDate: response.ES_CUSTOMER.EHLYTALISTAR
        },
        addressList: addressList,
        creditCards: user.creditCards
    }

    return userObj
}