export const AdditionalProductsResponse = (response) => {
    if (response === null) {
        return null
    }

    // Credit cards
    let products = []
    for (let index = 0; index < response.ET_EKURUN.length; index++) {
        const item = response.ET_EKURUN[index];

        products.push({
            productId:item.PRODUCT_ID,
            productName:item.PRODUCT_DESC,
            pricingGuid: item.PRICING_GUID,
            parentId:item.PARENT_ID,
            maxValue: Number(item.MAX_ADET),
            infoText: item.INFO_TEXT,
            required: item.ZORUNLU === 'X',
            productType: item.EKURUN_TIPI,
            billingType:item.FATURA_TIPI,
            value: Number(item.ADET),
            totalAmount:item.TOPLAM_TUTAR,
            paidAmount: Number(item.ODENMIS_TUTAR),
            amountToBePaid: Number(item.ODENECEK_TUTAR)
        })
    }

    return products
}