export const ComplaintListResponse = (complaintList) => {
    let complaints = []
    for (let index = 0; index < complaintList.length; index++) {
        const item = complaintList[index];

        complaints.push({
            complaintId: item.OBJECT_ID,
            parentGuid: item.PARENT_GUID,
            categoryGuid: item.CATEGORY_GUID,
            categoryId: item.CATEGORY_ID,
            categoryText: item.CATEGORY_TEXT,
            subTopic: item.ALT_KONU,
            subTopicDetail: item.ALT_KONU_DETAY,
            status: item.DURUM_DESC,
            title: item.BASLIK,
            message: item.NOTE,
        })
    }

    return complaints
}