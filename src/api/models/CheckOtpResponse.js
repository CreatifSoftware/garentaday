export const CheckOtpResponse = (response) => {
    if (response === null) {
        return null
    }

    return { code: response.ES_SMS_INFO.TEL_CODE, number: response.ES_SMS_INFO.TEL_NUMBER }    
}