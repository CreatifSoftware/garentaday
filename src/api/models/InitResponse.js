export const InitResponse = (response) => {
    if (response === null) {
        return null
    }

    let countries = []
    for (let index = 0; index < response.ET_ULKE.length; index++) {
        const item = response.ET_ULKE[index];

        countries.push({
            country:item.LAND1,
            countryName:item.LANDX50
        })
    }
    
    let cities = []
    for (let index = 0; index < response.ET_IL.length; index++) {
        const item = response.ET_IL[index];

        cities.push({
            country:item.LAND1,
            city:item.BLAND,
            cityName:item.BEZEI
        })
    }

    let districts = []
    for (let index = 0; index < response.ET_ILCE.length; index++) {
        const item = response.ET_ILCE[index];

        districts.push({
            country:item.COUNTRY,
            city:item.REGION,
            district:item.CITY_CODE,
            districtName:item.MC_CITY
        })
    }

    return {
        countries,districts,cities
    }
}