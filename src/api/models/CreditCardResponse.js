export const CreditCardResponse = (response, user) => {
    if (response === null) {
        return null
    }

    let cardList = []
    for (let index = 0; index < response.ET_ALL_CARDS.length; index++) {
        const item = response.ET_ALL_CARDS[index];

        console.log(item)
        if (item.ZZBLOCKED === ""){
            cardList.push({
                cardHolderName: user.personalInfo.firstName + ' ' + user.personalInfo.lastName,
                recordId: item.RECORD_ID,
                cardNo: item.ZZCARD_NO,
                cvv: item.ZZCVV_NUM,
                title: item.ZZFLD0000ZY,
                merkey: item.ZZMERCKEY,
                title2: item.ZZSTAMP_NAME,
                month: item.ZZVALID_MONTH,
                year: item.ZZVALID_YEAR
            })
        }
    }

    return cardList
}