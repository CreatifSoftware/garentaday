export const LoginResponse = (response) => {
    if (response === null) {
        return null
    }

    // Credit cards
    let cards = []
    console.log(response.ET_KREDI_KART)
    for (let index = 0; index < response.ET_KREDI_KART.length; index++) {
        const item = response.ET_KREDI_KART[index];

        cards.push({
            cardNo:item.DISPLAY_TEXT,
            merkey:item.MERKEY
        })
    }
    
    let user = {
        userId:response.ES_MUSTERI.MUSTERI_NO,
        notifCount: response.ES_MUSTERI.NOTIFICATION_COUNT && response.ES_MUSTERI.NOTIFICATION_COUNT,
        personalInfo: {
            fullName: response.ES_MUSTERI.MUSTERI_ADI && response.ES_MUSTERI.MUSTERI_ADI,
            dateOfBirth: response.ES_MUSTERI.DOGUM_TARIHI && response.ES_MUSTERI.DOGUM_TARIHI,
        },
        permissionInfo:{
            allowMarketing: response.ES_MUSTERI.ALLOW_MARKETING && response.ES_MUSTERI.ALLOW_MARKETING === 'X',
            confirmContract: response.ES_MUSTERI.SOZLESME_ONAY && response.ES_MUSTERI.SOZLESME_ONAY === 'X',
        },
        addressList: [],
        creditCards: cards
    }
    return user
}