
import {
    SEARCH_EQUI_ENDPOINT,
    SEARCH_ADDITIONAL_PRODUCTS_ENDPOINT,
    CREATE_RESERVATION_ENDPOINT,
    GET_RESERVATION_LIST_ENDPOINT,
    GET_RESERVATION_DETAIL_ENDPOINT,
    CANCEL_RESERVATION_ENDPOINT,
    UPDATE_RESERVATION_ENDPOINT,
    GET_CONTRACT_DETAIL_ENDPOINT,
    APPLY_COUPON_ENDPOINT
} from "../endpoints"
import { prepareSapProps } from "../apiHelpers"
import { encodeUTF16LE, getFormatedDate } from "../../utilities/helpers"
import { PAYMENT_TYPES } from '../../utilities/constants'
import { LANG_ID } from "../constants"

export const searchEquiRequest = (user, reservation,selectedFuelTypes, selectedTransmissionTypes) => {
    const fuelTypes = []
    for (let index = 0; index < selectedFuelTypes.length; index++) {
        const item = selectedFuelTypes[index];
        fuelTypes.push(item.YAKIT_TIP)
    }

    const transmissionTypes = []
    for (let index = 0; index < selectedTransmissionTypes.length; index++) {
        const item = selectedTransmissionTypes[index];
        transmissionTypes.push(item.SANZIMAN)
    }

    const jsonBody = {
        ...prepareSapProps(SEARCH_EQUI_ENDPOINT),
        import: {
            IS_SEARCH: {
                BELGE_NO: reservation.reservationId,
                SUBE: reservation.pickupBranch.branchId,
                HDF_SUBE: reservation.dropoffBranch.branchId,
                BSL_TARIH: getFormatedDate(reservation.pickupDateTime, 'YYYYMMDD'),
                BTS_TARIH: getFormatedDate(reservation.dropoffDateTime, 'YYYYMMDD'),
                BSL_SAAT: getFormatedDate(reservation.pickupDateTime, 'HHmm00'),
                BTS_SAAT: getFormatedDate(reservation.dropoffDateTime, 'HHmm00'),
                GRUP_KODU: "",
                KAMPANYA_ID: reservation.campaignId,
                MUSTERI_NO: user && user.userId,
                MUSTERI_TIPI: "",
                COMPANY_NO: "",
                SANZIMAN: transmissionTypes,
                YAKIT_TIP: fuelTypes
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const searchAdditionalProductsRequest = (user, reservation) => {
    const jsonBody = {
        ...prepareSapProps(SEARCH_ADDITIONAL_PRODUCTS_ENDPOINT),
        import: {
            IS_SEARCH: {
                BELGE_NO: reservation.reservationId,
                SUBE: reservation.pickupBranch.branchId,
                HDF_SUBE: reservation.dropoffBranch.branchId,
                BSL_TARIH: getFormatedDate(reservation.pickupDateTime, 'YYYYMMDD'),
                BTS_TARIH: getFormatedDate(reservation.dropoffDateTime, 'YYYYMMDD'),
                BSL_SAAT: getFormatedDate(reservation.pickupDateTime, 'HHmm00'),
                BTS_SAAT: getFormatedDate(reservation.dropoffDateTime, 'HHmm00'),
                GRUP_KODU: reservation.updatedGroup == null ? reservation.selectedGroup.groupCode : reservation.updatedGroup.groupCode,
                KAMPANYA_ID: "",
                BP_NUMBER: user.userId,
                MUSTERI_TIPI: "B",
                COMPANY_NO: "",
                SIMDI_ODE: reservation.paymentType === PAYMENT_TYPES.PAY_NOW ? "X" : ""
            }
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const createReservationRequest = (user, reservation) => {
    const products = []
    for (let index = 0; index < reservation.selectedProducts.length; index++) {
        const item = reservation.selectedProducts[index];
        if (item.value > 0) {
            products.push({
                PRODUCT_ID: item.productId,
                PRICING_GUID: item.pricingGuid,
                ADET: String(item.value),
                KAMPANYA_ID: "",
                FATURA_TIPI: item.billingType
            })
        }

    }

    const payments = []
    if (reservation.creditCardInfo) {
        const cardInfo = reservation.creditCardInfo
        // console.log(cardInfo.cardNo.replace(" ","").re)
        payments.push({
            KART_NUMARASI: reservation.paymentType === PAYMENT_TYPES.PAY_NOW ? cardInfo.cardNo.replace(" ", "").replace(" ", "").replace(" ", "") : "",
            KART_SAHIBI: reservation.paymentType === PAYMENT_TYPES.PAY_NOW && cardInfo.cardHolderName,
            VALID_MONTH: reservation.paymentType === PAYMENT_TYPES.PAY_NOW && cardInfo.month,
            VALID_YEAR: reservation.paymentType === PAYMENT_TYPES.PAY_NOW && cardInfo.year,
            TAHSILAT_TIPI: reservation.paymentType === PAYMENT_TYPES.PAY_NOW ? "K" : "P",
            MER_KEY: reservation.paymentType === PAYMENT_TYPES.PAY_NOW && cardInfo.merkey,
            GUVENLIK_KODU: reservation.paymentType === PAYMENT_TYPES.PAY_NOW && cardInfo.cvv,
            INSTALMENT: reservation.paymentType === PAYMENT_TYPES.PAY_NOW && "1",
        })
    }

    // let totalOBJ = {}

    let totalOBJ = {
        IS_HEADER: {
            SUBE: reservation.pickupBranch.branchId,
            HDF_SUBE: reservation.dropoffBranch.branchId,
            BSL_TARIH: getFormatedDate(reservation.pickupDateTime, 'YYYYMMDD'),
            BTS_TARIH: getFormatedDate(reservation.dropoffDateTime, 'YYYYMMDD'),
            BSL_SAAT: getFormatedDate(reservation.pickupDateTime, 'HHmm00'),
            BTS_SAAT: getFormatedDate(reservation.dropoffDateTime, 'HHmm00'),
            XBILL_EMAIL: "",
            COUPON_CODE: reservation.couponCode == undefined ? "" : reservation.couponCode,
        },
        IS_PARTNER: {
            BP_NUMBER: user.userId,
            ADDR_NUMBER_B: reservation.individualAddressId,
            ADDR_NUMBER_F: reservation.invoiceAddressId,
            MUSTERI_TIPI: "B",
            PRIORITY_TALEP: "",
            COMPANY_NO: "",
        },
        IS_ARAC: {
            PRICING_GUID: reservation.selectedGroup.pricingItem.pricingGuid,
            PRODUCT_ID: "",
            GRUP_KODU: reservation.selectedGroup.groupCode,
            RENK_NO: "",
            KAMPANYA_ID: reservation.campaignId == undefined ? "" : reservation.campaignId,
            TRANSACTION_ID: reservation.transactionId == undefined ? "" : reservation.transactionId
        },
        IV_ONLY_CHECK: ""
    }

    if (products.length > 0) {
        totalOBJ = Object.assign(totalOBJ, { IT_EKURUN: products })
    }
    if (payments.length > 0) {
        totalOBJ = Object.assign(totalOBJ, { IT_KREDI_KART: payments })
    }

    const jsonBody = {
        ...prepareSapProps(CREATE_RESERVATION_ENDPOINT),
        import: {
            ...totalOBJ
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const updateReservationRequest = (user, reservation) => {
    const products = []
    for (let index = 0; index < reservation.selectedProducts.length; index++) {
        const item = reservation.selectedProducts[index];
        if (item.value > 0) {
            products.push({
                PRODUCT_ID: item.productId,
                PRICING_GUID: item.pricingGuid,
                ADET: String(item.value),
                KAMPANYA_ID: "",
                FATURA_TIPI: item.billingType
            })
        }

    }

    const payments = []
    if (reservation.creditCardInfo) {
        const cardInfo = reservation.creditCardInfo
        payments.push({
            KART_NUMARASI: cardInfo.cardNo.length > 0 ? cardInfo.cardNo.replace(" ", "").replace(" ", "").replace(" ", "") : "",
            KART_SAHIBI: cardInfo.cardHolderName,
            VALID_MONTH: cardInfo.month,
            VALID_YEAR: cardInfo.year,
            TAHSILAT_TIPI: reservation.paymentType === PAYMENT_TYPES.PAY_NOW ? "K" : "P",
            MER_KEY: cardInfo.merkey,
            GUVENLIK_KODU: cardInfo.cvv,
            INSTALMENT: "1",
        })
    }

    // let totalOBJ = {}

    let totalOBJ = {
        IS_HEADER: {
            RESERV_ID: reservation.reservationId,
            SUBE: reservation.pickupBranch.branchId,
            HDF_SUBE: reservation.dropoffBranch.branchId,
            BSL_TARIH: getFormatedDate(reservation.pickupDateTime, 'YYYYMMDD'),
            BTS_TARIH: getFormatedDate(reservation.dropoffDateTime, 'YYYYMMDD'),
            BSL_SAAT: getFormatedDate(reservation.pickupDateTime, 'HHmm00'),
            BTS_SAAT: getFormatedDate(reservation.dropoffDateTime, 'HHmm00'),
            XBILL_EMAIL: "",
            COUPON_CODE: reservation.couponCode == undefined ? "" : reservation.couponCode,
        },
        IS_PARTNER: {
            BP_NUMBER: user.userId,
            ADDR_NUMBER_B: reservation.individualAddressId,
            ADDR_NUMBER_F: reservation.invoiceAddressId,
            MUSTERI_TIPI: "B",
            PRIORITY_TALEP: "",
            COMPANY_NO: "",
        },
        IS_ARAC: {
            PRICING_GUID: reservation.selectedGroup.pricingItem.pricingGuid,
            PRODUCT_ID: "",
            GRUP_KODU: reservation.selectedGroup.groupCode,
            RENK_NO: "",
            KAMPANYA_ID: reservation.campaignId == undefined ? "" : reservation.campaignId,
            TRANSACTION_ID: reservation.transactionId == undefined ? "" : reservation.transactionId
        },
        IV_ONLY_CHECK: ""
    }

    if (products.length > 0) {
        totalOBJ = Object.assign(totalOBJ, { IT_EKURUN: products })
    }
    if (payments.length > 0) {
        totalOBJ = Object.assign(totalOBJ, { IT_KREDI_KART: payments })
    }

    const jsonBody = {
        ...prepareSapProps(UPDATE_RESERVATION_ENDPOINT),
        import: {
            ...totalOBJ
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const getReservationListRequest = (user) => {
    const jsonBody = {
        ...prepareSapProps(GET_RESERVATION_LIST_ENDPOINT),
        import: {
            IV_BP_NUMBER: user.userId,
            IV_COMPANY_NO: "",
            IV_CUSTOMER_TYPE: "B",
            IV_LANGU: LANG_ID
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const getReservationDetailRequest = (reservationId) => {
    const jsonBody = {
        ...prepareSapProps(GET_RESERVATION_DETAIL_ENDPOINT),
        import: {
            IV_RESERV_ID: reservationId
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const getContractDetailRequest = (contractId) => {
    const jsonBody = {
        ...prepareSapProps(GET_CONTRACT_DETAIL_ENDPOINT),
        import: {
            IV_SOZLESME_ID: contractId
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const cancelReservationRequest = (reservationId, isConfirmed) => {
    const jsonBody = {
        ...prepareSapProps(CANCEL_RESERVATION_ENDPOINT),
        import: {
            IV_RESERV_ID: reservationId,
            IV_ACCEPTED: isConfirmed ? 'X' : ''
        }
    }
    return {
        parameter: JSON.stringify(jsonBody),
    }
}

export const applyCouponCodeRequest = (couponCode, userId, reservation) => {
    const pricingGuid = []
    if (reservation.selectedGroup) {
        pricingGuid.push({
            PRICING_GUID: reservation.selectedGroup.pricingItem.pricingGuid
        })
    }

    if (reservation.oldSelectedGroup) {
        pricingGuid.push({
            PRICING_GUID: reservation.oldSelectedGroup.pricingItem.pricingGuid
        })
    }

    for (let index = 0; index < reservation.selectedProducts.length; index++) {
        const item = reservation.selectedProducts[index];
        if (item.value > 0) {
            pricingGuid.push({
                PRICING_GUID: item.pricingGuid,
            })
        }
    }


    let totalOBJ = {
        IV_COUPON_CODE: couponCode,
        IV_BP_NUMBER: userId,
        IV_PAYMENT_TYPE: reservation.paymentType === PAYMENT_TYPES.PAY_NOW ? "X" : ""
    }
    if (pricingGuid.length > 0) {
        totalOBJ = Object.assign(totalOBJ, { IT_PRICING_GUID: pricingGuid })
    }

    const jsonBody = {
        ...prepareSapProps(APPLY_COUPON_ENDPOINT),
        import: {
            ...totalOBJ
        }
    }

    return {
        parameter: JSON.stringify(jsonBody),
    }
}

