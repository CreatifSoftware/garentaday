import axios from "axios"
import { API_HEADERS, API_URL } from "../constants"
import { applyCouponCodeRequest, cancelReservationRequest, createReservationRequest, getContractDetailRequest, getReservationDetailRequest, getReservationListRequest, searchAdditionalProductsRequest, searchEquiRequest, updateReservationRequest } from "./reservationRequest"

export const callSearchEquiApi = (user, reservation,selectedFuelTypes, selectedTransmissionTypes) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL,
            searchEquiRequest(user, reservation,selectedFuelTypes, selectedTransmissionTypes),
            { headers: API_HEADERS }
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callSearchAdditionalProductsApi = (user, reservation) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL,
            searchAdditionalProductsRequest(user, reservation),
            { headers: API_HEADERS }
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callCreateReservationApi = (user, reservation) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL,
            createReservationRequest(user, reservation),
            { headers: API_HEADERS }
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callUpdateReservationApi = (user, reservation) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL,
            updateReservationRequest(user, reservation),
            { headers: API_HEADERS }
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callGetReservationsListApi = (user) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL,
            getReservationListRequest(user),
            { headers: API_HEADERS }
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callGetReservationsDetailApi = (reservationId) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL,
            getReservationDetailRequest(reservationId),
            { headers: API_HEADERS }
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callGetContractDetailApi = (contractId) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL,
            getContractDetailRequest(contractId),
            { headers: API_HEADERS }
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callCancelReservationApi = (reservationId, isConfirmed) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL,
            cancelReservationRequest(reservationId, isConfirmed),
            { headers: API_HEADERS }
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callApplyCouponCode = (couponCode, userId, reservation) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL,
            applyCouponCodeRequest(couponCode, userId, reservation),
            { headers: API_HEADERS }
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}