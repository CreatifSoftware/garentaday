import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { RegisterStepOne } from '../containers/auth/RegisterStepOne'
import { RegisterStepThree } from '../containers/auth/RegisterStepThree'
import { RegisterStepTwo } from '../containers/auth/RegisterStepTwo'
import { hp } from '../styles/Dimens'
import { RentalTerms } from '../containers/account/RentalTerms'

const Stack = createStackNavigator()

export function SignupStack() {
    return (
        <Stack.Navigator
            initialRouteName={'RegisterStepOne'}
            screenOptions={{
                headerStyle: {
                    backgroundColor: 'transparent',
                },
                headerTintColor: '#fff',
                headerTitle: null,
                headerTitleStyle:{
                    fontFamily:'NunitoSans-Bold',
                    fontSize:hp(17)
                },
                headerShown: true
            }}>

            <Stack.Screen name="RegisterStepOne" component={RegisterStepOne}/>
            <Stack.Screen name="RegisterStepTwo" component={RegisterStepTwo} />
            <Stack.Screen name="RegisterStepThree" component={RegisterStepThree} />
            <Stack.Screen name="RentalTerms" component={RentalTerms} />
        </Stack.Navigator>
    )
}