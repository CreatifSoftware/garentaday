import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { getStatusBarHeight, hp } from '../styles/Dimens'
import { CampaignList } from '../containers/campaigns/CampaignList'
import { CampaignDetail } from '../containers/campaigns/CampaignDetail'
import { getFocusedRouteNameFromRoute } from '@react-navigation/native'
import { GradientHeader } from '../components/views/GradientHeader'
import { useDispatch } from 'react-redux'
import { ReservationStack } from './ReservationStack'
import { useSafeAreaInsets } from 'react-native-safe-area-context'

const Stack = createStackNavigator()

export function CampaignStack({ route, navigation }) {

    const dispatch = useDispatch()

    React.useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);
        if (routeName === undefined || routeName === "CampaignList") {
            navigation.setOptions({ tabBarVisible: true });
        } else {
            navigation.setOptions({ tabBarVisible: false });
        }
    }, [navigation, route]);

    const safeInset = useSafeAreaInsets()

    return (
        <Stack.Navigator
            initialRouteName={'CampaignList'}
            screenOptions={{
                headerBackground: () => <GradientHeader />,
                headerStyle: {
                    height: hp(68) + getStatusBarHeight(),
                    backgroundColor: 'transparent',
                },
                headerTintColor: '#fff',
                headerTitle: null,
                headerTitleStyle:{
                    fontFamily:'NunitoSans-Bold',
                    fontSize:hp(17)
                },
                headerShown: true,
            }}>

            <Stack.Screen name="CampaignList" component={CampaignList} options={{
                headerTitle: 'Kampanyalar',
            }} />
            <Stack.Screen name="CampaignDetail" component={CampaignDetail} options={{
                headerTitle: 'Kampanya Detayı',
            }} />
            <Stack.Screen name="ReservationStack" component={ReservationStack} options={{
                headerShown: false
            }} />
        </Stack.Navigator>
    )
}