import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { SplashNavigators } from './SplashNavigators';
import Spinner from 'react-native-loading-spinner-overlay';
import RNBootSplash from "react-native-bootsplash";

export const Container = () => {
    const [visible, setVisible] = useState(false)
    const loading = useSelector(state => state.masterReducer.loading)

    useEffect(() => {
        setVisible(loading)
    }, [loading]);

    return (
        <NavigationContainer onReady={() => RNBootSplash.hide()}>
            <Spinner
                visible={visible}
            />
            
            <SplashNavigators />
        </NavigationContainer>
    )
}