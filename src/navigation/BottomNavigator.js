import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Colors } from '../styles/Colors'
import { Image, StyleSheet, View, Platform } from 'react-native'
import { Test } from '../components/Test'
import { hp, wp, getStatusBarHeight } from '../styles/Dimens'
import { AccountStack } from './AccountStack'
import { HomeStack } from './HomeStack'
import { ReservationListTabNavigator } from './ReservationListTabNavigator'
import { CampaignStack } from './CampaignStack'
import { UpdateReservationStack } from './UpdateReservationStack'

const Tab = createBottomTabNavigator()
export function BottomNavigator() {

    return (
        <Tab.Navigator
            initialRouteName="HomeStack"
            tabBarOptions={{

                style: {
                    height: hp(63) + getStatusBarHeight()
                },
                tabStyle: {
                    paddingVertical: 8,
                },
                labelStyle: {
                    fontSize: 12,
                    fontFamily: 'NunitoSans-Bold'
                },
                activeTintColor: Colors.primaryBrand,
                inactiveTintColor: Colors.primaryTextV4,
            }}>
            <Tab.Screen name="HomeStack" component={HomeStack} options={{
                tabBarLabel: 'Ana Sayfa',
                tabBarIcon: ({ color }) => (
                    <View style={color === Colors.primaryBrand ? styles.iconContainer : styles.icon}>
                        <Image source={require('../assets/icons/ic_tab_home.png')} style={{ tintColor: color === Colors.primaryBrand ? 'white' : Colors.primaryTextV4 }} />
                    </View>

                )
            }} />
            <Tab.Screen name="ReservationListStack" component={UpdateReservationStack} options={{
                tabBarLabel: 'Rezervasyonlar',
                tabBarIcon: ({ color }) => (
                    <View style={color === Colors.primaryBrand ? styles.iconContainer : styles.icon}>
                        <Image source={require('../assets/icons/ic_tab_reservations.png')} style={{ tintColor: color === Colors.primaryBrand ? 'white' : Colors.primaryTextV4 }} />
                    </View>

                )
            }} />
            {/* listeners={() => getReservations()} /> */}

            <Tab.Screen name="CampaignStack" component={CampaignStack} options={{
                tabBarLabel: 'Kampanyalar',
                tabBarIcon: ({ color }) => (
                    <View style={color === Colors.primaryBrand ? styles.iconContainer : styles.icon}>
                        <Image source={require('../assets/icons/ic_tab_campaign.png')} style={{ tintColor: color === Colors.primaryBrand ? 'white' : Colors.primaryTextV4 }} />
                    </View>
                )
            }} />
            <Tab.Screen name="AccountStack" component={AccountStack} options={{
                tabBarLabel: 'Hesabım',
                tabBarIcon: ({ color }) => (
                    <View style={color === Colors.primaryBrand ? styles.iconContainer : styles.icon}>
                        <Image source={require('../assets/icons/ic_tab_profile.png')} style={{ tintColor: color === Colors.primaryBrand ? 'white' : Colors.primaryTextV4 }} />
                    </View>
                )
            }} />

        </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    iconContainer: {
        width: hp(40), height: hp(40), backgroundColor: Colors.primaryBrand, borderRadius: hp(10), alignItems: 'center', justifyContent: 'center', marginBottom: 5
    },
    icon: {
        width: hp(24), height: hp(24), backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center'
    }
})