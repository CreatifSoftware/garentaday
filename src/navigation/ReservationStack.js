import React, { useLayoutEffect } from 'react'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import { HomeScreen } from '../containers/home/HomeScreen'
import { DateAndBranchSelection } from '../containers/home/DateAndBranchSelection'
import { getStatusBarHeight, hp } from '../styles/Dimens'
import { getFocusedRouteNameFromRoute } from '@react-navigation/native'
import { BranchSelection } from '../containers/home/BranchSelection'
import { DateTimeSelection } from '../containers/home/DateTimeSelection'
import { CarGroupList } from '../containers/home/CarGroupList'
import { BackButton } from '../components/buttons/BackButton'
import { AdditionalProducts } from '../containers/home/AdditionalProducts'
import { AdditionalServices } from '../containers/home/AdditionalServices'
import { CarGroupDetail } from '../containers/home/CarGroupDetail'
import { CarGroupDetailUpdate } from '../containers/reservations/CarGroupDetailUpdate'
import { GradientHeader } from '../components/views/GradientHeader'
import { AddressSelection } from '../containers/home/AddressSelection'
import { AddressInformation } from '../components/views/AddressInformation'
import { ReservationCreate } from '../containers/home/ReservationCreate'
import { ReservationSummary } from '../containers/home/ReservationSummary'
import { HeaderCancelButton } from '../components/buttons/HeaderCancelButton'
import { StatusBar } from 'react-native'
import { LoginStack } from './LoginStack'
import { NearbyBranchSelection } from '../containers/home/NearbyBranchSelection'
import { RentalTerms } from '../containers/account/RentalTerms'

const Stack = createStackNavigator()
const RootStack = createStackNavigator()

export function ReservationStack2({ route, navigation }) {
    return (
        <Stack.Navigator
            initialRouteName='DateAndBranchSelection'
            screenOptions={{
                headerBackground: () => <GradientHeader />,
                headerLeft: props => <BackButton navigation={navigation} {...props} />,
                headerRight: () => <HeaderCancelButton navigation={navigation} route={route} title={"Araç kiralama işleminden çıkış yapmak istediğinize emin misiniz ?"}/>,
                headerStyle: {
                    height: hp(68) + getStatusBarHeight(),
                    backgroundColor: 'transparent',
                },
                headerTintColor: '#fff',
                headerTitle: null,
                headerTitleStyle:{
                    fontFamily:'NunitoSans-Bold',
                    fontSize:hp(17)
                },
                headerShown: true,
            }}>
            <Stack.Screen name="DateAndBranchSelection" component={DateAndBranchSelection} options={{
                headerTitle: 'Araç Bul & Kirala'
            }} />
            <Stack.Screen name="BranchSelection" component={BranchSelection} />
            <Stack.Screen name="NearbyBranchSelection" component={NearbyBranchSelection} />
            <Stack.Screen name="DateTimeSelection" component={DateTimeSelection} options={{
                headerTitle: 'Tarih ve Saat Seçimi'
            }} />
            <Stack.Screen name="CarGroupList" component={CarGroupList} />
            <Stack.Screen name="CarGroupDetail" component={CarGroupDetail}/>
            <Stack.Screen name="CarGroupDetailUpdate" component={CarGroupDetailUpdate} />
            <Stack.Screen name="AdditionalServices" component={AdditionalServices} />
            <Stack.Screen name="AdditionalProducts" component={AdditionalProducts} />
            <Stack.Screen name="AddressSelection" component={AddressSelection} />
            <Stack.Screen name="AddressInformation" component={AddressInformation} options={{
                headerTitle: 'Adreslerim',
            }} />
            <Stack.Screen name="ReservationCreate" component={ReservationCreate} />
            <Stack.Screen name="RentalTerms" component={RentalTerms} />
            <Stack.Screen name="ReservationSummary" component={ReservationSummary} options={{
                headerTitle: 'Rezervasyon Özeti',
            }} />
        </Stack.Navigator>
    )
}

export function ReservationStack({route, navigation}) {
    useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);
        if (routeName === undefined || routeName === "Home"){
            navigation.setOptions({tabBarVisible: true});
        }else {
            navigation.setOptions({tabBarVisible: false});
        }
    }, [navigation, route]);
    return (
        <RootStack.Navigator
            mode='modal'
            screenOptions={{
                header: props => <GradientHeader {...props} />,
                headerStyle: {
                    backgroundColor: 'transparent',
                },
                headerTintColor: '#fff',
                headerTitle: null,
                headerShown: false,
                gestureEnabled:true,
                gestureDirection:'vertical',
                cardStyle:{ backgroundColor:'transparent'},
                cardOverlayEnabled:true,
                ...TransitionPresets.ModalPresentationIOS
            }}>

            <RootStack.Screen name="Home" component={ReservationStack2} options={{
                headerShown:false
            }} />
            <RootStack.Screen name="LoginModal" component={LoginStack} options={{
                headerShown:false
            }}/>
        </RootStack.Navigator>
    )
}