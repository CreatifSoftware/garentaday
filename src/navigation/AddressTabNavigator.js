import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { InvoiceAddressList } from '../containers/account/profile/InvoiceAddressList'
import { PersonalAddressList } from '../containers/account/profile/PersonalAddressList'
import { INVOICEL_ADDRESS_LIST, PERSONAL_ADDRESS_LIST } from '../utilities/constants';
import { Colors, TextColors } from '../styles/Colors';
import { hp } from '../styles/Dimens';

const Tab = createMaterialTopTabNavigator();

export function AddressTabNavigator() {
    return (
        <Tab.Navigator
            initialRouteName={PERSONAL_ADDRESS_LIST}
            tabBarOptions={{
                activeTintColor: Colors.primaryBrand,
                inactiveTintColor: TextColors.primaryTextV3,
                labelStyle: { fontSize: hp(16), fontFamily: 'NunitoSans-SemiBold', textTransform: 'none' },
                indicatorStyle: { backgroundColor: Colors.primaryBrand, height: 4, borderTopEndRadius: 10, borderTopStartRadius: 10 },
                style: { backgroundColor: Colors.white, height: hp(50) },
            }}>

            <Tab.Screen name={PERSONAL_ADDRESS_LIST} component={PersonalAddressList} />
            <Tab.Screen name={INVOICEL_ADDRESS_LIST} component={InvoiceAddressList} />
        </Tab.Navigator>
    )
}