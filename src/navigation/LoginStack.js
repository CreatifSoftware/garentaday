import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Login } from '../containers/auth/Login'
import { getStatusBarHeight, hp } from '../styles/Dimens'
import { BottomNavigator } from './BottomNavigator'
import { ForgotPassword } from '../containers/auth/ForgotPassword'
import { ConfirmationCode } from '../containers/auth/ConfirmationCode'
import { SetPassword } from '../containers/auth/SetPassword'
import { RegisterStepOne } from '../containers/auth/RegisterStepOne'
import { RegisterStepTwo } from '../containers/auth/RegisterStepTwo'
import { RegisterStepThree } from '../containers/auth/RegisterStepThree'
import { GradientHeader } from '../components/views/GradientHeader'
import { BackButton } from '../components/buttons/BackButton'
import { useNavigation } from '@react-navigation/native'
import { RentalTerms } from '../containers/account/RentalTerms'

const Stack = createStackNavigator()

export function LoginStack() {
    const navigation = useNavigation()

    return (
        <Stack.Navigator
            initialRouteName={'Login'}
            screenOptions={{
                headerBackground: () => <GradientHeader />,
                headerLeft: props => <BackButton navigation={navigation} {...props} />,
                headerStyle: {
                    height: hp(68) + getStatusBarHeight(),
                    backgroundColor: 'transparent',
                },
                headerTintColor: '#fff',
                headerTitle: null,
                headerTitleStyle:{
                    fontFamily:'NunitoSans-Bold',
                    fontSize:hp(17)
                },
                headerShown: true
            }}>

            <Stack.Screen name="Login" component={Login} options={{
                headerShown:false
            }}/>
            <Stack.Screen name="BottomNavigator" component={BottomNavigator} options={{
                headerShown:false
            }}/>
            <Stack.Screen name="RegisterStepOne" component={RegisterStepOne}/>
            <Stack.Screen name="RegisterStepTwo" component={RegisterStepTwo} />
            <Stack.Screen name="RegisterStepThree" component={RegisterStepThree} />
            <Stack.Screen name="RentalTerms" component={RentalTerms} />
            <Stack.Screen name="ForgotPassword" component={ForgotPassword} options={{
                headerTitle: 'Şifremi Unuttum'
            }} />
            <Stack.Screen name="ConfirmationCode" component={ConfirmationCode} />
            <Stack.Screen name="SetPassword" component={SetPassword} options={{
                
                headerTitle:'Şifre Yenileme'
            }}/> 
        </Stack.Navigator>
    )
}