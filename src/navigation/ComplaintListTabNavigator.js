import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { ACTIVE_COMPLAINTS, COMPLETED_COMPLAINTS } from '../utilities/constants';
import { Colors, TextColors } from '../styles/Colors';
import { hp } from '../styles/Dimens';
import { GradientHeader } from '../components/views/GradientHeader';
import { View, Text, StyleSheet } from 'react-native';
import { ActiveComplaintList } from '../containers/account/complaints/ActiveComplaintList';
import { CompletedComplaintList } from '../containers/account/complaints/CompletedComplaintList';

const Tab = createMaterialTopTabNavigator();

export function ComplaintListTabNavigator() {

    return (
        <Tab.Navigator
                initialRouteName={ACTIVE_COMPLAINTS}
                tabBarOptions={{
                    activeTintColor: Colors.primaryBrand,
                    inactiveTintColor: TextColors.primaryTextV3,
                    labelStyle: { fontSize: hp(16), fontFamily: 'NunitoSans-SemiBold', textTransform: 'none' },
                    indicatorStyle: { backgroundColor: Colors.primaryBrand, height: 4, borderTopEndRadius: 10, borderTopStartRadius: 10 },
                    style: { backgroundColor: Colors.white, height: hp(50) },
                }}>

                <Tab.Screen name={ACTIVE_COMPLAINTS} component={ActiveComplaintList} />
                <Tab.Screen name={COMPLETED_COMPLAINTS} component={CompletedComplaintList} />
            </Tab.Navigator>

    )
}

const styles = StyleSheet.create({
    title: {
        position: 'absolute',
        alignSelf: 'center',
        bottom: hp(22),
        fontSize: hp(17),
        color: 'white',
        fontFamily: 'NunitoSans-Bold'
    }
})