import React, { useLayoutEffect } from 'react'
import { RegisterStepOne } from '../containers/auth/RegisterStepOne'
import { ConfirmationCode } from '../containers/auth/ConfirmationCode'
import { getStatusBarHeight, hp } from '../styles/Dimens'
import { RegisterStepThree } from '../containers/auth/RegisterStepThree'
import { RegisterStepTwo } from '../containers/auth/RegisterStepTwo'
import { AccountMenu } from '../containers/account/AccountMenu'
import { ProfileMenu } from '../containers/account/profile/ProfileMenu'
import { getFocusedRouteNameFromRoute, useNavigation } from '@react-navigation/native'
import { UpdateUserInformation } from '../containers/account/profile/UpdateUserInformation'
import { UpdateLicenseInformation } from '../containers/account/profile/UpdateLicenseInformation'
import { UserCreditCards } from '../containers/account/profile/UserCreditCards'
import { AddCreditCard } from '../containers/account/profile/AddCreditCard'
import { PasswordChange } from '../containers/account/profile/PasswordChange'
import { RentalTerms } from '../containers/account/RentalTerms'
import { CommunicationPref } from '../containers/account/settings/CommunicationPref'
import { SettingsMenu } from '../containers/account/settings/SettingsMenu'
import { BranchSelection } from '../containers/home/BranchSelection'
import { Notifications } from '../containers/account/Notifications'
import { BranchDetail } from '../containers/account/branchs/BranchDetail'
import { AddressTabNavigator } from './AddressTabNavigator'
import { BackButton } from '../components/buttons/BackButton'
import { AddressInformation } from '../components/views/AddressInformation'
import { GradientHeader } from '../components/views/GradientHeader'
import { InvoiceList } from '../containers/account/InvoiceList'
import { ComplaintListTabNavigator } from './ComplaintListTabNavigator'
import { ContactUsMenu } from '../containers/account/ContactUsMenu'
import { ReservationStack } from './ReservationStack'
import { ComplaintForm } from '../containers/account/complaints/ComplaintForm'
import { NearbyBranchSelection } from '../containers/home/NearbyBranchSelection'
import { createStackNavigator } from '@react-navigation/stack'

const Stack = createStackNavigator()

export function AccountStack({route}) {

    const navigation = useNavigation()
    useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);
        if (routeName === undefined || routeName === "AccountMenu"){
            navigation.setOptions({tabBarVisible: true});
        }else {
            navigation.setOptions({tabBarVisible: false});
        }
    }, [navigation, route]);

    return (
        <Stack.Navigator
            initialRouteName={'AccountMenu'}
            screenOptions={{
                headerBackground: () => <GradientHeader />,
                headerLeft: props => <BackButton navigation={navigation} {...props} />,
                headerStyle: {
                    height: hp(68) + getStatusBarHeight(),
                    backgroundColor: 'transparent',
                },
                headerTintColor: '#fff',
                headerTitle: null,
                headerShown: true
            }}>

            <Stack.Screen name="AccountMenu" component={AccountMenu} options={{
                headerShown: false
            }}/>
            <Stack.Screen name="ProfileMenu" component={ProfileMenu} options={{
                headerTitle: 'Profilim'
            }}/>
            <Stack.Screen name="InvoiceList" component={InvoiceList} options={{
                headerTitle: 'Faturalarım'
            }}/>
            <Stack.Screen name="ComplaintListTabNavigator" component={ComplaintListTabNavigator} options={{
                headerTitle: 'Şikayetlerim',
            }}/>
            <Stack.Screen name="UpdateUserInformation" component={UpdateUserInformation} options={{
                headerTitle: 'Üyelik Bilgileri'
            }}/>
            <Stack.Screen name="SettingsMenu" component={SettingsMenu} options={{
                headerTitle: 'Ayarlar'
            }}/>
            <Stack.Screen name="ContactUsMenu" component={ContactUsMenu} options={{
                headerTitle: 'İletişim'
            }}/>
            <Stack.Screen name="UpdateLicenseInformation" component={UpdateLicenseInformation} options={{
                headerTitle: 'Ehliyet Bilgileri'
            }}/>
            <Stack.Screen name="UserCreditCards" component={UserCreditCards} options={{
                headerTitle: 'Ödeme Araçlarım'
            }}/>
            <Stack.Screen name="AddCreditCard" component={AddCreditCard} options={{
                headerTitle: 'Kredi Kartı Ekle'
            }}/>
            <Stack.Screen name="PasswordChange" component={PasswordChange} options={{
                headerTitle: 'Şifre Değiştir'
            }}/>
            <Stack.Screen name="RentalTerms" component={RentalTerms} options={{
                headerTitle: 'Kiralama Koşulları'
            }}/>
            <Stack.Screen name="BranchSelection" component={BranchSelection} options={{
                headerTitle: 'Şube Listesi'
            }}/>
            <Stack.Screen name="NearbyBranchSelection" component={NearbyBranchSelection} options={{
                headerTitle: 'Yakınımdaki Şubeler'
            }}/>
            <Stack.Screen name="BranchDetail" component={BranchDetail} options={{
                headerTitle: 'Şube Detayı'
            }}/>
            <Stack.Screen name="CommunicationPref" component={CommunicationPref} options={{
                headerTitle: 'İletişim İzinleri'
            }}/>
            <Stack.Screen name="Notifications" component={Notifications} options={{
                headerTitle: 'Bildirimler'
            }}/>
            <Stack.Screen name="AddressTabNavigator" component={AddressTabNavigator} options={{
                headerTitle: 'Adreslerim',
            }}/>
            <Stack.Screen name="AddressInformation" component={AddressInformation} options={{
                headerTitle: 'Adreslerim',
            }}/>
            <Stack.Screen name="ComplaintForm" component={ComplaintForm} options={{
                headerTitle: 'Bize Ulaşın',
            }}/>
            <Stack.Screen name="RegisterStepOne" component={RegisterStepOne}/>
            <Stack.Screen name="RegisterStepTwo" component={RegisterStepTwo} />
            <Stack.Screen name="RegisterStepThree" component={RegisterStepThree}/>
            <Stack.Screen name="ConfirmationCode" component={ConfirmationCode} />
            <Stack.Screen name="ReservationStack" component={ReservationStack} options={{
                headerShown: false
            }}/>
        </Stack.Navigator>
    )
}