import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Splash } from '../containers/Splash'
import { LoginStack } from './LoginStack'
import { BottomNavigator } from './BottomNavigator'

const Stack = createStackNavigator()

export function SplashNavigators() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}>

            <Stack.Screen name="Splash" component={Splash} />
            <Stack.Screen name="LoginStack" component={LoginStack} />
            <Stack.Screen name="BottomNavigator" component={BottomNavigator} />
        </Stack.Navigator>
    )
}