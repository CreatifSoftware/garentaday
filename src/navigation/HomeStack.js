import React, { useLayoutEffect } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { HomeScreen } from '../containers/home/HomeScreen'
import { getStatusBarHeight, hp } from '../styles/Dimens'
import { getFocusedRouteNameFromRoute } from '@react-navigation/native'
import { BackButton } from '../components/buttons/BackButton'
import { GradientHeader } from '../components/views/GradientHeader'
import { ReservationStack } from './ReservationStack'
import { CampaignDetail } from '../containers/campaigns/CampaignDetail'

const Stack = createStackNavigator()

export function HomeStack({ route, navigation }) {
    useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);
        if (routeName === undefined || routeName === "Home") {
            navigation.setOptions({ tabBarVisible: true });
        } else {
            navigation.setOptions({ tabBarVisible: false });
        }
    }, [navigation, route]);

    return (
        <Stack.Navigator
            initialRouteName='Home'
            screenOptions={{
                headerBackground: () => <GradientHeader />,
                headerLeft: props => <BackButton navigation={navigation} {...props} />,
                headerStyle: {
                    height: hp(68) + getStatusBarHeight(),
                    backgroundColor: 'transparent',
                },
                headerTintColor: '#fff',
                headerTitleStyle:{
                    fontFamily:'NunitoSans-Bold',
                    fontSize:hp(17)
                },
                headerShown: false,
            }}>

            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="ReservationStack" component={ReservationStack} />
            <Stack.Screen name="CampaignDetail" component={CampaignDetail} options={{
                headerTitle: 'Kampanya Detayı',
                headerShown: true
            }}/>
        </Stack.Navigator>
    )
}