import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { getStatusBarHeight, hp } from '../styles/Dimens'
import { ReservationListTabNavigator } from './ReservationListTabNavigator'
import { ReservationDetail } from '../containers/reservations/ReservationDetail'
import { GradientHeader } from '../components/views/GradientHeader'
import { BackButton } from '../components/buttons/BackButton'
import { getFocusedRouteNameFromRoute, useNavigation } from '@react-navigation/native'
import { ReservationStack } from './ReservationStack'

const Stack = createStackNavigator()

export function UpdateReservationStack({ route, navigation }) {

    React.useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);
        if (routeName === undefined || routeName === "ReservationListTabNavigator") {
            navigation.setOptions({ tabBarVisible: true });
        } else {
            navigation.setOptions({ tabBarVisible: false });
        }
    }, [navigation, route]);

    return (
        <Stack.Navigator
            initialRouteName={'ReservationListTabNavigator'}
            screenOptions={{
                headerBackground: () => <GradientHeader />,
                headerLeft: props => <BackButton navigation={navigation} {...props} />,
                headerStyle: {
                    height: hp(68) + getStatusBarHeight(),
                    backgroundColor: 'transparent',
                },
                headerTintColor: '#fff',
                headerTitle: null,
                headerTitleStyle:{
                    fontFamily:'NunitoSans-Bold',
                    fontSize:hp(17)
                },
                headerShown: true
            }}>

            <Stack.Screen name="ReservationListTabNavigator" component={ReservationListTabNavigator} options={{
                headerTitle: 'Rezervasyonlar'
            }} />

            <Stack.Screen name="ReservationDetail" component={ReservationDetail}/>
            <Stack.Screen name="ReservationStack" component={ReservationStack} options={{
                headerShown: false
            }} />

        </Stack.Navigator>
    )
}