import React, { useLayoutEffect } from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { ACTIVE_RESERVATIONS, CANCELLED_RESERVATIONS, COMPLETED_RESERVATIONS } from '../utilities/constants';
import { Colors, TextColors } from '../styles/Colors';
import { hp } from '../styles/Dimens';
import { CompletedReservationList } from '../containers/reservations/CompletedReservationList';
import { CancelledReservationList } from '../containers/reservations/CancelledReservationList';
import { ActiveReservationList } from '../containers/reservations/ActiveReservationList';
import { GradientHeader } from '../components/views/GradientHeader';
import { View, Text, StyleSheet } from 'react-native';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { ReservationListResponse } from '../api/models/ReservationListResponse';
import { getReservationAction } from '../redux/actions/ReservationActions';
import { setLoadingAction } from '../redux/actions/MasterActions';
import { callGetReservationsListApi } from '../api/reservation/reservationService';

const Tab = createMaterialTopTabNavigator();

export function ReservationListTabNavigator() {

    const dispatch = useDispatch()
    const navigation = useNavigation()
    const user = useSelector(state => state.authReducer.user)
    const isFocused = useIsFocused()

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: null
        })
    }, [navigation]);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', e => {
            if (user) {
                getReservations()
            }
        });

        return unsubscribe;
    }, [isFocused]);

    const getReservations = () => {
        dispatch(setLoadingAction(true))
        callGetReservationsListApi(user)
            .then(response => {
                dispatch(setLoadingAction(false))
                dispatch(getReservationAction(ReservationListResponse(response.data.EXPORT)))
            })
            .catch(error => {

            })
    }

    return (
        <View style={{ flex: 1 }}>
            {/* <View style={{ height: hp(102), width: '100%', justifyContent: 'center', }}>
                <GradientHeader />
                <Text style={styles.title}>Rezervasyonlar</Text>
            </View> */}
            <Tab.Navigator
                initialRouteName={ACTIVE_RESERVATIONS}
                tabBarOptions={{
                    activeTintColor: Colors.primaryBrand,
                    inactiveTintColor: TextColors.primaryTextV3,
                    labelStyle: { fontSize: hp(16), fontFamily: 'NunitoSans-SemiBold', textTransform: 'none' },
                    indicatorStyle: { backgroundColor: Colors.primaryBrand, height: 4, borderTopEndRadius: 10, borderTopStartRadius: 10 },
                    style: { backgroundColor: Colors.white, height: hp(50) },
                }}>

                <Tab.Screen name={ACTIVE_RESERVATIONS} component={ActiveReservationList} />
                <Tab.Screen name={COMPLETED_RESERVATIONS} component={CompletedReservationList} />
                <Tab.Screen name={CANCELLED_RESERVATIONS} component={CancelledReservationList} />
            </Tab.Navigator>
        </View>

    )
}

const styles = StyleSheet.create({
    title: {
        position: 'absolute',
        alignSelf: 'center',
        bottom: hp(22),
        fontSize: hp(17),
        color: 'white',
        fontFamily: 'NunitoSans-Bold'
    }
})