import {
  widthPercentageToDP as wp2dp,
  heightPercentageToDP as hp2dp,
} from 'react-native-responsive-screen';

import { Dimensions, Platform, StatusBar } from "react-native";

/**
 * Width-Percentage
 * Converts width dimension to percentage
 * 360, 760 - design were made using this scale
 * @param dimension directly taken from design wireframes
 * @returns {string} percentage string e.g. '25%'
 */
export const wp = dimension => {
  return wp2dp((dimension / 375) * 100 + '%');
};

/**
 * Height-Percentage
 * Converts width dimension to percentage
 * * 360, 760 - design were made using this scale
 * @param dimension directly taken from design wireframes
 * @returns {string} percentage string e.g. '25%'
 */
export const hp = dimension => {
  // return dimension
  return hp2dp((dimension / 812) * 100 + '%');
}


export const getStatusBarHeight = () => {
  const STATUSBAR_DEFAULT_HEIGHT = 20;
  const STATUSBAR_X_HEIGHT = 44;
  const STATUSBAR_IP12_HEIGHT = 47;
  const STATUSBAR_IP12MAX_HEIGHT = 47;

  const X_WIDTH = 375;
  const X_HEIGHT = 812;

  const XSMAX_WIDTH = 414;
  const XSMAX_HEIGHT = 896;

  const IP12_WIDTH = 390;
  const IP12_HEIGHT = 844;

  const IP12MAX_WIDTH = 428;
  const IP12MAX_HEIGHT = 926;

  const { height: W_HEIGHT, width: W_WIDTH } = Dimensions.get("window");

  let statusBarHeight = STATUSBAR_DEFAULT_HEIGHT;

  if (Platform.OS === "ios" && !Platform.isPad && !Platform.isTVOS) {
    if (W_WIDTH === X_WIDTH && W_HEIGHT === X_HEIGHT) {
      isIPhoneWithMonobrow_v = true;
      statusBarHeight = STATUSBAR_X_HEIGHT;
    } else if (W_WIDTH === XSMAX_WIDTH && W_HEIGHT === XSMAX_HEIGHT) {
      statusBarHeight = STATUSBAR_X_HEIGHT;
    } else if (W_WIDTH === IP12_WIDTH && W_HEIGHT === IP12_HEIGHT) {
      statusBarHeight = STATUSBAR_IP12_HEIGHT;
    } else if (W_WIDTH === IP12MAX_WIDTH && W_HEIGHT === IP12MAX_HEIGHT) {
      statusBarHeight = STATUSBAR_IP12MAX_HEIGHT;
    }
  }

  return Platform.select({
    ios: statusBarHeight,
    android: StatusBar.currentHeight,
    default: 0,
  });
}