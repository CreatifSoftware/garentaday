import { StyleSheet } from "react-native";
import { TextColors, Colors } from "./Colors";
import { getStatusBarHeight, hp, wp } from "./Dimens";

export const Styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    rowContainer: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between'
    },
    title: {
        fontSize: hp(16),
        color: TextColors.primaryText,
        fontWeight:'700'
    },
    content: {
        fontSize: hp(14),
        color: TextColors.primaryTextV2,
    },
    shadowBox: {
        width: wp(375),
        minHeight: hp(62) + getStatusBarHeight(),
        // backgroundColor: Colors.mainBackground,
        backgroundColor: 'white',
        position: 'relative',
        bottom: 0,
        // add shadows for iOS only
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.15,
        elevation: 4,
    },
    draggableIcon:{
        width:wp(70),
        height:5,
        borderRadius:2.5,
        backgroundColor: 'gray',
    },
    modalContainer:{
        borderTopStartRadius:hp(24),
        borderTopEndRadius:hp(24)
    },
    continueButton: {
        width: wp(335),
        marginHorizontal: wp(20),
        marginTop: hp(16),
    },
}) 