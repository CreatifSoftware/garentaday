export const Colors = {
    primaryBrand: '#FF5000',
    primaryBrandV2: '#FE7333',
    primaryBrandV3: '#FE9666',
    primaryBrandV4: '#FFB999',
    primaryBrandV5: '#FFDCCC',
    white:'#FFFFFF',
    black:'#000000',
    green: '#2ac769',
    mainBackground:'#f7f7f7',
    checkboxBorder:'#BDBDBD',
    counterBackgorund:'#e0e0e0',
    disabledBackground:'#e0e0e0',
    seperatorColor:'#E0E0E0',
    seperatorColorV2:'#F5F5F5',
    borderColor:'#e0e0e0',
}

export const TextColors = {
    primaryText:'#333333',
    primaryTextV2:'#4F4F4F',
    primaryTextV3:'#828282',
    primaryTextV4:'#BDBDBD',
    primaryTextV5:'#E0E0E0',
    primaryTextV6:'#F5F5F5',
    primaryTextV7:'#999999',
    errorText:'#ff0000',
}