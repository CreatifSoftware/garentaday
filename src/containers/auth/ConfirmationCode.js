import { CommonActions, useNavigation } from '@react-navigation/native'
import axios from 'axios'
import React, { useCallback, useEffect, useLayoutEffect, useState } from 'react'
import { View, StyleSheet, Text, Alert } from "react-native"
import { useDispatch, useSelector } from 'react-redux'
import { getErrorMessage, isSuccess } from '../../api/apiHelpers'
import { callCheckGeneralOtpApi, callCheckLoginOtpApi, callCreateUserPermission, callForgotPasswordSendSMSApi, callSendPermissionSMS, callSignInApi, callSignUpApi, callValidateSMS } from '../../api/auth/authServices'
import { GetProfileResponse } from '../../api/models/GetProfileResponse'
import { LoginResponse } from '../../api/models/LoginResponse'
import { callGetProfileInfoApi, callUpdateUserApi } from '../../api/user/userService'
import { FilledButton } from '../../components/buttons/FilledButton'
import { HeaderCancelButton } from '../../components/buttons/HeaderCancelButton'
import { TextButton } from '../../components/buttons/TextButton'
import { StepHeader } from '../../components/headers/StepHeader'
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput'
import { VerticalSeperator } from '../../components/VerticalSeperator'
import { setTempUserAction, setUserAction } from '../../redux/actions/AuthActions'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { encodeUTF16LE, generateCode, saveJsonData } from '../../utilities/helpers'

export const ConfirmationCode = ({ route }) => {
    const dispatch = useDispatch()
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const [generatedCode, setGeneratedCode] = useState(null)
    const [confirmationCode, setConfirmationCode] = useState('')
    const [timeLeft, setTimeLeft] = useState(120);
    const [disableInput, setDissableInput] = useState(false);
    const [remainingCount, setRemainingCount] = useState(5);
    const { mobileConfirmation, emailConfirmation, mobile, email, isFromRegister, user, username, password, checkOtpFromLogin, rememberMe, isFromUpdate, smsSent } = route.params;
    const navigation = useNavigation()
    // const user = useSelector(state => state.authReducer.user)

    useEffect(() => {
        if (!smsSent) {
            setTimeout(() => {
                sendConfirmationCode()
            }, 1000);
        }
    }, []);

    useEffect(() => {
        if (!timeLeft) return;
        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 1);
            if (timeLeft == 0) {
                setDissableInput(true)
            }
        }, 1000);

        return () => clearInterval(intervalId);
    }, [timeLeft]);

    useLayoutEffect(() => {
        if (isFromRegister) {
            navigation.setOptions({
                headerShown: true,
                headerRight: () => <HeaderCancelButton navigation={navigation} title={"Kayıt olma işleminden çıkış yapmak istediğinize emin misiniz ?"} />
            })
        } else {
            navigation.setOptions({
                headerTitle: isFromUpdate ? 'Profil Güncelleme' : checkOtpFromLogin ? 'Üye Girişi' : isFromRegister ? '' : 'Şifremi Unuttum',
            })
        }
    }, [navigation, timeLeft]);

    const sendConfirmationCode = () => {
        setDissableInput(false)
        setTimeLeft(120)
        setRemainingCount(5)
        setConfirmationCode('')

        if (mobileConfirmation) {
            if (checkOtpFromLogin) {
                callSmsApiForLogin()
            } else if (isFromRegister) {
                callPermissionSMSForRegister()
            } else {
                callSmsApiForConfirmation()
            }
        }
        if (emailConfirmation) {
            // callSMSApi()
            callSendEmailApi()
        }
    }

    const callSendEmailApi = () => {
        let xmls = `<SendForgotPasswordMail xmlns=http://tempuri.org/>\
                    <toMail>${email}</toMail>\
                    <name>Garenta</name>\
                    <surname>Müşterisi</surname>\
                    <generatedCode>123456</generatedCode>\
                </SendForgotPasswordMail>`;

        axios.post('http://mobil.garenta.com.tr/mail/Mail.asmx',
            xmls,
            {
                headers:
                    { 'Content-Type': 'text/xml' }
            }).then(res => {

            }).catch(err => {

            });
    }

    const saveLoginInfo = async (user) => {
        let loginInfo = { rememberMe: rememberMe, username: username, password: password, isLoggedIn: true, user: user }
        saveJsonData('loginInfo', loginInfo)
    }

    const getProfileInfo = (user) => {
        callGetProfileInfoApi(user.userId)
            .then(response => {
                if (password) {
                    user.password = encodeUTF16LE(password)
                }

                dispatch(setLoadingAction(false))
                let userObj = GetProfileResponse(response.data.EXPORT, user)
                dispatch(setUserAction(userObj))
                saveLoginInfo(userObj)
                if (reservation.pickupBranch) {
                    navigation.navigate('CarGroupDetail');
                } else {
                    navigateHome()
                }
            })
            .catch(error => {
                dispatch(setUserAction(null))
                dispatch(setLoadingAction(false))
                Alert.alert('', error.message)
            })
    }

    const applyRemainingCount = () => {
        if (remainingCount != 0) {
            setRemainingCount(remainingCount - 1)
        }

        if (remainingCount - 1 == 0) {
            setDissableInput(true)
            setConfirmationCode('')
        }
    }

    const handleOnContinueButton = () => {
        if (checkOtpFromLogin) {
            dispatch(setLoadingAction(true))
            callSignInApi(username, password, confirmationCode)
                .then(response => {
                    if (isSuccess(response)) {
                        let user = LoginResponse(response.data.EXPORT)
                        // dispatch(getFavoriteBranchsAction(user.userId))
                        getProfileInfo(user)
                    } else {
                        applyRemainingCount()
                        dispatch(setUserAction(null))
                        dispatch(setLoadingAction(false))
                        Alert.alert('', getErrorMessage(response))
                    }
                })
                .catch(error => {
                    applyRemainingCount()
                    dispatch(setUserAction(null))
                    dispatch(setLoadingAction(false))
                    Alert.alert('', error.message)
                })
        } else if (isFromRegister) {
            if (user.permissionState == 1) {
                dispatch(setLoadingAction(true))
                callValidateSMS(user, "", confirmationCode)
                    .then(response => {
                        // dispatch(setLoadingAction(false))
                        if (isSuccess(response)) {
                            signup()
                        } else {
                            applyRemainingCount()
                            dispatch(setLoadingAction(false))
                            Alert.alert("Uyarı", getErrorMessage(response))
                        }
                    })
                    .catch(error => {
                        applyRemainingCount()
                        dispatch(setLoadingAction(false))
                        Alert.alert('', error.message)
                    })
            } else {
                dispatch(setLoadingAction(true))
                signup()
            }

        } else {
            checkGeneralOtpApi()
        }
    }

    const signup = () => {
        // dispatch(setLoadingAction(true))
        callSignUpApi(user)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    user.userId = response.data.EXPORT.ES_RETURN.BP_NUMBER
                    getProfileInfo(user)
                } else {
                    Alert.alert('', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('', error.message)
            })
    }

    const createPermission = () => {
        callCreateUserPermission(user, "ZMB")
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    if (reservation.pickupBranch) {
                        navigation.navigate('CarGroupDetail');
                    } else {
                        navigateHome()
                    }
                } else {
                    Alert.alert('', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('', error.message)
            })
    }

    const navigateHome = () => {
        const homeAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'BottomNavigator' }]
        })
        navigation.dispatch(homeAction)
    }

    const callSmsApiForConfirmation = async () => {
        //const code = generateCode()
        //setGeneratedCode(code)
        callForgotPasswordSendSMSApi(mobile.code, mobile.number)
            .then(response => {

            })
            .catch(error => {
                Alert.alert('Hata', error.message)
            })
    }

    const checkGeneralOtpApi = () => {
        dispatch(setLoadingAction(true))
        callCheckGeneralOtpApi(mobile.code, mobile.number, confirmationCode)
            .then(response => {
                if (isSuccess(response)) {
                    if (isFromUpdate) {
                        callUpdateUserApi(user)
                            .then(response => {
                                dispatch(setLoadingAction(false))
                                dispatch(setUserAction(user))
                                if (isSuccess(response)) {
                                    Alert.alert(
                                        "Üyelik Bilgileri",
                                        "Bilgileriniz başarılı bir şekilde değiştirildi.",
                                        [
                                            { text: "Tamam", onPress: () => navigation.navigate('ProfileMenu') }
                                        ],
                                        { cancelable: false }
                                    );
                                } else {
                                    Alert.alert('Hata', getErrorMessage(response))
                                }
                            })
                            .catch(error => {
                                dispatch(setLoadingAction(false))
                                Alert.alert('Hata', error.message)
                            })
                    } else {
                        dispatch(setLoadingAction(false))
                        navigation.navigate('SetPassword', {
                            email: email,
                            mobile: mobile,
                            mobileConfirmation: mobileConfirmation,
                            emailConfirmation: emailConfirmation,
                        })
                    }
                } else {
                    applyRemainingCount()
                    dispatch(setLoadingAction(false))
                    Alert.alert('Hata', getErrorMessage(response))
                }

            })
            .catch(error => {
                applyRemainingCount()
                Alert.alert('Hata', error.message)
            })
    }

    const callSmsApiForLogin = () => {
        callCheckLoginOtpApi(username, password)
            .then(response => {
                console.log(response.data)
            })
            .catch(error => {
                console.log(error)
            })
    }

    const callPermissionSMSForRegister = () => {
        callSendPermissionSMS(user)
            .then(response => {
                console.log(response.data)
            })
            .catch(error => {
                console.log(error)
            })
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: 'white'
        }}>
            {
                isFromRegister &&
                <StepHeader
                    stepCount={4}
                    step={4}
                    title={'Üye Ol'} />
            }

            {
                mobileConfirmation
                    ?
                    <Text style={styles.infoText}>Lütfen +{mobile.code} {mobile.number} numaralı cep telefonuna mesaj olarak gönderdiğimiz doğrulama kodunu aşağıya girin.</Text>
                    :
                    <Text style={styles.infoText}>Lütfen {email} adlı e-posta adresinize mail olarak gönderdiğimiz doğrulama kodunu aşağıya girin.</Text>
            }

            <FloatingTextInput
                label={'Doğrulama Kodu'}
                placeholder={'Doğrulama Kodu'}
                disabled={disableInput}
                autoFocus={true}
                onChangeText={text => setConfirmationCode(text)}
                keyboardType='number-pad'
                textContentType="oneTimeCode"
                maxLength={8}
                value={confirmationCode} />

            <Text style={styles.remainingCount}>Kalan Hakkınız: {remainingCount}</Text>

            <View style={styles.counter}>
                <Text style={styles.remainingTime}>{timeLeft} sn</Text>
            </View>

            <View style={styles.resendContainer}>
                <Text style={styles.resendTitle}>Kod ulaşmadı mı?</Text>
                <VerticalSeperator style={styles.seperator} />
                <TextButton disabled={(remainingCount > 0 && timeLeft > 0)}
                    style={(remainingCount > 0 && timeLeft > 0) ? styles.resendButtonDisable : styles.resendButton}
                    title='Tekrar Gönder'
                    onPress={sendConfirmationCode} />
            </View>

            <FilledButton
                disabled={confirmationCode.length == 0}
                style={styles.continueButton}
                onPress={handleOnContinueButton}
                title={'Devam Et'} />
        </View>
    )
}

const styles = StyleSheet.create({
    seperator: {
        height: hp(24),
        marginHorizontal: wp(14)
    },
    resendContainer: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: hp(20)
    },
    resendTitle: {
        color: TextColors.primaryTextV4,
        alignSelf: 'center',
        fontSize: hp(14),
    },
    resendButton: {
        color: Colors.primaryBrand,
        alignSelf: 'center',
    },
    resendButtonDisable: {
        color: Colors.borderColor,
        alignSelf: 'center',
    },
    counter: {
        width: wp(107),
        height: hp(35),
        backgroundColor: Colors.counterBackgorund,
        borderRadius: hp(10),
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp(36)
    },
    remainingTime: {
        color: TextColors.primaryText,
        fontSize: hp(20),
        fontFamily: 'NunitoSans-Bold',
    },
    remainingCount: {
        color: Colors.primaryBrand,
        fontSize: hp(16),
        alignSelf: 'center',
        marginTop: hp(18),
        fontFamily: 'NunitoSans-Bold',
    },
    confirmationCode: {
        marginHorizontal: wp(20),
        marginTop: hp(18),
        backgroundColor: 'transparent'
    },
    infoText: {
        color: TextColors.primaryText,
        fontSize: hp(16),
        marginHorizontal: wp(20),
        marginTop: hp(20),
    },
    continueButton: {
        width: wp(335),
        position: 'absolute',
        bottom: hp(44),
        marginHorizontal: wp(20)
    }
})