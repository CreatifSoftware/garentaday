import { useNavigation } from '@react-navigation/native'
import React, { useState, useLayoutEffect, useRef } from 'react'
import { View, StyleSheet, Text, Alert, ScrollView, SafeAreaView } from "react-native"
import { callCheckMernisApi } from '../../api/auth/authServices'
import { FilledButton } from '../../components/buttons/FilledButton'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { StepHeader } from '../../components/headers/StepHeader';
import { FloatingSelectionInput } from '../../components/inputs/FloatingSelectionInput';
import { RadioButtonView } from '../../components/buttons/RadioButtonView';
import { BackButton } from '../../components/buttons/BackButton';
import { useDispatch } from 'react-redux';
import { setUserAction } from '../../redux/actions/AuthActions';
import { PreviousNextView } from 'react-native-keyboard-manager';
import RBSheet from "react-native-raw-bottom-sheet";
import { CountrySelectionModal } from '../modals/CountrySelectionModal';
import { getErrorMessage, isSuccess } from '../../api/apiHelpers';
import { setLoadingAction } from '../../redux/actions/MasterActions';
import { UserInformation } from '../../components/views/UserInformation'

import moment from 'moment'
import { getDateErrorText } from '../../utilities/helpers'
import { Styles } from '../../styles/Styles'

export const RegisterStepOne = () => {
    const [isTurkishCitizen, setIsTurkishCitizen] = useState(true)
    const [govermentId, setGovermentId] = useState('')
    const [passport, setPassport] = useState('')
    const [nationality, setNationality] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [dateOfBirth, setDateOfBirth] = useState('')
    const [gender, setGender] = useState(null)
    const [govermentError, setGovermentError] = useState(null)
    const [dateOfBirthError, setDateOfBirthError] = useState(null)

    const [buttonDisabled, setButtonDisabled] = useState(false);
    const navigation = useNavigation()
    const dispatch = useDispatch()
    const refRBSheet = useRef();

    // const user = useSelector(state => state.authReducer.user)

    useLayoutEffect(() => {
        setContinueButtonState()
    }, [navigation, govermentId, passport, firstName, lastName, gender, dateOfBirth]);

    const setContinueButtonState = () => {
        setButtonDisabled(false)
        let govError = null
        let birthdateError = getDateErrorText(dateOfBirth)

        if (isTurkishCitizen) {
            if (govermentId.length === 0 || govermentId.length === 11) {
                govError = null
            } else {
                govError = 'T.C. Kimlik Numarası 11 hane olmalıdır'
            }
        }

        if (((isTurkishCitizen && !govError && govermentId.length === 11) ||
            (!isTurkishCitizen && passport.length > 0)) &&
            firstName.length > 0 &&
            lastName.length > 0 &&
            gender &&
            dateOfBirth.length === 10 &&
            !birthdateError) {
            setButtonDisabled(false)
            setGovermentError(null)
            setDateOfBirthError(null)
        } else {
            setGovermentError(govError)
            setDateOfBirthError(birthdateError)
            setButtonDisabled(true)
        }
    }

    const handleOnCountrySelection = (item) => {
        setNationality(item)
        refRBSheet.current.close()
    }

    const handleOnContinueButton = () => {
        // dispatch(setLoadingAction(true))
        let user = { personalInfo: {} }
        user.personalInfo.firstName = firstName
        user.personalInfo.lastName = lastName
        user.personalInfo.gender = gender
        user.personalInfo.nationality = nationality
        user.personalInfo.isTurkishCitizen = isTurkishCitizen
        user.personalInfo.govermentId = govermentId
        user.personalInfo.passport = passport
        user.personalInfo.dateOfBirth = dateOfBirth.split('/')[2] + dateOfBirth.split('/')[1] + dateOfBirth.split('/')[0]

        // navigateRegisterStepTwo(user)
        
        if (isTurkishCitizen) {
            checkMernis(user)
        }else{
            navigateRegisterStepTwo(user)
        }
    }

    const checkMernis = (user) => {
        dispatch(setLoadingAction(true))
        callCheckMernisApi(user)
            .then(response => {
                console.warn(response)
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    navigateRegisterStepTwo(user)
                } else {
                    Alert.alert('', response.data.EXPORT.ES_RETURN.MESSAGE)
                    // Alert.alert('', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('', error.message)
            })
    }

    const navigateRegisterStepTwo = (user) => {
        navigation.navigate('RegisterStepTwo', { user: user })
    }

    return (
        <View style={{ flex: 1 }}>
            <StepHeader
                stepCount={4}
                step={1}
                title={'Üye Ol'} />
            <ScrollView
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={styles.container}
                style={{ backgroundColor: 'white' }}>

                <View style={styles.nationalityGroup}>
                    <RadioButtonView label='T.C. Vatandaşı' selected={isTurkishCitizen} onPress={() => setIsTurkishCitizen(true)} />
                    <RadioButtonView label='Diğer' selected={!isTurkishCitizen} onPress={() => setIsTurkishCitizen(false)} />
                </View>
                <UserInformation
                    isTurkishCitizen={isTurkishCitizen}
                    setGovermentId={(text) => setGovermentId(text)}
                    showSecureGovermentId={false}
                    govermentId={govermentId}
                    govermentError={govermentError}
                    showCountryModal={() => refRBSheet.current.open()}
                    nationality={nationality && nationality.countryName}
                    setPassport={(text) => setPassport(text)}
                    passport={passport}
                    setFirstName={(text) => setFirstName(text)}
                    firstName={firstName}
                    setLastName={(text) => setLastName(text)}
                    lastName={lastName}
                    setDateOfBirth={(text) => setDateOfBirth(text)}
                    dateOfBirth={dateOfBirth}
                    dateOfBirthError={dateOfBirthError}
                    gender={gender}
                    setGender={(text) => setGender(text)} />

            </ScrollView>

            <View style={Styles.shadowBox} >
                <FilledButton
                    disabled={buttonDisabled}
                    style={Styles.continueButton}
                    onPress={handleOnContinueButton}
                    title={'Devam Et'} />
            </View>

            <RBSheet
                height={hp(718)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    container: {
                        borderRadius: hp(24),
                    }
                }}>
                <CountrySelectionModal onPress={handleOnCountrySelection} removeTurkey={true} current={nationality && nationality.countryName}/>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        paddingBottom: hp(20)
    },
    nationalityGroup: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between',
        marginTop: hp(24)
    },
    genderTitle: {
        fontFamily: 'NunitoSans-Bold',
        fontSize: hp(18),
        color: TextColors.primaryText,
        // marginTop: hp(36),
        marginTop: 36,
        marginStart: wp(20)
    },
    genderGroup: {
        marginHorizontal: wp(20),
    },
    genderRadio: {
        width: wp(335),
        marginTop: hp(10)
    },
    continueButton: {
        width: wp(335),
        marginTop: hp(16),
        marginHorizontal: wp(20)
    }
})