import { CommonActions, useNavigation } from '@react-navigation/native'
import React, { useState, useEffect, useLayoutEffect } from 'react'
import { View, StyleSheet, Text, Alert, ScrollView } from "react-native"
import { callCheckLoginOtpApi, callSignInApi } from '../../api/auth/authServices'
import { BorderedButton } from '../../components/buttons/BorderedButton'
import { FilledButton } from '../../components/buttons/FilledButton'
import { HorizontalSeperator } from '../../components/HorizontalSeperator'
import { LoginHeader } from '../../components/headers/LoginHeader'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import CheckBox from '@react-native-community/checkbox';
import { TextButton } from '../../components/buttons/TextButton'
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { setTempUserAction, setUserAction } from '../../redux/actions/AuthActions';
import { useDispatch, useSelector } from 'react-redux';
import { getErrorMessage, isSuccess } from '../../api/apiHelpers';
import { setLoadingAction } from '../../redux/actions/MasterActions';
import { LoginResponse } from '../../api/models/LoginResponse';
import { GetProfileResponse } from '../../api/models/GetProfileResponse';
import { callGetProfileInfoApi } from '../../api/user/userService';
import { encodeUTF16LE, getJsonData, saveJsonData } from '../../utilities/helpers';
import { getFavoriteBranchsAction } from '../../redux/actions/UserActions';
import { CheckOtpResponse } from '../../api/models/CheckOtpResponse'
import { clearReservationDataAction } from '../../redux/actions/ReservationActions'

const initialUser = {
    personalInfo: {},
    licenseInfo: {},
    contactInfo: {},
    permissionInfo: {},
    addressList: [],
    creditCards: []
}

export const Login = () => {

    const dispatch = useDispatch()
    const reservation = useSelector(state => state.reservationReducer.reservation)
    // const [username, setUsername] = useState('16918749184')
    // const [password, setPassword] = useState('123456')

    // const [username, setUsername] = useState('21647014274')
    // const [password, setPassword] = useState('123456')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [rememberMe, setRememberMe] = useState(false)
    const navigation = useNavigation()
    const [buttonDisabled, setButtonDisabled] = useState(true);
    const [secureEntry, setSecureEntry] = useState(true)
    const [isFromReservation, setIsFromReservation] = useState(false)

    useLayoutEffect(() => {
        (username.length > 0 && password.length > 0) ? setButtonDisabled(false) : setButtonDisabled(true);
    }, [username, password]);

    useEffect(() => {
        dispatch(setLoadingAction(false))
        // (username.length > 0 && password.length > 0) ? setButtonDisabled(false) : setButtonDisabled(true);
        getLoginInfo()
        getIsFromReservation()
    }, []);

    const getLoginInfo = async () => {
        getJsonData('loginInfo')
            .then(response => {
                if (response != null && response.rememberMe) {
                    setUsername(response.username)
                    setPassword(response.password)
                }
            })
    }

    const getIsFromReservation = async () => {
        getJsonData('isFromReservation')
            .then(response => {
                if (response != null) {
                    setIsFromReservation(response.isFromReservation)
                }
            })
    }

    // const saveLoginInfo = async () => {
    //     let loginInfo = { rememberMe: rememberMe, username: username, password: password, isLoggedIn: true }
    //     saveJsonData('loginInfo', loginInfo)
    // }

    const handleOnLoginButton = () => {
        dispatch(setLoadingAction(true))
        if (!isFromReservation) {
            dispatch(clearReservationDataAction())
        }
        callCheckLoginOtpApi(username, password)
            .then(response => {
                if (isSuccess(response)) {
                    dispatch(setLoadingAction(false))
                    let smsInfo = CheckOtpResponse(response.data.EXPORT)
                    navigation.navigate('ConfirmationCode', {
                        rememberMe: rememberMe,
                        username: username,
                        password: password,
                        mobile: smsInfo,
                        mobileConfirmation: true,
                        smsSent:false,
                        checkOtpFromLogin: true
                    })
                } else {
                    dispatch(setUserAction(null))
                    dispatch(setLoadingAction(false))
                    Alert.alert('', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setUserAction(null))
                dispatch(setLoadingAction(false))
                Alert.alert('', error.message)
            })
    }

    const navigateHome = () => {
        const homeAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'BottomNavigator' }]
        })
        navigation.dispatch(homeAction)
    }

    const handleOnContinueButton = () => {
        dispatch(setUserAction(null))
        navigateHome()
    }

    const handleOnRegisterButton = () => {
        let userObj = { personalInfo: {}, licenseInfo: {}, contactInfo: {} }
        dispatch(setTempUserAction(userObj))
        navigation.navigate('RegisterStepOne')
    }

    const handleOnForgotPasswordButton = () => {
        navigation.navigate('ForgotPassword')
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            display: 'flex'
        }}>
            <LoginHeader />
            <ScrollView contentContainerStyle={{ paddingBottom: 40 }}>
                <>
                    <FloatingTextInput
                        key={'login'}
                        label={'Cep Tel. No / T.C. Kimlik No'}
                        placeholder={'Cep Tel. No / T.C. Kimlik No'}
                        autoCapitalize='none'
                        keyboardType='email-address'
                        onChangeText={(text) => setUsername(text)}
                        value={username}
                    />

                    <FloatingTextInput
                        key={'password'}
                        label={'Şifreniz'}
                        placeholder={'Şifreniz'}
                        secureTextEntry={secureEntry}
                        iconSource={secureEntry ? require('../../assets/icons/ic_eye_close.png') : require('../../assets/icons/ic_eye.png')}
                        iconOnPress={(value) => setSecureEntry(!secureEntry)}
                        onChangeText={(text) => setPassword(text)}
                        value={password}
                    />
                    <View style={styles.forgotPasswordContainer}>
                        <View style={styles.rememberMeContainer}>
                            <CheckBox
                                style={styles.checkbox}
                                tintColor={Colors.checkboxBorder}
                                onCheckColor={Colors.white}
                                onFillColor={Colors.primaryBrand}
                                onTintColor={Colors.primaryBrand}
                                animationDuration={0.4}
                                boxType={'square'}
                                value={rememberMe}
                                onValueChange={(value) => setRememberMe(value)} />

                            <Text style={styles.rememberMeText}>Beni Hatırla</Text>
                        </View>

                        <TextButton
                            onPress={handleOnForgotPasswordButton}
                            title='Şifremi Unuttum'
                            style={styles.forgotPasswordText} />
                    </View>

                    <FilledButton
                        disabled={buttonDisabled}
                        style={styles.loginButton}
                        onPress={handleOnLoginButton}
                        title={'Giriş Yap'} />

                    <HorizontalSeperator style={styles.seperator} />

                    <Text style={styles.infoText}>Henüz hesabınız yok mu?</Text>

                    <FilledButton
                        style={styles.registerButton}
                        textStyle={styles.registerText}
                        onPress={handleOnRegisterButton}
                        title={'Üye Ol'} />

                    {
                        !isFromReservation &&
                        <BorderedButton
                            style={styles.continueButton}
                            onPress={handleOnContinueButton}
                            title={'Giriş Yapmadan Devam Et'} />
                    }

                </>
            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    loginButton: {
        marginHorizontal: wp(20),
        marginTop: hp(38)
    },
    registerButton: {
        backgroundColor: Colors.primaryBrandV5,
        marginHorizontal: wp(20),
        marginTop: hp(16)
    },
    registerText: {
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',
    },
    continueButton: {
        marginHorizontal: wp(20),
        marginTop: hp(10)
    },
    forgotPasswordText: {
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',
        fontSize: hp(14)
    },
    seperator: {
        marginHorizontal: wp(20),
        marginVertical: hp(16)
    },
    infoText: {
        alignSelf: 'center',
        color: TextColors.primaryTextV7,
        fontSize: hp(14)
    },
    checkbox: {
        width: hp(24),
        height: hp(24)
    },
    forgotPasswordContainer: {
        marginHorizontal: wp(20),
        marginTop: hp(20),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    rememberMeContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    rememberMeText: {
        marginStart: wp(8),
        color: TextColors.primaryText,
        fontSize: hp(14),
    },

})