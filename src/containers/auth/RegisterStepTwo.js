import { useNavigation } from '@react-navigation/native'
import React, { useState, useLayoutEffect, useRef } from 'react'
import { hp, wp } from '../../styles/Dimens'
import { StepHeader } from '../../components/headers/StepHeader';
import { BackButton } from '../../components/buttons/BackButton';
import { useDispatch, useSelector } from 'react-redux'
import { setUserAction } from '../../redux/actions/AuthActions'
import { HeaderCancelButton } from '../../components/buttons/HeaderCancelButton'
import { LicenseClassModal } from '../modals/LicenseClassModal'
import RBSheet from "react-native-raw-bottom-sheet";
import { LicenseInformation } from '../../components/views/LicenseIformation'
import { getDateErrorText } from '../../utilities/helpers';
import { Styles } from '../../styles/Styles';

export const RegisterStepTwo = ({ route }) => {
    const dispatch = useDispatch()
    const [licenseNumber, setLicenseNumber] = useState('')
    const [licenseClass, setLicenseClass] = useState('')
    const [licensePlace, setLicensePlace] = useState('')
    const [licenseDate, setLicenseDate] = useState('')
    const [licenseDateError, setLicenseDateError] = useState(null)
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const refRBSheet = useRef();

    const navigation = useNavigation()
    // const user = useSelector(state => state.authReducer.user)
    const { user } = route.params
    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => <HeaderCancelButton navigation={navigation} title={"Kayıt olma işleminden çıkış yapmak istediğinize emin misiniz ?"}/>
        })
        setContinueButtonState()
    }, [navigation, licenseNumber, licenseClass, licensePlace, licenseDate]);

    const setContinueButtonState = () => {
        setButtonDisabled(false)
        let dateError = getDateErrorText(licenseDate)

        if (licenseNumber.length > 0 &&
            licenseClass.length > 0 &&
            licensePlace.length > 0 &&
            licenseDate.length === 10 &&
            !dateError) {
            setButtonDisabled(false)
            setLicenseDateError(null)
        } else {
            setButtonDisabled(true)
            setLicenseDateError(dateError)
        }
    }

    const handleOnLicenseClassSelection = (value) => {
        refRBSheet.current.close()
        setLicenseClass(value)
    }

    const handleOnContinueButton = () => {
        let userObj = { ...user, licenseInfo: {} }

        userObj.licenseInfo.licenseNumber = licenseNumber
        userObj.licenseInfo.licensePlace = licensePlace
        userObj.licenseInfo.licenseClass = licenseClass
        userObj.licenseInfo.licenseDate = licenseDate.split('/')[2] + licenseDate.split('/')[1] + licenseDate.split('/')[0]

        // dispatch(setUserAction(userObj))
        navigation.navigate('RegisterStepThree', { user: userObj })
    }

    return (
        <>
            <StepHeader
                stepCount={4}
                step={2}
                title={'Üye Ol'} />
            <LicenseInformation
                autoFocus={true}
                setLicenseNumber={(text) => setLicenseNumber(text)}
                licenseNumber={licenseNumber}
                showLicenseClassModal={() => refRBSheet.current.open()}
                licenseClass={licenseClass}
                setLicensePlace={(text) => setLicensePlace(text)}
                licensePlace={licensePlace}
                handleOnDateOfLicenseChange={(text) => setLicenseDate(text)}
                licenseDate={licenseDate}
                buttonDisabled={buttonDisabled}
                handleOnContinueButton={handleOnContinueButton}
                buttonTitle={'Devam Et'}
                licenseDateError={licenseDateError} />

            <RBSheet
                height={hp(352)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <LicenseClassModal onPress={handleOnLicenseClassSelection} current={licenseClass} />
            </RBSheet>

        </>
    )
}