import { useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect, useRef, useState } from 'react'
import { View, StyleSheet, Text, Alert } from "react-native"
import { FilledButton } from '../../components/buttons/FilledButton'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput'
import { useDispatch } from 'react-redux'
import { BackButton } from '../../components/buttons/BackButton'
import { MobileNumberView } from '../../components/views/MobileNumberView'
import RBSheet from "react-native-raw-bottom-sheet";
import { CountrySelectionModal } from '../modals/CountrySelectionModal';
import { RadioButtonView } from '../../components/buttons/RadioButtonView'
import { Styles } from '../../styles/Styles'

export const ForgotPassword = () => {
    const dispatch = useDispatch()
    const [email, setEmail] = useState(null)
    const [mobileCode, setMobileCode] = useState({
        LAND1: "TR",
        LANDX: "Türkiye",
        mobileCode: '90'
    })
    const [mobilePhone, setMobilePhone] = useState(null)
    const [segmentIndex, setSegmentIndex] = useState(0)
    const navigation = useNavigation()
    const refRBSheet = useRef();

    const handleOnCountryCodeSelection = (item) => {
        setMobileCode(item)
        refRBSheet.current.close()
    }

    const navigateConfirmationCode = () => {
        navigation.navigate('ConfirmationCode', {
            email: email,
            mobile: { code: mobileCode.mobileCode, number: mobilePhone },
            mobileConfirmation: segmentIndex == 0,
            smsSent:false,
            emailConfirmation: segmentIndex == 1,
        })
    }

    function callEmailApi() {
        if (email === null) {
            Alert.alert('', 'E-Posta adresi boş bırakılamaz')
            return
        }
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: 'white'
        }}>
            {/* <View style={styles.nationalityGroup}>
                <RadioButtonView label='Telefon' selected={segmentIndex == 0} onPress={() => setSegmentIndex(0)} />
                <RadioButtonView label='E-Posta' selected={segmentIndex == 1} onPress={() => setSegmentIndex(1)} />
            </View> */}
            {
                segmentIndex === 0
                    ?
                    <View>
                        <Text style={styles.infoText}>Şifrenizi yenilemek istiyorsanız. Lütfen cep telefonu numaranızı girin.</Text>
                        <MobileNumberView
                            mobilePhone={mobilePhone}
                            autoFocus={true}
                            mobileCode={mobileCode && ('+' + mobileCode.mobileCode)}
                            onFocus={() => refRBSheet.current.open()}
                            onChangeText={text => setMobilePhone(text)} />
                    </View>
                    :
                    <View>
                        <Text style={styles.infoText}>Şifrenizi yenilemek istiyorsanız. Lütfen e-posta adresinizi girin.</Text>
                        <FloatingTextInput
                            label={'E-posta Adresi'}
                            placeholder={'E-posta Adresi'}
                            style={styles.email}
                            autoFocus={true}
                            keyboardType='email-address'
                            onChangeText={text => setEmail(text)}
                            value={email} />
                    </View>
            }

            <FilledButton
                style={styles.continueButton}
                onPress={navigateConfirmationCode}
                title={'Devam Et'} />

            <RBSheet
                height={hp(718)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon:Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <CountrySelectionModal onPress={handleOnCountryCodeSelection} mobileCodeSelection={true} />
            </RBSheet>
        </View >
    )
}

const styles = StyleSheet.create({
    segment: {
        marginHorizontal: wp(20),
        marginTop: hp(32),
        height: hp(44),
        borderColor: Colors.primaryBrand,
        borderWidth: 1,
    },
    loginButton: {
        marginHorizontal: wp(20),
        marginTop: hp(38)
    },
    mobileContainer: {
        flexDirection: 'row',
        marginTop: hp(36)
    },
    mobileCode: {
        marginStart: wp(20),
        marginEnd: wp(17),
        width: wp(120),
        backgroundColor: 'transparent'
    },
    mobilePhone: {
        marginEnd: wp(20),
        width: wp(198),
        backgroundColor: 'transparent'
    },
    email: {
        // marginHorizontal: wp(20),
        // marginTop: hp(36),
        backgroundColor: 'transparent'
    },
    infoText: {
        color: TextColors.primaryText,
        fontSize: hp(18),
        marginHorizontal: wp(20),
        marginTop: hp(20),
    },
    continueButton: {
        width: wp(335),
        position: 'absolute',
        bottom: hp(44),
        marginHorizontal: wp(20)
    },
    nationalityGroup: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between',
        marginTop: hp(24)
    },
})