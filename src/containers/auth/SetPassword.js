import { CommonActions, useNavigation } from '@react-navigation/native'
import React, { useState, useLayoutEffect } from 'react'
import { View, StyleSheet, Text, Alert, SafeAreaView } from "react-native"
import { callForgotPasswordApi } from '../../api/auth/authServices'
import { FilledButton } from '../../components/buttons/FilledButton'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { BackButton } from '../../components/buttons/BackButton';
import { getErrorMessage, isSuccess } from '../../api/apiHelpers';
import { saveJsonData } from '../../utilities/helpers'
import { useDispatch } from 'react-redux'
import { setLoadingAction } from '../../redux/actions/MasterActions'

export const SetPassword = ({ route }) => {
    const dispatch = useDispatch()
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [passwordError, setPasswordError] = useState(null)
    const [passwordError2, setPasswordError2] = useState(null)
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const [secureEntry, setSecureEntry] = useState(true)
    const [secureEntry2, setSecureEntry2] = useState(true)
    const navigation = useNavigation()
    const { mobileConfirmation, emailConfirmation, mobile, email } = route.params;

    useLayoutEffect(() => {
        setContinueButtonState()
    }, [password, confirmPassword]);

    const setContinueButtonState = () => {
        if (password.length > 0 && password.length < 6) {
            setButtonDisabled(true)
            setPasswordError('Şifreniz en az 6 haneli olmalıdır.')
        }
        else if (password.length > 0 &&
            confirmPassword.length > 0) {
            if (password === confirmPassword) {
                setButtonDisabled(false)
                setPasswordError(null)
                setPasswordError2(null)
            } else {
                setButtonDisabled(true)
                setPasswordError(null)
                setPasswordError2('İki şifre bilgisi eşleşmemektedir. Lütfen şifrenizi yeniden girin')
            }
        } else {
            setPasswordError(null)
            setPasswordError2(null)
            setButtonDisabled(true)
        }
    }

    const handleOnContinueButton = () => {
        dispatch(setLoadingAction(true))
        callForgotPasswordApi(mobile.code, mobile.number, email, password)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    Alert.alert(
                        "",
                        "Şifre değişikliği başarılı. Yeni şifrenizle giriş yapabilirsiniz",
                        [
                            { text: "Tamam", onPress: () => navigateHome() }
                        ],
                        { cancelable: false }
                    );
                } else {
                    Alert.alert(
                        "",
                        getErrorMessage(response),
                        { cancelable: false }
                    );
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata',error.message)
            })
    }

    function navigateHome() {
        let data = { isFromReservation: false }
        saveJsonData('isFromReservation', data)
        const loginAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'LoginStack' }]
        })
        navigation.dispatch(loginAction)
    }

    return (
        <SafeAreaView
            style={styles.container}>

            <Text style={styles.infoText}>Lütfen yeni şifrenizi belirleyin. Şifreniz en az 6 haneli olmalıdır</Text>

            <FloatingTextInput
                label={'Şifre'}
                placeholder={'Şifre'}
                error={passwordError}
                onChangeText={(text) => setPassword(text)}
                value={password}
                secureTextEntry={secureEntry}
                iconSource={secureEntry ? require('../../assets/icons/ic_eye_close.png') : require('../../assets/icons/ic_eye.png')}
                iconOnPress={() => setSecureEntry(!secureEntry)}
            />

            <FloatingTextInput
                label={'Şifre Tekrarı'}
                placeholder={'Şifre Tekrarı'}
                onChangeText={(text) => setConfirmPassword(text)}
                error={passwordError2}
                value={confirmPassword}
                secureTextEntry={secureEntry2}
                iconSource={secureEntry2 ? require('../../assets/icons/ic_eye_close.png') : require('../../assets/icons/ic_eye.png')}
                iconOnPress={() => setSecureEntry2(!secureEntry2)}
            />

            <FilledButton
                disabled={buttonDisabled}
                style={styles.continueButton}
                onPress={handleOnContinueButton}
                title={'Kaydet'} />
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    infoText: {
        color: TextColors.primaryText,
        fontSize: hp(16),
        marginHorizontal: wp(20),
        marginTop: hp(20),
    },
    continueButton: {
        width: wp(335),
        position: 'absolute',
        bottom: hp(44),
        marginHorizontal: wp(20)
    }
})