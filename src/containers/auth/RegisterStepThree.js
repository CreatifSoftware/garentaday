import { useNavigation } from '@react-navigation/native'
import React, { useState, useLayoutEffect, useRef } from 'react'
import { StyleSheet, ScrollView, Text, View, Alert, SafeAreaView } from "react-native"
import { FilledButton } from '../../components/buttons/FilledButton'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { StepHeader } from '../../components/headers/StepHeader';
import { BackButton } from '../../components/buttons/BackButton';
import { PreviousNextView } from 'react-native-keyboard-manager';
import { MobileNumberView } from '../../components/views/MobileNumberView';
import { HeaderCancelButton } from '../../components/buttons/HeaderCancelButton'
import RBSheet from "react-native-raw-bottom-sheet";
import { CountrySelectionModal } from '../modals/CountrySelectionModal';
import { useDispatch, useSelector } from 'react-redux'
import { setUserAction } from '../../redux/actions/AuthActions'
import { Styles } from '../../styles/Styles'
import CheckBox from '@react-native-community/checkbox'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { callSendPermissionSMS } from '../../api/auth/authServices'
import { getErrorMessage, isSuccess } from '../../api/apiHelpers'

export const RegisterStepThree = ({ route }) => {
    const [mobileCode, setMobileCode] = useState({
        LAND1: "TR",
        LANDX: "Türkiye",
        mobileCode: '90'
    })

    const dispatch = useDispatch()
    const [mobilePhone, setMobilePhone] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [email, setEmail] = useState('')
    const [secureEntry, setSecureEntry] = useState(true)
    const [secureEntry2, setSecureEntry2] = useState(true)
    const [passwordError, setPasswordError] = useState(null)
    const [passwordError2, setPasswordError2] = useState(null)
    const [mobileError, setMobileError] = useState(null)
    const [emailError, setEmailError] = useState(null)
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const [kvkkChecked, setKvkChecked] = useState(false);
    const [etkChecked, setEtkChecked] = useState(false);
    const navigation = useNavigation()
    const refRBSheet = useRef();
    // const user = useSelector(state => state.authReducer.user)
    const { user } = route.params
    useLayoutEffect(() => {
        navigation.setOptions({
            headerShown: true,
            headerRight: () => <HeaderCancelButton navigation={navigation} title={"Kayıt olma işleminden çıkış yapmak istediğinize emin misiniz ?"} />
        })

        setContinueButtonState()
    }, [navigation, email, mobilePhone, password, confirmPassword, kvkkChecked]);

    const validate = (text) => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            setEmailError('Geçerli bir email giriniz.')
            setButtonDisabled(true)
        }
        else {
            setEmailError(null)
            // setButtonDisabled(false)
        }
    }

    const setContinueButtonState = () => {
        setButtonDisabled(false)

        if (email.length > 0 &&
            mobilePhone.length > 0 &&
            (password.length > 0 ||
                confirmPassword.length > 0) &&
            kvkkChecked) {

            if (mobilePhone.length === 15) {
                setButtonDisabled(false)
                setMobileError(null)
            }
            else {
                setButtonDisabled(true)
                setMobileError('Cep numaranız 10 hane olmalıdır.')
            }

            if (password.length > 0 && password.length < 6) {
                setButtonDisabled(true)
                setPasswordError('Şifreniz en az 6 haneli olmalıdır.')
            }
            else if (password.length > 0 &&
                confirmPassword.length > 0) {
                if (password === confirmPassword) {
                    setButtonDisabled(false)
                    setPasswordError(null)
                    setPasswordError2(null)
                } else {
                    setButtonDisabled(true)
                    setPasswordError(null)
                    setPasswordError2('İki şifre bilgisi eşleşmemektedir. Lütfen şifrenizi yeniden girin')
                }
            } else {
                setPasswordError(null)
                setPasswordError2(null)
                setButtonDisabled(true)
            }
            validate(email.trim())

        } else {
            setPasswordError(null)
            setPasswordError2(null)
            setButtonDisabled(true)
        }
    }

    const handleOnContinueButton = () => {
        let userObj = { ...user, contactInfo: {}, permissionInfo: {} }

        userObj.contactInfo.email = email.trim()
        userObj.contactInfo.mobileCode = mobileCode.mobileCode
        userObj.contactInfo.mobilePhone = mobilePhone
        userObj.password = password

        userObj.permissionInfo.allowSms = etkChecked
        userObj.permissionInfo.allowEmail = etkChecked
        userObj.permissionInfo.allowPhone = etkChecked
        userObj.permissionInfo.allowLetter = etkChecked
        userObj.permissionInfo.allowMarketing = etkChecked
        userObj.permissionInfo.allowPrivate = etkChecked
        userObj.permissionInfo.agreementChecked = kvkkChecked

        userObj.permissionState = etkChecked ? 1 : 2
        // dispatch(setUserAction(userObj))
        callPermissionSMSForRegister(userObj)
    }

    const callPermissionSMSForRegister = (userObj) => {
        dispatch(setLoadingAction(true))
        callSendPermissionSMS(userObj)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    navigation.navigate('ConfirmationCode', {
                        email: email,
                        mobile: { code: mobileCode.mobileCode, number: userObj.contactInfo.mobilePhone },
                        mobileConfirmation: true,
                        emailConfirmation: false,
                        smsSent: true,
                        isFromRegister: true,
                        user: userObj
                    })
                } else {
                    Alert.alert(
                        "",
                        response.data.EXPORT.EV_MESSAGE,
                        { cancelable: false }
                    );
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    const handleOnCountryCodeSelection = (item) => {
        setMobileCode(item)
        refRBSheet.current.close()
    }

    return (
        <View style={{ flex: 1 }}>
            <StepHeader
                stepCount={4}
                step={3}
                title={'Üye Ol'} />
            <ScrollView
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={styles.container}
                style={{ backgroundColor: 'white' }}>

                <PreviousNextView>

                    <FloatingTextInput
                        label={email && 'E-posta Adresi'}
                        placeholder={'E-posta Adresi'}
                        autoFocus={true}
                        keyboardType='email-address'
                        onChangeText={text => setEmail(text)}
                        value={email}
                        error={emailError} />

                    <MobileNumberView
                        style={{ marginHorizontal: 20 }}
                        mobilePhone={mobilePhone}
                        mobileCode={mobileCode && ('+' + mobileCode.mobileCode)}
                        error={mobileError}
                        onFocus={() => refRBSheet.current.open()}
                        onChangeText={text => setMobilePhone(text)} />

                    <FloatingTextInput
                        label={'Şifre'}
                        placeholder={'Şifre'}
                        error={passwordError}
                        onChangeText={(text) => setPassword(text)}
                        value={password}
                        secureTextEntry={secureEntry}
                        iconSource={secureEntry ? require('../../assets/icons/ic_eye_close.png') : require('../../assets/icons/ic_eye.png')}
                        iconOnPress={() => setSecureEntry(!secureEntry)}
                    />

                    <FloatingTextInput
                        label={'Şifre Tekrarı'}
                        placeholder={'Şifre Tekrarı'}
                        onChangeText={(text) => setConfirmPassword(text)}
                        error={passwordError2}
                        value={confirmPassword}
                        secureTextEntry={secureEntry2}
                        returnKeyType={'done'}
                        iconSource={secureEntry2 ? require('../../assets/icons/ic_eye_close.png') : require('../../assets/icons/ic_eye.png')}
                        iconOnPress={() => setSecureEntry2(!secureEntry2)}
                    />


                </PreviousNextView>

                <View style={styles.checkboxContainer}>
                    <CheckBox
                        style={styles.checkbox}
                        tintColor={Colors.checkboxBorder}
                        onCheckColor={Colors.white}
                        onFillColor={Colors.primaryBrand}
                        onTintColor={Colors.primaryBrand}
                        animationDuration={0.4}
                        boxType={'square'}
                        value={kvkkChecked}
                        onValueChange={(value) => setKvkChecked(value)} />

                    <Text style={styles.agreement}>
                        <Text onPress={() => navigation.navigate('RentalTerms', { termsUrl: 'https://wsport.garenta.com.tr/mobile/agreements/MembershipRules.html', title: 'Üyelik Sözleşmesi' })} style={styles.agreementHyperlink}>Üyelik Sözleşmesini</Text> okudum, kabul ediyorum.</Text>
                </View>

                <View style={styles.checkboxContainer}>
                    <CheckBox
                        style={styles.checkbox}
                        tintColor={Colors.checkboxBorder}
                        onCheckColor={Colors.white}
                        onFillColor={Colors.primaryBrand}
                        onTintColor={Colors.primaryBrand}
                        animationDuration={0.4}
                        boxType={'square'}
                        value={etkChecked}
                        onValueChange={(value) => setEtkChecked(value)} />

                    <Text style={styles.agreement}>
                        <Text onPress={() => navigation.navigate('RentalTerms', { termsUrl: 'https://wsport.garenta.com.tr/mobile/agreements/AydinlatmaMetni.html', title: 'Aydınlatma Metni' })} style={styles.agreementHyperlink}>Çelik Motor Ticaret Anonim Şirketi Kişisel Verilerin Korunması ve İşlenmesi Aydınlatma Metni</Text>'ni belirtilen kapsamda kişisel verilerimin, kampanya iletişimi yapılması, ürün ve hizmetlerin pazarlama süreçlerinin planlanması ve icrası, ürün ve hizmetlerin satış ve pazarlaması için pazar araştırması faaliyetlerinin planlanması ve icrası, şirketin sunduğu ürün ve hizmetlere bağlılık oluşturulması ve arttırılması süreçlerinin planlanması ve icrası, Çelik Motor tarafından sunulan ürün ve hizmetlerin ihtiyaçlarıma ve alışkanlıklarıma göre özelleştirilmesi ile bana özel kampanyalar ve imkanlar oluşturulması amaçlarıyla işlenmesini ve bu kapsamda yukarıdaki formda belirtmiş olduğum iletişim bilgilerime Çelik Motor Ticaret A.Ş.’nin Garenta, MOOV ve ikinciyeni.com markaları altında sunduğu ürün ve hizmetlere ilişkin olarak reklam, promosyon, kampanya ve benzeri ticari elektronik ileti gönderilmesini ve bu amaçla sınırlı olarak Şirket’in hizmet aldığı üçüncü kişilerle paylaşılmasını kabul ediyorum.</Text>
                </View>
            </ScrollView>

            <View style={Styles.shadowBox} >
                <FilledButton
                    disabled={buttonDisabled}
                    style={Styles.continueButton}
                    onPress={handleOnContinueButton}
                    title={'Devam Et'} />
            </View>

            <RBSheet
                height={hp(718)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <CountrySelectionModal onPress={handleOnCountryCodeSelection} mobileCodeSelection={true} />
            </RBSheet>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        paddingBottom: hp(140)
    },
    nationalityGroup: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between',
        marginTop: hp(24)
    },
    genderTitle: {
        fontFamily: 'NunitoSans-Bold',
        fontSize: hp(18),
        color: TextColors.primaryText,
        // marginTop: hp(36),
        marginTop: 36,
        marginStart: wp(20)
    },
    genderGroup: {
        marginHorizontal: wp(20),
    },
    genderRadio: {
        width: wp(335),
        marginTop: hp(10)
    },
    continueButton: {
        width: wp(335),
        marginTop: hp(20),
        marginHorizontal: wp(20)
    },
    checkboxContainer: {
        flexDirection: "row",
        marginHorizontal: wp(20),
        marginTop: hp(25),
        // alignItems: 'center'
    },
    checkbox: {
        width: wp(24),
        height: hp(24),
        marginEnd: wp(10)
    },
    agreementHyperlink: {
        fontSize: hp(14),
        color: Colors.primaryBrand,
        alignSelf: 'center',
        fontFamily: 'NunitoSans-SemiBold',
        marginEnd: wp(20)
    },
    agreement: {
        fontSize: hp(14),
        alignSelf: 'center',
        color: TextColors.primaryText,
        marginEnd: wp(20)
    }
})