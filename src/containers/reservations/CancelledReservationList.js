import {CommonActions, useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { PERSONAL_ADDRESS } from '../../utilities/constants'
import { Image, Text, StyleSheet, View } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { ReservationList } from '../../components/views/ReservationList'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { callGetReservationsDetailApi } from '../../api/reservation/reservationService'
import { ReservationDetailResponse } from '../../api/models/ReservationDetailResponse'
import { setReservationDataAction } from '../../redux/actions/ReservationActions'
import moment from 'moment'
import localization from 'moment/locale/tr'
import { getFormatedDate, saveJsonData } from '../../utilities/helpers'
import { FilledButton } from '../../components/buttons/FilledButton'
moment.updateLocale('tr', localization)

export const CancelledReservationList = () => {
    const cancelledReservations = useSelector(state => state.reservationReducer.reservationList.cancelledReservations)
    let reservation = useSelector(state => state.reservationReducer.reservation)
    const branchs = useSelector(state => state.masterReducer.branchs)
    const navigation = useNavigation()
    const dispatch = useDispatch()
    const user = useSelector(state => state.authReducer.user)

    const handleOnLoginButton = () => {
        let data = { isFromReservation: false }
        saveJsonData('isFromReservation', data)
        const loginAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'LoginStack' }]
        })
        navigation.dispatch(loginAction)
    }
    
    return (
        <View style={styles.container}>
            {
                !user ?
                <>
                    <Image style={styles.emptyImage} source={require('../../assets/icons/ic_no_address.png')} />
                    <Text style={styles.emptyText}>Rezervasyonlarınızı görüntülemek için üye girişi yapmalısınız.</Text>
                    <FilledButton
                        style={styles.registerButton}
                        textStyle={styles.registerText}
                        onPress={handleOnLoginButton}
                        title={'Üye Girişi'} />
                </>
                :
                cancelledReservations.length > 0 ?
                    <ReservationList
                        disabled={true}
                        reservationList={cancelledReservations} />

                    :

                    <>
                        <Image style={styles.emptyImage} source={require('../../assets/icons/ic_no_address.png')} />
                        <Text style={styles.emptyText}> İptal edilen bir rezervasyonunuz bulunmamaktadır.</Text>
                    </>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground,
        alignItems: 'center'
    },
    registerButton: {
        backgroundColor: Colors.primaryBrandV5,
        width:wp(335),
        marginTop: hp(16)
    },
    registerText: {
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',
    },
    emptyImage: {
        marginTop: hp(99),
    },
    emptyText: {
        textAlign: 'center',
        fontSize: hp(16),
        color: TextColors.primaryTextV3,
        marginTop: hp(38),
        marginHorizontal:wp(50)
    }
})