import { useNavigation } from '@react-navigation/native'
import React, { useRef, useState } from 'react'
import { StyleSheet, Text, View, FlatList, Image, ScrollView, Alert } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import FastImage from 'react-native-fast-image'
import { PaymentSelectionHeader } from '../../components/headers/PaymentSelectionHeader'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { VerticalSeperator } from '../../components/VerticalSeperator'
import { Styles } from '../../styles/Styles'
import { FilledButton } from '../../components/buttons/FilledButton'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { CollabsableCard } from '../../components/views/CollapsableCard'
import RBSheet from 'react-native-raw-bottom-sheet'
import { ReservationProcessModal } from '../modals/ReservationProcessModal'
import { ReservationSummaryInfo } from '../../components/views/ReservationSummaryInfo'
import * as AddCalendarEvent from 'react-native-add-calendar-event';
import Share from "react-native-share";
import { callCancelReservationApi } from '../../api/reservation/reservationService'
import { CollapsableGroupCodeInfo } from '../../components/collapsable/CollapsableGroupCodeInfo'
import { CollapsableReservationInfo } from '../../components/collapsable/CollapsableReservationInfo'
import { CollapsablePaymentInfo } from '../../components/collapsable/CollapsablePaymentInfo'
import { getFormatedDate } from '../../utilities/helpers'
import LinearGradient from 'react-native-linear-gradient'
import { ReservationDetailTitle } from '../../components/titles/ReservationDetailTitle'
import { getErrorMessage, isSuccess } from '../../api/apiHelpers'

export const ReservationDetail = () => {
    const dispatch = useDispatch()
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const navigation = useNavigation()
    const [showReservationInfo, setShowReservationInfo] = useState(false)
    const [showGroupInfo, setShowGroupInfo] = useState(false)
    const refRBSheet = useRef();

    const groupInformations = [
        { title: reservation.oldSelectedGroup.transmissionDesc, icon: require('../../assets/icons/ic_transmission.png') },
        { title: reservation.oldSelectedGroup.fuelTypeDesc, icon: require('../../assets/icons/ic_fuel.png') },
        { title: 'Klima', icon: require('../../assets/icons/ic_aircondition.png') },
        { title: 'Kapı: ' + reservation.oldSelectedGroup.doorNumber, icon: require('../../assets/icons/ic_door.png') },
        { title: 'Bluetooth', icon: require('../../assets/icons/ic_bluetooth.png') }
    ]

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerTitle: <ReservationDetailTitle reservation={reservation} />
        })
    }, [navigation]);

    const renderGroupInformation = ({ item }) => {
        return (
            <View style={styles().infoContainer}>
                <Image source={item.icon} />
                <Text style={styles().infoTitle}>{item.title}</Text>
            </View>
        )
    }

    const handleOnContinueButton = () => {
        refRBSheet.current.open()
    }

    const handleOnAddCalendar = () => {

        const eventConfig = {
            title: `Garenta - ${reservation.pickupBranch.branchName} araç kiralama`,
            startDate: reservation.pickupDateTime.toISOString(),
            endDate: reservation.dropoffDateTime.toISOString(),
            // and other options
        };

        AddCalendarEvent.presentEventCreatingDialog(eventConfig)
            .then(eventInfo => {
                //console.warn(JSON.stringify(eventInfo));
                refRBSheet.current.close()
            })
            .catch(error => {
                //console.warn(error);
                refRBSheet.current.close()
            });
    }

    function cancelReservation(isConfirmed) {
        dispatch(setLoadingAction(true))
        callCancelReservationApi(reservation.reservationId, isConfirmed)
            .then(response => {
                let finePrice = Number(response.data.EXPORT.ES_RETURN.CEZA_TUTAR)
                let refundPrice = Number(response.data.EXPORT.ES_RETURN.IADE_TUTAR)
                if (finePrice > 0 || refundPrice > 0 && !isSuccess(response)) {
                    dispatch(setLoadingAction(false))
                    Alert.alert(
                        "",
                        `Ödemeniz gereken ceza tutarı ${finePrice.toFixed(2)} TL\nİade edilecek tutar ${refundPrice.toFixed(2)} TL`,
                        [
                            {
                                text: "Vazgeç",
                                style: 'cancel'
                            },
                            {
                                text: "İptal Et", onPress: () => cancelReservation(true),
                                style: 'destructive'
                            }
                        ],
                        { cancelable: false }
                    );
                } else if (finePrice == 0 && refundPrice == 0 && !isSuccess(response)) {
                    dispatch(setLoadingAction(false))
                    Alert.alert('Hata',getErrorMessage(response))
                } else {
                    dispatch(setLoadingAction(false))
                    Alert.alert(
                        "",
                        'Rezervasyonunuz başarıyla iptal edilmiştir',
                        [
                            {
                                text: "Tamam", onPress: () => navigation.goBack(),
                                style: 'default'
                            }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata',error.message)
            })
    }

    const handleOnCancelReservation = () => {
        refRBSheet.current.close()
        Alert.alert(
            "Rezervasyon İptali",
            'Rezervasyonunuz iptal edilecektir, emin misiniz?',
            [
                {
                    text: "Vazgeç",
                    style: 'cancel'
                },
                {
                    text: "İptal Et", onPress: () => cancelReservation(false),
                    style: 'destructive'
                }
            ],
            { cancelable: false }
        );

        // refRBSheet.current.close()
    }

    const handleOnChangeReservation = () => {

        refRBSheet.current.close()
        navigation.navigate('ReservationStack')
    }

    const handleOnShareReservation = () => {
        let message = `${reservation.pickupBranch.branchName} ofisimizden ${reservation.oldSelectedGroup.groupCode} grubu için, ${getFormatedDate(reservation.pickupDateTime, 'DD.MM.YYYY HH:mm')} - ${getFormatedDate(reservation.dropoffDateTime, 'DD.MM.YYYY HH:mm')} tarihleri arasında rezervasyon yapılmıştır.
                \nGarenta'yı tercih ettiğiniz için teşekkür ederiz.`
        const options = {
            title: 'Rezervasyonunu paylaş',
            message
        };

        Share.open(options)
            .then((res) => { refRBSheet.current.close() })
            .catch((err) => { refRBSheet.current.close() });
    }

    const handleOnMakePayment = () => {
        refRBSheet.current.close()
    }

    return (
        <View style={styles().container}>
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                colors={[Colors.primaryBrand, Colors.primaryBrandV2]}
                style={[StyleSheet.absoluteFill, { height: 200 }]}></LinearGradient>

            <ScrollView contentContainerStyle={{ backgroundColor: Colors.mainBackground, paddingBottom: 40 }}>

                <PaymentSelectionHeader
                    navigation={navigation}
                    reservation={reservation} />

                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                    <FastImage
                        style={styles().image}
                        source={{
                            uri: reservation.selectedGroup.imageUrl,
                            priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                    <View style={styles().segmentContainer}>
                        <Text style={styles().segmentText}>{reservation.oldSelectedGroup.segmentDesc}</Text>
                        <VerticalSeperator style={{ marginHorizontal: wp(8), width: 2 }} />
                        <Text style={styles().segmentText}>{reservation.oldSelectedGroup.segmentId} Grubu</Text>
                    </View>

                    <Text style={styles().displayText}>{reservation.oldSelectedGroup.displayText}</Text>

                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ marginTop: hp(20) }}
                        data={groupInformations}
                        renderItem={renderGroupInformation}
                        keyExtractor={item => item.title} />

                    <CollapsableReservationInfo
                        reservation={reservation} />

                    <CollapsableGroupCodeInfo
                        groupInfo={reservation.oldSelectedGroup} />

                    <CollapsablePaymentInfo
                        reservation={reservation} />



                </View>
            </ScrollView>

            {
                !reservation.isEditable && !reservation.isCancelable ?
                    null

                    :

                    <View style={[Styles.shadowBox]} >
                        <FilledButton
                            style={Styles.continueButton}
                            title={'Rezervasyon İşlemleri'}
                            onPress={handleOnContinueButton} />
                    </View>
            }

            <RBSheet
                height={hp(349)}
                ref={refRBSheet}
                closeOnDragDown={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <ReservationProcessModal
                    reservation={reservation}
                    onMakePayment={() => handleOnMakePayment()}
                    onChangeReservation={() => handleOnChangeReservation()}
                    addCalendar={() => handleOnAddCalendar()}
                    shareReservation={() => handleOnShareReservation()}
                    onCancelReservation={() => handleOnCancelReservation()} />
            </RBSheet>
        </View >
    )
}

const styles = (isSelected) => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground,
    },
    infoContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(70),
        height: hp(64),
        borderRadius: hp(10),
        marginStart: wp(20)
    },
    infoTitle: {
        fontSize: hp(12),
        color: TextColors.primaryText,
        marginTop: hp(8)
    },
    selectedPaymentType: {
        backgroundColor: 'white',
        width: wp(167.5),
        height: '100%',
        borderWidth: 2,
        borderColor: Colors.primaryBrand,
        borderRadius: hp(20),
        justifyContent: 'center',
        paddingStart: wp(20)
    },
    paymentType: {
        width: wp(167),
        height: '100%',
        justifyContent: 'center',
        paddingStart: wp(20)
    },
    paymentTypeContainer: {
        backgroundColor: '#f5f5f5',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: Colors.borderColor,
        height: hp(70),
        borderRadius: hp(20),
        alignItems: 'center',
        marginBottom: hp(20)
    },
    paymentTitle: {
        fontSize: hp(12),
        color: isSelected ? Colors.primaryBrand : TextColors.primaryTextV3,
        fontFamily: 'NunitoSans-Bold'
    },
    paymentAmount: {
        fontSize: hp(22),
        color: isSelected ? Colors.primaryBrand : TextColors.primaryTextV3,
        fontFamily: 'NunitoSans-Bold'
    },
    paymentDecimal: {
        fontSize: hp(9),
        color: isSelected ? Colors.primaryBrand : TextColors.primaryTextV3,
        fontFamily: 'NunitoSans-Bold'
    },
    displayText: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryText,
        marginTop: hp(9),
        textAlign: 'center',
        marginHorizontal: wp(40)
    },
    image: {
        height: hp(147),
        width: wp(300),
        marginHorizontal: wp(10),
    },
    segmentContainer: {
        flexDirection: 'row',
    },
    segmentText: {
        fontSize: hp(12),
        fontWeight: '700',
        color: TextColors.primaryTextV3,
    },
})



