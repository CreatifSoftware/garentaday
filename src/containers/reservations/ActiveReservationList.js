
import { CommonActions, useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { PERSONAL_ADDRESS } from '../../utilities/constants'
import { Image, Text, StyleSheet, View, SafeAreaView, Alert } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { ReservationList } from '../../components/views/ReservationList'
import { GradientHeader } from '../../components/views/GradientHeader'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { callGetContractDetailApi, callGetReservationsDetailApi } from '../../api/reservation/reservationService'
import { ReservationDetailResponse } from '../../api/models/ReservationDetailResponse'
import { setReservationDataAction } from '../../redux/actions/ReservationActions'
import moment from 'moment'
import { getFormatedDate, saveJsonData } from '../../utilities/helpers'
import { FilledButton } from '../../components/buttons/FilledButton'
import localization from 'moment/locale/tr'
moment.updateLocale('tr', localization)

export const ActiveReservationList = () => {
    const activeReservations = useSelector(state => state.reservationReducer.reservationList.activeReservations)
    let reservation = useSelector(state => state.reservationReducer.reservation)
    const user = useSelector(state => state.authReducer.user)
    const branchs = useSelector(state => state.masterReducer.branchs)
    const navigation = useNavigation()
    const dispatch = useDispatch()

    useLayoutEffect(() => {
        navigation.setOptions({
            headerShown: true
        })
    }, [navigation]);

    const handleOnReservationClick = (item) => {
        dispatch(setLoadingAction(true))
        if (item.contractId) {
            getContractDetail(item)
        } else {
            getReservationDetail(item)
        }
    }

    const getReservationDetail = (item) => {
        callGetReservationsDetailApi(item.reservationId)
            .then(response => {
                reservation = ReservationDetailResponse(response.data.EXPORT, reservation)
                let pickupBranch = branchs.filter(arg => arg.branchId === item.pickupBranchId)
                let dropoffBranch = branchs.filter(arg => arg.branchId === item.dropoffBranchId)

                reservation.reservationId = item.reservationId
                reservation.pnrNo = item.pnrNo
                reservation.pickupBranch = pickupBranch[0]
                reservation.dropoffBranch = dropoffBranch[0]
                reservation.dropoffBranch = dropoffBranch[0]
                reservation.pickupDateTime = moment(getFormatedDate((item.pickupDate + ' ' + item.pickupTime), 'yyyy-MM-DD HH:mm'));
                reservation.dropoffDateTime = moment(getFormatedDate((item.dropoffDate + ' ' + item.dropoffTime), 'yyyy-MM-DD HH:mm'));

                dispatch(setLoadingAction(false))
                dispatch(setReservationDataAction(reservation))

                navigation.navigate('ReservationDetail')
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    const getContractDetail = (item) => {
        callGetContractDetailApi(item.contractId)
            .then(response => {
                reservation = ReservationDetailResponse(response.data.EXPORT, reservation)
                let pickupBranch = branchs.filter(arg => arg.branchId === item.pickupBranchId)
                let dropoffBranch = branchs.filter(arg => arg.branchId === item.dropoffBranchId)

                reservation.reservationId = item.reservationId
                reservation.contractId = item.contractId
                reservation.pnrNo = item.pnrNo
                reservation.contractPnrNo = item.contractPnrNo
                reservation.pickupBranch = pickupBranch[0]
                reservation.dropoffBranch = dropoffBranch[0]
                reservation.dropoffBranch = dropoffBranch[0]
                reservation.pickupDateTime = moment(getFormatedDate((item.pickupDate + ' ' + item.pickupTime), 'yyyy-MM-DD HH:mm'));
                reservation.dropoffDateTime = moment(getFormatedDate((item.dropoffDate + ' ' + item.dropoffTime), 'yyyy-MM-DD HH:mm'));

                dispatch(setLoadingAction(false))
                dispatch(setReservationDataAction(reservation))

                navigation.navigate('ReservationDetail')
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    const handleOnLoginButton = () => {
        let data = { isFromReservation: false }
        saveJsonData('isFromReservation', data)
        const loginAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'LoginStack' }]
        })
        navigation.dispatch(loginAction)
    }

    return (
        <SafeAreaView style={styles.container}>
            {
                !user ?
                    <>
                        <Image style={styles.emptyImage} source={require('../../assets/icons/ic_no_address.png')} />
                        <Text style={styles.emptyText}>Rezervasyonlarınızı görüntülemek için üye girişi yapmalısınız.</Text>
                        <FilledButton
                            style={styles.registerButton}
                            textStyle={styles.registerText}
                            onPress={handleOnLoginButton}
                            title={'Üye Girişi'} />
                    </>
                    :
                    (activeReservations && activeReservations.length > 0) ?
                        <ReservationList
                            reservationList={activeReservations}
                            onPress={handleOnReservationClick} />

                        :

                        <>
                            <Image style={styles.emptyImage} source={require('../../assets/icons/ic_no_address.png')} />
                            <Text style={styles.emptyText}> Henüz bir rezervasyonunuz bulunmamaktadır.</Text>
                        </>
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground,
        alignItems: 'center'
    },
    registerButton: {
        backgroundColor: Colors.primaryBrandV5,
        width: wp(335),
        marginTop: hp(16)
    },
    registerText: {
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',
    },
    emptyImage: {
        marginTop: hp(99),
    },
    emptyText: {
        textAlign: 'center',
        fontSize: hp(16),
        color: TextColors.primaryText,
        marginTop: hp(38),
        marginHorizontal: wp(50)
    }
})