import { useNavigation } from '@react-navigation/native';
import React, { useRef, useState } from 'react';
import { Text, FlatList, SectionList, StyleSheet, View, SafeAreaView, TouchableOpacity } from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import { useDispatch, useSelector } from 'react-redux';
import { callFilterCampaignsApi, getFullCampaignList } from '../../api/campaigns/campaignService';
import { TextButton } from '../../components/buttons/TextButton';
import { CampaignItem } from '../../components/views/CampaignItem';
import { setLoadingAction } from '../../redux/actions/MasterActions';
import { clearReservationDataAction } from '../../redux/actions/ReservationActions';
import { Colors, TextColors } from '../../styles/Colors';
import { hp, wp } from '../../styles/Dimens';
import { Styles } from '../../styles/Styles';
import { getCollaborationCampaigns } from '../../utilities/helpers';
import { FilterCampaignModal } from '../modals/FilterCampaignModal';

export const CampaignList = ({route}) => {

    // const campaigns = useSelector(state => state.reservationReducer.campaignList)
    const navigation = useNavigation()
    const [sectionList, setSectionList] = useState([])
    const [campaigns, setCampaigns] = useState([])
    const [initialSectionList, setInitialSectionList] = useState([])
    const dispatch = useDispatch()
    const refRBSheet = useRef()
    const [selectedCampaignTypes, setSelectedCampaignTypes] = useState([])
    const [selectedCampaignSections, setSelectedCampaignSections] = useState([])

    // React.useEffect(() => {

    // }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', e => {
            getCampaigns()
            dispatch(clearReservationDataAction())
        });
        return unsubscribe;
    }, [navigation]);

    const getCampaigns = () => {
        dispatch(setLoadingAction(true))
        getFullCampaignList()
            .then(response => {
                //dispatch(setCampaignListAction(getcoll(response.data)))

                let campaignData = getCollaborationCampaigns(response.data)
                setCampaigns(campaignData)
                let sectionData = prepareSectionList(campaignData)
                setSectionList(sectionData)
                setInitialSectionList(sectionData)
                // campaignData.forEach(obj => {
                //     if (!newArray.some(o => o.collaborationGroup === obj.collaborationGroup)) {
                //         newArray.push({ ...obj })
                //         sectionData.push({
                //             title: obj.collaborationGroup,
                //             data: campaignData.filter(args => args.collaborationGroup === obj.collaborationGroup)
                //         })
                //     }
                // });

                // setSectionList(sectionData)
                // setInitialSectionList(sectionData)
                dispatch(setLoadingAction(false))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    const prepareSectionList = (campaignData) => {
        const newArray = [];
        const sectionData = []
        campaignData.forEach(obj => {
            if (!newArray.some(o => o.collaborationGroup === obj.collaborationGroup)) {
                newArray.push({ ...obj })
                sectionData.push({
                    title: obj.collaborationGroup,
                    data: campaignData.filter(args => args.collaborationGroup === obj.collaborationGroup)
                })
            }
        });

        return sectionData

    }

    const handleOnCampaignSelection = (item) => {
        navigation.navigate('CampaignDetail', { campaign: item })
    }

    const handleOnFilter = () => {
        refRBSheet.current.open()
    }

    const handleOnFilterSelected = (campaignSections, campaignTypes) => {
        if (campaignSections.length == 0 && campaignTypes.length == 0) {
            getCampaigns()
        }
        else if (campaignTypes.length > 0) {
            dispatch(setLoadingAction(true))
            callFilterCampaignsApi([], campaignTypes)
                .then(response => {
                    const filtered = campaigns.filter(campaign => {
                        return response.data.EXPORT.ET_CAMPAIGN.find(arg => arg.EXTERNAL_ID === campaign.campaignId);
                    });
                    if (campaignSections.length > 0) {
                        const filtered2 = filtered.filter(campaign => {
                            return campaignSections.find(arg => arg.title === campaign.collaborationGroup);
                        });

                        let sectionData = prepareSectionList(filtered2)
                        setSectionList(sectionData)

                    } else {
                        let sectionData = prepareSectionList(filtered)
                        setSectionList(sectionData)
                    }
                    
                    dispatch(setLoadingAction(false))
                })
                .catch(error => {
                    dispatch(setLoadingAction(false))
                })
        } else {
            setSectionList(campaignSections)
        }

        refRBSheet.current.close()
        setSelectedCampaignSections(campaignSections)
        setSelectedCampaignTypes(campaignTypes)
    }

    const renderItem = ({ item }) => {
        return (
            <CampaignItem
                item={item}
                style={{ width: wp(335) }}
                onPress={() => handleOnCampaignSelection(item)} />
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <TextButton
                onPress={handleOnFilter}
                buttonStyle={styles.button}
                style={styles.buttonText}
                title={'Filtrele'}
                source={require('../../assets/icons/ic_filter.png')} />

            <SectionList
                sections={sectionList}
                keyExtractor={(item, index) => item + index}
                renderItem={renderItem}
                renderSectionHeader={({ section: { title } }) => (
                    <Text style={styles.header}>{title}</Text>
                )}
            />

            <RBSheet
                height={hp(718)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <FilterCampaignModal
                    selectedCampaignSections={selectedCampaignSections}
                    selectedCampaignTypes={selectedCampaignTypes}
                    handleOnFilterSelected={handleOnFilterSelected}
                    sectionList={initialSectionList}
                    onDismiss={() => refRBSheet.current.close()} />
            </RBSheet>

        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground
    },
    header: {
        fontSize: hp(18),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-Bold',
        paddingStart: wp(20),
        paddingTop: hp(16),
        marginBottom: hp(16),
        backgroundColor: Colors.mainBackground
    },
    button: {
        height: hp(50),
        backgroundColor: 'white',
        justifyContent: 'center'
    },
    buttonText: {
        color: TextColors.primaryTextV3,
        fontFamily: 'NunitoSans-SemiBold'
    }
})
