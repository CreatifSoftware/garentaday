import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { Text, FlatList, SectionList, StyleSheet, View, ScrollView, SafeAreaView, Alert } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { IMAGE_URL } from '../../api/constants';
import { BorderedButton } from '../../components/buttons/BorderedButton';
import { TextButton } from '../../components/buttons/TextButton';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { CampaignItem } from '../../components/views/CampaignItem';
import { CollabsableCard } from '../../components/views/CollapsableCard';
import { Colors, TextColors } from '../../styles/Colors';
import { hp, wp } from '../../styles/Dimens';
import { WebView } from "react-native-webview";
import { Styles } from '../../styles/Styles';
import { FilledButton } from '../../components/buttons/FilledButton';
import { BackButton } from '../../components/buttons/BackButton';
import { clearReservationDataAction, setReservationDataAction } from '../../redux/actions/ReservationActions';
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { callCheckCampaignCodeApi } from '../../api/campaigns/campaignService';
import { getErrorMessage, isSuccess } from '../../api/apiHelpers';
import { setLoadingAction } from '../../redux/actions/MasterActions';

export const CampaignDetail = ({ route }) => {

    const selectedCampaign = route.params.campaign
    const [showCampaignDetail, setShowCampaignDetail] = useState(false)
    const [codeText, setCodeText] = useState('')
    const [phoneText, setPhoneText] = useState('')
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const transactionId = ""

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: props => <BackButton navigation={navigation} {...props} />,
        })
    }, [navigation])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', e => {
            dispatch(clearReservationDataAction())
        });

        return unsubscribe;
    }, [navigation]);

    function handleOnContinueButton() {
        dispatch(clearReservationDataAction())
        if (selectedCampaign.isTextForm === 'X' || selectedCampaign.isPhoneForm === 'X') {
            checkCampaignCode()
        } else {
            navigateToReservationStack(null)
        }
    }

    function checkCampaignCode() {
        dispatch(setLoadingAction(true))
        callCheckCampaignCodeApi(selectedCampaign.campaignId, phoneText, codeText)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    navigateToReservationStack(response.data.EXPORT.ES_RETURN.TRANSACTION_ID)
                } else {
                    dispatch(setLoadingAction(false))
                    Alert.alert('Hata', response.data.EXPORT.ES_RETURN.MESSAGE.MESSAGE)
                    // Alert.alert('Hata', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    function navigateToReservationStack(transactionId) {

        reservation.campaignId = selectedCampaign.campaignId
        reservation.transactionId = transactionId
        dispatch(setReservationDataAction(reservation))

        navigation.navigate('ReservationStack')
    }

    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: 40 }}>
                <View style={styles.headerContainer}>
                    <FastImage
                        style={{ width: wp(225), height: hp(120), marginVertical: hp(10) }}
                        source={{
                            uri: IMAGE_URL + selectedCampaign.imageUrl,
                            priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                    <Text style={styles.campaignTitle}>{selectedCampaign.title}</Text>
                </View>

                <HorizontalSeperator style={{ width: '100%' }} />

                {
                    selectedCampaign.isTextForm === 'X' ?
                        <FloatingTextInput
                            label={selectedCampaign.campaignType.text1}
                            placeholder={selectedCampaign.campaignType.text1}
                            onChangeText={(text) => setCodeText(text)}
                            value={codeText}
                        />

                        :

                        selectedCampaign.isPhoneForm === 'X'
                            ?
                            <>
                                <FloatingTextInput
                                    label={selectedCampaign.campaignType.text1}
                                    placeholder={selectedCampaign.campaignType.text1}
                                    onChangeText={(text) => setPhoneText(text)}
                                    value={phoneText}
                                    keyboardType='number-pad'
                                    type={'custom'}
                                />

                                <FloatingTextInput
                                    label={selectedCampaign.campaignType.text2}
                                    placeholder={selectedCampaign.campaignType.text2}
                                    onChangeText={(text) => setCodeText(text)}
                                    value={codeText}
                                />
                            </>

                            : null

                }

                <Text style={styles.annotation}>*{selectedCampaign.annotation}</Text>

                <CollabsableCard
                    title={'Kampanya Bilgileri'}
                    showDetail={showCampaignDetail}
                    onPress={() => setShowCampaignDetail(!showCampaignDetail)}>
                    <WebView
                        incognito={true}
                        style={{ width: wp(335), height: 500 }}
                        // originWhitelist={['*']}
                        source={{ html: selectedCampaign.campaignTerms }}
                    />
                </CollabsableCard>


            </ScrollView>

            {/* <BorderedButton
                style={{ marginHorizontal: wp(20), width: wp(335), marginTop: hp(20), backgroundColor: 'white', position: 'absolute', bottom: hp(40) }}
                title={'Kampanyadan Yararlan'}
                source={require('../../assets/icons/ic_plus_simple.png')} /> */}

            <SafeAreaView style={[Styles.shadowBox]} >
                <FilledButton
                    style={Styles.continueButton}
                    onPress={handleOnContinueButton}
                    title={'Devam Et'} />
            </SafeAreaView>
        </View>


    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    webContainer: {
        flex: 1,
        backgroundColor: Colors.white
    },
    headerContainer: {
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
    campaignTitle: {
        fontSize: hp(18),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-Bold',
        marginHorizontal: wp(55),
        textAlign: 'center',
        marginBottom: hp(20)
    },
    annotation: {
        fontSize: hp(12),
        color: TextColors.primaryText,
        marginHorizontal: wp(20),
        textAlign: 'center',
        marginTop: hp(10)
    },
    continueButton: {
        width: wp(335),
        marginTop: hp(20),
        marginHorizontal: wp(20)
    }
})
