import { useNavigation } from '@react-navigation/native'
import React, { useState, useLayoutEffect, useEffect } from 'react'
import { StyleSheet, ScrollView, Alert } from "react-native"
import { Colors } from '../../../styles/Colors'
import { hp, wp } from '../../../styles/Dimens'
import { FloatingTextInput } from '../../../components/inputs/FloatingTextInput';
import { BackButton } from '../../../components/buttons/BackButton';
import { PreviousNextView } from 'react-native-keyboard-manager';
import { useDispatch, useSelector } from 'react-redux'
import { setUserAction } from '../../../redux/actions/AuthActions'
import { FilledButton } from '../../../components/buttons/FilledButton'
import { setLoadingAction } from '../../../redux/actions/MasterActions'
import { callChangePasswordApi } from '../../../api/user/userService'
import { getErrorMessage, isSuccess } from '../../../api/apiHelpers'
import { encodeUTF16LE, getJsonData, saveJsonData } from '../../../utilities/helpers'

export const PasswordChange = () => {
    const dispatch = useDispatch()
    const [currentPassword, setCurrentPassword] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [currentSecureEntry, setCurrentSecureEntry] = useState(true)
    const [passwordSecureEntry, setPasswordSecureEntry] = useState(true)
    const [confirmPasswordSecureEntry, setConfirmPasswordSecureEntry] = useState(true)
    const [passwordError, setPasswordError] = useState(null)
    const [passwordError2, setPasswordError2] = useState(null)
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const [loginInfo, setLoginInfo] = useState(null);
    const navigation = useNavigation()

    const user = useSelector(state => state.authReducer.user)

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />,
        })

        setContinueButtonState()
    }, [navigation, currentPassword, password, confirmPassword]);

    useEffect(() => {
        getLoginInfo()
    },[])

    const getLoginInfo = async () => {
        getJsonData('loginInfo')
            .then(response => {
                setLoginInfo(response)
            })
    }

    const setContinueButtonState = () => {
        setButtonDisabled(false)

        if (currentPassword.length > 0) {
            if (password.length > 0 && password.length < 6) {
                setButtonDisabled(true)
                setPasswordError('Şifreniz en az 6 haneli olmalıdır.')
            }  else if (password.length > 0 &&
                confirmPassword.length > 0) {
                if (password === confirmPassword) {
                    setButtonDisabled(false)
                    setPasswordError(null)
                    setPasswordError2(null)
                } else {
                    setButtonDisabled(true)
                    setPasswordError(null)
                    setPasswordError2('İki şifre bilgisi eşleşmemektedir. Lütfen şifrenizi yeniden girin')
                }
            } else {
                setPasswordError(null)
                setPasswordError2(null)
                setButtonDisabled(true)
            }
        } else {
            setPasswordError(null)
            setPasswordError2(null)
            setButtonDisabled(true)
        }
    }

    const saveLoginInfo = async () => {
        let loginInfoJson = { rememberMe: loginInfo.rememberMe, username: loginInfo.username, password: password, isLoggedIn: true, user: user }
        saveJsonData('loginInfo', loginInfoJson)
    }

    const handleOnContinueButton = () => {
        dispatch(setLoadingAction(true))
        callChangePasswordApi(user.userId, currentPassword, password)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    let userObj = { ...user }
                    userObj.password = encodeUTF16LE(password)
                    dispatch(setUserAction(userObj))
                    saveLoginInfo()
                    Alert.alert(
                        "Şifre Değişikliği",
                        "Şifreniz başarılı bir şekilde değiştirildi.",
                        [
                            { text: "Tamam", onPress: () => navigation.goBack() }
                        ],
                        { cancelable: false }
                    );
                } else {
                    Alert.alert('', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('', error.message)
            })
    }

    return (
        <>
            <ScrollView
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={styles.container}
                style={{ backgroundColor: 'white' }}>
                <PreviousNextView>
                    <FloatingTextInput
                        label={'Eski Şifreniz'}
                        placeholder={'Eski Şifreniz'}
                        onChangeText={(text) => setCurrentPassword(text)}
                        value={currentPassword}
                        secureTextEntry={currentSecureEntry}
                        iconSource={currentSecureEntry ? require('../../../assets/icons/ic_eye_close.png') : require('../../../assets/icons/ic_eye.png')}
                        iconOnPress={() => setCurrentSecureEntry(!currentSecureEntry)}
                    />

                    <FloatingTextInput
                        label={'Yeni Şifre'}
                        placeholder={'Yeni Şifre'}
                        onChangeText={(text) => setPassword(text)}
                        value={password}
                        error={passwordError}
                        secureTextEntry={passwordSecureEntry}
                        iconSource={passwordSecureEntry ? require('../../../assets/icons/ic_eye_close.png') : require('../../../assets/icons/ic_eye.png')}
                        iconOnPress={() => setPasswordSecureEntry(!passwordSecureEntry)}
                    />

                    <FloatingTextInput
                        label={'Yeni Şifre Tekrarı'}
                        placeholder={'Yeni Şifre Tekrarı'}
                        onChangeText={(text) => setConfirmPassword(text)}
                        error={passwordError2}
                        value={confirmPassword}
                        secureTextEntry={confirmPasswordSecureEntry}
                        returnKeyType={'done'}
                        iconSource={confirmPasswordSecureEntry ? require('../../../assets/icons/ic_eye_close.png') : require('../../../assets/icons/ic_eye.png')}
                        iconOnPress={() => setConfirmPasswordSecureEntry(!confirmPasswordSecureEntry)}
                    />
                </PreviousNextView>
            </ScrollView>

            <FilledButton
                disabled={buttonDisabled}
                style={styles.continueButton}
                onPress={handleOnContinueButton}
                title={'Şifremi Güncelle'} />
        </>

    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white
    },
    continueButton: {
        width: wp(335),
        position: 'absolute',
        bottom: hp(44),
        marginHorizontal: wp(20)
    }
})