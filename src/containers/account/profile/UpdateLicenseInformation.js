import { useNavigation } from '@react-navigation/native'
import React, { useState, useLayoutEffect, useRef } from 'react'
import { hp, wp } from '../../../styles/Dimens'
import { StepHeader } from '../../../components/headers/StepHeader';
import { BackButton } from '../../../components/buttons/BackButton';
import { useDispatch, useSelector } from 'react-redux'
import { setUserAction } from '../../../redux/actions/AuthActions'
import { HeaderCancelButton } from '../../../components/buttons/HeaderCancelButton'
import { LicenseClassModal } from '../../modals/LicenseClassModal'
import RBSheet from "react-native-raw-bottom-sheet";
import { LicenseInformation } from '../../../components/views/LicenseIformation'
import { setLoadingAction } from '../../../redux/actions/MasterActions';
import { callUpdateUserApi } from '../../../api/user/userService';
import { Alert } from 'react-native';
import { getErrorMessage, isSuccess } from '../../../api/apiHelpers';
import { Styles } from '../../../styles/Styles';
import { convertDate } from '../../../utilities/helpers';

export const UpdateLicenseInformation = () => {
    const dispatch = useDispatch()
    const user = useSelector(state => state.authReducer.user)
    const [licenseNumber, setLicenseNumber] = useState(user.licenseInfo.licenseNumber)
    const [licenseClass, setLicenseClass] = useState(user.licenseInfo.licenseClass)
    const [licensePlace, setLicensePlace] = useState(user.licenseInfo.licensePlace)
    const [licenseDate, setLicenseDate] = useState(convertDate(user.licenseInfo.licenseDate))
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const refRBSheet = useRef();

    const navigation = useNavigation()

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />,
        })
        setContinueButtonState()
    }, [navigation, licenseNumber, licenseClass, licensePlace, licenseDate]);

    const handleOnDateOfLicenseChange = (text) => {
        if (text.length == 2) {
            text = text + '/'
        }
        if (text.length == 5) {
            text = text + '/'
        }
        if (text.length == 10 && !Date.parse(text)) {
            setLicenseDate('Geçerli tarih girin')
        } else {
            setLicenseDate('')
        }
        setLicenseDate(text)
    }

    const setContinueButtonState = () => {
        setButtonDisabled(false)
        if (licenseNumber.length > 0 &&
            licenseClass.length > 0 &&
            licensePlace.length > 0 &&
            licenseDate.length > 0) {
            setButtonDisabled(false)
        } else {
            setButtonDisabled(true)
        }
    }

    const handleOnLicenseClassSelection = (value) => {
        refRBSheet.current.close()
        setLicenseClass(value)
    }

    const handleOnContinueButton = () => {

        user.licenseInfo.licenseNumber = licenseNumber
        user.licenseInfo.licensePlace = licensePlace
        user.licenseInfo.licenseClass = licenseClass

        dispatch(setLoadingAction(true))
        callUpdateUserApi(user)
            .then(response => {
                dispatch(setLoadingAction(false))
                dispatch(setUserAction(user))
                if (isSuccess(response)) {
                    Alert.alert(
                        "Ehliyet Bilgileri",
                        "Bilgileriniz başarılı bir şekilde değiştirildi.",
                        [
                            { text: "Tamam", onPress: () => navigation.goBack() }
                        ],
                        { cancelable: false }
                    );
                } else {
                    Alert.alert('Hata', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    return (
        <>
            <LicenseInformation
                autoFocus={false}
                setLicenseNumber={(text) => setLicenseNumber(text)}
                licenseNumber={licenseNumber}
                showLicenseClassModal={() => refRBSheet.current.open()}
                licenseClass={licenseClass}
                setLicensePlace={(text) => setLicensePlace(text)}
                licensePlace={licensePlace}
                handleOnDateOfLicenseChange={(text) => handleOnDateOfLicenseChange(text)}
                licenseDate={licenseDate}
                buttonDisabled={buttonDisabled}
                handleOnContinueButton={handleOnContinueButton}
                buttonTitle={'Kaydet'}
                disabled={true} />

            <RBSheet
                height={hp(352)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon:Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <LicenseClassModal onPress={handleOnLicenseClassSelection} current={licenseClass} />
            </RBSheet>

        </>
    )
}