import { useNavigation } from '@react-navigation/native'
import React, { useState, useLayoutEffect, useRef } from 'react'
import { StyleSheet, Alert, ScrollView, View, Text } from "react-native"
import { callCheckMernisApi } from '../../../api/auth/authServices'
import { FilledButton } from '../../../components/buttons/FilledButton'
import { Colors, TextColors } from '../../../styles/Colors'
import { getStatusBarHeight, hp, wp } from '../../../styles/Dimens'
import { FloatingTextInput } from '../../../components/inputs/FloatingTextInput';
import { StepHeader } from '../../../components/headers/StepHeader';
import { BackButton } from '../../../components/buttons/BackButton';
import { useDispatch, useSelector } from 'react-redux';
import { setUserAction } from '../../../redux/actions/AuthActions';
import { PreviousNextView } from 'react-native-keyboard-manager';
import RBSheet from "react-native-raw-bottom-sheet";
import { CountrySelectionModal } from '../../modals/CountrySelectionModal';
import { getErrorMessage, isSuccess } from '../../../api/apiHelpers';
import { setLoadingAction } from '../../../redux/actions/MasterActions';
import { UserInformation } from '../../../components/views/UserInformation'
import { MobileNumberView } from '../../../components/views/MobileNumberView'
import { callUpdateUserApi } from '../../../api/user/userService'
import { Styles } from '../../../styles/Styles'
import { convertDate } from '../../../utilities/helpers'

export const UpdateUserInformation = () => {
    const user = useSelector(state => state.authReducer.user)
    const countries = useSelector(state => state.masterReducer.countries)

    const [mobileCode, setMobileCode] = useState({
        LAND1: "TR",
        LANDX: "Türkiye",
        mobileCode: user.contactInfo.mobileCode
    })

    let filtered = countries.filter(item => item.country == user.personalInfo.nationality)
    // let nationality = filtered.length > 0 && filtered[0]

    // const [isTurkishCitizen, setIsTurkishCitizen] = useState(user.personalInfo.nationality.country == "TR")
    const [govermentId, setGovermentId] = useState(user.personalInfo.govermentId)
    const [passport, setPassport] = useState(user.personalInfo.passport)
    const [nationality, setNationality] = useState(filtered.length > 0 && filtered[0])
    const [firstName, setFirstName] = useState(user.personalInfo.firstName)
    const [lastName, setLastName] = useState(user.personalInfo.lastName)
    const [dateOfBirth, setDateOfBirth] = useState(convertDate(user.personalInfo.dateOfBirth))
    const [gender, setGender] = useState(user.personalInfo.gender)
    const [govermentError, setGovermentError] = useState(null)
    const [dateOfBirthError, setDateOfBirthError] = useState(null)

    const [mobilePhone, setMobilePhone] = useState(user.contactInfo.mobilePhone)
    const [email, setEmail] = useState(user.contactInfo.email)
    const [mobileError, setMobileError] = useState(null)
    const [emailError, setEmailError] = useState(null)

    const [buttonDisabled, setButtonDisabled] = useState(false);
    const navigation = useNavigation()
    const dispatch = useDispatch()
    const refRBSheet = useRef();

    // const user = useSelector(state => state.authReducer.user)

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />
        })
        setContinueButtonState()
    }, [navigation, mobilePhone, mobileCode, email]);

    const setContinueButtonState = () => {

        setButtonDisabled(false)
        if (mobilePhone) {
            if (mobilePhone.length === 10 || mobilePhone.length === 15) {
                setButtonDisabled(false)
                setMobileError(null)
            } else {
                setButtonDisabled(true)
                setMobileError('Cep numaranız 10 hane olmalıdır.')
            }

            validate(email.trim())
        } else {
            setButtonDisabled(true)
        }
    }

    const handleOnCountryCodeSelection = (item) => {
        setMobileCode(item)
        refRBSheet.current.close()
    }

    const handleOnContinueButton = () => {
        user.personalInfo.gender = gender
        user.personalInfo.passport = passport
        user.contactInfo.email = email.trim()
        user.contactInfo.mobileCode = mobileCode.mobileCode
        user.contactInfo.mobilePhone = mobilePhone.replace('(', '').replace(')', '').replace(' ', '').replace(' ', '').replace(' ', '')

        navigation.navigate('ConfirmationCode', {
            email: email,
            mobile: { code: mobileCode.mobileCode, number: user.contactInfo.mobilePhone },
            mobileConfirmation: true,
            emailConfirmation: false,
            isFromUpdate: true,
            smsSent:false,
            user: user
        })
    }

    const validate = (text) => {
        const reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (reg.test(text) === false) {
            setEmailError('Geçerli bir email giriniz.')
            setButtonDisabled(true)
        }
        else {
            setEmailError(null)
            setButtonDisabled(false)
        }
    }

    const checkMernis = (user) => {
        dispatch(setLoadingAction(true))
        callCheckMernisApi(user)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    navigateRegisterStepTwo(user)
                } else {
                    Alert.alert('', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('', error.message)
            })
    }

    const navigateRegisterStepTwo = (user) => {
        dispatch(setUserAction(user))
        navigation.navigate('RegisterStepTwo')
    }

    return (
        <View style={{ flex: 1 }}>
            <ScrollView
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={styles.container}
                style={{ backgroundColor: 'white' }}>

                <PreviousNextView >
                    <UserInformation
                        isTurkishCitizen={user.personalInfo.isTurkishCitizen}
                        showSecureGovermentId={true}
                        govermentId={govermentId}
                        govermentError={govermentError}
                        showCountryModal={() => refRBSheet.current.open()}
                        nationality={nationality && nationality.country}
                        setPassport={(text) => setPassport(text)}
                        passport={passport}
                        firstName={firstName}
                        lastName={lastName}
                        dateOfBirth={dateOfBirth}
                        gender={gender}
                        setGender={(text) => setGender(text)}
                        disabled={true} />

                    <FloatingTextInput
                        label={email && 'E-posta Adresi'}
                        placeholder={'E-posta Adresi'}
                        keyboardType='email-address'
                        onChangeText={text => setEmail(text)}
                        value={email}
                        error={emailError} />

                    <MobileNumberView
                        style={{ marginHorizontal: 20 }}
                        mobilePhone={mobilePhone}
                        mobileCode={mobileCode && ('+' + mobileCode.mobileCode)}
                        error={mobileError}
                        onFocus={() => refRBSheet.current.open()}
                        onChangeText={text => setMobilePhone(text)} />
                </PreviousNextView>

                <Text style={styles.agreement}>
                        Tarafımıza ilettiğiniz kişisel verileriniz
                        <Text onPress={() => navigation.navigate('RentalTerms', { 
                            termsUrl: 'https://wsport.garenta.com.tr/mobile/agreements/AydinlatmaMetni.html', 
                            title: 'Aydınlatma Metni' })} 
                            style={styles.agreementHyperlink}> Çelik Motor Müşteri Aydınlatma Metni</Text>'nde belirtildiği şekilde işlenebilecektir.</Text>

            </ScrollView>

            <View style={[Styles.shadowBox]} >
                <FilledButton
                    disabled={buttonDisabled}
                    style={Styles.continueButton}
                    onPress={handleOnContinueButton}
                    title={'Kaydet'} />
            </View>


            <RBSheet
                height={hp(718)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <CountrySelectionModal onPress={handleOnCountryCodeSelection} mobileCodeSelection={true} current={nationality && nationality.countryName}/>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        paddingBottom:hp(40)
    },
    box1: {
        width: wp(375),
        height: hp(62) + getStatusBarHeight(),
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        // add shadows for iOS only
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
    },
    nationalityGroup: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between',
        marginTop: hp(24)
    },
    genderTitle: {
        fontFamily: 'NunitoSans-Bold',
        fontSize: hp(18),
        color: TextColors.primaryText,
        // marginTop: hp(36),
        marginTop: 36,
        marginStart: wp(20)
    },
    genderGroup: {
        marginHorizontal: wp(20),
    },
    genderRadio: {
        width: wp(335),
        marginTop: hp(10)
    },
    continueButton: {
        width: wp(335),
        marginVertical: hp(40),
        marginHorizontal: wp(20)
    },
    agreementHyperlink: {
        fontSize: hp(14),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-SemiBold',
        marginEnd: wp(20)
    },
    agreement: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginHorizontal:wp(20),
        marginTop: hp(20)
    }
})