import { useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect } from 'react'
import { useSelector } from 'react-redux'
import { BackButton } from '../../../components/buttons/BackButton'
import { AddressList } from '../../../components/views/AddressList'
import { INVOICE_ADDRESS } from '../../../utilities/constants'
import { Image, Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import { Colors, TextColors } from '../../../styles/Colors'
import { hp, wp } from '../../../styles/Dimens'

export const InvoiceAddressList = () => {
    const user = useSelector(state => state.authReducer.user)
    const addressList = user.addressList.filter((item) => item.type == INVOICE_ADDRESS)
    const navigation = useNavigation()
    const handleOnAddAddress = () => {
        navigation.navigate('AddressInformation', { addressItem: null, type: INVOICE_ADDRESS })
    }

    const handleOnEditAddress = (item) => {
        navigation.navigate('AddressInformation', { addressItem: item, type: INVOICE_ADDRESS })
    }

    return (
        <View style={styles.container}>
            {
                addressList.length > 0 ?
                    <AddressList
                        addressList={addressList}
                        handleOnEditAddress={handleOnEditAddress} />

                    :

                    <>
                        <Image style={styles.emptyImage} source={require('../../../assets/icons/ic_no_address.png')} />
                        <Text style={styles.emptyText}> Henüz fatura adresiniz bulunmamaktadır.</Text>
                    </>
            }

            <TouchableOpacity
                style={styles.addButton}
                onPress={handleOnAddAddress}>
                <Image style={{ tintColor: 'white' }} source={require('../../../assets/icons/ic_plus_simple.png')} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground,
        alignItems: 'center'
    },
    addButton: {
        width: 64,
        height: 64,
        borderRadius: 64 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: hp(45),
        end: wp(20),
        backgroundColor: Colors.primaryBrand
    },
    emptyImage: {
        marginTop: hp(99),
    },
    emptyText: {
        textAlign: 'center',
        fontSize: hp(16),
        color: TextColors.primaryText,
        marginTop: hp(38)
    }
})