import { useIsFocused, useNavigation } from '@react-navigation/native'
import React, { useEffect, useLayoutEffect } from 'react'
import { Text, View, Image, StyleSheet, Alert } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { useDispatch, useSelector } from 'react-redux'
import { callGetProfileInfoApi } from '../../../api/user/userService'
import { BackButton } from '../../../components/buttons/BackButton'
import { setLoadingAction } from '../../../redux/actions/MasterActions'
import { Colors, TextColors } from '../../../styles/Colors'
import { hp, wp } from '../../../styles/Dimens'
import { GetProfileResponse } from '../../../api/models/GetProfileResponse'
import { setUserAction } from '../../../redux/actions/AuthActions'

export const ProfileMenu = () => {

    const dispatch = useDispatch()
    const navigation = useNavigation()
    const user = useSelector(state => state.authReducer.user)
    const isFocused = useIsFocused()

    const profileMenuItems = [
        { icon: require('../../../assets/icons/ic_tab_profile.png'), title: 'Üyelik Bilgileri', index: 0 },
        { icon: require('../../../assets/icons/ic_license.png'), title: 'Ehliyet Bilgileri', index: 1 },
        // { icon: require('../../../assets/icons/ic_credit_card.png'), title: 'Ödeme Araçlarım', index: 2 },
        { icon: require('../../../assets/icons/ic_location.png'), title: 'Adreslerim', index: 3 },
        { icon: require('../../../assets/icons/ic_change_password.png'), title: 'Şifre Değiştir', index: 4 }
    ]

    // useLayoutEffect(() => {
    //     navigation.setOptions({
    //         headerLeft: () => <BackButton navigation={navigation} />
    //     })
    // }, [navigation]);

    useEffect(() => {
        getProfileInfo()
    }, [isFocused])

    const getProfileInfo = () => {
        dispatch(setLoadingAction(true))
        callGetProfileInfoApi(user.userId)
            .then(response => {
                dispatch(setLoadingAction(false))
                dispatch(setUserAction(GetProfileResponse(response.data.EXPORT, user)))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    function navigateMenu(index) {
        switch (index) {
            case 0:
                navigation.navigate('UpdateUserInformation')
                break
            case 1:
                navigation.navigate('UpdateLicenseInformation')
                break
            case 2:
                navigation.navigate('UserCreditCards')
                break
            case 3:
                navigation.navigate('AddressTabNavigator')
                break
            case 4:
                navigation.navigate('PasswordChange')
                break
            default:
                return 'Authentication failed.';
        }
    }

    const renderItem = (item) => {
        return (
            <TouchableOpacity key={item.index} style={styles.itemContainer} onPress={() => navigateMenu(item.index)}>
                <Image style={styles.icon} source={item.icon} />
                <Text style={styles.title}>{item.title}</Text>
                <Image style={styles.arrow} source={require('../../../assets/icons/ic_small_arrow_right.png')} />
            </TouchableOpacity>
        )
    }

    return (
        <ScrollView style={styles.container}>
            {
                profileMenuItems.map(item => {
                    return (
                        renderItem(item)
                    )
                })
            }
        </ScrollView>
        // <View style={styles.container}>

        // </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground,
    },
    itemContainer: {
        flex: 1,
        backgroundColor: Colors.white,
        flexDirection: 'row',
        height: hp(72),
        alignItems: 'center',
        marginHorizontal: wp(20),
        paddingStart: wp(24),
        borderRadius: hp(10),
        marginTop: hp(10)
    },
    logoutButton: {
        width: wp(150),
        marginStart: wp(24),
        marginTop: hp(10),
        backgroundColor: Colors.primaryBrandV5,
        marginBottom: hp(50)
    },
    icon: {
        tintColor: TextColors.primaryText
    },
    arrow: {
        tintColor: '#bdbdbd',
        marginEnd: wp(20)
    },
    title: {
        flex: 1,
        fontSize: hp(17),
        marginStart: wp(18),
        fontWeight:'500',
        color: TextColors.primaryText
    }
})



