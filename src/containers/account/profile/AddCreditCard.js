import React, { useLayoutEffect, useState } from 'react'
import { View, ScrollView, StyleSheet, Text, Alert } from 'react-native'
import { FloatingTextInput } from '../../../components/inputs/FloatingTextInput'
import { Colors, TextColors } from '../../../styles/Colors'
import { hp, wp } from '../../../styles/Dimens'
import { PreviousNextView } from 'react-native-keyboard-manager';
import { ImageButton } from '../../../components/buttons/ImageButton'
import { FilledButton } from '../../../components/buttons/FilledButton'
import { useNavigation } from '@react-navigation/native'
import { BackButton } from '../../../components/buttons/BackButton'
import { AddCardInformation } from '../../../components/views/AddCardInformation'
import { useDispatch, useSelector } from 'react-redux'
import { setLoadingAction } from '../../../redux/actions/MasterActions'
import { callAddCreditCardApi } from '../../../api/user/userService'
import { getErrorMessage, isSuccess } from '../../../api/apiHelpers'
import { Styles } from '../../../styles/Styles'

export const AddCreditCard = () => {
    const dispatch = useDispatch()
    const [cardHolderName, setCardHolderName] = useState('')
    const [cardNumber, setCardNumber] = useState('')
    const [expDate, setExpDate] = useState('')
    const [cvv, setCvv] = useState('')
    const [cardDescription, setCardDescription] = useState('')
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const navigation = useNavigation()
    const user = useSelector(state => state.authReducer.user)

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />
        })
        setContinueButtonState()
    }, [navigation, cardHolderName, cardNumber, expDate, cvv, cardDescription]);

    const setContinueButtonState = () => {
        if (cardHolderName.length > 0 &&
            cardNumber.length > 0 &&
            expDate.length > 0 &&
            cvv.length > 0 &&
            cardDescription.length > 0) {

                let cardInfo = {
                    month: expDate.split('/')[0],
                    year: "20" + expDate.split('/')[1],
                }
                let formattedExpDate = `01/${cardInfo.month}/${cardInfo.year}`
                today = new Date();
                someday = new Date();
                someday.setFullYear(cardInfo.year, cardInfo.month, 1);
    
                if (cardInfo.month > 12) {
                    setButtonDisabled(true)
                }
                else if (someday < today) {
                    setButtonDisabled(true)
                }
                else if (!Date.parse(formattedExpDate)) {
                    setButtonDisabled(true)
                } else {
                    setButtonDisabled(false)
                }
        } else {
            setButtonDisabled(true)
        }
    }

    const handleOnInfoButton = () => {

    }

    const handleOnContinueButton = () => {
        let cardInfo = {
            cardNo: cardNumber.replace(" ", "").replace(" ", "").replace(" ", ""),
            cardHolderName: cardHolderName,
            cvv: cvv,
            month: expDate.split('/')[0],
            year: "20" + expDate.split('/')[1],
            cardDesc: cardDescription
        }

        dispatch(setLoadingAction(true))
        callAddCreditCardApi(user.userId, cardInfo)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    Alert.alert(
                        "Başarılı",
                        "Kart ekleme işlemi başarılı",
                        [
                            { text: "Tamam", onPress: () => navigation.goBack() }
                        ],
                        { cancelable: false }
                    );
                } else {
                    Alert.alert('Hata', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    return (
        <View style={{ flex: 1 }}>
            <ScrollView
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={styles.container}
                style={{ backgroundColor: 'white' }}>

                <AddCardInformation
                    cardHolderName={cardHolderName}
                    cardNumber={cardNumber}
                    expDate={expDate}
                    cvv={cvv}
                    cardDescription={cardDescription}
                    cardHolderNameChange={(text) => setCardHolderName(text)}
                    cardNumberChange={(text) => setCardNumber(text)}
                    expDateChange={(text) => setExpDate(text)}
                    cvvChange={(text) => setCvv(text)}
                    cardDescriptionChange={(text) => setCardDescription(text)}
                    saveCreditCard={true}
                    showSwitch={true} />
            </ScrollView>

            <View style={Styles.shadowBox} >
                <FilledButton
                    disabled={buttonDisabled}
                    style={Styles.continueButton}
                    onPress={handleOnContinueButton}
                    title={'Kartımı Kaydet'} />
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        paddingBottom: 40
    },
    nationalityGroup: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        justifyContent: 'space-between',
        marginTop: hp(24)
    },
    genderTitle: {
        fontFamily: 'NunitoSans-Bold',
        fontSize: hp(18),
        color: TextColors.primaryText,
        // marginTop: hp(36),
        marginTop: 36,
        marginStart: wp(20)
    },
    genderGroup: {
        marginHorizontal: wp(20),
    },
    genderRadio: {
        width: wp(335),
        marginTop: hp(10)
    },
    continueButton: {
        width: wp(335),
        marginTop: hp(20),
        marginHorizontal: wp(20)
    },
})