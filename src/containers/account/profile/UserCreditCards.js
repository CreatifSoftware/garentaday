import { useNavigation } from '@react-navigation/native'
import React, { useEffect, useLayoutEffect, useState } from 'react'
import { FlatList, StyleSheet, View, TouchableOpacity, Image } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { isSuccess } from '../../../api/apiHelpers'
import { CreditCardResponse } from '../../../api/models/CreditCardResponse'
import { callGetCreditCardApi } from '../../../api/user/userService'
import { BackButton } from '../../../components/buttons/BackButton'
import { CreditCardItem } from '../../../components/views/CreditCardItem'
import { setLoadingAction } from '../../../redux/actions/MasterActions'
import { Colors, TextColors } from '../../../styles/Colors'
import { hp, wp } from '../../../styles/Dimens'

export const UserCreditCards = () => {
    const user = useSelector(state => state.authReducer.user)
    const navigation = useNavigation()
    const [creditCards, setCreditCards] = useState([])
    const dispatch = useDispatch()

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />
        })
    }, [navigation]);

    useEffect(() => {
        dispatch(setLoadingAction(true))
        callGetCreditCardApi(user.userId)
            .then(response => {
                if (isSuccess(response)) {
                    let cards = CreditCardResponse(response.data.EXPORT, user)
                    setCreditCards(cards)
                }
                dispatch(setLoadingAction(false))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }, [])

    const renderItem = ({ item }) => {
        return (
            <CreditCardItem item={item} />
        )
    }

    const handleOnAddCredit = () => {
        navigation.navigate('AddCreditCard')
    }

    return (
        <View style={styles.container}>
            {
                creditCards.length > 0 ?

                    <FlatList
                        contentContainerStyle={{ marginTop: hp(24) }}
                        data={creditCards}
                        renderItem={renderItem}
                        keyExtractor={item => item.merkey}
                    />

                    :

                    <>
                        <Image style={styles.emptyImage} source={require('../../../assets/icons/ic_empty_card_bg.png')} />
                        <Text style={styles.emptyText}>Henüz kayıtlı kredi kartınız bulunmamaktadır.</Text>
                    </>
            }


            <TouchableOpacity style={styles.addButton} onPress={handleOnAddCredit}>
                <Image style={{ tintColor: 'white' }} source={require('../../../assets/icons/ic_plus_simple.png')} />
            </TouchableOpacity>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground
    },
    addButton: {
        width: 64,
        height: 64,
        borderRadius: 64 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: hp(45),
        end: wp(20),
        backgroundColor: Colors.primaryBrand
    },
    emptyImage: {
        width: 93,
        height: 93,
        marginTop: hp(99),
        alignSelf: 'center'
    },
    emptyText: {
        textAlign: 'center',
        fontSize: hp(16),
        color: TextColors.primaryTextV3,
        marginTop: hp(38)
    },
})