import { CommonActions, useNavigation } from '@react-navigation/native'
import React, { useEffect } from 'react'
import { SafeAreaView, Text, View, Image, StyleSheet, Alert } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { useDispatch, useSelector } from 'react-redux'
import { isSuccess } from '../../api/apiHelpers'
import { callGetFavoriteBranchApi, callGetInvoiceListApi } from '../../api/user/userService'
import { FilledButton } from '../../components/buttons/FilledButton'
import { AccountHeader } from '../../components/headers/AccountHeader'
import { setUserAction } from '../../redux/actions/AuthActions'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { clearReservationDataAction, setReservationDataAction } from '../../redux/actions/ReservationActions'
import { setFavoriteBranchsAction } from '../../redux/actions/UserActions'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { getJsonData, saveJsonData } from '../../utilities/helpers'

export const AccountMenu = () => {

    const dispatch = useDispatch()
    const navigation = useNavigation()
    const user = useSelector(state => state.authReducer.user)

    const accoutnMenuItems = [
        { icon: require('../../assets/icons/ic_tab_profile.png'), title: 'Profilim', index: 0 },
        // { icon: require('../../assets/icons/ic_bills.png'), title: 'Faturalarım', index: 1 },
        { icon: require('../../assets/icons/ic_complaint.png'), title: 'Şikayetlerim', index: 2 },
        { icon: require('../../assets/icons/ic_documents.png'), title: 'Kiralama Koşulları', index: 3 },
        { icon: require('../../assets/icons/ic_notification.png'), title: 'Bildirimlerim', index: 4 },
        { icon: require('../../assets/icons/ic_branch.png'), title: 'Şube Listesi', index: 5 },
        // { icon: require('../../assets/icons/ic_settings.png'), title: 'Ayarlar', index: 6 },
        { icon: require('../../assets/icons/ic_contact_us.png'), title: 'İletişim', index: 7 },
        { icon: require('../../assets/icons/ic_support.png'), title: 'Bize Yazın', index: 8 }
    ]

    function handleOnLogout() {
        Alert.alert(
            "",
            "Uygulamadan çıkış yapmak yapılacaktır, emin misiniz ?",
            [
                {
                    text: "Vazgeç",
                    style: 'cancel'
                },
                {
                    text: "Çıkış Yap", onPress: () => logoutConfirmed(),
                    style: 'destructive'
                }
            ],
            { cancelable: false }
        );
    }

    const logoutConfirmed = () => {
        getJsonData('loginInfo')
            .then(response => {
                let loginInfo = {
                    rememberMe: response.rememberMe,
                    username: response.rememberMe ? response.username : '',
                    password: response.rememberMe ? response.password : '', isLoggedIn: false,
                    user: null
                }
                saveJsonData('loginInfo', loginInfo)
                dispatch(setUserAction(null))
                dispatch(clearReservationDataAction())
                let data = { isFromReservation: false }
                saveJsonData('isFromReservation', data)
                const loginAction = CommonActions.reset({
                    index: 0,
                    routes: [{ name: 'LoginStack' }]
                })
                navigation.dispatch(loginAction)
            })
    }

    function getInvoiceList() {
        dispatch(setLoadingAction(true))
        callGetInvoiceListApi(user)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    navigation.navigate('InvoiceList', { invoiceList: response.data.EXPORT.ET_BILL })
                } else {
                    navigation.navigate('InvoiceList', { invoiceList: [] })
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    function navigateMenu(index) {
        switch (index) {
            case 0:
                if (user != null) {
                    navigation.navigate('ProfileMenu')
                } else {
                    Alert.alert('Uyarı', 'Profilinizi görebilmek için giriş yapmış olmalısınız')
                }
                break
            case 1:
                if (user != null) {
                    getInvoiceList()
                } else {
                    Alert.alert('Uyarı', 'Faturalarınızı sayfasını görebilmek için giriş yapmış olmalısınız')
                }

                break
            case 2:
                if (user != null) {
                    navigation.navigate('ComplaintListTabNavigator')
                } else {
                    Alert.alert('Uyarı', 'Şikayetlerinizi görebilmek için giriş yapmış olmalısınız')
                }

                break
            case 3:
                navigation.navigate('RentalTerms', { termsUrl: 'https://mobil.garenta.com.tr/agreements/GeneralRentingTerms.html', title: 'Kiralama Koşulları' })
                break
            case 4:
                if (user != null) {
                    navigation.navigate('Notifications')
                } else {
                    Alert.alert('Uyarı', 'Bildirimlerim sayfasını görebilmek için giriş yapmış olmalısınız')
                }
                break
            case 5:
                if (user) {
                    dispatch(setLoadingAction(true))
                    callGetFavoriteBranchApi(user.userId)
                        .then((response) => {
                            dispatch(setLoadingAction(false))
                            dispatch(setFavoriteBranchsAction(response.data.EXPORT))
                            navigation.navigate('BranchSelection', { fromSettings: true })
                        })
                        .catch((error) => {
                            dispatch(setLoadingAction(false))
                            dispatch(setFavoriteBranchsAction([]))
                            navigation.navigate('BranchSelection', { fromSettings: true })
                        })
                } else {
                    dispatch(setFavoriteBranchsAction([]))
                    navigation.navigate('BranchSelection', { fromSettings: true })
                }

                break
            case 6:
                navigation.navigate('SettingsMenu')
                break
            case 7:
                navigation.navigate('ContactUsMenu')
                break
            case 8:
                navigation.navigate('ComplaintForm')
                break

            default:
                return 'Authentication failed.';
        }
    }


    const renderItem = (item) => {
        return (
            <TouchableOpacity key={item.index} style={styles.itemContainer} onPress={() => navigateMenu(item.index)}>
                <Image style={styles.icon} source={item.icon} />
                <Text style={styles.title}>{item.title}</Text>
            </TouchableOpacity>
        )
    }

    const navigateLogin = () => {
        dispatch(clearReservationDataAction())
        let data = { isFromReservation: false }
        saveJsonData('isFromReservation', data)
        const loginAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'LoginStack' }]
        })
        navigation.dispatch(loginAction)
    }

    const navigateRegister = () => {
        dispatch(clearReservationDataAction())
        navigation.navigate('RegisterStepOne')
    }

    return (
        <View style={styles.container}>
            <AccountHeader
                onLoginPress={navigateLogin}
                onRegisterPress={navigateRegister}
                user={user} />
            <ScrollView>
                {
                    accoutnMenuItems.map(item => {
                        return (
                            renderItem(item)
                        )
                    })
                }

                {
                    user &&
                    <FilledButton
                        title={'Çıkış Yap'}
                        style={styles.logoutButton}
                        source={require('../../assets/icons/ic_logout.png')}
                        iconStyle={{ tintColor: Colors.primaryBrand }}
                        textStyle={{ color: Colors.primaryBrand }}
                        onPress={handleOnLogout} />
                }
            </ScrollView>
        </View>

    )

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    itemContainer: {
        flexDirection: 'row',
        height: hp(64),
        alignItems: 'center',
        paddingStart: wp(24)
    },
    logoutButton: {
        width: wp(150),
        marginStart: wp(24),
        marginTop: hp(10),
        backgroundColor: Colors.primaryBrandV5,
        marginBottom: hp(50)
    },
    icon: {
        tintColor: TextColors.primaryText
    },
    title: {
        fontSize: hp(17),
        marginStart: wp(18),
        fontWeight: '500',
        color: TextColors.primaryText
    }
})



