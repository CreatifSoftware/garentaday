import { useNavigation } from "@react-navigation/native";
import React, { useLayoutEffect } from "react";
import { StyleSheet, View } from "react-native";
import WebView from "react-native-webview";
import { BackButton } from "../../components/buttons/BackButton";
import { Colors } from "../../styles/Colors";

export const RentalTerms = ({ route }) => {
    const termsUrl = route.params.termsUrl
    const navigation = useNavigation()

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />,
            headerTitle: route.params.title
        })
    }, [navigation]);

    return (
        <View style={styles.container}>
            <WebView
                incognito={true}
                source={{ uri: termsUrl }}
                style={{ marginTop: 20 }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    }
})