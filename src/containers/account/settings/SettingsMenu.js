import { useNavigation } from '@react-navigation/native'
import React, { useEffect, useLayoutEffect } from 'react'
import { Text, View, Image, StyleSheet, Alert } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { useDispatch, useSelector } from 'react-redux'
import { callGetProfileInfoApi } from '../../../api/user/userService'
import { BackButton } from '../../../components/buttons/BackButton'
import { setLoadingAction } from '../../../redux/actions/MasterActions'
import { Colors, TextColors } from '../../../styles/Colors'
import { hp, wp } from '../../../styles/Dimens'

export const SettingsMenu = () => {

    const navigation = useNavigation()
    const user = useSelector(state => state.authReducer.user)
    const settingsMenuItems = [
        { icon: require('../../../assets/icons/ic_communication_pref.png'), title: 'İletişim Tercihleri', index: 0 },
        { icon: require('../../../assets/icons/ic_language.png'), title: 'Dil', index: 1 },
    ]

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />
        })
    }, [navigation]);

    function navigateMenu(index) {
        switch (index) {
            case 0:
                if (user != null) {
                    navigation.navigate('CommunicationPref')
                } else {
                    Alert.alert('Uyarı','İletişim tercihlerininizi değiştirebilmek için üye girişi yapmalısınız.')
                }
                break
            case 1:
                // navigation.navigate('CommunicationPref')
                break
            default:
                return 'Authentication failed.';
        }
    }

    const renderItem = (item) => {
        return (
            <TouchableOpacity key={item.index} style={styles.itemContainer} onPress={() => navigateMenu(item.index)}>
                <Image style={styles.icon} source={item.icon} />
                <Text style={styles.title}>{item.title}</Text>
                <Image style={styles.arrow} source={require('../../../assets/icons/ic_small_arrow_right.png')} />
            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.container}>
            {
                settingsMenuItems.map(item => {
                    return (
                        renderItem(item)
                    )
                })
            }
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground,
        paddingTop:hp(14)
    },
    itemContainer: {
        backgroundColor:Colors.white,
        flexDirection: 'row',
        height: hp(72),
        alignItems: 'center',
        marginHorizontal: wp(20),
        paddingStart: wp(24),
        borderRadius:hp(10),
        marginTop: hp(10)
    },
    logoutButton: {
        width: wp(150),
        marginStart: wp(24),
        marginTop: hp(10),
        backgroundColor: Colors.primaryBrandV5,
        marginBottom: hp(50)
    },
    icon: {
        tintColor: TextColors.primaryText
    },
    arrow:{
        tintColor:'#bdbdbd',
        marginEnd:wp(20)
    },
    title: {
        flex:1,
        fontSize: hp(17),
        marginStart: wp(18),
        fontWeight:'700',
        color: TextColors.primaryText
    }
})



