import { useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect, useState } from 'react'
import { StyleSheet, View, Text, Switch, Image } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { callUpdateUserApi } from '../../../api/user/userService'
import { BackButton } from '../../../components/buttons/BackButton'
import { HorizontalSeperator } from '../../../components/HorizontalSeperator'
import { setUserAction } from '../../../redux/actions/AuthActions'
import { setLoadingAction } from '../../../redux/actions/MasterActions'
import { Colors, TextColors } from '../../../styles/Colors'
import { hp, wp } from '../../../styles/Dimens'

export const CommunicationPref = () => {
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const [allowEmail, setAllowEmail] = useState(false)
    const [allowSms, setAllowSms] = useState(false)
    const [allowPhone, setAllowPhone] = useState(false)
    const user = useSelector(state => state.authReducer.user)

    const menuItems = [
        { title: 'E-Posta', subtitle: 'E-posta ile kampanyalardan haberdar olmak istiyorum.', index: 0 },
        { title: 'SMS', subtitle: 'SMS ile kampanyalardan haberdar olmak istiyorum.', index: 1 },
        { title: 'Telefon', subtitle: 'Kampanyalardan haberdar olmak için cep telefonumdan aranmak istiyorum.', index: 2 }
    ]

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />
        })
    }, [navigation]);


    const updatePermissions = () => {
        user.permissionInfo.allowSms = allowSms
        user.permissionInfo.allowPhone = allowPhone
        user.permissionInfo.allowEmail = allowEmail

        dispatch(setLoadingAction(true))
        callUpdateUserApi(user)
            .then(response => {
                dispatch(setLoadingAction(false))
                dispatch(setUserAction(user))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    const toggleSwitch = (item) => {
        switch (item.index) {
            case 0:
                setAllowEmail(!allowEmail)
                updatePermissions()
                break
            case 1:
                setAllowSms(!allowSms)
                updatePermissions()
                break
            case 2:
                setAllowPhone(!allowPhone)
                updatePermissions()
                break
            default:
                return 'Authentication failed.';
        }


    }

    const renderItem = (item) => {
        const isEnabled = item.index == 0 ? allowEmail : item.index == 1 ? allowSms : allowPhone

        return (
            <View key={item.index} style={{ width: wp(335), marginHorizontal: wp(20) }}>
                <View style={styles.itemContainer}>
                    <View style={{ flex: 1,marginEnd:20 }}>
                        <Text style={styles.title}>{item.title}</Text>
                        <Text style={styles.subtitle}>{item.subtitle}</Text>
                    </View>
                    <Switch
                        trackColor={{ false: Colors.white, true: Colors.primaryBrand }}
                        thumbColor={Colors.white}
                        onValueChange={() => toggleSwitch(item)}
                        value={isEnabled}
                    />
                </View>
                {
                    item.index !== 2 &&
                    <HorizontalSeperator />
                }


            </View>
        )
    }

    return (
        <View style={styles.container}>
            {
                menuItems.map(item => {
                    return (
                        renderItem(item)
                    )
                })
            }

            <View style={styles.infoContainer}>
                <Image source={require('../../../assets/icons/ic_info.png')} style={{ tintColor: Colors.primaryBrand }} />
                <Text style={styles.infoText}>İletişim ayarlarınız sadece kampanyalar özelinde geçerlidir. İletişim tercihleriniz kapalı olmasına rağmen üyelik bilgileri ve rezervasyonlarınız için bildirim, eposta, sms veya arama almaya devam edersiniz.</Text>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    itemContainer: {
        height: hp(102),
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    infoContainer: {
        backgroundColor: '#ffeae0',
        paddingVertical: hp(10),
        flexDirection: 'row',
        paddingStart: wp(20),
        paddingEnd: wp(20),
        marginHorizontal: wp(20),
        borderRadius: hp(10),
        marginTop: hp(30)
    },
    infoText: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontWeight: '700',
        marginHorizontal: wp(10),
    },
    title: {
        fontSize: hp(16),
        color: TextColors.primaryText,
        fontWeight: '700'
    },
    subtitle: {
        fontSize: hp(14),
        color: TextColors.primaryTextV3,
        marginTop: hp(8)
    }
})