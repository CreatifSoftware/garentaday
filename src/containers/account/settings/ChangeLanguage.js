import React, { useEffect, useRef } from 'react'
import { StyleSheet, View } from 'react-native'
import RBSheet from 'react-native-raw-bottom-sheet'
import { useSelector } from 'react-redux'
import { FloatingSelectionInput } from '../../../components/inputs/FloatingSelectionInput'
import { setLanguageAction } from '../../../redux/actions/MasterActions'
import { Styles } from '../../../styles/Styles'
import { KEY_TURKISH_LANG } from '../../../utilities/constants'
import { LanguageModal } from '../../modals/LanguageModal'

export const ChangeLanguage = () => {

    const dispatch = useDispatch()
    const langId = useSelector(state => state.masterReducer.langId)
    const [language, setLanguage] = useState(langId)
    const refRBSheet = useRef()

    const handleOnLanguageChange = (lang) => {
        dispatch(setLanguageAction(lang))
    }

    return (
        <View style={styles.container}>
            <FloatingSelectionInput
                label={'Dil'}
                placeholder={'Dil'}
                onFocus={() => refRBSheet.current.open()}
                value={language === 'TR' ? KEY_TURKISH_LANG : KEY_ENGLISH_LANG} />

            <RBSheet
                height={hp(257)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon:Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <LanguageModal onPress={handleOnLanguageChange} current={language === 'TR' ? KEY_TURKISH_LANG : KEY_ENGLISH_LANG} />
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})