import { useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect } from 'react'
import { ScrollView, StyleSheet, TouchableOpacity, Image, View, Text, Linking } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { callSetFavoriteBranchApi } from '../../../api/user/userService'
import { BackButton } from '../../../components/buttons/BackButton'
import { FilledButton } from '../../../components/buttons/FilledButton'
import { HorizontalSeperator } from '../../../components/HorizontalSeperator'
import { setLoadingAction } from '../../../redux/actions/MasterActions'
import { setFavoriteBranchsAction } from '../../../redux/actions/UserActions'
import { Colors, TextColors } from '../../../styles/Colors'
import { hp, wp } from '../../../styles/Dimens'
import { convertTime, isFavoriteBranch } from '../../../utilities/helpers'

export const BranchDetail = ({ route }) => {

    const dispatch = useDispatch()
    const user = useSelector(state => state.authReducer.user)
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const favoriteBranchs = useSelector(state => state.userReducer.favoriteBranchs)
    const workingHours = useSelector(state => state.masterReducer.workingHours)
    const navigation = useNavigation()

    const { branch } = route.params

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />,
        })
    }, [navigation]);

    const handleOnSetFavorite = () => {
        let isFavorite = isFavoriteBranch(favoriteBranchs,branch.branchId)

        dispatch(setLoadingAction(true))
        callSetFavoriteBranchApi(user.userId, branch.branchId, isFavorite ? 'D' : 'C')
            .then(response => {
                dispatch(setFavoriteBranchsAction(response.data.EXPORT))
                dispatch(setLoadingAction(false))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    const handleOnSelectBranch = () => {
        reservation.pickupBranch = branch
        reservation.dropoffBranch = branch
        navigation.navigate('ReservationStack')
    }

    const handleOnCallBranch = () => {
        Linking.openURL(
            `tel:${branch.telephone}`
        )
    }

    const renderHeader = () => {
        let isFavorite = isFavoriteBranch(favoriteBranchs,branch.branchId)
        return (
            <View style={styles.header}>
                <View style={{ marginEnd: wp(20) }}>
                    <Text style={styles.city}>{branch.cityName}</Text>
                    <Text style={styles.branch}>{branch.branchName}</Text>
                </View>

                <View style={{ alignSelf: 'flex-end' }}>
                    {/* <Text style={styles.distance}>3km</Text> */}
                    {
                        user &&
                        <TouchableOpacity style={{ justifyContent: 'center',alignItems:'flex-end', marginTop: hp(21),width:60, height:60 }} onPress={handleOnSetFavorite}>
                            <Image style={{tintColor:isFavorite ? Colors.primaryBrand : Colors.checkboxBorder}} source={isFavorite ? require('../../../assets/icons/ic_favorite_fill.png') : require('../../../assets/icons/ic_favorite.png')}></Image>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        )
    }

    const renderAddress = () => {
        return (
            <View style={styles.addressContainer}>
                <View style={{ marginEnd: wp(20), flexDirection: 'row' }}>
                    <Image source={require('../../../assets/icons/ic_location.png')} style={{ tintColor: Colors.primaryBrand }}></Image>
                    <Text style={styles.addressTitle}>Adres Bilgileri</Text>
                </View>
                <Text style={styles.address}>{branch.address}</Text>
            </View>
        )
    }

    const renderWorkingHours = () => {
        const filteredWorkingHours = workingHours.filter(function (arg) {
            return arg.ALT_SUBE == branch.branchId;
        })

        return (
            <View style={styles.addressContainer}>
                <View style={{ marginEnd: wp(20), marginBottom: hp(20), flexDirection: 'row' }}>
                    <Image source={require('../../../assets/icons/ic_alarm.png')} style={{ tintColor: Colors.primaryBrand }}></Image>
                    <Text style={styles.addressTitle}>Çalışma Saatleri</Text>
                </View>

                {
                    filteredWorkingHours.map(item => {
                        return (
                            <View style={{ marginEnd: wp(20), marginTop: hp(10), flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={styles.workingDay}>{item.CADAYTX}</Text>
                                <Text style={styles.workingHour}>{convertTime(item.BEGTI)} - {convertTime(item.ENDTI)}</Text>
                            </View>
                        )
                    })
                }
            </View>
        )
    }

    return (
        <ScrollView contentContainerStyle={{ backgroundColor: Colors.white }}>
            {renderHeader()}
            <HorizontalSeperator style={{ marginHorizontal: wp(20) }} />
            {renderAddress()}
            <HorizontalSeperator style={{ marginHorizontal: wp(20) }} />
            <FilledButton
                showIcon={true}
                source={require('../../../assets/icons/ic_location.png')}
                style={{ marginHorizontal: wp(20), marginTop: hp(20) }}
                title={'Şubeyi Seçin'}
                onPress={handleOnSelectBranch} />

            <FilledButton
                showIcon={true}
                source={require('../../../assets/icons/ic_phone.png')}
                iconStyle={{ tintColor: Colors.primaryBrand }}
                style={{ marginHorizontal: wp(20), marginVertical: hp(20), backgroundColor: Colors.primaryBrandV5 }}
                title={branch.telephone}
                textStyle={{ color: Colors.primaryBrand }}
                onPress={handleOnCallBranch} />

            <HorizontalSeperator style={{ marginHorizontal: wp(20) }} />
            {renderWorkingHours()}
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    header: {
        paddingHorizontal: wp(20),
        backgroundColor: Colors.white,
        paddingTop: hp(24),
        paddingBottom: hp(20),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    addressContainer: {
        paddingHorizontal: wp(20),
        backgroundColor: Colors.white,
        paddingTop: hp(24),
        paddingBottom: hp(20),
        justifyContent: 'space-between'
    },
    addressTitle: {
        fontSize: hp(16),
        fontWeight:'700',
        color: TextColors.primaryText,
        marginStart: wp(8)
    },
    address: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginTop: hp(21)
    },
    city: {
        fontSize: hp(18),
        fontWeight:'700',
        color: TextColors.primaryText
    },
    ditance: {
        fontSize: hp(14),
        fontWeight:'700',
        color: TextColors.primaryTextV2,
        marginEnd: wp(20)
    },
    branch: {
        fontSize: hp(18),
        color: TextColors.primaryText,
        marginTop: hp(21)
    },
    workingDay: {
        fontSize: hp(16),
        color: TextColors.primaryText
    },
    workingHour: {
        fontSize: hp(16),
        color: TextColors.primaryText,
        fontWeight:'700',
    }
})