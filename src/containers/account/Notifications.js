import { useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect } from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'
import { BackButton } from '../../components/buttons/BackButton'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'

export const Notifications = () => {

    const navigation = useNavigation()

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />
        })
    }, [navigation]);

    return(
        <View style={styles.container}>
            <Image style={styles.emptyImage} source={require('../../assets/icons/ic_empty_notification.png')}/>
            <Text style={styles.emptyText}> Henüz bildirim bulunmamaktadır.</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:Colors.mainBackground,
        alignItems:'center'
    },
    emptyImage:{
        marginTop:hp(99),
    },
    emptyText:{
        textAlign:'center',
        fontSize:wp(16),
        color:TextColors.primaryText,
        marginTop:hp(38)
    }
})