import React from 'react'
import { Image, ScrollView, StyleSheet, View, Text, Linking } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { wp } from '../../styles/Dimens'

export const ContactUsMenu = () => {

    const handleOnCall = (event) => {
        Linking.openURL(
            `tel:${event._dispatchInstances.memoizedProps.children}`
        )
    }

    const handleOnMail = (event) => {
        Linking.openURL(
            `mailto:${event._dispatchInstances.memoizedProps.children}`
        )
    }

    return (
        <ScrollView contentContainerStyle={{ backgroundColor: Colors.mainBackground }}>
            <View style={styles.itemContainer}>
                <Image style={styles.image} source={require('../../assets/icons/ic_call_center.png')} />
                <Text style={styles.title}>Çağrı Merkezi</Text>
                <Text style={styles.subtitle}>Sorularınız, memnuniyetiniz, öneri ve görüşleriniz için, 7 gün 24 saat e-posta aracılığı ile veya 08:00 - 22:00 arası çağrı merkezimizden bize ulaşabilirsiniz.</Text>
                <Text style={styles.link} onPress={(event) => handleOnCall(event)}>444 5 478</Text>
                <Text style={styles.link} onPress={(event) => handleOnMail(event)}>crm@garenta.com.tr</Text>
            </View>

            <View style={styles.itemContainer}>
                <Image style={styles.image} source={require('../../assets/icons/ic_reservation_center.png')} />
                <Text style={styles.title}>7/24 Rezervasyon Merkezi</Text>
                <Text style={styles.subtitle}>Size özel fiyatlandırma ve çözümler ile zengin marka ve en yeni model araç kiralama teklifleri için 8:00 - 22:00 arası, tüm Türkiye’de hizmet veren “Rezervasyon Merkezi” hattımızı arayabilir veya e-posta gönderebilirsiniz.</Text>
                <Text style={styles.link} onPress={(event) => handleOnCall(event)}>444 5 478</Text>
                <Text style={styles.link} onPress={(event) => handleOnMail(event)}>4445478@garenta.com.tr</Text>
            </View>

            <View style={styles.itemContainer}>
                <Image style={styles.image} source={require('../../assets/icons/ic_help_center.png')} />
                <Text style={styles.title}>7/24 Acil Yardım Hattı</Text>
                <Text style={styles.subtitle}>Aracınızla bir kaza geçirmeniz, aracın çalınması, arzalanması, lastik patlaması ve her türlü acil yardıma ihtiyaç duyduğunuz durumda, 7 gün 24 saat, tüm Türkiye’de hizmet veren “Acil Yardım” hattımızı arayabilirsiniz.</Text>
                <Text style={styles.link} onPress={(event) => handleOnCall(event)}>0850 222 54 78</Text>
            </View>

            <View style={styles.itemContainer}>
                <Image style={styles.image} source={require('../../assets/icons/ic_center_office.png')} />
                <Text style={styles.title}>Genel Müdürlük</Text>
                <Text style={styles.subtitle}>Çelik Motor Ticaret A.Ş.{"\n"}Garenta Yeni Nesil Rent a Car{"\n"}Fatih Sultan Mehmet Mahallesi Balkan Caddesi No : 58 Buyaka E Blok Tepeüstü / Ümraniye 34771 İSTANBUL</Text>
                <Text style={styles.link} onPress={(event) => handleOnCall(event)}>0216 656 26 00</Text>
                <Text style={styles.link} onPress={(event) => handleOnMail(event)}>info@garenta.com.tr</Text>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    image: {
        marginTop: 40,
    },
    itemContainer: {
        marginBottom: 4,
        backgroundColor: 'white',
        alignItems: 'center',
        paddingHorizontal: wp(20)
    },
    title: {
        fontSize: 18,
        fontFamily: 'NunitoSans-Bold',
        color: TextColors.primaryText,
        textAlign: 'center',
        marginTop:20
    },
    subtitle: {
        fontSize: 14,
        fontFamily: 'NunitoSans-Regular',
        color: TextColors.primaryText,
        textAlign: 'center',
        marginTop:10,
        marginBottom:20
    },
    link: {
        fontSize: 18,
        fontFamily: 'NunitoSans-Black',
        color: Colors.primaryBrand,
        marginBottom:20
    }
})