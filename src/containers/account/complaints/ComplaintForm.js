import { useNavigation } from '@react-navigation/core';
import React, { useLayoutEffect, useRef, useState } from 'react';
import { Text, SafeAreaView, StyleSheet, Dimensions, ScrollView, View, Alert, Image, FlatList, TouchableOpacity } from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import { useDispatch, useSelector } from 'react-redux';
import { FilledButton } from '../../../components/buttons/FilledButton';
import { FloatingSelectionInput } from '../../../components/inputs/FloatingSelectionInput';
import { FloatingTextInput } from '../../../components/inputs/FloatingTextInput';
import { MobileNumberView } from '../../../components/views/MobileNumberView';
import { Colors, TextColors } from '../../../styles/Colors';
import { hp, wp } from '../../../styles/Dimens';
import { Styles } from '../../../styles/Styles';
import { COMPLAINT_RESPONSE_CHANNEL, COMPLAINT_SUBJECT } from '../../../utilities/constants';
import { ComplaintResponseChannelSelectionModal } from '../../modals/ComplaintResponseChannelSelectionModal';
import { ComplaintSubjectSelectionModal } from '../../modals/ComplaintSubjectSelectionModal';
import { CountrySelectionModal } from '../../modals/CountrySelectionModal';
import { PreviousNextView } from 'react-native-keyboard-manager';
import { setLoadingAction } from '../../../redux/actions/MasterActions';
import { callCreateComplaintApi, callGetcomplaintCategoriesApi } from '../../../api/user/userService';
import { getErrorMessage, isSuccess } from '../../../api/apiHelpers';
// import * as ImagePicker from 'react-native-image-picker';
import { Button } from 'react-native-share';

export const ComplaintForm = ({ route }) => {
    const user = useSelector(state => state.authReducer.user)
    const [mobileCode, setMobileCode] = useState({
        LAND1: "TR",
        LANDX: "Türkiye",
        mobileCode: user ? user.contactInfo.mobileCode : '90'
    })
    // const [isTurkishCitizen, setIsTurkishCitizen] = useState(user.personalInfo.nationality.country == "TR")
    // const [govermentId, setGovermentId] = useState( user.personalInfo.govermentId)
    // const [passport, setPassport] = useState(user.personalInfo.passport)
    // const [nationality, setNationality] = useState(user.personalInfo.nationality)
    const [name, setName] = useState(user ? user.personalInfo.firstName : '')
    const [surname, setSurname] = useState(user ? user.personalInfo.lastName : '')
    const [email, setEmail] = useState(user ? user.contactInfo.email : '')
    const [mobilePhone, setMobilePhone] = useState(user ? user.contactInfo.mobilePhone : '')
    const [subject, setSubject] = useState(null)
    const [responseChannel, setResponseChannel] = useState('')
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [categoryGuids, setCategoryGuids] = useState([])
    const [subjectList, setSubjectList] = useState([])
    const [buttonDisabled, setButtonDisabled] = useState(false)
    const [emailError, setEmailError] = useState(null)
    const [response, setResponse] = useState(null);

    const navigation = useNavigation()
    const refRBSheetMobileCodes = useRef();
    const refRBSheetResponseChannels = useRef();
    const refRBSheetSubjects = useRef();
    const dispatch = useDispatch()

    // const onButtonPress = React.useCallback(() => {
    //     let options = {
    //         selectionLimit: 0,
    //         mediaType: 'mixed',
    //         selectionLimit: 5 - (response ? response.assets : 0),
    //         quality: 0,
    //         includeBase64: true
    //     }
    //     ImagePicker.launchImageLibrary(options, setResponse);
    //     // if (type === 'capture') {
    //     //     ImagePicker.launchCamera(options, setResponse);
    //     // } else {
    //     //     ImagePicker.launchImageLibrary(options, setResponse);
    //     // }
    // }, []);

    useLayoutEffect(() => {
        setContinueButtonState()
    }, [navigation, name, surname, email, mobilePhone, subject, responseChannel, description, title]);

    React.useEffect(() => {
        dispatch(setLoadingAction(true))
        callGetcomplaintCategoriesApi()
            .then(response => {
                dispatch(setLoadingAction(false))
                setCategoryGuids(response.data.EXPORT.ET_KONU)
                let filtered = response.data.EXPORT.ET_KONU.filter(arg => arg.PARENT_GUID === "00000000000000000000000000000000")
                setSubjectList(filtered)
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }, [])

    const setContinueButtonState = () => {
        if (name.length > 0 &&
            surname.length > 0 &&
            subject &&
            responseChannel.length > 0 &&
            title.length > 0 &&
            description.length > 0) {

            if (responseChannel !== COMPLAINT_RESPONSE_CHANNEL.MAIL) {
                if (mobilePhone.length > 0 && mobilePhone.length === 10 || mobilePhone.length === 15) {
                    setButtonDisabled(false)
                } else {
                    setButtonDisabled(true)
                }
            }

            if (responseChannel == COMPLAINT_RESPONSE_CHANNEL.MAIL) {
                validate(email.trim())
            }
        } else {
            setButtonDisabled(true)
        }
    }

    const validate = (text) => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            setEmailError('Geçerli bir email giriniz.')
            setButtonDisabled(true)
        }
        else {
            setEmailError(null)
            setButtonDisabled(false)
        }
    }

    const handleOnContinueButton = () => {
        let responseChannelCode = ""
        if (responseChannel == COMPLAINT_RESPONSE_CHANNEL.PHONE) {
            responseChannelCode = "01"
        }
        else if (responseChannel == COMPLAINT_RESPONSE_CHANNEL.SMS) {
            responseChannelCode = "02"
        }
        else if (responseChannel == COMPLAINT_RESPONSE_CHANNEL.MAIL) {
            responseChannelCode = "03"
        }

        let filtered = categoryGuids.filter(arg => arg.CATEGORY_ID === subject.CATEGORY_ID)

        let complaintObj = {
            userId: user ? user.userId : '',
            firstName: name,
            lastName: surname,
            govermentId: "",
            passportNo: "",
            mobilePhone: mobilePhone,
            email: email.trim,
            title: title,
            description: description,
            subject: filtered[0].CATEGORY_GUID,
            responseChannel: responseChannelCode,
            //assets: response.assets
        }

        dispatch(setLoadingAction(true))
        callCreateComplaintApi(complaintObj)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    let message = `${response.data.EXPORT.ES_RETURN.SIKAYET_NO} numarası ile oluşturulan şikayetinizi, mobil uygulamamızdan, çağrı merkezimizden ve şubelerimizden takip edebilirsiniz. İyi günler dileriz.`
                    Alert.alert(
                        "Başarılı",
                        message,
                        [
                            { text: "Tamam", onPress: () => navigation.goBack() }
                        ],
                        { cancelable: false }
                    );
                } else {
                    Alert.alert('Hata', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    const handleOnCountryCodeSelection = (item) => {
        setMobileCode(item)
        refRBSheetMobileCodes.current.close()
    }

    const handleOnSubjectSelection = (item) => {
        setSubject(item)
        refRBSheetSubjects.current.close()
    }

    const handleOnResponseChannelSelection = (item) => {
        setResponseChannel(item)
        refRBSheetResponseChannels.current.close()
    }

    const deletePhoto = (item) => {
        const filtered = response.assets.filter(asset => {
            return asset.uri != item.uri
        });
        response.assets = filtered
        setResponse({ ...response })
    }

    const renderPhotoItem = ({ item }) => {
        return (
            <View key={item.uri} style={styles.image}>
                <Image
                    resizeMode="cover"
                    resizeMethod="scale"
                    style={{ width: wp(85), height: wp(85), backgroundColor: Colors.borderColor, borderRadius: wp(10) }}
                    source={{ uri: item.uri }}
                />

                <TouchableOpacity
                    onPress={() => deletePhoto(item)}
                    style={{
                        width: wp(35),
                        height: hp(35),
                        start: wp(70),
                        bottom: hp(60),
                        position: 'absolute'
                    }}>
                    <View style={{
                        backgroundColor: Colors.primaryBrand, width: wp(25),
                        height: wp(25), borderRadius: wp(17), justifyContent: 'center'
                    }}>
                        <Image style={{ tintColor: Colors.white, width: 15, height: 15, alignSelf: 'center', justifyContent: 'center' }} source={require('../../../assets/icons/ic_delete.png')} />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <ScrollView
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={styles.container}
                style={{ backgroundColor: 'white' }}>

                <Text style={styles.infoText}>Size daha iyi bir hizmet verebilmemiz için lütfen istenen bilgileri eksiksiz doldurunuz.</Text>
                <PreviousNextView>
                    <FloatingTextInput
                        label={'Adınız*'}
                        placeholder={'Adınız*'}
                        onChangeText={(text) => setName(text)}
                        value={name}
                    />

                    <FloatingTextInput
                        label={'Soyadınız*'}
                        placeholder={'Soyadınız*'}
                        onChangeText={(text) => setSurname(text)}
                        value={surname}
                    />

                    {
                        // subject.CATEGORY_ID != "02" && subject.CATEGORY_ID != "03" &&
                        <>
                            <FloatingSelectionInput
                                label={'Dönüş Kanalı*'}
                                placeholder={'Dönüş Kanalı*'}
                                onFocus={() => refRBSheetResponseChannels.current.open()}
                                value={responseChannel} />
                            {
                                responseChannel === COMPLAINT_RESPONSE_CHANNEL.MAIL ?
                                    <FloatingTextInput
                                        label={'E-Posta*'}
                                        placeholder={'E-Posta*'}
                                        onChangeText={(text) => setEmail(text)}
                                        value={email}
                                        error={emailError}
                                    />

                                    :

                                    <MobileNumberView
                                        style={{ marginHorizontal: 20 }}
                                        mobilePhone={mobilePhone}
                                        mobileCode={mobileCode && ('+' + mobileCode.mobileCode)}
                                        // error={mobileError}
                                        onFocus={() => refRBSheetMobileCodes.current.open()}
                                        onChangeText={text => setMobilePhone(text)} />
                            }
                        </>
                    }

                    <FloatingSelectionInput
                        label={'Konu*'}
                        placeholder={'Konu*'}
                        onFocus={() => refRBSheetSubjects.current.open()}
                        value={subject && subject.CATEGORY_TEXT} />

                    <FloatingTextInput
                        label={'Başlık*'}
                        placeholder={'Başlık*'}
                        onChangeText={(text) => setTitle(text)}
                        value={title}
                    />

                    <FloatingTextInput
                        label={'Açıklama*'}
                        placeholder={'Açıklama*'}
                        onChangeText={(text) => setDescription(text)}
                        value={description}
                    />
                    {/* <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                            style={{
                                marginTop: hp(30),
                                marginStart: wp(20),
                                marginEnd: wp(8),
                                width: wp(85), height: wp(85), backgroundColor: Colors.borderColor, borderRadius: wp(10), alignItems: 'center',
                                justifyContent: 'center'
                            }}
                            onPress={onButtonPress}>
                            <>
                                <Image style={{ tintColor: Colors.primaryBrand }} source={require('../../../assets/icons/ic_plus_simple.png')} />
                                <Text style={styles.addDocumentTitle}>Döküman Ekle</Text>
                            </>

                        </TouchableOpacity>
                        {
                            response && response.assets &&
                            <FlatList
                                horizontal={true}
                                contentContainerStyle={{ marginEnd: wp(16) }}
                                data={response ? response.assets : []}
                                renderItem={renderPhotoItem}
                                keyExtractor={item => item.uri}
                            />
                        }

                    </View> */}

                    <Text style={styles.agreement}>
                        Tarafımıza ilettiğiniz kişisel verileriniz
                        <Text onPress={() => navigation.navigate('RentalTerms', {
                            termsUrl: 'https://wsport.garenta.com.tr/mobile/agreements/AydinlatmaMetni.html',
                            title: 'Aydınlatma Metni'
                        })}
                            style={styles.agreementHyperlink}>Çelik Motor Müşteri Aydınlatma Metni</Text>'nde belirtildiği şekilde işlenebilecektir.
                    </Text>
                </PreviousNextView>
            </ScrollView>

            <View style={Styles.shadowBox} >
                <FilledButton
                    disabled={buttonDisabled}
                    style={Styles.continueButton}
                    onPress={handleOnContinueButton}
                    title={'Gönder'} />
            </View>

            {/* <RBSheet
                height={hp(718)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    container: {
                        borderRadius: hp(24),
                    }
                }}>
                <CountrySelectionModal onPress={handleOnCountrySelection} removeTurkey={true} current={nationality && nationality.countryName} />
            </RBSheet> */}

            <RBSheet
                height={Dimensions.get('screen').height * 0.8}
                ref={refRBSheetMobileCodes}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <CountrySelectionModal onPress={handleOnCountryCodeSelection} mobileCodeSelection={true} />
            </RBSheet>

            <RBSheet
                ref={refRBSheetResponseChannels}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <ComplaintResponseChannelSelectionModal
                    onPress={handleOnResponseChannelSelection}
                    current={responseChannel} />
            </RBSheet>

            <RBSheet
                ref={refRBSheetSubjects}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <ComplaintSubjectSelectionModal
                    onPress={handleOnSubjectSelection}
                    current={subject}
                    selectionArr={subjectList} />
            </RBSheet>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        paddingBottom: 40
    },
    infoText: {
        color: TextColors.primaryText,
        fontSize: hp(16),
        marginHorizontal: wp(20),
        marginTop: hp(20),
    },
    continueButton: {
        width: wp(335),
        position: 'absolute',
        bottom: hp(44),
        marginHorizontal: wp(20)
    },
    agreementHyperlink: {
        fontSize: hp(14),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-SemiBold',
        marginEnd: wp(20)
    },
    agreement: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginHorizontal: wp(20),
        marginTop: hp(20)
    },
    image: {
        paddingTop: hp(30),
        marginHorizontal: wp(8),
        alignItems: 'center',
        borderRadius: hp(10)
    },
    addDocumentTitle: {
        fontSize: hp(10),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-SemiBold',
        marginTop: wp(8)
    }
})