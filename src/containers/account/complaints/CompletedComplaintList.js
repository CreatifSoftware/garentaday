import { useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect, useState } from 'react'
import { Image, Text, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native'
import { Colors, TextColors } from '../../../styles/Colors'
import { hp, wp } from '../../../styles/Dimens'
import { ComplaintList } from '../../../components/views/ComplaintList'
import { callGetCompletedComplaintListApi } from '../../../api/user/userService'
import { useDispatch, useSelector } from 'react-redux'
import { setLoadingAction } from '../../../redux/actions/MasterActions'
import { ComplaintListResponse } from '../../../api/models/ComplaintListResponse'

export const CompletedComplaintList = () => {
    const [complaintList, setComplaintList] = useState([])
    const user = useSelector(state => state.authReducer.user)
    const navigation = useNavigation()
    const dispatch = useDispatch()

    useLayoutEffect(() => {
        navigation.setOptions({
            headerShown: true
        })
    }, [navigation]);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', e => {
            getCompletedComplaintList()
        });

        return unsubscribe;
    }, [navigation]);

    const getCompletedComplaintList = () => {
        dispatch(setLoadingAction(true))
        callGetCompletedComplaintListApi(user.userId)
            .then(response => {
                dispatch(setLoadingAction(false))
                setComplaintList(ComplaintListResponse(response.data.EXPORT.ET_RESULT))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    const handleOnComplaintClick = (item) => {
        // navigation.navigate('AddressInformation', { addressItem: null, type: PERSONAL_ADDRESS })
    }

    const handleOnAddNewComplaint = (item) => {
        navigation.navigate('ComplaintForm')
    }

    return (
        <SafeAreaView style={styles.container}>
            {
                complaintList && complaintList.length > 0 ?
                    <ComplaintList
                        complaintList={complaintList}
                        onPress={() => handleOnComplaintClick(item)} />

                    :

                    <>
                        <Image style={styles.emptyImage} source={require('../../../assets/icons/ic_no_complaint.png')} />
                        <Text style={styles.emptyText}> Kapalı şikayetiniz bulunmamaktadır.</Text>
                    </>
            }
            <TouchableOpacity 
                style={styles.addButton} 
                onPress={handleOnAddNewComplaint}>
                <Image style={{ tintColor: 'white' }} source={require('../../../assets/icons/ic_plus_simple.png')} />
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground,
        alignItems: 'center'
    },
    addButton: {
        width: 64,
        height: 64,
        borderRadius: 64 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: hp(45),
        end: wp(20),
        backgroundColor: Colors.primaryBrand
    },
    emptyImage: {
        marginTop: hp(99),
    },
    emptyText: {
        textAlign: 'center',
        fontSize: hp(16),
        color: TextColors.primaryText,
        marginTop: hp(38),
        marginHorizontal: wp(50)
    }
})