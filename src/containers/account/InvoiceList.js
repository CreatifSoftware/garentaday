import React from 'react'
import { Image, StyleSheet, View, Text, FlatList } from 'react-native'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { Styles } from '../../styles/Styles'
import { FilledButton } from '../..//components/buttons/FilledButton'
import { convertDate } from '../../utilities/helpers'

export const InvoiceList = ({ route }) => {
    const invoiceList = route.params.invoiceList

    const handleOnPreview = () => {

    }

    const handleOnDownload = () => {

    }

    const renderItem = ({ item }) => {
        return (
            <View style={localStyles.container}>
                <View style={{ flexDirection: 'row' }}>
                    <Image source={require('../../assets/icons/ic_bills.png')} />
                    <Text style={[Styles.title, { marginBottom: hp(12), marginStart: wp(20), color: Colors.primaryBrand }]}>{convertDate(String(item.FKDAT))}</Text>
                </View>

                <Text style={[Styles.title, { marginBottom: hp(12) }]}>PNR No: {item.OBJECT_CODE}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <FilledButton
                        onPress={handleOnPreview}
                        title={'İncele'}
                        style={styles.previewButton}
                        textStyle={styles.previewButtonText} />
                    <FilledButton
                        onPress={handleOnDownload}
                        title={'İndir'}
                        style={{ flex: 1, height: 40, marginStart:wp(10) }} />
                </View>

            </View>
        )
    }

    return (
        <View style={styles.container}>
            {
                invoiceList.length > 0 ?
                    <FlatList
                        data={invoiceList}
                        renderItem={renderItem}
                        keyExtractor={item => item.invoiceId} />

                    :

                    <>
                        <Image style={styles.emptyImage} source={require('../../assets/icons/ic_no_bills.png')} />
                        <Text style={styles.emptyText}> Henüz faturanız bulunmamaktadır.</Text>
                    </>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground,
        alignItems: 'center'
    },
    emptyImage: {
        width: 93,
        height: 93,
        marginTop: hp(99),
        alignSelf: 'center'
    },
    emptyText: {
        textAlign: 'center',
        fontSize: hp(16),
        color: TextColors.primaryTextV3,
        marginTop: hp(38)
    },
    previewButton: {
        backgroundColor: Colors.primaryBrandV5,
        flex: 1, 
        height: 40
    },
    previewButtonText: {
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',
    },
})

const localStyles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        marginHorizontal: wp(20),
        width:wp(335),
        paddingVertical: hp(20),
        paddingHorizontal: wp(20),
        borderRadius: hp(10),
        marginTop: hp(16),
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
    },
    addressTitle: {
        fontSize: hp(16),
        fontWeight: '700',
        color: TextColors.primaryText,
    },
    address: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginTop: hp(5)
    },
    cardType: {
        alignSelf: 'center',
        marginHorizontal: wp(20),
    }
})