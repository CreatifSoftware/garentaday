import { CommonActions, useNavigation } from '@react-navigation/native';
import React, { useEffect } from 'react'
import { StyleSheet, Platform, Alert, Linking } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { isSuccess } from '../api/apiHelpers';
import { callCheckVersionApi, callInitialDataApi, callSignInApi } from '../api/auth/authServices';
import { GetProfileResponse } from '../api/models/GetProfileResponse';
import { LoginResponse } from '../api/models/LoginResponse';
import { callGetProfileInfoApi } from '../api/user/userService';
import { setUserAction } from '../redux/actions/AuthActions'
import { setInitialDataAction, setLoadingAction } from '../redux/actions/MasterActions';
import { clearReservationDataAction } from '../redux/actions/ReservationActions';
import { getFavoriteBranchsAction } from '../redux/actions/UserActions';
import { Colors } from '../styles/Colors';
import { hp, wp } from '../styles/Dimens';
import { encodeUTF16LE, getJsonData, saveJsonData } from '../utilities/helpers';
import RNBootSplash from "react-native-bootsplash";

export const Splash = () => {
    const dispatch = useDispatch()
    const user = useSelector(state => state.authReducer.user)
    const navigation = useNavigation()

    useEffect(() => {
        dispatch(setLoadingAction(false))
        checkVersion()
    }, [])

    const getLoginInfo = async () => {
        getJsonData('loginInfo')
            .then(response => {
                if (response == null) {
                    navigateLogin()
                } else if (response.isLoggedIn) {
                    dispatch(setUserAction(response.user))
                    navigateHome()
                } else {
                    navigateLogin()
                }
            })
            .catch(() => {
                navigateLogin()
            })
    }

    const checkVersion = async () => {
        callCheckVersionApi()
            .then((response) => {
                debugger
                if (isSuccess(response)) {
                    // dispatch(initialDataAction())
                    callInitialDataApi()
                        .then(response => {
                            dispatch(setInitialDataAction(response))
                            getLoginInfo()
                        })
                        .catch(error => {
                            Alert.alert('Hata', 'Sistemsel bir hata oluştu, lütfen tekrar deneyin')
                            navigateLogin()
                        })

                } else {
                    Alert.alert(
                        "Uyarı",
                        "Uygulamamızın yeni versiyonunu indirmenizi rica ederiz",
                        [
                            {
                                text: "Vazgeç",
                                style: 'cancel'
                            },
                            {
                                text: "Güncelle", onPress: () => navigateToStore(),
                                style: 'destructive'
                            }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch(() => {
                navigateLogin()
            })
    }

    const navigateToStore = () => {
        const link = ""
        if (Platform.OS === 'ios') {
            link = 'itms-apps://apps.apple.com/id/app/garenta/id1107610443?mt=8';
        } else {
            link = "market://details?id=acropole.garenta"
        }
        Linking.canOpenURL(link).then(supported => {
            supported && Linking.openURL(link);
        }, (err) => console.log(err));
    }

    const callLoginApi = (username, password) => {
        callSignInApi(username, password)
            .then(response => {
                if (isSuccess(response)) {
                    let user = LoginResponse(response.data.EXPORT)
                    dispatch(getFavoriteBranchsAction(user.userId))
                    getProfileInfo(user, password)
                } else {
                    navigateLogin()
                }
            })
            .catch(() => {
                navigateLogin()
            })
    }

    const getProfileInfo = (user, password) => {
        callGetProfileInfoApi(user.userId)
            .then(response => {
                user.password = encodeUTF16LE(password)
                dispatch(setUserAction(GetProfileResponse(response.data.EXPORT, user)))
                navigateHome()
            })
            .catch(() => {
                navigateLogin()
            })
    }

    const navigateLogin = () => {
        let data = { isFromReservation: false }
        saveJsonData('isFromReservation', data)
        dispatch(clearReservationDataAction())
        dispatch(setUserAction(null))
        RNBootSplash.hide({ fade: true });
        const loginAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'LoginStack' }]
        })
        navigation.dispatch(loginAction)
    }

    const navigateHome = () => {
        let data = { isFromReservation: false }
        saveJsonData('isFromReservation', data)
        dispatch(clearReservationDataAction())
        RNBootSplash.hide({ fade: true });
        const homeAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'BottomNavigator' }]
        })
        navigation.dispatch(homeAction)
    }

    return (
        null
        // <View style={styles.container}>
        //     <Image resizeMode={'contain'} resizeMethod={'auto'} source={require('../assets/icons/garenta_logo_white.png')} />
        // </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primaryBrand,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        height: hp(40),
        width: wp(240),
    }
})



