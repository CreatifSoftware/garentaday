
import React, { useEffect, useState } from 'react'
import { StyleSheet, TouchableOpacity, Text, SafeAreaView, FlatList, View } from "react-native"
import { useSelector } from 'react-redux';
import { Colors, TextColors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { filterCitiesByCountryCode } from '../../utilities/helpers'
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';

export const CitySelectionModal = (props) => {

    String.prototype.turkishToEnglish = function () {
        return this.replace('Ğ', 'g')
            .replace('Ü', 'u')
            .replace('Ş', 's')
            .replace('I', 'i')
            .replace('İ', 'i')
            .replace('Ö', 'o')
            .replace('Ç', 'c')
            .replace('ğ', 'g')
            .replace('ü', 'u')
            .replace('ş', 's')
            .replace('ı', 'i')
            .replace('İ', 'i')
            .replace('ö', 'o')
            .replace('ç', 'c')
    }

    // const dispatch = useDispatch()
    //const cities = useSelector(state => state.masterReducer.cities)
    let cities = filterCitiesByCountryCode(props.countryCode)
    const branchs = useSelector(state => state.masterReducer.branchs)
    const [searchText, setSearchText] = useState('')
    const [filteredData, setFilteredData] = useState(cities)

    React.useEffect(() => {
        if (props.fromCampaign) {
            

            let tempCities = []
            for (let index = 0; index < cities.length; index++) {
                const item = cities[index];
                let filteredBranchs = branchs.filter(arg => arg.city === item.city)
                if (filteredBranchs.length > 0){
                    tempCities.push(item)
                }
            }

            tempCities.splice(0, 0, {
                country: "",
                city: "",
                cityName: "Tümü"
            });

            setFilteredData(tempCities)
        }
    }, [])

    const handleOnSelection = (item) => {
        let obj = item
        if (props.mobileCodeSelection) {
            const filteredData = mobileCodes.filter(function (arg) {
                return arg.LAND1 == item.LAND1;
            })
            if (filteredData.length > 0) {
                obj.mobileCode = filteredData[0].TELEFTO
            }
        }
        props.onPress(obj)
    }

    const filterCities = (text) => {
        const filtered = cities.filter(function (arg) {
            const itemData = arg.cityName ? arg.cityName.turkishToEnglish().toUpperCase() : ''.turkishToEnglish().toUpperCase();
            const textData = text.turkishToEnglish().toUpperCase()

            return itemData.indexOf(textData) > -1
        })

        if (text === "") {
            setSearchText(text)
            setFilteredData(cities)
        }
        else {
            // const filtered = cities.filter(item.cityName.toLowerCase().includes(text.toLowerCase()))
            setSearchText(text)
            setFilteredData(filtered)
        }
    }

    const renderItem = ({ item }) => {
        return (
            <RadioButtonItem key={item} label={item.cityName} selected={props.current == item.cityName} onPress={() => handleOnSelection(item)} />
            // <TouchableOpacity style={{ height: hp(42), flexDirection: 'row', alignItems: 'center' }} onPress={() => handleOnSelection(item)}>
            //     <Text style={{ color: TextColors.primaryText, fontSize: hp(16), marginStart: wp(18) }}>{item.cityName}</Text>

            // </TouchableOpacity>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <FloatingTextInput
                label={'Şehir Ara'}
                placeholder={'Şehir Ara'}
                onChangeText={(text) => filterCities(text)}
                value={searchText} />
            <HorizontalSeperator style={{ marginVertical: hp(20) }} />
            <FlatList
                data={filteredData}
                renderItem={renderItem}
                keyExtractor={item => item.city}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        alignSelf: 'center',
        color: Colors.white,
    },
    icon: {
        tintColor: Colors.primaryBrand,
        position: 'absolute',
        start: wp(36),
        bottom: 36,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
