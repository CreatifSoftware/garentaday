
import React from 'react'
import { StyleSheet, View, Text } from "react-native"
import { useDispatch } from 'react-redux';
import { Colors, TextColors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';
import { ImageButton } from '../../components/buttons/ImageButton';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { FilledButton } from '../../components/buttons/FilledButton';
import { Styles } from '../../styles/Styles';

export const CouponCodeModal = (props) => {
    
    return (
        <View style={{ flex: 1, paddingBottom: 40 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: hp(20) }}>
                <Text style={styles.title}>İndirim Kodu Ekle</Text>
                <ImageButton
                    onPress={props.onDismiss}
                    style={{ marginEnd: wp(20) }}
                    imageStyle={{ tintColor: 'black' }}
                    source={require('../../assets/icons/ic_close.png')} />
            </View>

            <HorizontalSeperator style={{ marginVertical: hp(20) }} />

            <FloatingTextInput
                label={'İndirim Kodu'}
                placeholder={'İndirim Kodu'}
                onChangeText={props.setCouponCode}
                value={props.couponCode}
            />

            <FilledButton
                onPress={props.onPress}
                style={styles.continueButton}
                title={'İndirim Kodu Ekle'} />
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        alignSelf: 'center',
        color: Colors.white,
    },
    title: {
        marginStart: wp(20),
        fontSize: hp(20),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-Black'
    },
    continueButton: {
        width: wp(335),
        marginTop: hp(40),
        marginHorizontal: wp(20)
    },
})
