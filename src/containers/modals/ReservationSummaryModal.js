
import React from 'react'
import { ScrollView, View, Text, StyleSheet, SafeAreaView } from 'react-native';
import { ImageButton } from '../../components/buttons/ImageButton';
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { ReservationSummaryInfo } from '../../components/views/ReservationSummaryInfo';
import { TextColors } from '../../styles/Colors';
import { hp, wp } from '../../styles/Dimens';
import { SORT_TYPES } from '../../utilities/constants';

export const ReservationSummaryModal = (props) => {
    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: hp(20) }}>
                <Text style={styles.title}>Özet</Text>
                <ImageButton 
                    onPress={props.onPress}
                    style={{ marginEnd: wp(20) }} 
                    imageStyle={{tintColor:'black'}}
                    source={require('../../assets/icons/ic_close.png')} />
            </View>

            <HorizontalSeperator style={{ marginVertical: hp(20) }} />
            <ScrollView contentContainerStyle={{paddingBottom: hp(40)}}>
                <ReservationSummaryInfo />
            </ScrollView>

        </View>

    )
}

const styles = StyleSheet.create({
    title: {
        marginStart: wp(20),
        fontSize: hp(20),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-Black'
    },
})
