
import React, { useEffect, useState } from 'react'
import { StyleSheet, TouchableOpacity, Text, SafeAreaView, FlatList, View } from "react-native"
import { useSelector } from 'react-redux';
import { Colors, TextColors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { filterDistrictsByCityCode } from '../../utilities/helpers'
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';

export const DistrictSelectionModal = (props) => {
    String.prototype.turkishToEnglish = function () {
        return this.replace('Ğ', 'g')
            .replace('Ü', 'u')
            .replace('Ş', 's')
            .replace('I', 'i')
            .replace('İ', 'i')
            .replace('Ö', 'o')
            .replace('Ç', 'c')
            .replace('ğ', 'g')
            .replace('ü', 'u')
            .replace('ş', 's')
            .replace('ı', 'i')
            .replace('İ', 'i')
            .replace('ö', 'o')
            .replace('ç', 'c')
    }

    const districts = useSelector(state => state.masterReducer.districts)
    const filteredDistricts = filterDistrictsByCityCode(props.cityCode, props.countryCode, districts)
    const [searchText, setSearchText] = useState('')
    const [filteredData, setFilteredData] = useState(filteredDistricts)

    const handleOnSelection = (item) => {
        let obj = item
        if (props.mobileCodeSelection) {
            const filteredData = mobileCodes.filter(function (arg) {
                return arg.LAND1 == item.LAND1;
            })
            if (filteredData.length > 0) {
                obj.mobileCode = filteredData[0].TELEFTO
            }
        }
        props.onPress(obj)
    }

    const filterDistricts = (text) => {
        const filtered = filteredDistricts.filter(function (arg) {
            const itemData = arg.districtName ? arg.districtName.turkishToEnglish().toUpperCase() : ''.turkishToEnglish().toUpperCase();
            const textData = text.turkishToEnglish().toUpperCase()

            return itemData.indexOf(textData) > -1
        })

        if (text === "") {
            setSearchText(text)
            setFilteredData(filteredDistricts)
        }
        else {
            setSearchText(text)
            setFilteredData(filtered)
        }
    }

    const renderItem = ({ item }) => {
        return (
            // <TouchableOpacity style={{ height: hp(42), flexDirection: 'row', alignItems: 'center' }} onPress={() => handleOnSelection(item)}>
            //     <Text style={{ color: TextColors.primaryText, fontSize: hp(16), marginStart: wp(18) }}>{item.districtName}</Text>
            // </TouchableOpacity>
            <RadioButtonItem key={item} label={item.districtName} selected={props.current == item.districtName} onPress={() => handleOnSelection(item)} />
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <FloatingTextInput
                label={'İlçe Ara'}
                placeholder={'İlçe Ara'}
                onChangeText={(text) => filterDistricts(text)}
                value={searchText} />
            <HorizontalSeperator style={{ marginTop: hp(20) }} />
            <FlatList
                contentContainerStyle={{ marginTop: hp(24), paddingBottom: 60 }}
                data={filteredData}
                renderItem={renderItem}
                keyExtractor={item => item.district}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        alignSelf: 'center',
        color: Colors.white,
    },
    icon: {
        tintColor: Colors.primaryBrand,
        position: 'absolute',
        start: wp(36),
        bottom: 36,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
