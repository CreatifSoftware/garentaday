
import React from 'react'
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';

export const LicenseClassModal = (props) => {
    const licenseClassArr = ["B", "C", "D", "E", "USB"]

    return (
        licenseClassArr.map(item => {
            return (
                <RadioButtonItem key={item} label={item} selected={props.current == item} onPress={() => props.onPress(item)} />
            )
        })
    )
}
