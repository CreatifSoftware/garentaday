
import React, { useState } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, ScrollView,FlatList } from "react-native"
import { useDispatch } from 'react-redux';
import { Colors, TextColors } from "../../styles/Colors";
import { getStatusBarHeight, hp, wp } from '../../styles/Dimens';
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';
import { FilterGroupList } from '../../components/views/FilterGroupList';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { ImageButton } from '../../components/buttons/ImageButton';
import { Styles } from '../../styles/Styles';
import { FilledButton } from '../../components/buttons/FilledButton';

export const FilterCampaignModal = (props) => {
    const [selectedCampaignTypes, setSelectedCampaignTypes] = useState(props.selectedCampaignTypes)
    const [selectedCampaignSections, setSelectedCampaignSections] = useState(props.selectedCampaignSections)

    const campaignTypes = [
        { code: "D", title: "Günlük" },
        { code: "M", title: "Aylık" }
    ];

    const handleCampaignTypeSelection = (item) => {
        let list = selectedCampaignTypes
        let filtered = list.filter(arg => arg.code === item.code)
        if (filtered.length > 0) {
            var index = list.indexOf(item);
            list.splice(index, 1);
        } else {
            list.push(item)
        }

        setSelectedCampaignTypes([...list])
    }

    const handleCampaignSectionSelection = (item) => {
        let list = selectedCampaignSections
        let filtered = list.filter(arg => arg.title === item.title)
        if (filtered.length > 0) {
            var index = list.indexOf(item);
            list.splice(index, 1);
        } else {
            list.push(item)
        }

        setSelectedCampaignSections([...list])
    }

    const renderCampaignTypeItem = ({ item }) => {
        let isSelected = selectedCampaignTypes.filter(arg => arg.code === item.code).length > 0
        return (
            <TouchableOpacity
                onPress={() => handleCampaignTypeSelection(item)}
                style={isSelected ? styles.advancedFilterContainerSelected : styles.advancedFilterContainer}>
                <Text style={isSelected ? styles.advancedFilterItemSelected : styles.advancedFilterItem}>{item.title}</Text>
            </TouchableOpacity>
        )
    }

    const renderCampaignSectionItem = ({ item }) => {
        let isSelected = selectedCampaignSections.filter(arg => arg.title === item.title).length > 0
        return (
            <TouchableOpacity
                onPress={() => handleCampaignSectionSelection(item)}
                style={isSelected ? styles.advancedFilterContainerSelected : styles.advancedFilterContainer}>
                <Text style={isSelected ? styles.advancedFilterItemSelected : styles.advancedFilterItem}>{item.title}</Text>
            </TouchableOpacity>
        )
    }

    const renderCampaignTypeList = () => {
        return (
            <View style={{ paddingStart: wp(20) }}>
                <Text style={styles.advancedFilterTitle}>Kiralama</Text>
                <FlatList
                    horizontal={true}
                    contentContainerStyle={{ marginTop: hp(16) }}
                    data={campaignTypes}
                    renderItem={renderCampaignTypeItem}
                    keyExtractor={item => item.code}
                />
            </View>
        )
    }

    const renderCampaignSectionList = () => {
        return (
            // <View style={{ paddingStart: wp(20), marginTop: hp(20) }}>
                <View style={{ paddingStart: wp(20)}}>
                <Text style={styles.advancedFilterTitle}>İş Birlikleri</Text>
                <FlatList
                    horizontal={false}
                    numColumns={5}
                    columnWrapperStyle={{ flexWrap: 'wrap', flex: 1}}
                    contentContainerStyle={{ marginTop: hp(16) }}
                    extraData={props.sectionList}
                    data={props.sectionList}
                    renderItem={renderCampaignSectionItem}
                    keyExtractor={item => item.SANZIMAN}
                />
            </View>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: hp(20) }}>
                <Text style={styles.title}>Filtrele</Text>
                <ImageButton
                    onPress={props.onDismiss}
                    style={{ marginEnd: wp(20) }}
                    imageStyle={{ tintColor: 'black' }}
                    source={require('../../assets/icons/ic_close.png')} />
            </View>

            <HorizontalSeperator style={{ marginVertical: hp(20) }} />
            <ScrollView >
                {/* {renderCampaignTypeList()} */}
                {renderCampaignSectionList()}
            </ScrollView>

            <View style={styles.shadowBox} >
                <FilledButton
                    // disabled={(selectedFuelTypes.length == 0 && selectedTransmissionTypes.length == 0)}
                    style={Styles.continueButton}
                    title={'Filtrele'}
                    onPress={() => props.handleOnFilterSelected(selectedCampaignSections, selectedCampaignTypes)} />
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    shadowBox: {
        width: wp(375),
        height: hp(62) + getStatusBarHeight(),
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0,
        // add shadows for iOS only
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
    },
    title: {
        marginStart: wp(20),
        fontSize: hp(20),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-Black'
    },
    continueButton: {
        width: wp(335),
        marginTop: hp(20),
        marginHorizontal: wp(20)
    },
    advancedFilterTitle: {
        fontSize: hp(18),
        color: TextColors.primaryTextV4,
        fontWeight: '700',
    },
    advancedFilterContainerSelected: {
        backgroundColor: Colors.primaryBrand,
        height: hp(40),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: hp(22),
        marginEnd: wp(10),
        marginTop:10
    },
    advancedFilterItemSelected: {
        fontSize: hp(14),
        color: 'white',
        fontWeight: '700',
        paddingHorizontal: wp(12),
    },
    advancedFilterContainer: {
        backgroundColor: Colors.white,
        height: hp(40),
        borderWidth: 1,
        borderColor: Colors.borderColor,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: hp(22),
        marginTop:10,
        marginEnd: wp(10)
    },
    advancedFilterItem: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontWeight: '700',
        paddingHorizontal: wp(12),
    }
})
