


import React, { useEffect, useState } from 'react'
import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { Colors, TextColors } from '../../styles/Colors';
import { hp, wp } from '../../styles/Dimens';
import { Styles } from '../../styles/Styles';

export const ReservationProcessModal = (props) => {
    const[modalItems, setModalItems] = useState([])

    useEffect(() => {
        const items = [
            { icon: require('../../assets/icons/ic_date.png'), title:'Takvime Ekle', index: 2 },
            { icon: require('../../assets/icons/ic_share.png'), title:'Paylaş', index: 3 }
        ]
        if (!props.reservation.isEditable && props.reservation.isCancelable) {
            items.push({ icon: require('../../assets/icons/ic_close_circle.png'), title:'İptal Et', index: 4 })
        }else{
            // if (!props.reservation.isPaid) {
            //     items.push({ icon: require('../../assets/icons/ic_credit_card.png'), title:'Ödeme Yap', index: 0 })
            // }

            items.push({ icon: require('../../assets/icons/ic_update.png'), title:'Değişiklik Yap', index: 1 })
            items.push({ icon: require('../../assets/icons/ic_close_circle.png'), title:'İptal Et', index: 4 })
        }

        setModalItems(items)
    },[])

    function didSelectRow(item) {
        switch (item.index) {
            case 0:
                props.onMakePayment()
                break
            case 1:
                props.onChangeReservation()
                break
            case 2:
                props.addCalendar()
                break
            case 3:
                props.shareReservation()
                break
            case 4:
                props.onCancelReservation()
                break
            default:
                break;
        }
    }

    return (
        <SafeAreaView>
            <Text style={styles.title}>Rezervasyon İşlemleri</Text>
            <HorizontalSeperator style={{ marginVertical: hp(20) }} />
            {
                modalItems.map(item => {
                    return (
                        <TouchableOpacity
                            key={item.index}
                            style={{ flexDirection: 'row', height: hp(48) }}
                            onPress={() => didSelectRow(item)}>
                            <Image style={{ tintColor: Colors.primaryBrand, marginHorizontal: wp(20) }} source={item.icon} />
                            <Text style={Styles.content}>{item.title}</Text>
                        </TouchableOpacity>
                    )
                })
            }
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    title: {
        fontFamily: 'NunitoSans-Black',
        marginStart: wp(20),
        fontSize: hp(20),
        color: TextColors.primaryText,
        fontWeight: 'bold',
        marginTop: hp(20)
    },
})
