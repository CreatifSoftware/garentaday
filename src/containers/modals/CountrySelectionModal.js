
import React, { useState } from 'react'
import { StyleSheet, TouchableOpacity, Text, SafeAreaView, FlatList, View } from "react-native"
import { useSelector } from 'react-redux';
import { Colors, TextColors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';

export const CountrySelectionModal = (props) => {
    String.prototype.turkishToEnglish = function () {
        return this.replace('Ğ', 'g')
            .replace('Ü', 'u')
            .replace('Ş', 's')
            .replace('I', 'i')
            .replace('İ', 'i')
            .replace('Ö', 'o')
            .replace('Ç', 'c')
            .replace('ğ', 'g')
            .replace('ü', 'u')
            .replace('ş', 's')
            .replace('ı', 'i')
            .replace('İ', 'i')
            .replace('ö', 'o')
            .replace('ç', 'c')
    }

    const countries = useSelector(state => state.masterReducer.countries)
    const mobileCodes = useSelector(state => state.masterReducer.mobileCodes)
    const [searchText, setSearchText] = useState('')
    const [filteredData, setFilteredData] = useState(props.removeTurkey ? countries.filter(arg => arg.country != 'TR') : countries)

    React.useEffect(() => {
        
    },[])

    const handleOnSelection = (item) => {
        let obj = item
        if (props.mobileCodeSelection){
            const filteredData = mobileCodes.filter(function(arg){
                return arg.LAND1 == item.country;
            })
            if (filteredData.length > 0) {
                obj.mobileCode = filteredData[0].TELEFTO
            }
        }
        props.onPress(obj)
    }

    const getMobileCode = (item) => {
        const filteredData = mobileCodes.filter(function(arg){
            return arg.LAND1 == item.country;
        })
        if (filteredData.length > 0) {
            return '(+'+filteredData[0].TELEFTO+')'
        }
    }

    const filterCountries = (text) => {
        const filtered = countries.filter(function (arg) {
            const itemData = arg.countryName ? arg.countryName.turkishToEnglish().toUpperCase() : ''.turkishToEnglish().toUpperCase();
            const textData = text.turkishToEnglish().toUpperCase()

            return itemData.indexOf(textData) > -1
        })

        if (text === "") {
            setSearchText(text)
            setFilteredData(countries)
        }
        else{
            // const filtered = countries.filter(item => item.countryName.toLowerCase().includes(text.toLowerCase()))
            setSearchText(text)
            setFilteredData(filtered)
        }
    }

    const renderItem = ({ item }) => {
        return (
            <RadioButtonItem key={item} label={`${item.countryName} ${props.mobileCodeSelection ? getMobileCode(item) : ''}`}  selected={props.current == item.countryName} onPress={() => handleOnSelection(item)} />
            // <TouchableOpacity style={{ height: hp(42), flexDirection: 'row', alignItems: 'center' }} onPress={() => handleOnSelection(item)}>
            //     <View style={{ width: wp(24), height: hp(16), backgroundColor: Colors.seperatorColor, marginStart: wp(18) }}></View>
            //     <Text style={{ color: TextColors.primaryText, fontSize: hp(16), marginStart: wp(18) }}>{item.countryName} {props.mobileCodeSelection && getMobileCode(item)}</Text>
            // </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={{flex:1}}>
            <FloatingTextInput
                label={'Ülke Ara'}
                placeholder={'Ülke Ara'}
                onChangeText={(text) => filterCountries(text)}
                value={searchText} />
            {/* <Image style={styles.icon} source={require('../../assets/icons/ic_search.png')} /> */}
            <HorizontalSeperator style={{ marginTop: hp(20) }} />
            <FlatList
                contentContainerStyle={{ marginTop: hp(24) }}
                data={filteredData}
                renderItem={renderItem}
                keyExtractor={item => item.country}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        alignSelf: 'center',
        color: Colors.white,
    },
    icon: {
        tintColor: Colors.primaryBrand,
        position: 'absolute',
        start: wp(36),
        bottom: 36,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
