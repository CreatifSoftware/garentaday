
import React, { useState } from 'react'
import { StyleSheet, View, Text } from "react-native"
import { useDispatch } from 'react-redux';
import { Colors, TextColors } from "../../styles/Colors";
import { getStatusBarHeight, hp, wp } from '../../styles/Dimens';
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';
import { FilterGroupList } from '../../components/views/FilterGroupList';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { ImageButton } from '../../components/buttons/ImageButton';
import { Styles } from '../../styles/Styles';
import { FilledButton } from '../../components/buttons/FilledButton';

export const FilterGroupListModal = (props) => {
    const [selectedTransmissionTypes, setSelectedTransmissionTypes] = useState(props.transmissionTypes)
    const [selectedFuelTypes, setSelectedFuelTypes] = useState(props.fuelTypes)

    const handleFuelTypeSelection = (item) => {
        let list = selectedFuelTypes
        let filtered = list.filter(arg => arg.YAKIT_TIP === item.YAKIT_TIP)
        if (filtered.length > 0) {
            var index = list.indexOf(item);
            list.splice(index, 1);
        } else {
            list.push(item)
        }

        setSelectedFuelTypes([...list])
    }

    const handleTransmissionTypeSelection = (item) => {
        let list = selectedTransmissionTypes
        let filtered = list.filter(arg => arg.SANZIMAN === item.SANZIMAN)
        if (filtered.length > 0) {
            var index = list.indexOf(item);
            list.splice(index, 1);
        } else {
            list.push(item)
        }

        setSelectedTransmissionTypes([...list])
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: hp(20) }}>
                <Text style={styles.title}>Filtrele</Text>
                <ImageButton
                    onPress={props.onDismiss}
                    style={{ marginEnd: wp(20) }}
                    imageStyle={{ tintColor: 'black' }}
                    source={require('../../assets/icons/ic_close.png')} />
            </View>

            <HorizontalSeperator style={{ marginVertical: hp(20) }} />
            <FilterGroupList
                selectedFuelTypes={selectedFuelTypes}
                selectedTransmissionTypes={selectedTransmissionTypes}
                handleFuelTypeSelection={handleFuelTypeSelection}
                handleTransmissionTypeSelection={handleTransmissionTypeSelection} />

            <View style={styles.shadowBox} >
                <FilledButton
                    // disabled={(selectedFuelTypes.length == 0 && selectedTransmissionTypes.length == 0)}
                    style={Styles.continueButton}
                    title={'Araçları Göster'}
                    onPress={() => props.handleOnFilterSelected(selectedFuelTypes, selectedTransmissionTypes)} />
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    shadowBox: {
        width: wp(375),
        height: hp(62) + getStatusBarHeight(),
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0,
        // add shadows for iOS only
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
    },
    title: {
        marginStart: wp(20),
        fontSize: hp(20),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-Black'
    },
    continueButton: {
        width: wp(335),
        marginTop: hp(20),
        marginHorizontal: wp(20)
    }
})
