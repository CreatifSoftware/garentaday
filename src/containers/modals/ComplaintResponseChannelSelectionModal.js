import React from 'react'
import { View } from 'react-native';
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { hp } from '../../styles/Dimens';
import { COMPLAINT_RESPONSE_CHANNEL } from '../../utilities/constants';

export const ComplaintResponseChannelSelectionModal = (props) => {
    const selectionArr = [
        COMPLAINT_RESPONSE_CHANNEL.SMS,
        COMPLAINT_RESPONSE_CHANNEL.MAIL,
        COMPLAINT_RESPONSE_CHANNEL.PHONE,
    ]

    return (
        <View style={{ flex: 1, paddingBottom: hp(40) }}>
            <HorizontalSeperator style={{ marginVertical: hp(20) }} />
            {
                selectionArr.map(item => {
                    return (
                        <RadioButtonItem key={item} label={item} selected={props.current == item} onPress={() => props.onPress(item)} />
                    )
                })
            }
        </View>
    )
}
