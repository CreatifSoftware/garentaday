
import React from 'react'
import { View } from 'react-native';
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { hp } from '../../styles/Dimens';
import { SORT_TYPES } from '../../utilities/constants';

export const SortCarGroupModal = (props) => {
    const sortArr = [
        SORT_TYPES.SORT_LOW_TO_HIGH_PRICE,
        SORT_TYPES.SORT_HIGHT_TO_LOW_PRICE,
        SORT_TYPES.SORT_BY_CAMPAIGN
    ]

    return (
        <View style={{ flex: 1, paddingBottom: hp(40) }}>
            <HorizontalSeperator style={{ marginVertical: hp(20) }} />
            {
                sortArr.map(item => {
                    return (
                        <RadioButtonItem key={item} label={item} selected={props.current == item} onPress={() => props.onPress(item)} />
                    )
                })
            }
        </View>

    )
}
