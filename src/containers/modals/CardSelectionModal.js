
import React from 'react'
import { StyleSheet, TouchableOpacity, View, Image, Text, SafeAreaView, ScrollView } from 'react-native';
import { useSelector } from 'react-redux';
import { RadioButton, RadioButtonItem } from '../../components/buttons/RadioButtonView';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { CreditCardInfo } from '../../components/views/CreditCardInfo';
import { Colors, TextColors } from '../../styles/Colors';
import { hp, wp } from '../../styles/Dimens';
export const CardSelectionModal = (props) => {
    const user = useSelector(state => state.authReducer.user)

    return (
        <View>
            <Text style={styles.title}>Kart Seçimi</Text>
            <HorizontalSeperator style={{ marginTop: hp(13), marginBottom: hp(10) }} />
                {
                    props.creditCards.map((item, index) => {
                        const isSelected = item.cardNo == props.selectedCard.cardNo
                        return (
                            <TouchableOpacity
                                onPress={() => props.onPress(item)}
                                style={isSelected ? styles.selectedView : styles.cardView}>
                                <RadioButton selected={isSelected} activeColor={Colors.primaryBrand} />
                                <CreditCardInfo item={item} />
                            </TouchableOpacity>
                        )
                    })
                }
        </View>

    )
}
const styles = StyleSheet.create({
    title: {
        marginStart: wp(20),
        fontSize: hp(20),
        color: TextColors.primaryText,
        fontWeight: 'bold'
    },
    cardView: {
        marginHorizontal: wp(20),
        paddingHorizontal: wp(20),
        marginTop: hp(10),
        backgroundColor: 'white',
        borderColor: Colors.borderColor,
        paddingVertical: hp(14),
        borderWidth: 1,
        borderRadius: hp(10),
        alignItems: 'center',
        flexDirection: 'row'
    },
    selectedView: {
        minHeight: hp(74),
        marginHorizontal: wp(20),
        paddingHorizontal: wp(20),
        marginTop: hp(10),
        backgroundColor: 'white',
        paddingVertical: hp(14),
        borderColor: Colors.primaryBrand,
        borderWidth: 1,
        borderRadius: hp(10),
        flexDirection: 'row',
        alignItems: 'center'
    }

})