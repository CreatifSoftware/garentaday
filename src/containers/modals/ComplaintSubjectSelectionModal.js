import React from 'react'
import { View } from 'react-native';
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { hp } from '../../styles/Dimens';
import { COMPLAINT_SUBJECT } from '../../utilities/constants';

export const ComplaintSubjectSelectionModal = (props) => {
    // const selectionArr = [
    //     COMPLAINT_SUBJECT.SIKAYET,
    //     COMPLAINT_SUBJECT.TESEKKUR,
    //     COMPLAINT_SUBJECT.ONERI,
    //     COMPLAINT_SUBJECT.TALEP
    // ]

    return (
        <View style={{ flex: 1, paddingBottom: hp(40) }}>
            <HorizontalSeperator style={{ marginVertical: hp(20) }} />
            {
                props.selectionArr.map(item => {
                    return (
                        <RadioButtonItem
                            key={item.CATEGORY_ID}
                            label={item.CATEGORY_TEXT}
                            selected={props.current && props.current.CATEGORY_ID == item.CATEGORY_ID} 
                            onPress={() => props.onPress(item)} />
                    )
                })
            }
        </View>
    )
}
