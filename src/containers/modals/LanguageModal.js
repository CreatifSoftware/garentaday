
import React from 'react'
import { StyleSheet } from "react-native"
import { useDispatch } from 'react-redux';
import { Colors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';
import { RadioButtonItem } from '../../components/buttons/RadioButtonView';

export const LanguageModal = (props) => {
    const dispatch = useDispatch()
    const licenseClassArr = [KEY_TURKISH_LANG, KEY_ENGLISH_LANG]

    return (
        licenseClassArr.map(item => {
            return (
                <RadioButtonItem key={item} label={item} selected={props.current == item} onPress={() => props.onPress(item)} />
            )
        })
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        alignSelf: 'center',
        color: Colors.white,
    },
})
