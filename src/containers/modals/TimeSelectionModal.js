
import React, { useEffect, useState } from 'react'
import { Colors, TextColors } from '../../styles/Colors';
import { FilledButton } from '../../components/buttons/FilledButton';
import { hp, wp } from '../../styles/Dimens';
import { Platform, SafeAreaView, ScrollView, Text, View } from 'react-native';
import { convertTime, generateArrayOfTimes, getFormatedDate } from '../../utilities/helpers';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment'
import { useSelector } from 'react-redux';
import DatePicker from 'react-native-date-picker'

export const TimeSelectionModal = (props) => {
    const roundedUp = Math.ceil(moment().minute() / 15) * 15;
    const [time, setTime] = useState(moment().minute(roundedUp).toDate());
    const workingHours = useSelector(state => state.masterReducer.workingHours)
    const [minTime, setMinTime] = useState(null)
    const [maxTime, setMaxTime] = useState(null)
    const [text, setText] = useState('')

    const isToday = (someDate) => {
        const today = new Date()
        return someDate.getDate() == today.getDate() &&
            someDate.getMonth() == today.getMonth() &&
            someDate.getFullYear() == today.getFullYear()
    }

    useEffect(() => {
        const filteredWorkingHours = workingHours.filter(function (arg) {
            return arg.ALT_SUBE == props.branchId;
        })

        if (filteredWorkingHours.length > 0) {
            let minHour = Number(filteredWorkingHours[0].BEGTI.substring(0, 2))
            let minMinute = Number(filteredWorkingHours[0].BEGTI.substring(2, 4))
            let maxHour = Number(filteredWorkingHours[0].ENDTI.substring(0, 2))
            let maxMinute = Number(filteredWorkingHours[0].ENDTI.substring(2, 4))

            setMaxTime(moment().hour(maxHour).minute(maxMinute).toDate())
            if (!isToday(new Date(props.date))) {
                setMinTime(moment().hour(minHour).minute(minMinute).toDate())
            }
            setText(`Çalışma saati : ${minHour >= 10 ? minHour : "0" + minHour}:${minMinute >= 10 ? minMinute : "0" + minMinute} - ${maxHour >= 10 ? maxHour : "0" + maxHour}:${maxMinute >= 10 ? maxMinute : "0" + maxMinute}`)
        } else {
            setText('Şube seçilen günde çalışmamaktadır.')
        }

        if (isToday(new Date(props.date))) {
            setMinTime(moment().minute(roundedUp).toDate())
        }

        // eğer tarih seçilmişse seçilen tarihi set et
        if (props.time) {
            setTime(new Date(props.time))
        }
    }, [])

    function handleOnTimeSelection() {
        props.onPress(time)
    }

    const onChange = (selectedDate) => {
        const currentDate = selectedDate || time;
        setTime(currentDate);
    };

    return (
        <View style={{ alignItems: 'center', flex: 1 }}>
            <Text style={{
                alignSelf: 'center',
                fontWeight: '700',
                color: TextColors.primaryText,
                fontSize: hp(18),
                marginTop: hp(30)
            }}>{props.title}</Text>

            <Text style={{
                alignSelf: 'center',
                fontWeight: '700',
                color: TextColors.primaryTextV3,
                fontSize: hp(14),
                marginTop: hp(6)
            }}>{text}</Text>

            {
                time && Platform.OS == 'ios' &&
                <DatePicker
                    date={time}
                    mode={'time'}
                    minimumDate={minTime}
                    maximumDate={maxTime}
                    locale={'tr_TR'}
                    theme={'light'}
                    minuteInterval={15}
                    androidVariant={'nativeAndroid'}
                    onDateChange={onChange}
                />

            }

            {
                time && Platform.OS == 'android' &&
                <DatePicker
                    date={time}
                    mode={'time'}
                    locale={'tr_TR'}
                    theme={'light'}
                    minuteInterval={15}
                    androidVariant={'nativeAndroid'}
                    onDateChange={onChange}
                />
            }

            {/* <Picker
                selectedValue={selectedValue}
                itemStyle={{ color: Colors.primaryBrand, fontSize: hp(20) }}
                onValueChange={(index) => onPickerSelect(index)}>
                {timeArr.map((value, i) => (
                    <PickerItem label={value} value={i} key={i} />
                ))}
            </Picker> */}
            <FilledButton
                onPress={handleOnTimeSelection}
                title={'Tamam'}
                style={{ width: wp(335), bottom: hp(30), position: 'absolute' }} />
        </View>
    )
}
