import { useIsFocused, useNavigation } from '@react-navigation/native'
import React, { useEffect, useRef, useState } from 'react'
import {  StyleSheet, Text, View, TouchableOpacity, FlatList, Image, ScrollView, Platform, Alert,PermissionsAndroid, Linking } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { callFilterCampaignsApi, getFullCampaignList } from '../../api/campaigns/campaignService'
import { MainHeader } from '../../components/headers/HomeHeader'
import { hp, wp } from '../../styles/Dimens'
import { Styles } from '../../styles/Styles'
import { getMainPageCampaign } from '../../utilities/helpers'
import { TextColors, Colors } from '../../styles/Colors'
import { clearReservationDataAction, setReservationDataAction } from '../../redux/actions/ReservationActions'
import { CampaignItem } from '../../components/views/CampaignItem'
import { CitySelectionModal } from '../modals/CitySelectionModal'
import RBSheet from 'react-native-raw-bottom-sheet'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { setFavoriteBranchsAction } from '../../redux/actions/UserActions'
import { callGetFavoriteBranchApi } from '../../api/user/userService'
import { convertDistance, getDistance } from 'geolib';
import Geolocation from 'react-native-geolocation-service';
import { NearbyBranchItem } from '../../components/views/NearbyBranchItem'

export const HomeScreen = () => {

    const dispatch = useDispatch()
    const user = useSelector(state => state.authReducer.user)
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const branchs = useSelector(state => state.masterReducer.branchs)
    const navigation = useNavigation()
    const [mainPageCampaigns, setMainPageCampaigns] = useState([])
    const [initialCampaigns, setInitialCampaigns] = useState([])
    const [nearbyBranchs, setNearbyBranchs] = useState([])
    const [city, setCity] = useState({
        country: "",
        city: "",
        cityName: "Tümü"
    })
    const [forceLocation, setForceLocation] = useState(true);
    const [highAccuracy, setHighAccuracy] = useState(true);
    const [locationDialog, setLocationDialog] = useState(true);
    const refRBSheet = useRef();
    const isFocused = useIsFocused()

    function handleOnRentButton(fromNearby) {
        if (fromNearby) {
            // dispatch(setReservationDataAction(reservation))
        } else {
            dispatch(clearReservationDataAction())
        }

        if (user) {
            dispatch(setLoadingAction(true))
            callGetFavoriteBranchApi(user.userId)
                .then((response) => {
                    dispatch(setLoadingAction(false))
                    dispatch(setFavoriteBranchsAction(response.data.EXPORT))
                    navigation.navigate('ReservationStack')
                })
                .catch((error) => {
                    dispatch(setLoadingAction(false))
                    dispatch(setFavoriteBranchsAction([]))
                    navigation.navigate('ReservationStack')
                })
        } else {
            dispatch(setFavoriteBranchsAction([]))
            navigation.navigate('ReservationStack')
        }
    }

    useEffect(() => {
        dispatch(setLoadingAction(false))
        getMainPageCampaigns()
        getLocation()
    }, [isFocused])

    const getMainPageCampaigns = () => {
        getFullCampaignList()
            .then(response => {
                setMainPageCampaigns(getMainPageCampaign(response.data))
                setInitialCampaigns(getMainPageCampaign(response.data))
            })
            .catch(error => {

            })
    }

    const handleOnCampaignSelection = (item) => {
        navigation.navigate('CampaignDetail', { campaign: item })
    }

    const handleOnCitySelection = () => {
        refRBSheet.current.open()
    }

    const handleOnCitySelected = (item) => {
        setCity(item)
        if (item.city === "") {
            getMainPageCampaigns()
        } else {
            let filteredBranchs = branchs.filter(arg => arg.city === item.city)
            getCampaignsForSelectedCity(filteredBranchs)
        }

        refRBSheet.current.close()
    }

    const getCampaignsForSelectedCity = (branchList) => {
        dispatch(setLoadingAction(true))
        callFilterCampaignsApi(branchList, [])
            .then(response => {
                const filtered = initialCampaigns.filter(campaign => {
                    return response.data.EXPORT.ET_CAMPAIGN.find(arg => arg.EXTERNAL_ID === campaign.campaignId);
                });

                setMainPageCampaigns(filtered)
                dispatch(setLoadingAction(false))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    const getLocation = async () => {
        const hasPermission = await hasLocationPermission();

        if (!hasPermission) {
            return;
        }

        Geolocation.getCurrentPosition(
            (position) => {
                let data = []
                for (let index = 0; index < branchs.length; index++) {
                    const item = branchs[index];
                    var distance = getDistance({
                        latitude: Number(item.longitude),
                        longitude: Number(item.latitude)
                    }, position.coords)

                    let distanceInKm = convertDistance(distance, 'km')
                    item.distance = distanceInKm < 1 ? distanceInKm.toFixed(2) : distanceInKm.toFixed(0)
                    data.push(item)
                }
                setNearbyBranchs(data.sort((a, b) => Number(b.distance) < Number(a.distance) ? 1 : -1).slice(0, 10))
            },
            (error) => {
                // setLocation(null);
            },
            {
                accuracy: {
                    android: 'high',
                    ios: 'best',
                },
                enableHighAccuracy: highAccuracy,
                timeout: 15000,
                maximumAge: 10000,
                distanceFilter: 0,
                forceRequestLocation: forceLocation,
                showLocationDialog: locationDialog,
            },
        );
    };

    const hasLocationPermission = async () => {
        if (Platform.OS === 'ios') {
            const hasPermission = await hasPermissionIOS();
            return hasPermission;
        }

        if (Platform.OS === 'android' && Platform.Version < 23) {
            return true;
        }

        const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (hasPermission) {
            return true;
        }

        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) {
            return true;
        }

        if (status === PermissionsAndroid.RESULTS.DENIED) {
            Alert.alert('Uyarı','Konum bilgilerinize erişmeye iznimiz yok. Telefonunuzun ayarlar bölümünden aktifleştirmeniz gerekiyor.')
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            Alert.alert('Uyarı','Konum bilgilerinize erişmeye iznimiz yok. Telefonunuzun ayarlar bölümünden aktifleştirmeniz gerekiyor.')
        }

        return false;
    };

    const hasPermissionIOS = async () => {
        const openSetting = () => {
            Linking.openSettings().catch(() => {
                Alert.alert('Unable to open settings');
            });
        };
        const status = await Geolocation.requestAuthorization('whenInUse');

        if (status === 'granted') {
            return true;
        }

        // if (status === 'denied') {
        //     Alert.alert('Uyarı','Konum bilgilerinize erişmeye iznimiz yok. Telefonunuzun ayarlar bölümünden aktifleştirmeniz gerekiyor.')
        // }

        if (status === 'disabled') {
            Alert.alert(
                'Hata',
                'Yakınınızdaki şubeleri görebilemek için uygulamanın konumuna erişimine izin vermen gerekiyor.',
                [
                    { text: 'Aarlar', onPress: openSetting },
                    { text: "Vazgeç", onPress: () => { } },
                ],
            );
        }

        return false;
    };

    const renderCampaignItem = ({ item }) => {
        return (
            <CampaignItem
                item={item}
                onPress={() => handleOnCampaignSelection(item)} />
        )
    }

    const renderNearbyBranchItem = ({ item }) => {
        return (
            <NearbyBranchItem
                item={item}
                onPress={() => handleOnNearbyBranhcSelection(item)} />
        )
    }

    const handleOnNearbyBranhcSelection = (branch) => {
        let resObj = {
            pickupBranch: branch,
            dropoffBranch: branch,
            pickupDateTime: null,
            dropoffDateTime: null,
            selectedAdditionalServices: [],
            selectedAdditionalProducts: [],
            campaignId: null,
            transactionId: null,
            individualAddressId: null,
            invoiceAddressId: null
        }
        dispatch(setReservationDataAction(resObj))
        handleOnRentButton(true)
    }

    return (
        <View style={styles.container}>
            <MainHeader
                onPress={() => handleOnRentButton(false)}
                user={user} />

            <ScrollView>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: hp(20) }}>
                    <Text style={styles.flatListTitle}>Kampanyalar</Text>
                    <TouchableOpacity
                        onPress={handleOnCitySelection}
                        style={{ flexDirection: 'row', alignItems: 'center', marginEnd: wp(24) }}>
                        <Text style={styles.campaignCity}>{city == null ? "Tümü" : city.cityName}</Text>
                        <Image style={{ tintColor: Colors.primaryBrand }} source={require('../../assets/icons/ic_small_arrow_down.png')}></Image>
                    </TouchableOpacity>
                </View>

                <FlatList
                    horizontal={true}
                    contentContainerStyle={{ marginVertical: hp(16) }}
                    data={mainPageCampaigns}
                    renderItem={renderCampaignItem}
                    keyExtractor={item => item.campaignId}
                />

                <Text style={styles.flatListTitle}>Yakınımdaki Şubeler</Text>
                {
                    nearbyBranchs.length > 0 ?

                        <FlatList
                            horizontal={true}
                            contentContainerStyle={{ marginVertical: hp(16) }}
                            data={nearbyBranchs}
                            renderItem={renderNearbyBranchItem}
                            keyExtractor={item => item.branchId}
                        />

                        :

                        <TouchableOpacity style={styles.emptyContainer} onPress={getLocation}>
                            <Text style={{
                                fontFamily: 'NunitoSans-Black',
                                fontSize: wp(16),
                                color: TextColors.primaryTextV2
                            }}>Konum izni verilmemiştir</Text>
                            <Text style={{
                                fontFamily: 'NunitoSans-Regular',
                                fontSize: wp(14),
                                color: TextColors.primaryTextV2,
                                marginTop: hp(10)
                            }}>Yakınınızdaki şubeleri görebilmek için tıklayın.</Text>
                        </TouchableOpacity>


                }
            </ScrollView>



            <RBSheet
                height={hp(718)}
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <CitySelectionModal onPress={handleOnCitySelected} countryCode={"TR"} current={city && city.cityName} fromCampaign={true} />
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    emptyContainer: {
        height: hp(108),
        marginTop: hp(16),
        borderRadius: hp(10),
        marginStart: wp(20),
        backgroundColor: 'white',
        width: wp(325),
        shadowColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.1,
        elevation: 4,
        marginBottom: hp(16),
        paddingHorizontal: wp(16)
    },
    flatListTitle: {
        color: TextColors.primaryText,
        fontSize: hp(20),
        fontFamily: 'NunitoSans-Bold',
        marginStart: wp(20),
        // marginTop: hp(20)
    },
    campaignCity: {
        color: Colors.primaryBrand,
        fontSize: hp(16),
        fontFamily: 'NunitoSans-Bold',
        marginEnd: wp(8),
    },
})



