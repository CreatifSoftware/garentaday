import { useIsFocused, useNavigation } from '@react-navigation/native'
import React, { useEffect, useLayoutEffect, useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, FlatList, ScrollView, Alert, SafeAreaView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { BackButton } from '../../components/buttons/BackButton'
import { FilledButton } from '../../components/buttons/FilledButton'
import { BranchSelectionContainer } from '../../components/views/BranchSelectionContainer'
import { DateTimeSelectionContainer } from '../../components/views/DateTimeSelectionContainer'
import { Colors, TextColors } from '../../styles/Colors'
import { getStatusBarHeight, hp, wp } from '../../styles/Dimens'
import { TextButton } from '../../components/buttons/TextButton'
import moment from 'moment'
import { callSearchEquiApi } from '../../api/reservation/reservationService'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { getErrorMessage, isSuccess } from '../../api/apiHelpers'
import { SearchEquiResponse } from '../../api/models/SearchEquiResponse'
import { setReservationDataAction } from '../../redux/actions/ReservationActions'
import { Styles } from '../../styles/Styles'
import { FilterGroupList } from '../../components/views/FilterGroupList'

export const DateAndBranchSelection = () => {

    const dispatch = useDispatch()
    const user = useSelector(state => state.authReducer.user)
    const { fuelTypes, transmissionTypes, timeLimit } = useSelector(state => state.masterReducer)
    const reservationData = useSelector(state => state.reservationReducer.reservation)
    const [tempReservation, setTempReservation] = useState(null)
    const [buttonDisabled, setButtonDisabled] = useState(true);
    const [showAdvancedFilter, setShowAdvancedFilter] = useState(false);
    const navigation = useNavigation()
    const isFocused = useIsFocused()
    const [selectedTransmissionTypes, setSelectedTransmissionTypes] = useState([])
    const [selectedFuelTypes, setSelectedFuelTypes] = useState([])

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: null
        })

    }, [navigation]);

    useEffect(() => {
        const roundedUp = Math.ceil(moment().minute() / 15) * 15;
        // if (reservationData.pickupDateTime == null) {
        //     reservationData.pickupDateTime = moment().minute(roundedUp).second(0).add(timeLimit.SURE1, 'minute')
        // }
        // if (reservationData.dropoffDateTime == null) {
        //     reservationData.dropoffDateTime = moment(reservationData.pickupDateTime).add(1, 'days')
        // }

        if (reservationData.pickupBranch != null &&
            reservationData.dropoffBranch != null &&
            reservationData.pickupDateTime != null &&
            reservationData.dropoffDateTime != null) {
            setButtonDisabled(false)
        }

        setTempReservation(reservationData)
    }, [isFocused])

    function handleOnContinueButton() {
        dispatch(setLoadingAction(true))
        callSearchEquiApi(user, reservationData, selectedFuelTypes, selectedTransmissionTypes)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    const { carGroupList, duration } = SearchEquiResponse(response.data.EXPORT)
                    reservationData.duration = duration
                    dispatch(setReservationDataAction(reservationData))
                    navigation.navigate('CarGroupList', { carGroupList,selectedTransmissionTypes,selectedFuelTypes })
                } else {
                    Alert.alert('Hata', getErrorMessage(response))
                }

            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    function handleOnBranchSelection(key) {
        navigation.navigate('BranchSelection', { modalKey: key })
    }

    function handleOnDateTimeSelection(key) {
        navigation.navigate('DateTimeSelection', { modalKey: key })
    }

    function setFavorite(branch) {

    }

    const checkSelectedItemInArray = (arr, item) => {
        return arr.indexOf(item) > -1
    }

    const handleFuelTypeSelection = (item) => {
        let list = selectedFuelTypes
        let filtered = list.filter(arg => arg.YAKIT_TIP === item.YAKIT_TIP)
        if (filtered.length > 0) {
            var index = list.indexOf(item);
            list.splice(index, 1);
        } else {
            list.push(item)
        }

        setSelectedFuelTypes([...list])
    }

    const handleTransmissionTypeSelection = (item) => {
        let list = selectedTransmissionTypes
        let filtered = list.filter(arg => arg.SANZIMAN === item.SANZIMAN)
        if (filtered.length > 0) {
            var index = list.indexOf(item);
            list.splice(index, 1);
        } else {
            list.push(item)
        }

        setSelectedTransmissionTypes([...list])
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
            <ScrollView contentContainerStyle={{ flex: 1, paddingTop: hp(32) }} >
                <BranchSelectionContainer
                    isDisabled={reservationData.reservationId !== undefined}
                    pickupBranch={tempReservation && tempReservation.pickupBranch}
                    dropoffBranch={tempReservation && tempReservation.dropoffBranch}
                    handleOnPickupBranch={() => handleOnBranchSelection("PICKUP_BRANCH")}
                    handleOnDropoffBranch={() => handleOnBranchSelection("DROPOFF_BRANCH")} />
                <DateTimeSelectionContainer
                    isDisabled={(tempReservation && tempReservation.pickupBranch == null) || (tempReservation && tempReservation.dropoffBranch == null)}
                    pickupDate={tempReservation && tempReservation.pickupDateTime}
                    dropoffDate={tempReservation && tempReservation.dropoffDateTime}
                    handleOnPickupDate={() => handleOnDateTimeSelection("PICKUP_DATE")}
                    handleOnDropoffDate={() => handleOnDateTimeSelection("DROPOFF_DATE")} />

                <TextButton
                    onPress={() => setShowAdvancedFilter(!showAdvancedFilter)}
                    buttonStyle={{ marginTop: hp(20) }}
                    title={'Gelişmiş Arama'}
                    style={{ color: Colors.primaryBrand }} />
                {
                    showAdvancedFilter &&
                    <FilterGroupList
                        selectedFuelTypes={selectedFuelTypes}
                        selectedTransmissionTypes={selectedTransmissionTypes}
                        handleFuelTypeSelection={handleFuelTypeSelection}
                        handleTransmissionTypeSelection={handleTransmissionTypeSelection} />
                }
            </ScrollView>
            <View style={styles.box1}>
                <FilledButton
                    disabled={buttonDisabled}
                    style={Styles.continueButton}
                    onPress={handleOnContinueButton}
                    title={'Araçları Göster'} />
            </View>
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    box1: {
        width: wp(375),
        height: hp(62) + getStatusBarHeight(),
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0,
        // add shadows for iOS only
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.10,
        elevation: 4,
    },
    continueButton: {
        width: wp(335),
        marginHorizontal: wp(20),
        marginTop: hp(16),
    },
    advancedFilterTitle: {
        fontSize: hp(18),
        color: TextColors.primaryTextV4,
        fontWeight: '700',
    },
    advancedFilterContainerSelected: {
        backgroundColor: Colors.primaryBrand,
        height: hp(44),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: hp(22),
        marginEnd: wp(10)
    },
    advancedFilterItemSelected: {
        fontSize: hp(16),
        color: TextColors.primaryTextV4,
        fontWeight: '700',
        paddingHorizontal: wp(12),
    },
    advancedFilterContainer: {
        backgroundColor: Colors.white,
        height: hp(44),
        borderWidth: 1,
        borderColor: Colors.borderColor,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: hp(22),
        marginEnd: wp(10)
    },
    advancedFilterItem: {
        fontSize: hp(16),
        color: TextColors.primaryText,
        fontWeight: '700',
        paddingHorizontal: wp(12),
    }
})





