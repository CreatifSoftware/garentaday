import React, { useEffect, useRef, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image, ScrollView, Alert, Dimensions } from 'react-native'
import RBSheet from 'react-native-raw-bottom-sheet'
import { useDispatch, useSelector } from 'react-redux'
import { BorderedButton } from '../../components/buttons/BorderedButton'
import { RadioButton } from '../../components/buttons/RadioButtonView'
import { StepHeader } from '../../components/headers/StepHeader'
import { HorizontalSeperator } from '../../components/HorizontalSeperator'
import { AddCardInformation } from '../../components/views/AddCardInformation'
import { CreditCardInfo } from '../../components/views/CreditCardInfo'
import { ReservationBottomView } from '../../components/views/ReservationBottomView'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { Styles } from '../../styles/Styles'
import { PAYMENT_TYPES } from '../../utilities/constants'
import { CardSelectionModal } from '../modals/CardSelectionModal'
import CheckBox from '@react-native-community/checkbox';
import { CommonActions, useNavigation } from '@react-navigation/native'
import { calculateReservationProductsPrice, calculateTotalAmount, getCarGroupPrice, hasGarentaXpressAdded, isUpdate } from '../../utilities/helpers'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { callApplyCouponCode, callCreateReservationApi, callUpdateReservationApi } from '../../api/reservation/reservationService'
import { getErrorMessage, isSuccess } from '../../api/apiHelpers'
import { setReservationDataAction } from '../../redux/actions/ReservationActions'
import { CouponCodeModal } from '../modals/CouponCodeModal'
import { callGetCreditCardApi } from '../../api/user/userService'
import { CreditCardResponse } from '../../api/models/CreditCardResponse'
import { setUserAction } from '../../redux/actions/AuthActions'
import { BackButton } from '../../components/buttons/BackButton'
import moment from 'moment'

export const ReservationCreate = () => {
    const [couponCode, setCouponCode] = useState('')
    const [couponCodeApplied, setCouponCodeApplied] = useState(false)
    const user = useSelector(state => state.authReducer.user)
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const [creditCards, setCreditCards] = useState([])
    const [addCreditCard, setAddCreditCard] = useState(false)
    const [selectedCard, setSelectedCard] = useState(null)
    const refRBSheet = useRef();
    const refRBSheetCoupon = useRef();
    const [cardHolderName, setCardHolderName] = useState('')
    const [cardNumber, setCardNumber] = useState('')
    const [expDate, setExpDate] = useState('')
    const [cvv, setCvv] = useState('')
    const [cardDescription, setCardDescription] = useState('')
    const [saveCreditCard, setSaveCreditCard] = useState(false)
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const isPayNow = reservation.paymentType === PAYMENT_TYPES.PAY_NOW
    const [agreementChecked, setAgreementChecked] = useState(false)
    const navigation = useNavigation()
    const dispatch = useDispatch()

    // React.useLayoutEffect(() => {
    //     navigation.setOptions({
    //         headerLeft: () => <BackButton navigation={navigation} override={true} onPress={handleOnBackPressed}/>
    //     })
    // }, [navigation]);

    React.useEffect(() => {
        if (isUpdate(reservation) && reservation.creditCardInfo.merkey) {
            setAddCreditCard(true)
            setCardHolderName(user.personalInfo.firstName)
            setCardNumber(reservation.creditCardInfo.cardNo)
            setExpDate('**/**')
            setCvv('***')
        }
        else if (hasGarentaXpressAdded(reservation.selectedProducts)) {
            dispatch(setLoadingAction(true))
            callGetCreditCardApi(user.userId)
                .then(response => {
                    if (isSuccess(response)) {
                        let cards = CreditCardResponse(response.data.EXPORT, user)
                        setCreditCards(cards.length > 0 ? cards : [])
                        setSelectedCard(cards.length > 0 ? cards[0] : null)
                    }
                    dispatch(setLoadingAction(false))
                })
                .catch(error => {
                    dispatch(setLoadingAction(false))
                })
        }
    }, [])

    const handleOnBackPressed = () => {
        setCouponCode('')
        handleOnCouponCodeAdded()
    }

    const handleOnContinueButton = () => {
        if (isUpdate(reservation)) {
            Alert.alert(
                "Rezervasyon güncelleme",
                "Rezervasyonunuz güncellenecektir emin misiniz?",
                [
                    {
                        text: "Hayır",
                        onPress: () => console.log("Cancel Pressed"),
                        style: "cancel"
                    },
                    { text: "Evet", onPress: () => updateReservation() }
                ],
                { cancelable: false }
            );
        } else {
            Alert.alert(
                "Rezervasyon oluşturma",
                "Rezervasyonunuz oluşturulacaktır emin misiniz?",
                [
                    {
                        text: "Hayır",
                        onPress: () => console.log("Cancel Pressed"),
                        style: "cancel"
                    },
                    { text: "Evet", onPress: () => createReservation() }
                ],
                { cancelable: false }
            );
        }
    }

    const prepareCreditCard = () => {
        // reservation.creditCardInfo = {}
        if (selectedCard) {
            reservation.creditCardInfo = selectedCard
        } else if (isUpdate(reservation) && reservation.creditCardInfo.merkey.length > 0) {
            reservation.creditCardInfo.cardNo = ''
            reservation.creditCardInfo.cardHolderName = ''
            reservation.creditCardInfo.month = ''
            reservation.creditCardInfo.year = ''
            reservation.creditCardInfo.merkey = reservation.creditCardInfo.merkey
            reservation.creditCardInfo.cvv = ''
        } else {
            reservation.creditCardInfo = {}
            reservation.creditCardInfo.cardNo = cardNumber
            reservation.creditCardInfo.cardHolderName = cardHolderName
            reservation.creditCardInfo.month = expDate.split('/')[0]
            reservation.creditCardInfo.year = "20" + expDate.split('/')[1]
            reservation.creditCardInfo.merkey = ''
            reservation.creditCardInfo.cvv = cvv
        }

        console.log()
    }

    const updateReservation = () => {
        prepareCreditCard()

        dispatch(setLoadingAction(true))
        callUpdateReservationApi(user, reservation)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    dispatch(setReservationDataAction(reservation))
                    navigateSummary()
                } else {
                    Alert.alert('Hata', getErrorMessage(response))
                }
            })
            .catch(error => {
                Alert.alert('Hata', error.message)
            })
    }


    const createReservation = () => {
        if (reservation.paymentType === PAYMENT_TYPES.PAY_NOW) {
            prepareCreditCard()
        }

        dispatch(setLoadingAction(true))
        callCreateReservationApi(user, reservation)
            .then(response => {
                dispatch(setLoadingAction(false))
                if (isSuccess(response)) {
                    reservation.pnrNo = response.data.EXPORT.ES_RETURN.RESERVATION_CODE
                    dispatch(setReservationDataAction(reservation))
                    navigateSummary()
                } else {
                    Alert.alert('Hata', getErrorMessage(response))
                }


            })
            .catch(error => {
                Alert.alert('Hata', error.message)
            })
    }

    const navigateSummary = () => {
        const homeAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'ReservationSummary' }]
        })
        navigation.dispatch(homeAction)
    }

    useEffect(() => {
        setContinueButtonState()
    }, [cardHolderName, cardNumber, expDate, cvv, cardDescription, saveCreditCard, addCreditCard, agreementChecked]);

    const setContinueButtonState = () => {
        if (isPayNow) {
            if (selectedCard) {
                if (agreementChecked) {
                    setButtonDisabled(false)
                } else {
                    setButtonDisabled(true)
                }
            } else {
                if (cardHolderName.length > 0 &&
                    cardNumber.length == 19 &&
                    expDate.length == 5 &&
                    cvv.length == 3 &&
                    agreementChecked) {

                    if (isUpdate(reservation) && reservation.creditCardInfo.merkey) {
                        setButtonDisabled(false)
                    } else {
                        let cardInfo = {
                            month: expDate.split('/')[0],
                            year: "20" + expDate.split('/')[1],
                        }
                        let formattedExpDate = `${cardInfo.year}-${cardInfo.month}-28`
                        let today = new Date();
                        let someday = new Date();
                        someday.setFullYear(cardInfo.year, Number(cardInfo.month) - 1, 1);
            
                        var date = moment(formattedExpDate);

                        if (Number(cardInfo.month) > 12) {
                            setButtonDisabled(true)
                        }
                        else if (someday < today) {
                            setButtonDisabled(true)
                        }
                        else if (!date.isValid()) {
                            setButtonDisabled(true)
                        } else {
                            setButtonDisabled(false)
                        }
    
                        if (saveCreditCard) {
                            setButtonDisabled(cardDescription.length == 0)
                        }
                    }
                } else {
                    setButtonDisabled(true)
                }
            }
        }
        else if (agreementChecked) {
            setButtonDisabled(false)
        } else {
            setButtonDisabled(true)
        }
    }

    const handleOnAddNewCard = () => {
        setAddCreditCard(true)
    }

    const handleOnOpenCardModal = () => {
        setAddCreditCard(false)
        refRBSheet.current.open()
    }

    const handleOnCardSelected = (item) => {
        refRBSheet.current.close()
        setSelectedCard(item)
    }

    const renderCreditCardItem = () => {
        return (
            <TouchableOpacity
                onPress={handleOnOpenCardModal}
                style={addCreditCard ? styles.cardView : styles.selectedView}>
                <RadioButton selected={!addCreditCard} activeColor={Colors.primaryBrand} />
                <CreditCardInfo item={selectedCard} />
            </TouchableOpacity>
        )
    }

    const renderPayWithAnotherCard = () => {
        return (
            <TouchableOpacity
                onPress={handleOnAddNewCard}
                style={addCreditCard ? styles.selectedView : styles.cardView}>
                <RadioButton selected={addCreditCard} activeColor={Colors.primaryBrand} />
                <View style={{ flexDirection: 'row', marginStart: wp(20), alignItems: 'center', }}>
                    <Image source={require('../../assets/icons/ic_credit_card.png')} style={{ tintColor: Colors.primaryBrand }} />
                    <Text style={styles.cardName}>Başka Kart ile Öde</Text>
                </View>
            </TouchableOpacity>
        )
    }

    const handleOnCouponCodeAdded = () => {
        refRBSheetCoupon.current.close()
        dispatch(setLoadingAction(true))
        callApplyCouponCode(couponCode.toUpperCase(), user.userId, reservation)
            .then(response => {
                if (isSuccess(response)) {
                    let tempPriceList = response.data.EXPORT.ET_PRICE_LIST
                    let priceList = []
                    let uniqueIds = []
                    for (let index = 0; index < tempPriceList.length; index++) {
                        const price = tempPriceList[index];
                        uniqueIds.includes(price.OLD_PRICING_GUID)
                        if (uniqueIds.includes(price.OLD_PRICING_GUID)) {
                            price.NEW_PRICE = price.NEW_PRICE + price.NEW_PRICE;
                            price.NEW_DISC_PRICE = price.NEW_DISC_PRICE + price.NEW_DISC_PRICE;

                            priceList.splice(priceList.length - 1, 0, price);
                        } else {
                            priceList.push(price);
                        }

                        uniqueIds.push(price.OLD_PRICING_GUID);
                    }


                    for (let index = 0; index < priceList.length; index++) {
                        const element = priceList[index];

                        if (element.OLD_PRICING_GUID == reservation.selectedGroup.pricingItem.pricingGuid) {
                            reservation.selectedGroup.pricingItem.pricingGuid = element.NEW_PRICING_GUID;
                            reservation.selectedGroup.amountItem.payLaterAmount = element.NEW_PRICE;
                            reservation.selectedGroup.amountItem.payNowAmount = element.NEW_DISC_PRICE;
                        }
                        else {
                            for (let index = 0; index < reservation.selectedProducts.length; index++) {
                                const product = reservation.selectedProducts[index];
                                if (element.OLD_PRICING_GUID == product.pricingGuid) {
                                    product.pricingGuid = element.NEW_PRICING_GUID;
                                    //tempEqui.equipmentPrice.
                                    if (reservation.paymentType === PAYMENT_TYPES.PAY_NOW) {
                                        product.totalAmount = element.NEW_DISC_PRICE / product.value;
                                    } else {
                                        product.totalAmount = element.NEW_PRICE / product.value;
                                    }
                                }
                            }
                        }
                    }

                    if (couponCode.length > 0) {
                        dispatch(setLoadingAction(false))
                        setCouponCodeApplied(true)
                    } else {
                        dispatch(setLoadingAction(false))
                        setCouponCodeApplied(false)
                    }

                } else {
                    dispatch(setLoadingAction(false))
                    Alert.alert('Hata', getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
                Alert.alert('Hata', error.message)
            })
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <StepHeader
                stepCount={3}
                step={3}
                title={'Ödeme Bilgileri'} />
            <ScrollView contentContainerStyle={styles.container}>
                <Text style={styles.title}>Ödeme Yöntemi</Text>
                {/* {
                    (isUpdate(reservation) && reservation.creditCardInfo.merkey) &&
                    <Text style={styles.provision}>Rezervasyon oluştururken girmiş olduğunuz kart bilgileri kullanılacaktır.</Text>
                } */}
                {
                    !isPayNow && isUpdate(reservation) &&
                    <Text style={styles.provision}>Kredi kartı kontrolü için kartınızdan 1 TL çekim yapılacak ve hemen iade edilecek. Bankanızın süreçlerine göre kartınıza yansıması 4 iş günü içinde olacaktır.</Text>

                }
                {
                    !isPayNow && !isUpdate(reservation) ?
                        <View style={styles.payLaterContainer}>
                            <Image source={require('../../assets/icons/ic_info.png')} style={{ tintColor: '#f6a609' }} />
                            <View style={{ marginStart: wp(10) }}>
                                <Text style={styles.payLaterTitle}>SONRA ÖDE</Text>
                                <Text style={styles.payLaterInfo}>Ödeme, aracı kiralayan şahıs tarafından aracı şubeden teslim alırken kredi kartı ile tahsil edilecektir. </Text>
                            </View>
                        </View>

                        :

                        selectedCard ?

                            <View>
                                {renderCreditCardItem()}
                                {/* {renderPayWithAnotherCard()} */}
                            </View>

                            :

                            (isUpdate(reservation) && reservation.creditCardInfo.merkey) ?

                                null

                                :

                                <BorderedButton
                                    onPress={handleOnAddNewCard}
                                    style={{ marginHorizontal: wp(20) }}
                                    title={'Kredi Kartı Ekle'}
                                    source={require('../../assets/icons/ic_add_card.png')} />
                }

                {
                    addCreditCard &&
                    <AddCardInformation
                        disabled={isUpdate(reservation) && reservation.isPaid}
                        cardHolderName={cardHolderName}
                        cardNumber={cardNumber}
                        expDate={expDate}
                        cvv={cvv}
                        cardDescription={cardDescription}
                        cardHolderNameChange={(text) => setCardHolderName(text)}
                        cardNumberChange={(text) => setCardNumber(text)}
                        expDateChange={(text) => setExpDate(text)}
                        cvvChange={(text) => setCvv(text)}
                        cardDescriptionChange={(text) => setCardDescription(text)}
                        showSwitch={false}
                        saveCreditCard={saveCreditCard}
                        toggleSwitch={(value) => setSaveCreditCard(value)} />

                }

                <HorizontalSeperator style={{ marginTop: hp(30) }} />
                <Text style={styles.title}>İndirim Kodu</Text>
                {
                    couponCodeApplied ?

                        <View style={{ marginHorizontal: wp(20), width: wp(335), paddingVertical: hp(8), borderColor: Colors.primaryBrand, borderWidth: 1, borderRadius: 10, justifyContent: 'center' }}>
                            <Text style={styles.couponCode}>{couponCode}</Text>
                            <Text style={styles.couponCodeInfo}>Tebrikler! İndirim kodu başarıyla uygulandı!</Text>
                        </View>

                        :

                        <>
                            <Text style={styles.codeInfo}>* Kampanya kodu girerek, size özel indirimli fiyatları daha da avantajlı hale getirebilirsiniz.</Text>
                            <BorderedButton
                                onPress={() => refRBSheetCoupon.current.open()}
                                style={{ marginHorizontal: wp(20) }}
                                title={'İndirim Kodu Ekle'}
                                source={require('../../assets/icons/ic_plus_simple.png')} />
                        </>
                }

                <View style={styles.checkboxContainer}>
                    <CheckBox
                        onTintColor={Colors.primaryBrand}
                        onCheckColor={Colors.primaryBrand}
                        value={agreementChecked}
                        boxType={'square'}
                        onValueChange={(value) => setAgreementChecked(value)}
                        style={styles.checkbox}
                    />
                    <Text onPress={() => navigation.navigate('RentalTerms', { termsUrl: 'https://wsport.garenta.com.tr/mobile/agreements/RentingAgreement.html', title: 'Kiralama Koşulları' })}
                        style={styles.agreement}><Text style={styles.agreementHyperlink}>Kiralama Sözleşmesini</Text> okudum, kabul ediyorum.</Text>
                </View>
            </ScrollView>
            <ReservationBottomView
                // showReservationDetail={() => refRBSheet.current.open()}
                onPress={handleOnContinueButton}
                disabled={buttonDisabled}
                title={isPayNow ? 'Ödeme Yap' : isUpdate(reservation) ? 'Rezervasyon Güncelle' : 'Rezervasyon Oluştur'}
                reservation={reservation}
                totalAmount={calculateTotalAmount(reservation, reservation.selectedProducts)}
                buttonStyle={{ width: isPayNow ? wp(154) : wp(200) }} />

            <RBSheet
                ref={refRBSheet}
                height={(2 * hp(84)) + hp(145)}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <CardSelectionModal selectedCard={selectedCard} onPress={handleOnCardSelected} creditCards={creditCards} />
            </RBSheet>

            <RBSheet
                ref={refRBSheetCoupon}
                height={Dimensions.get('screen').height * 0.4}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <CouponCodeModal
                    onDismiss={() => refRBSheetCoupon.current.close()}
                    setCouponCode={(text) => setCouponCode(text)}
                    couponCode={couponCode}
                    onPress={handleOnCouponCodeAdded} />
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: 'white',
    },
    checkboxContainer: {
        paddingEnd: wp(20),
        flexDirection: "row",
        marginBottom: 20,
        marginHorizontal: wp(20),
        marginVertical: hp(35),
        alignItems: 'center'
    },
    title: {
        fontSize: hp(20),
        color: TextColors.primaryText,
        fontWeight: '700',
        marginTop: hp(20),
        marginBottom: hp(10),
        marginStart: wp(20)
    },
    provision: {
        fontSize: hp(16),
        color: TextColors.primaryTextV2,
        fontFamily: 'NunitoSans-SemiBold',
        marginBottom: hp(10),
        marginStart: wp(20),
        marginEnd: wp(20)
    },
    codeInfo: {
        fontSize: hp(12),
        color: TextColors.primaryText,
        marginBottom: hp(10),
        marginHorizontal: wp(20),
        textAlign: 'center'
    },
    checkbox: {
        alignSelf: "center",
        marginEnd: wp(8),
    },
    cardView: {
        minHeight: hp(54),
        marginHorizontal: wp(20),
        paddingHorizontal: wp(20),
        marginTop: hp(10),
        backgroundColor: 'white',
        borderColor: Colors.borderColor,
        paddingVertical: hp(14),
        borderWidth: 1,
        borderRadius: hp(10),
        alignItems: 'center',
        flexDirection: 'row'
    },
    selectedView: {
        minHeight: hp(54),
        marginHorizontal: wp(20),
        paddingHorizontal: wp(20),
        marginTop: hp(10),
        backgroundColor: 'white',
        paddingVertical: hp(14),
        borderColor: Colors.primaryBrand,
        borderWidth: 1,
        borderRadius: hp(10),
        flexDirection: 'row',
        alignItems: 'center'
    },
    cardName: {
        fontSize: hp(16),
        fontWeight: '700',
        color: TextColors.primaryText,
        marginStart: wp(13)
    },
    payLaterContainer: {
        marginHorizontal: wp(20),
        flexDirection: 'row',
        backgroundColor: '#ffeac1',
        borderRadius: hp(10),
        paddingVertical: hp(20),
        paddingHorizontal: wp(20)
    },
    payLaterTitle: {
        fontSize: hp(16),
        color: TextColors.primaryText,
        fontWeight: '700'
    },
    payLaterInfo: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginTop: hp(10),
        marginEnd: wp(20)
    },
    agreementHyperlink: {
        fontSize: hp(16),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-SemiBold'
    },
    agreement: {
        fontSize: hp(16),
        color: TextColors.primaryText,
    },
    couponCode: {
        fontSize: hp(20),
        marginStart: wp(20),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-SemiBold'
    },
    couponCodeInfo: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        fontFamily: 'NunitoSans-SemiBold',
        marginEnd: wp(20), marginTop: hp(4),
        marginStart: wp(20)
    },
})