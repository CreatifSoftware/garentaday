
import React, { useEffect, useLayoutEffect, useState } from 'react'
import { StyleSheet, TouchableOpacity, Text, View, Image, SafeAreaView, Platform } from "react-native"
import { useDispatch, useSelector } from 'react-redux';
import { Colors, TextColors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { getBranchesCityList, getCollaborationCampaigns, isFavoriteBranch } from '../../utilities/helpers';
import { useNavigation } from '@react-navigation/native';
import { BackButton } from '../../components/buttons/BackButton';
import { setReservationDataAction } from '../../redux/actions/ReservationActions';
import { setLoadingAction } from '../../redux/actions/MasterActions';
import { callSetFavoriteBranchApi } from '../../api/user/userService';
import { setFavoriteBranchsAction } from '../../redux/actions/UserActions';
import { convertDistance, getDistance, getPreciseDistance } from 'geolib';
import Geolocation from 'react-native-geolocation-service';

export const NearbyBranchSelection = ({ route }) => {

    String.prototype.turkishToEnglish = function () {
        return this.replace('Ğ', 'g')
            .replace('Ü', 'u')
            .replace('Ş', 's')
            .replace('I', 'i')
            .replace('İ', 'i')
            .replace('Ö', 'o')
            .replace('Ç', 'c')
            .replace('ğ', 'g')
            .replace('ü', 'u')
            .replace('ş', 's')
            .replace('ı', 'i')
            .replace('İ', 'i')
            .replace('ö', 'o')
            .replace('ç', 'c')
    }

    const dispatch = useDispatch()
    const branchs = useSelector(state => state.masterReducer.branchs)
    const favoriteBranchs = useSelector(state => state.userReducer.favoriteBranchs)
    const user = useSelector(state => state.authReducer.user)
    const reservation = useSelector(state => state.reservationReducer.reservation)

    const [filteredData, setFilteredData] = useState([])
    const navigation = useNavigation()
    const { modalKey, fromSettings } = route.params;

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />,
            headerRight: null,
            headerTitle: 'Yakınımdaki Şubeler'
        })
    }, [navigation]);

    useEffect(() => {
        dispatch(setLoadingAction(true))
        Geolocation.getCurrentPosition(
            (position) => {
                console.log(position)
                let data = []
                for (let index = 0; index < branchs.length; index++) {
                    const item = branchs[index];
                    var distance = getDistance({
                        latitude: Number(item.longitude),
                        longitude: Number(item.latitude)
                    }, position.coords)

                    let distanceInKm = convertDistance(distance, 'km')
                    item.distance = distanceInKm < 1 ? distanceInKm.toFixed(2) : distanceInKm.toFixed(0)
                    data.push(item)
                }

                dispatch(setLoadingAction(false))
                setFilteredData(data.sort((a, b) => Number(b.distance) < Number(a.distance) ? 1 : -1).slice(0, 10))
                Geolocation.stopObserving()
            },
            () => {
                dispatch(setLoadingAction(false))
                alert('Konum bilgileri alınamadı');
                Geolocation.stopObserving()
            }
        );
    }, [])

    function handleOnBranchSelection(branch) {
        if (fromSettings) {
            navigation.navigate('BranchDetail', { branch: branch })
        } else {
            if (modalKey === 'PICKUP_BRANCH') {
                reservation.pickupBranch = branch
                reservation.dropoffBranch = branch
            } else {
                reservation.dropoffBranch = branch
            }

            dispatch(setReservationDataAction(reservation))
            navigation.navigate('DateAndBranchSelection')
        }
    }

    const handleOnSetFavorite = (branch) => {
        let isFavorite = isFavoriteBranch(favoriteBranchs, branch.branchId)

        dispatch(setLoadingAction(true))
        callSetFavoriteBranchApi(user.userId, branch.branchId, isFavorite ? 'D' : 'C')
            .then(response => {
                dispatch(setFavoriteBranchsAction(response.data.EXPORT))
                dispatch(setLoadingAction(false))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    const renderBranchItem = (branch) => {
        let isFavorite = isFavoriteBranch(favoriteBranchs, branch.branchId)
        return (
            <TouchableOpacity style={styles.itemContainer} key={branch.branchId} onPress={() => handleOnBranchSelection(branch)} >
                <View style={styles.item}>
                    <Text style={styles.itemTitle}>{branch.cityName}</Text>
                    <View style={styles.distanceContainer}>
                        <Text style={styles.distance}>{branch.distance} km</Text>
                    </View>
                </View>

                <View style={styles.item}>
                    <Text style={styles.itemContent}>{branch.branchName}</Text>
                    {
                        user &&
                        <TouchableOpacity style={{ justifyContent: 'center', marginBottom: hp(20) }} onPress={() => handleOnSetFavorite(branch)}>
                            {
                                isFavorite ?

                                    <Image style={{ tintColor: Colors.primaryBrand }} source={require('../../assets/icons/ic_favorite_fill.png')}></Image>
                                    :

                                    <Image style={{ tintColor: Colors.checkboxBorder }} source={require('../../assets/icons/ic_favorite.png')}></Image>
                            }

                        </TouchableOpacity>
                    }
                </View>



            </TouchableOpacity>
        )
    }

    return (
        <View style={{ backgroundColor: 'white', flex: 1 }}>
            <ScrollView
                contentContainerStyle={{ paddingBottom: 40 }}
                style={styles.container}>
                {
                    filteredData.map(item => {
                        return (
                            renderBranchItem(item)
                        )
                    })
                }
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.mainBackground,
    },
    text: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        alignSelf: 'center',
        color: Colors.white,
    },
    icon: {
        tintColor: Colors.primaryBrand,
        position: 'absolute',
        start: wp(36),
        bottom: 36,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemContainer: {
        width: '100%',
        backgroundColor: 'white',
        marginBottom: 4,
        paddingHorizontal: wp(20)
    },
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemTitle: {
        color: TextColors.primaryText,
        fontSize: hp(18),
        fontWeight: '700',
        marginTop: hp(20),
        marginBottom: hp(22)
    },
    itemContent: {
        color: TextColors.primaryText,
        fontSize: hp(16),
        fontFamily: 'NunitoSans-Regular',
        marginBottom: hp(20)
    },
    distanceContainer: {
        backgroundColor: '#f5f5f5',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: wp(8),
        paddingVertical: hp(2),
        borderRadius: 10
    },
    distance: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        alignSelf: 'center',
        color: TextColors.primaryText
    }
})
