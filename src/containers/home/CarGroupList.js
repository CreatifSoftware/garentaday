import React, { useLayoutEffect, useRef, useState } from 'react'
import { FlatList, SafeAreaView, StyleSheet, View, Image, TouchableOpacity, Text, Switch, Dimensions } from 'react-native'
import { VerticalSeperator } from '../../components/VerticalSeperator'
import { CarGroupItem } from '../../components/views/CarGroupItem'
import { filterIcon, sortIcon } from '../../assets/Icon'
import { hp, wp } from '../../styles/Dimens'
import { Colors, TextColors } from '../../styles/Colors'
import { TextButton } from '../../components/buttons/TextButton'
import { useNavigation } from '@react-navigation/native'
import { useDispatch, useSelector } from 'react-redux'
import { getFormatedDate, isUpdate } from '../../utilities/helpers'
import { SortCarGroupModal } from '../modals/SortCarGroupModal'
import RBSheet from 'react-native-raw-bottom-sheet'
import { PAYMENT_TYPES, SORT_TYPES } from '../../utilities/constants'
import { Styles } from '../../styles/Styles'
import { setReservationDataAction } from '../../redux/actions/ReservationActions'
import { CarGroupTitle } from '../../components/titles/CarGroupTitle'
import { CarGroupItemUpdate } from '../../components/views/CarCroupItemUpdate'
import { FilterGroupListModal } from '../modals/FilterGroupListModal'

export const CarGroupList = ({ route }) => {
    // const { carGroupList, pricingList } = route.params
    const [filteredGroupList, setFilteredGroupList] = useState(route.params.carGroupList)
    const [filterCampaigns, setfilterCampaigns] = useState(false)
    const [selectedSortType, setSelectedSortType] = useState(SORT_TYPES.SORT_LOW_TO_HIGH_PRICE)
    const [selectedTransmissionTypes, setSelectedTransmissionTypes] = useState(route.params.selectedTransmissionTypes)
    const [selectedFuelTypes, setSelectedFuelTypes] = useState(route.params.selectedFuelTypes)
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const user = useSelector(state => state.authReducer.user)
    const navigation = useNavigation()
    const refRBSheet = useRef();
    const refRBSheetFilter = useRef();
    const flatListRef = useRef()
    const dispatch = useDispatch()

    // let selectedFuelTypes = []
    // let selectedTransmissionTypes = []

    useLayoutEffect(() => {
        navigation.setOptions({
            headerTitle: <CarGroupTitle reservation={reservation} />
        })
    }, [navigation]);

    React.useEffect(() => {
        sortListByType(selectedSortType)
    }, [])

    const hasCampaign = (item) => {
        return item.pricingItem && item.pricingItem.campaignId.length > 0
    }

    const handleOnSortButton = () => {
        refRBSheet.current.open()
    }

    const sortListByType = (item) => {
        if (item === SORT_TYPES.SORT_LOW_TO_HIGH_PRICE) {
            filteredGroupList.sort((a, b) => Number(a.amountItem.payNowAmount) > Number(b.amountItem.payNowAmount) ? 1 : -1);
        }
        if (item === SORT_TYPES.SORT_HIGHT_TO_LOW_PRICE) {
            filteredGroupList.sort((a, b) => Number(b.amountItem.payNowAmount) > Number(a.amountItem.payNowAmount) ? 1 : -1);
        }
        if (item === SORT_TYPES.SORT_BY_CAMPAIGN) {
            filteredGroupList.sort((a, b) => a.pricingItem.campaignId.length < b.pricingItem.campaignId.length ? 1 : -1);
        }

        setFilteredGroupList([...filteredGroupList])
    }

    const handleOnSortItemSelected = (item) => {
        refRBSheet.current.close()
        flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
        setSelectedSortType(item)
        sortListByType(item)
        
    }

    const handleOnFilterButton = () => {
        refRBSheetFilter.current.open()
    }

    const handleOnCarItemSelection = (item) => {
        if (item.pricingItem.campaignId) {
            reservation.campaignId = item.pricingItem.campaignId
        } else {
            reservation.campaignId = undefined
        }
        reservation.selectedGroup = item

        if (isUpdate(reservation)) {
            dispatch(setReservationDataAction(reservation))
            navigation.navigate('CarGroupDetailUpdate')
        } else {
            reservation.paymentType = PAYMENT_TYPES.PAY_NOW
            dispatch(setReservationDataAction(reservation))
            navigation.navigate('CarGroupDetail')
        }
    }

    const renderItem = ({ item }) => {        
        if (reservation.reservationId !== undefined) {
            return (
                <CarGroupItemUpdate
                    reservation={reservation}
                    onPress={handleOnCarItemSelection}
                    hasCampaign={hasCampaign(item)}
                    item={item}
                    payNowAmount={item.amountItem.payNowAmount}
                    payLaterAmount={item.amountItem.payLaterAmount} />
            )
        } else {
            return (
                <CarGroupItem
                    onPress={handleOnCarItemSelection}
                    hasCampaign={hasCampaign(item)}
                    item={item}
                    payNowAmount={item.amountItem.payNowAmount}
                    payLaterAmount={item.amountItem.payLaterAmount} />
            )
        }
    }

    const filterGroupByCampaign = (value) => {
        let filtered = []
        if (value) {
            filtered = filteredGroupList.filter(arg => arg.pricingItem.campaignId.length > 0)
        } else {
            filtered = route.params.carGroupList
        }
        setfilterCampaigns(!filterCampaigns)
        setFilteredGroupList(filtered)
    }

    const handleOnFilterSelected = (fuelTypes, transmissionTypes) => {
        let list = (fuelTypes.length == 0 && transmissionTypes == 0) ? route.params.carGroupList : []

        for (let index = 0; index < fuelTypes.length; index++) {
            const element = fuelTypes[index];
            filtered = route.params.carGroupList.filter(arg => arg.fuelType == element.YAKIT_TIP)
            for (let index = 0; index < filtered.length; index++) {
                const element2 = filtered[index];
                list.push(element2)
            }
        }

        for (let index = 0; index < transmissionTypes.length; index++) {
            const element = transmissionTypes[index];
            filtered = route.params.carGroupList.filter(arg => arg.transmisionId == element.SANZIMAN)
            for (let index = 0; index < filtered.length; index++) {
                const element2 = filtered[index];
                list.push(element2)
            }
        }

        setSelectedFuelTypes(fuelTypes)
        setSelectedTransmissionTypes(transmissionTypes)
        // selectedFuelTypes = fuelTypes
        // selectedTransmissionTypes = transmissionTypes
        setFilteredGroupList([...list])
        refRBSheetFilter.current.close()
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.filterContainer}>
                <TextButton
                    buttonStyle={styles.button}
                    style={styles.buttonTitle}
                    title={'Sırala'}
                    source={require('../../assets/icons/ic_sort.png')}
                    onPress={handleOnSortButton} />
                <VerticalSeperator style={styles.seperator} />
                <TextButton
                    buttonStyle={styles.button}
                    style={styles.buttonTitle}
                    title={'Filtrele'}
                    source={require('../../assets/icons/ic_filter.png')}
                    onPress={handleOnFilterButton} />
            </View>
            <View style={styles.showCampaign}>
                <Text style={styles.showCampaignTitle}>Sadece Kampanyalıları Göster</Text>
                <Switch
                    trackColor={{ false: Colors.white, true: Colors.primaryBrand }}
                    thumbColor={Colors.white}
                    onValueChange={(value) => filterGroupByCampaign(value)}
                    value={filterCampaigns}
                />
            </View>
            <FlatList
                ref={flatListRef}
                data={filteredGroupList}
                renderItem={renderItem}
                keyExtractor={item => item.groupCode}
            />

            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <SortCarGroupModal onPress={handleOnSortItemSelected} current={selectedSortType} />
            </RBSheet>

            <RBSheet
                ref={refRBSheetFilter}
                height={Dimensions.get('screen').height * 0.8}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <FilterGroupListModal
                    fuelTypes={selectedFuelTypes}
                    transmissionTypes={selectedTransmissionTypes}
                    handleOnFilterSelected={handleOnFilterSelected}
                    onDismiss={() => refRBSheetFilter.current.close()} />
            </RBSheet>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    filterContainer: {
        backgroundColor: Colors.white,
        flexDirection: 'row',
        width: wp(375),
        height: hp(50),
        justifyContent: 'center',
        alignItems: 'center',
    },
    showCampaign: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        height: hp(60),
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    showCampaignTitle: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryTextV3
    },
    button: {
        width: wp(186),
        height: hp(50),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonTitle: {
        fontSize: hp(16),
        fontWeight: '700',
        color: TextColors.primaryTextV3
    },
    headerTitle: {
        fontSize: hp(12),
        color: Colors.white,
        textAlign: 'center',
        fontWeight: '700',
        marginTop: hp(3)
    },
    seperator: {
        height: hp(26),
        width: wp(2)
    },
})