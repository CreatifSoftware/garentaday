import { CommonActions, useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect } from 'react'
import { ScrollView, StyleSheet, View, Image, Text, SafeAreaView } from 'react-native'
import { useSelector } from 'react-redux'
import { FilledButton } from '../../components/buttons/FilledButton'
// import { CollapsbleCard } from '../../components/views/CollapsableCard'
import { ReservationSummaryInfo } from '../../components/views/ReservationSummaryInfo'
import { Colors, TextColors } from '../../styles/Colors'
import { wp, hp } from '../../styles/Dimens'
import * as AddCalendarEvent from 'react-native-add-calendar-event';
import Share from "react-native-share";
import { getFormatedDate, isUpdate } from '../../utilities/helpers'
import { PAYMENT_TYPES } from '../../utilities/constants'

export const ReservationSummary = () => {
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const user = useSelector(state => state.authReducer.user)
    const navigation = useNavigation()

    const handleOnReturnHomePage = () => {
        const homeAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'BottomNavigator' }]
        })
        navigation.dispatch(homeAction)
    }

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: null,
            headerRight: null
        })

    }, [navigation]);

    React.useEffect(() => {
        reservation.isPaid = reservation.paymentType === PAYMENT_TYPES.PAY_NOW
    },[])

    const handleOnAddCalendar = () => {
        const eventConfig = {
            title: `Garenta - ${reservation.pickupBranch.branchName} araç kiralama`,
            startDate: reservation.pickupDateTime.toISOString(),
            endDate: reservation.dropoffDateTime.toISOString(),
            // and other options
        };

        AddCalendarEvent.presentEventCreatingDialog(eventConfig)
            .then(eventInfo => {
                //console.warn(JSON.stringify(eventInfo));
            })
            .catch(error => {
                //console.warn(error);
            });
    }

    const handleOnShare = () => {
        let message = `${reservation.pickupBranch.branchName} ofisimizden ${reservation.selectedGroup.groupCode} grubu için, ${getFormatedDate(reservation.pickupDateTime, 'DD.MM.YYYY HH:mm')} - ${getFormatedDate(reservation.dropoffDateTime, 'DD.MM.YYYY HH:mm')} tarihleri arasında rezervasyon yapılmıştır.
                \nGarenta'yı tercih ettiğiniz için teşekkür ederiz.`
        const options = {
            title: 'Rezervasyonunu paylaş',
            message
        };

        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    return (
        <View style={{ flex: 1,paddingBottom:24 }}>
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.infoContainer}>
                    <View style={styles.iconContainer}>
                        <Image
                            source={require('../../assets/icons/ic_success.png')}
                            style={styles.icon} />
                    </View>

                    <Text style={styles.infoBoldTitle}>{isUpdate(reservation) ? 'Rezervasyonunuz Güncellendi.' : 'Rezervasyonunuz Oluşturuldu.'}</Text>
                    <Text style={styles.infoTitle}>PNR No <Text style={styles.infoBoldTitle}>{reservation.pnrNo}</Text></Text>
                    <Text style={styles.infoTitle}>Rezervasyon ile ilgili detaylar <Text style={[styles.infoBoldTitle, { fontSize: hp(14) }]}>{user.contactInfo.email}</Text> adresine gönderildi.</Text>
                </View>

                <ReservationSummaryInfo />
                <FilledButton
                    onPress={handleOnReturnHomePage}
                    style={{ marginHorizontal: wp(20), marginTop: hp(30) }}
                    title={'Ana Sayfaya Dön'} />
                <FilledButton
                    onPress={handleOnAddCalendar}
                    style={{ marginHorizontal: wp(20), backgroundColor: '#ffdccc', marginTop: hp(10) }}
                    textStyle={{ color: Colors.primaryBrand }}
                    title={'Takvime Ekle'}
                    iconStyle={{ tintColor: Colors.primaryBrand }}
                    source={require('../../assets/icons/ic_date.png')} />
                <FilledButton
                    onPress={handleOnShare}
                    style={{ marginHorizontal: wp(20), backgroundColor: '#ffdccc', marginTop: hp(10) }}
                    textStyle={{ color: Colors.primaryBrand }}
                    title={'Paylaş'}
                    iconStyle={{ tintColor: Colors.primaryBrand }}
                    source={require('../../assets/icons/ic_date.png')} />
            </ScrollView>

            {/* <View style={{position:'absolute', bottom:0, width:'100%'}}>
                
            </View> */}
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingBottom: hp(40)
    },
    infoContainer: {
        backgroundColor: '#deffeb',
        paddingHorizontal: wp(20),
        paddingVertical: hp(20),
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconContainer: {
        width: wp(60),
        height: wp(60),
        borderRadius: wp(30),
        backgroundColor: '#baffd5',
        alignItems: 'center',
        justifyContent: 'center',
    },
    infoBoldTitle: {
        fontSize: hp(18),
        color: TextColors.primaryText,
        fontWeight: '700',
        marginTop: hp(10),
        textAlign: 'center'
    },
    infoTitle: {
        fontSize: hp(14),
        color: TextColors.primaryText,
        marginTop: hp(10),
        textAlign: 'center'
    },
    icon: {
        width: wp(40),
        height: wp(40),
        tintColor: '#2ac769'
    }
})