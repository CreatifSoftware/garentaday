import React, { useEffect, useRef, useState } from 'react'
import { Alert, FlatList, StyleSheet, View } from 'react-native'
import { hp, wp } from '../../styles/Dimens'
import { Colors, TextColors } from '../../styles/Colors'
import { Styles } from '../../styles/Styles'
import { useNavigation } from '@react-navigation/native'
import { useDispatch, useSelector } from 'react-redux'
import { AdditionalProductItem } from '../../components/views/AdditionalProductItem'
import { ReservationBottomView } from '../../components/views/ReservationBottomView'
import { StepHeader } from '../../components/headers/StepHeader'
import { setReservationDataAction } from '../../redux/actions/ReservationActions'
import { calculateReservationProductsPrice, calculateTotalAmount, getCarGroupPrice, getOldGroupAmountByPaymentType, isUpdate } from '../../utilities/helpers'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { callGetCreditCardApi } from '../../api/user/userService'
import { isSuccess } from '../../api/apiHelpers'
import { CreditCardResponse } from '../../api/models/CreditCardResponse'

export const AdditionalServices = ({ route }) => {

    const dispatch = useDispatch()
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const user = useSelector(state => state.authReducer.user)
    const navigation = useNavigation()
    const [totalAmount, setTotalAmount] = useState(0)
    const [refresh, setRefresh] = useState(false)
    const [products, setProducts] = useState(route.params.products)
    const [cards, setCards] = useState([])
    let canAddGarentaXpress = false
    // const products = route.params.products
    // const oldProducts = route.params.products

    const handleOnSortItemSelected = () => {

    }

    useEffect(() => {
        if (isUpdate(reservation)) {
            // GarentaXpress için kartları çekiyorum
            // dispatch(setLoadingAction(true))
            callGetCreditCardApi(user.userId)
    
                .then(response => {   
                    if (isSuccess(response)) {
                        let cardList = CreditCardResponse(response.data.EXPORT, user)
                        setCards(cardList)
                    }

                    // dispatch(setLoadingAction(false))
                })
                .catch(error => {
                    // dispatch(setLoadingAction(false))
                    console.log(error)
                })
        }
        _calculateTotalAmount()
        // if (isUpdate(reservation)) {
        determineIfDisableProducts()
        // }
    }, [])

    const determineIfDisableProducts = () => {
        for (let index = 0; index < products.length; index++) {
            let item = products[index]
            const parentIds = item.parentId.split(",")
            for (let index = 0; index < parentIds.length; index++) {
                const element = parentIds[index]
                let filtered = products.filter(arg => arg.productId === element)
                filtered.forEach(arg => {
                    if (arg.required) {
                        item.disabled = true
                    }
                })
            }
        }

        setRefresh(true)
    }

    const handleOnContinueButton = () => {
        reservation.selectedProducts = products.filter(arg => arg.value > 0)
        dispatch(setReservationDataAction(reservation))
        navigation.navigate('AddressSelection')
        // navigation.navigate('AdditionalProducts', { products: route.params.products })
    }

    const _calculateTotalAmount = () => {
        let sum = calculateTotalAmount(reservation, products)
        reservation.selectedProducts = products.filter(arg => arg.value > 0)
        setTotalAmount(sum)

        // setRefresh(true)
    }

    const handleItemSelected = (item) => {
        if (isUpdate(reservation) && item.productId === "HZM0034") {
            debugger
            if (reservation.creditCardInfo){
                canAddGarentaXpress = false
                const reservationCard = reservation.creditCardInfo.cardNo
                let trimmedCard = reservationCard.replace(' ', '').replace(' ', '').replace(' ', '')
                for (let index = 0; index < cards.length; index++) {
                    const element = cards[index];
                    if (element.cardNo.slice(0, 6) === trimmedCard.slice(0, 6) &&
                        element.cardNo.slice(12, 16) === trimmedCard.slice(12, 16)) {
                        canAddGarentaXpress = true
                    }
                }

                if (!canAddGarentaXpress){
                    Alert.alert('', 'GarentaXpress hizmeti eklemek için kayıtlı kredi kartınızla rezervasyon yapmalısınız')
                    return
                }
            } else {
                Alert.alert('', 'GarentaXpress hizmeti eklemek için kayıtlı kredi kartınızla rezervasyon yapmalısınız')
                return
            }
            
        }

        item.value = item.value == 0 ? 1 : 0
        if (item.value > 0) {
            findParentItem(item)
        }
        setProducts([...products])
        _calculateTotalAmount()
    }

    const handleOnMinusPressed = (item) => {
        item.value--
        setProducts([...products])
        _calculateTotalAmount()
    }

    const handleOnPlusPressed = (item) => {
        item.value++
        if (item.value > 0) {
            findParentItem(item)
        }
        setProducts([...products])
        _calculateTotalAmount()
    }

    const findParentItem = (item) => {

        // first check if selected item is parent
        let filtered = products.filter(arg => arg.parentId.includes(item.productId))
        filtered.forEach(item => {
            item.value = 0
        }, () => {
            setProducts([...products])
            _calculateTotalAmount()
        })

        // then check if selected item has parent
        const parentIds = item.parentId.split(",")
        for (let index = 0; index < parentIds.length; index++) {
            const element = parentIds[index]
            let filtered = products.filter(arg => arg.productId === element)
            filtered.forEach(item => {
                item.value = 0
            }, () => {
                setProducts([...products])
                _calculateTotalAmount()
            })
        }
    }

    const handleOnInfoButtonPressed = (item) => {
        Alert.alert('', item.infoText)
    }

    const renderItem = ({ item }) => {

        return (
            <AdditionalProductItem
                item={item}
                reservation={reservation}
                duration={reservation.duration}
                onPress={() => handleItemSelected(item)}
                onMinusPress={() => handleOnMinusPressed(item)}
                onInfoPress={() => handleOnInfoButtonPressed(item)}
                onPlusPress={() => handleOnPlusPressed(item)} />
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <StepHeader
                stepCount={3}
                step={1}
                title={'Ek Hizmetler'} />

            <FlatList
                data={products}
                extraData={products}
                renderItem={renderItem}
                keyExtractor={item => item.productId} />

            <ReservationBottomView
                onPress={handleOnContinueButton}
                title={'Devam Et'}
                totalAmount={totalAmount}
                reservation={reservation}
                buttonStyle={{ width: wp(154) }} />
        </View>
    )
}

const styles = StyleSheet.create({
    filterContainer: {
        backgroundColor: Colors.white,
        flexDirection: 'row',
        width: wp(375),
        height: hp(50),
        justifyContent: 'center',
        alignItems: 'center',
    },
    showCampaign: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        height: hp(60),
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    showCampaignTitle: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryTextV3
    },
    button: {
        width: wp(186),
        height: hp(50),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonTitle: {
        fontSize: hp(16),
        fontWeight: '700',
        color: TextColors.primaryTextV3
    },
    headerTitle: {
        fontSize: hp(12),
        color: Colors.white,
        textAlign: 'center',
        fontWeight: '700',
        marginTop: hp(3)
    },
    seperator: {
        height: hp(26),
        width: wp(2)
    },
})