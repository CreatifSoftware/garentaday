import React, { useRef } from 'react'
import { FlatList, StyleSheet, View } from 'react-native'
import { hp, wp } from '../../styles/Dimens'
import { Colors, TextColors } from '../../styles/Colors'
import { Styles } from '../../styles/Styles'
import { useState } from 'react/cjs/react.development'
import { useNavigation } from '@react-navigation/native'
import { useSelector } from 'react-redux'
import RBSheet from 'react-native-raw-bottom-sheet'
import { AdditionalProductItem } from '../../components/views/AdditionalProductItem'
import { ReservationBottomView } from '../../components/views/ReservationBottomView'
import { ReservationSummaryModal } from '../modals/ReservationSummaryModal'
import { StepHeader } from '../../components/headers/StepHeader'

export const AdditionalProducts = ({ route }) => {
    // const { additionalProducts } = route.params
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const user = useSelector(state => state.authReducer.user)
    const navigation = useNavigation()
    const refRBSheet = useRef();
    const [products, setProducts] = useState(route.params.products.filter(item => item.productType === 'EU'))

    const handleOnSortItemSelected = () => {

    }

    const renderItem = ({ item }) => {
        return (
            <AdditionalProductItem item={item} isSelected={true}/>
        )
    }

    const handleOnContinueButton = () => {
        navigation.navigate('AddressSelection')
    }

    return (
        <View style={{ flex: 1 }}>
            <StepHeader
                stepCount={4}
                step={2}
                title={'Ek Ürünler'} />
            <FlatList
                data={products}
                renderItem={renderItem}
                keyExtractor={item => item.productId} />

            <ReservationBottomView
                onPress={handleOnContinueButton}
                title={'Devam Et'}
                reservation={reservation}
                buttonStyle={{ width: wp(154) }} />
        </View>
    )
}

const styles = StyleSheet.create({
    filterContainer: {
        backgroundColor: Colors.white,
        flexDirection: 'row',
        width: wp(375),
        height: hp(50),
        justifyContent: 'center',
        alignItems: 'center',
    },
    showCampaign: {
        flexDirection: 'row',
        marginHorizontal: wp(20),
        height: hp(60),
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    showCampaignTitle: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryTextV3
    },
    button: {
        width: wp(186),
        height: hp(50),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonTitle: {
        fontSize: hp(16),
        fontWeight: '700',
        color: TextColors.primaryTextV3
    },
    headerTitle: {
        fontSize: hp(12),
        color: Colors.white,
        textAlign: 'center',
        fontWeight: '700',
        marginTop: hp(3)
    },
    seperator: {
        height: hp(26),
        width: wp(2)
    },
})