import { useNavigation } from '@react-navigation/native'
import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, FlatList, Image, ScrollView, Alert } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import FastImage from 'react-native-fast-image'
import { PaymentSelectionHeader } from '../../components/headers/PaymentSelectionHeader'
import { CarGroupTitle } from '../../components/titles/CarGroupTitle'
import { Colors, TextColors } from '../../styles/Colors'
import { hp, wp } from '../../styles/Dimens'
import { VerticalSeperator } from '../../components/VerticalSeperator'
import { Styles } from '../../styles/Styles'
import { FilledButton } from '../../components/buttons/FilledButton'
import { callSearchAdditionalProductsApi } from '../../api/reservation/reservationService'
import { getErrorMessage, isSuccess } from '../../api/apiHelpers'
import { setLoadingAction } from '../../redux/actions/MasterActions'
import { AdditionalProductsResponse } from '../../api/models/AdditionalProductsResponse'
import { PAYMENT_TYPES } from '../../utilities/constants'
import { setReservationDataAction } from '../../redux/actions/ReservationActions'
import { CollapsableGroupCodeInfo } from '../../components/collapsable/CollapsableGroupCodeInfo'
import LinearGradient from 'react-native-linear-gradient'
import { saveJsonData } from '../../utilities/helpers'

export const CarGroupDetail = () => {
    const dispatch = useDispatch()
    const user = useSelector(state => state.authReducer.user)
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const navigation = useNavigation()
    const [paymentType, setPaymentType] = useState(reservation.paymentType)
    const [showGroupInfo, setShowGroupInfo] = useState(false)


    const groupInformations = [
        { title: reservation.selectedGroup.transmissionDesc, icon: require('../../assets/icons/ic_transmission.png') },
        { title: reservation.selectedGroup.fuelTypeDesc, icon: require('../../assets/icons/ic_fuel.png') },
        { title: 'Klima', icon: require('../../assets/icons/ic_aircondition.png') },
        { title: 'Kapı: ' + reservation.selectedGroup.doorNumber, icon: require('../../assets/icons/ic_door.png') },
        { title: 'Bluetooth', icon: require('../../assets/icons/ic_bluetooth.png') }
    ]

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerTitle: <CarGroupTitle reservation={reservation} />
        })
    }, [navigation]);

    const renderGroupInformation = ({ item }) => {
        return (
            <View style={styles().infoContainer}>
                <Image source={item.icon} />
                <Text style={styles().infoTitle}>{item.title}</Text>
            </View>
        )
    }

    const handleOnContinueButton = () => {
        if (user) {
            let data = { isFromReservation: false }
            saveJsonData('isFromReservation', data)
            reservation.paymentType = paymentType
            dispatch(setLoadingAction(true))
            callSearchAdditionalProductsApi(user, reservation)
                .then(response => {
                    dispatch(setLoadingAction(false))
                    if (isSuccess(response)) {
                        dispatch(setReservationDataAction(reservation))
                        navigation.navigate('AdditionalServices', { products: AdditionalProductsResponse(response.data.EXPORT) })
                    }
                    else {
                        Alert.alert('Hata', getErrorMessage(response))
                    }
                })
                .catch(error => {
                    dispatch(setLoadingAction(false))
                    Alert.alert('Hata', '', error.message)
                })
        } else {
            let data = { isFromReservation: true }
            saveJsonData('isFromReservation', data)
            navigation.navigate('LoginModal')
        }


    }

    return (
        <View style={styles().container}>
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                colors={[Colors.primaryBrand, Colors.primaryBrandV2]}
                style={[StyleSheet.absoluteFill, { height: 200 }]}></LinearGradient>

            <ScrollView contentContainerStyle={{ backgroundColor: Colors.mainBackground, paddingBottom: 40 }}>

                <PaymentSelectionHeader
                    navigation={navigation}
                    reservation={reservation} />

                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                    <FastImage
                        style={styles().image}
                        source={{
                            uri: reservation.selectedGroup.imageUrl,
                            priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                    <View style={styles().segmentContainer}>
                        <Text style={styles().segmentText}>{reservation.selectedGroup.segmentDesc}</Text>
                        <VerticalSeperator style={{ marginHorizontal: wp(8), width: 2 }} />
                        <Text style={styles().segmentText}>{reservation.selectedGroup.segmentId} Grubu</Text>
                    </View>

                    <Text style={styles().displayText}>{reservation.selectedGroup.displayText}</Text>

                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ marginTop: hp(20) }}
                        data={groupInformations}
                        renderItem={renderGroupInformation}
                        keyExtractor={item => item.title} />

                    <CollapsableGroupCodeInfo
                        groupInfo={reservation.selectedGroup} />
                </View>
            </ScrollView>
            <View
                style={[Styles.shadowBox, { paddingTop: hp(16), paddingBottom: hp(30), paddingHorizontal: wp(20) }]}>
                <View style={styles().paymentTypeContainer}>
                    <TouchableOpacity
                        style={paymentType === PAYMENT_TYPES.PAY_NOW ?
                            styles().selectedPaymentType : styles().paymentType}
                        onPress={() => setPaymentType(PAYMENT_TYPES.PAY_NOW)}>
                        <Text style={styles(paymentType === PAYMENT_TYPES.PAY_NOW).paymentTitle}>ŞİMDİ ÖDE</Text>
                        <Text style={styles(paymentType === PAYMENT_TYPES.PAY_NOW).paymentAmount}>
                            {reservation.selectedGroup.amountItem.payNowAmount}TL
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={paymentType === PAYMENT_TYPES.PAY_LATER ?
                            styles().selectedPaymentType : styles().paymentType}
                        onPress={() => setPaymentType(PAYMENT_TYPES.PAY_LATER)}>
                        <Text style={styles(paymentType === PAYMENT_TYPES.PAY_LATER).paymentTitle}>SONRA ÖDE</Text>
                        <Text style={styles(paymentType === PAYMENT_TYPES.PAY_LATER).paymentAmount}>
                            {reservation.selectedGroup.amountItem.payLaterAmount}TL
                        </Text>
                    </TouchableOpacity>
                </View>
                <FilledButton
                    title={'Hemen Kirala'}
                    onPress={handleOnContinueButton} />
            </View>
        </View >
    )
}

const styles = (isSelected) => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.mainBackground,
    },
    homeView: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    infoContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(70),
        height: hp(64),
        borderRadius: hp(10),
        marginStart: wp(20)
    },
    infoTitle: {
        fontSize: hp(12),
        color: TextColors.primaryText,
        marginTop: hp(8)
    },
    selectedPaymentType: {
        backgroundColor: 'white',
        width: wp(167.5),
        borderWidth: 2,
        borderColor: Colors.primaryBrand,
        borderRadius: hp(20),
        justifyContent: 'center',
        paddingStart: wp(20)
    },
    paymentType: {
        width: wp(167),
        justifyContent: 'center',
        paddingStart: wp(20)
    },
    paymentTypeContainer: {
        backgroundColor: '#f5f5f5',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: Colors.borderColor,
        borderRadius: hp(20),
        alignItems: 'center',
        marginBottom: hp(20)
    },
    paymentTitle: {
        fontSize: hp(12),
        marginTop: 8,
        color: isSelected ? Colors.primaryBrand : TextColors.primaryTextV3,
        fontFamily: 'NunitoSans-Black'
    },
    paymentAmount: {
        fontSize: hp(20),
        marginBottom: 8,
        color: isSelected ? Colors.primaryBrand : TextColors.primaryTextV3,
        fontFamily: 'NunitoSans-Black'
    },
    paymentDecimal: {
        fontSize: hp(9),
        color: isSelected ? Colors.primaryBrand : TextColors.primaryTextV3,
        fontFamily: 'NunitoSans-Black'
    },
    displayText: {
        fontSize: hp(14),
        fontWeight: '700',
        color: TextColors.primaryText,
        marginTop: hp(9),
        textAlign: 'center',
        marginHorizontal: wp(40)
    },
    image: {
        height: hp(147),
        width: wp(300),
        marginHorizontal: wp(10),
    },
    segmentContainer: {
        flexDirection: 'row',
    },
    segmentText: {
        fontSize: hp(12),
        fontWeight: '700',
        color: TextColors.primaryTextV3,
    },
})



