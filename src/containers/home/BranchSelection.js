
import React, { useEffect, useLayoutEffect, useState } from 'react'
import { StyleSheet, TouchableOpacity, Text, View, Image, SafeAreaView, Platform, Alert, Linking, PermissionsAndroid } from "react-native"
import { useDispatch, useSelector } from 'react-redux';
import { Colors, TextColors } from "../../styles/Colors";
import { hp, wp } from '../../styles/Dimens';
import { HorizontalSeperator } from '../../components/HorizontalSeperator';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { getBranchesCityList, isFavoriteBranch } from '../../utilities/helpers';
import { useNavigation } from '@react-navigation/native';
import { BackButton } from '../../components/buttons/BackButton';
import { setReservationDataAction } from '../../redux/actions/ReservationActions';
import { setLoadingAction } from '../../redux/actions/MasterActions';
import { callSetFavoriteBranchApi } from '../../api/user/userService';
import { setFavoriteBranchsAction } from '../../redux/actions/UserActions';
import Geolocation from 'react-native-geolocation-service';

export const BranchSelection = ({ route }) => {

    String.prototype.turkishToEnglish = function () {
        return this.replace('Ğ', 'g')
            .replace('Ü', 'u')
            .replace('Ş', 's')
            .replace('I', 'i')
            .replace('İ', 'i')
            .replace('Ö', 'o')
            .replace('Ç', 'c')
            .replace('ğ', 'g')
            .replace('ü', 'u')
            .replace('ş', 's')
            .replace('ı', 'i')
            .replace('İ', 'i')
            .replace('ö', 'o')
            .replace('ç', 'c')
    }

    const dispatch = useDispatch()
    const branchs = useSelector(state => state.masterReducer.branchs)
    const user = useSelector(state => state.authReducer.user)
    const favoriteBranchs = useSelector(state => state.userReducer.favoriteBranchs)
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const [searchText, setSearchText] = useState('')
    const [filteredData, setFilteredData] = useState([])
    const [filteredFavorites, setFilteredFavroites] = useState([])
    const navigation = useNavigation()
    const { modalKey, fromSettings, favBranchs } = route.params;
    // const favoriteBranchs = favBranchs

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => <BackButton navigation={navigation} />,
            headerRight: null,
            headerTitle: fromSettings ? 'Şube Listesi' : modalKey === 'PICKUP_BRANCH' ? 'Alış Şubesi' : 'Teslim Şubesi'
        })
    }, [navigation]);

    useEffect(() => {
        let data = []
        let favorites = []
        for (let index = 0; index < branchs.length; index++) {
            const item = branchs[index];

            if (isFavoriteBranch(favoriteBranchs, item.branchId)) {
                favorites.push(item)
            } else {
                data.push(item)
            }
        }

        setFilteredData(data)
        setFilteredFavroites(favorites)
    }, [favoriteBranchs])

    function handleOnBranchSelection(branch) {
        if (fromSettings) {
            navigation.navigate('BranchDetail', { branch: branch })
        } else {
            if (modalKey === 'PICKUP_BRANCH') {
                reservation.pickupBranch = branch
                reservation.dropoffBranch = branch
                // if (reservation.dropoffBranch == null) {
                //     reservation.dropoffBranch = branch
                // }

            } else {
                reservation.dropoffBranch = branch
            }

            dispatch(setReservationDataAction(reservation))
            navigation.goBack()
        }
    }

    const handleOnSetFavorite = (branch) => {
        let isFavorite = isFavoriteBranch(favoriteBranchs, branch.branchId)

        dispatch(setLoadingAction(true))
        callSetFavoriteBranchApi(user.userId, branch.branchId, isFavorite ? 'D' : 'C')
            .then(response => {
                dispatch(setFavoriteBranchsAction(response.data.EXPORT))
                favoriteBranchs = response.data.EXPORT.ET_FAV_BRANCH_LIST
                dispatch(setLoadingAction(false))
            })
            .catch(error => {
                dispatch(setLoadingAction(false))
            })
    }

    const getLocation = async () => {
        const hasPermission = await hasLocationPermission();

        console.log(hasPermission)
        if (!hasPermission) {
            return;
        }

        navigation.navigate('NearbyBranchSelection', { modalKey: modalKey, fromSettings: fromSettings })
    }

    const hasLocationPermission = async () => {
        if (Platform.OS === 'ios') {
            const hasPermission = await hasPermissionIOS();
            return hasPermission;
        }

        if (Platform.OS === 'android' && Platform.Version < 23) {
            return true;
        }

        const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (hasPermission) {
            return true;
        }

        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) {
            return true;
        }

        if (status === PermissionsAndroid.RESULTS.DENIED) {
            Alert.alert('Uyarı', 'Konum bilgilerinize erişmeye iznimiz yok. Telefonunuzun ayarlar bölümünden aktifleştirmeniz gerekiyor.')
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            Alert.alert('Uyarı', 'Konum bilgilerinize erişmeye iznimiz yok. Telefonunuzun ayarlar bölümünden aktifleştirmeniz gerekiyor.')
        }

        return false;
    };

    const hasPermissionIOS = async () => {
        const openSetting = () => {
            Linking.openSettings().catch(() => {
                Alert.alert('Unable to open settings');
            });
        };
        const status = await Geolocation.requestAuthorization('whenInUse');

        if (status === 'granted') {
            return true;
        }

        // if (status === 'denied') {
        //     Alert.alert('Uyarı','Konum bilgilerinize erişmeye iznimiz yok. Telefonunuzun ayarlar bölümünden aktifleştirmeniz gerekiyor.')
        // }

        if (status === 'disabled') {
            Alert.alert(
                'Hata',
                'Yakınınızdaki şubeleri görebilemek için uygulamanın konumuna erişimine izin vermen gerekiyor.',
                [
                    { text: 'Aarlar', onPress: openSetting },
                    { text: "Vazgeç", onPress: () => { } },
                ],
            );
        }

        return false;
    };

    const filterBranchs = (text) => {
        let data = []
        let favorites = []
        for (let index = 0; index < branchs.length; index++) {
            const item = branchs[index];

            if (isFavoriteBranch(favoriteBranchs, item.branchId)) {
                favorites.push(item)
            } else {
                data.push(item)
            }
        }

        const filtered = data.filter(function (arg) {
            const itemData = arg.branchName ? arg.branchName.turkishToEnglish().toUpperCase() : ''.turkishToEnglish().toUpperCase();
            const textData = text.turkishToEnglish().toUpperCase()

            return itemData.indexOf(textData) > -1
        })

        const filteredFavs = favorites.filter(function (arg) {
            const itemData = arg.branchName ? arg.branchName.turkishToEnglish().toUpperCase() : ''.turkishToEnglish().toUpperCase();
            const textData = text.turkishToEnglish().toUpperCase()

            return itemData.indexOf(textData) > -1
        })

        console.log(filtered.length)

        if (text === "") {
            setSearchText(text)
            setFilteredData(data)
            setFilteredFavroites(favorites)
        }
        else {
            setSearchText(text)
            setFilteredData(filtered)
            setFilteredFavroites(filteredFavs)
        }
    }

    const renderHeader = (header) => {
        return (
            <View key={header} style={styles.header}>
                <Text style={styles.headerTitle}>{header}</Text>
            </View>
        )
    }

    const renderBranchItem = (item) => {
        // get filtered list by city code
        const filteredBranchs = filteredData.filter(function (arg) {
            return arg.city == item.city;
        })

        return (
            <View style={{ marginBottom: hp(4), backgroundColor: 'white', paddingHorizontal: wp(20) }}>
                <Text style={styles.itemTitle}>{item.cityName}</Text>
                {
                    filteredBranchs.map(branch => {
                        let isFavorite = isFavoriteBranch(favoriteBranchs, branch.branchId)
                        return (
                            <>
                                {
                                    !isFavorite &&
                                    <View style={styles.item} key={branch.branchId}>
                                        <TouchableOpacity style={{ flex: 1, marginEnd: wp(20) }} onPress={() => handleOnBranchSelection(branch)}>
                                            <Text style={styles.itemContent}>{branch.branchName}</Text>
                                        </TouchableOpacity>
                                        {
                                            user &&
                                            <TouchableOpacity style={{ justifyContent: 'center', marginBottom: hp(20) }} onPress={() => handleOnSetFavorite(branch)}>
                                                <Image style={{ tintColor: Colors.checkboxBorder }} source={require('../../assets/icons/ic_favorite.png')}></Image>
                                            </TouchableOpacity>
                                        }

                                    </View>
                                }

                            </>

                        )
                    })
                }
            </View>
        )
    }

    const renderFavoriteBrancItem = (item) => {
        // get filtered list by city code
        const filteredBranchs = filteredFavorites.filter(function (arg) {
            return arg.city == item.city;
        })

        return (
            <View style={{ marginBottom: hp(4), backgroundColor: 'white', paddingHorizontal: wp(20) }}>
                <Text style={styles.itemTitle}>{item.cityName}</Text>
                {
                    filteredBranchs.map(branch => {
                        let isFavorite = isFavoriteBranch(favoriteBranchs, branch.branchId)
                        return (
                            <>
                                {
                                    isFavorite &&
                                    <View style={styles.item} key={branch.branchId}>
                                        <TouchableOpacity style={{ flex: 1, marginEnd: wp(20) }} onPress={() => handleOnBranchSelection(branch)}>
                                            <Text style={styles.itemContent}>{branch.branchName}</Text>
                                        </TouchableOpacity>
                                        {
                                            user &&
                                            <TouchableOpacity style={{ justifyContent: 'center', marginBottom: hp(20) }} onPress={() => handleOnSetFavorite(branch)}>
                                                <Image style={{ tintColor: Colors.primaryBrand }} source={require('../../assets/icons/ic_favorite_fill.png')}></Image>
                                            </TouchableOpacity>
                                        }

                                    </View>
                                }

                            </>

                        )
                    })
                }
            </View>
        )
    }

    return (
        <SafeAreaView style={{ backgroundColor: 'white' }}>
            <ScrollView style={styles.container} keyboardShouldPersistTaps={'handled'}>
                <TextInput
                    style={styles.textInput}
                    autoCorrect={false}
                    spellCheck={false}
                    returnKeyType={'next'}
                    placeholder={'Şube Ara'}
                    onChangeText={(text) => filterBranchs(text)}
                    value={searchText} />
                <TouchableOpacity style={{ flexDirection: 'row', marginBottom: hp(20), marginStart: wp(20) }} onPress={getLocation}>
                    <Image source={require('../../assets/icons/ic_location_near_me.png')} style={{ tintColor: Colors.primaryBrand }} />
                    <Text style={{ color: Colors.primaryBrand, fontSize: hp(16), fontWeight: '700', marginStart: wp(8) }}>Yakınımdaki Şubeler</Text>
                </TouchableOpacity>
                <HorizontalSeperator />
                {
                    filteredFavorites.length > 0 &&
                    renderHeader("Favori Şubeler")
                }
                {
                    filteredFavorites.length > 0 &&
                    getBranchesCityList(filteredFavorites).map(item => {
                        return (
                            renderFavoriteBrancItem(item)
                            // renderBranchItem(item)
                        )
                    })
                }
                {
                    renderHeader("Şube Listesi")
                }
                {
                    getBranchesCityList(filteredData).map(item => {
                        return (
                            renderBranchItem(item)
                        )
                    })
                }
            </ScrollView>
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.mainBackground,
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    searchIcon: {
        padding: 10,
    },
    textInput: {
        paddingStart: wp(10),
        marginHorizontal: wp(20),
        marginTop: hp(20),
        marginBottom: hp(20),
        height: hp(49),
        backgroundColor: 'white',
        borderRadius: hp(10)
    },
    text: {
        fontSize: hp(14),
        fontFamily: 'NunitoSans-Bold',
        alignSelf: 'center',
        color: Colors.white,
    },
    icon: {
        tintColor: Colors.primaryBrand,
        position: 'absolute',
        start: wp(36),
        bottom: 36,
        justifyContent: 'center',
        alignItems: 'center'
    },
    header: {
        backgroundColor: Colors.mainBackground,
        height: hp(49),
        width: wp(375),
        justifyContent: 'center'
    },
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: hp(50),
        alignItems: 'center',
    },
    itemTitle: {
        color: TextColors.primaryText,
        fontSize: hp(18),
        fontWeight: '700',
        marginTop: hp(20),
        marginBottom: hp(10)
    },
    itemContent: {
        color: TextColors.primaryTextV2,
        fontSize: hp(16),
        marginBottom: hp(20)
    },
    headerTitle: {
        color: TextColors.primaryTextV4,
        fontSize: hp(20),
        fontWeight: '700',
        marginStart: wp(20)
    }
})
