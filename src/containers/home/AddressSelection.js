import { useIsFocused, useNavigation } from "@react-navigation/native"
import React, { useEffect, useState } from "react"
import { Image, TouchableOpacity, View, Text, StyleSheet, FlatList, ScrollView } from "react-native"
import { useDispatch, useSelector } from "react-redux"
import { isSuccess } from "../../api/apiHelpers"
import { ImageButton } from "../../components/buttons/ImageButton"
import { RadioButton, RadioButtonItem } from "../../components/buttons/RadioButtonView"
import { StepHeader } from "../../components/headers/StepHeader"
import { ReservationBottomView } from "../../components/views/ReservationBottomView"
import { setLoadingAction } from "../../redux/actions/MasterActions"
import { setReservationDataAction } from "../../redux/actions/ReservationActions"
import { Colors, TextColors } from "../../styles/Colors"
import { hp, wp } from "../../styles/Dimens"
import { Styles } from "../../styles/Styles"
import { INVOICE_ADDRESS, PERSONAL_ADDRESS } from "../../utilities/constants"
import { calculateReservationProductsPrice, calculateTotalAmount, getCarGroupPrice, getCityItem, getCountryItem, getDistrictItem } from "../../utilities/helpers"

export const AddressSelection = () => {

    const dispatch = useDispatch()
    const user = useSelector(state => state.authReducer.user)
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const countries = useSelector(state => state.masterReducer.countries)
    const cities = useSelector(state => state.masterReducer.cities)
    const districts = useSelector(state => state.masterReducer.districts)

    const navigation = useNavigation()
    const [selectedPersonalAddressId, setSelectedPersonalAddressId] = useState(null)
    const [selectedInvoiceAddressId, setSelectedInvoiceAddressId] = useState(null)
    const [buttonDisabled, setButtonDisabled] = useState(true);
    const [totalAmount, setTotalAmount] = useState(0)
    const isFocused = useIsFocused()
    // const forceUpdate = React.useState()[1].bind(null, {})  // see NOTE above
    // const forceUpdate = React.useReducer(() => ({}))[1]

    useEffect(() => {
        setContinueButtonState()
    }, [selectedPersonalAddressId, selectedInvoiceAddressId, isFocused])

    const setContinueButtonState = () => {
        if (selectedPersonalAddressId != null &&
            selectedInvoiceAddressId != null) {
            setButtonDisabled(false)
        } else {
            setButtonDisabled(true)
        }
    }

    const handleOnContinueButton = () => {
        reservation.individualAddressId = selectedPersonalAddressId
        reservation.invoiceAddressId = selectedInvoiceAddressId
        dispatch(setReservationDataAction(reservation))
        navigation.navigate('ReservationCreate')
    }

    const handleOnAddressItemSelection = (item) => {
        if (item.type === PERSONAL_ADDRESS) {
            setSelectedPersonalAddressId(item.addressId)
        } else {
            setSelectedInvoiceAddressId(item.addressId)
        }
    }

    const handleOnEditAddress = (item) => {
        navigation.navigate('AddressInformation', { addressItem: item, type: item.type })
    }

    const renderPersonalAddressItem = (item) => {
        const countryItem = getCountryItem(countries, item.country)
        const cityItem = getCityItem(cities, item.country, item.city)
        const districtItem = getDistrictItem(districts, item.district)

        const district = districtItem && districtItem.districtName

        return (
            <TouchableOpacity
                style={item.addressId === selectedPersonalAddressId ? styles.selectedAddressContainer : styles.addressContainer}
                onPress={() => handleOnAddressItemSelection(item)}>
                <RadioButton selected={item.addressId === selectedPersonalAddressId} activeColor={Colors.primaryBrand} />
                <View style={{ marginHorizontal: wp(10), marginTop: hp(-4) }}>
                    <Text style={[Styles.title, { marginBottom: hp(8) }]}>{item.addressTitle}</Text>
                    {
                        district ?
                            <Text style={Styles.content}>{district} - {cityItem.cityName} / {countryItem.countryName}</Text>
                            :
                            <Text style={Styles.content}>{cityItem.cityName} / {countryItem.countryName}</Text>
                    }

                    <Text style={[Styles.content, { marginBottom: hp(20), marginEnd: wp(25) }]}>{item.address}</Text>
                </View>
                <ImageButton
                    onPress={() => handleOnEditAddress(item)}
                    style={styles.edit}
                    source={require('../../assets/icons/ic_edit.png')}
                    imageStyle={{ tintColor: Colors.primaryBrand }} />
            </TouchableOpacity>
        )
    }

    const renderInvoiceAddressItem = (item) => {
        const countryItem = getCountryItem(countries, item.country)
        const cityItem = getCityItem(cities, item.country, item.city)
        const districtItem = getCountryItem(districts, item.district)

        const district = districtItem && districtItem.district

        return (
            <TouchableOpacity
                style={item.addressId === selectedInvoiceAddressId ? styles.selectedAddressContainer : styles.addressContainer}
                onPress={() => handleOnAddressItemSelection(item)}>
                <RadioButton selected={item.addressId === selectedInvoiceAddressId} activeColor={Colors.primaryBrand} />
                <View style={{ marginHorizontal: wp(10), marginTop: hp(-4) }}>
                    <Text style={[Styles.title, { marginBottom: hp(8) }]}>{item.addressTitle}</Text>
                    {
                        district ?
                            <Text style={Styles.content}>{district} - {cityItem.cityName} / {countryItem.countryName}</Text>
                            :
                            <Text style={Styles.content}>{cityItem.cityName} / {countryItem.countryName}</Text>
                    }
                    <Text style={[Styles.content, { marginBottom: hp(20), marginEnd: wp(25) }]}>{item.address}</Text>
                </View>
                <ImageButton
                    onPress={() => handleOnEditAddress(item)}
                    style={styles.edit}
                    source={require('../../assets/icons/ic_edit.png')}
                    imageStyle={{ tintColor: Colors.primaryBrand }} />
            </TouchableOpacity>
        )
    }

    const renderItem = ({ item }) => {
        return (item.type === PERSONAL_ADDRESS ? renderPersonalAddressItem(item) : renderInvoiceAddressItem(item))
    }

    const renderAddressContainer = (addressList) => {
        return (
            <>
                {
                    addressList.length > 0 ?
                        <FlatList
                            horizontal
                            contentContainerStyle={{ marginTop: hp(10) }}
                            data={addressList}
                            extraData={
                                selectedPersonalAddressId,
                                selectedInvoiceAddressId
                            }
                            renderItem={renderItem}
                            keyExtractor={item => item.addressId} />

                        :

                        <View style={styles.emptyContainer}>
                            <Text style={{
                                fontFamily: 'NunitoSans-Black',
                                fontSize: wp(16),
                                color: TextColors.primaryTextV2
                            }}>Kayıtlı adresiniz bulunmamaktadır</Text>
                            <Text style={{
                                fontFamily: 'NunitoSans-SemiBold',
                                fontSize: wp(14),
                                marginHorizontal:wp(40),
                                textAlign:'center',
                                color: TextColors.primaryTextV2,
                                marginTop: hp(10)
                            }}>Lütfen 'Yeni Ekle' butonu ile adres ekleyiniz.</Text>
                        </View>

                        // <View style={styles.noAddressContainer}>
                        //     <Image style={{ width: wp(80), height: hp(80) }} source={require('../../assets/icons/ic_no_address.png')} resizeMode={'contain'} />
                        //     <Text style={[Styles.container, { marginTop: hp(8) }]}>Henüz kayıtlı adresiniz bulunmamaktadır.</Text>
                        // </View>
                }
            </>
        )
    }

    const handleOnAddIndividualAddress = () => {
        navigation.navigate('AddressInformation', { addressItem: null, type: PERSONAL_ADDRESS, hideDeleteButton: true })
    }

    const handleOnAddInvoiceAddress = () => {
        navigation.navigate('AddressInformation', { addressItem: null, type: INVOICE_ADDRESS, hideDeleteButton: true })
    }


    return (
        <>
            <View style={{ flex: 1 }}>
                <StepHeader
                    stepCount={3}
                    step={2}
                    title={'Adres Seçimi'} />

                <ScrollView contentContainerStyle={{ paddingBottom: 40 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: wp(20), marginTop: hp(30) }}>
                        <Text style={styles.title}>Kayıtlı Adreslerim</Text>
                        <TouchableOpacity
                            style={{ flexDirection: 'row' }}
                            onPress={handleOnAddIndividualAddress}>
                            <Image style={{ tintColor: Colors.primaryBrand }} source={require('../../assets/icons/ic_plus.png')} />
                            <Text style={styles.addTitle}>Yeni Ekle</Text>
                        </TouchableOpacity>
                    </View>
                    {
                        renderAddressContainer(user.addressList.filter((item) => item.type == PERSONAL_ADDRESS))
                    }

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: wp(20), marginTop: hp(30) }}>
                        <Text style={styles.title}>Fatura Adreslerim</Text>
                        <TouchableOpacity
                            style={{ flexDirection: 'row' }}
                            onPress={handleOnAddInvoiceAddress}>
                            <Image style={{ tintColor: Colors.primaryBrand }} source={require('../../assets/icons/ic_plus.png')} />
                            <Text style={styles.addTitle}>Yeni Ekle</Text>
                        </TouchableOpacity>
                    </View>

                    {
                        renderAddressContainer(user.addressList.filter((item) => item.type == INVOICE_ADDRESS))
                    }
                </ScrollView>
            </View>
            <ReservationBottomView
                // showReservationDetail={() => refRBSheet.current.open()}
                disabled={buttonDisabled}
                onPress={handleOnContinueButton}
                title={'Devam Et'}
                reservation={reservation}
                totalAmount={calculateTotalAmount(reservation, reservation.selectedProducts)}
                buttonStyle={{ width: wp(154) }} />
        </>
    )
}

const styles = StyleSheet.create({
    addressContainer: {
        width: wp(290),
        height: hp(143),
        flexDirection: 'row',
        backgroundColor: Colors.white,
        paddingVertical: hp(20),
        marginStart: wp(20),
        paddingHorizontal: wp(20),
        borderRadius: hp(10)
    },
    selectedAddressContainer: {
        width: wp(290),
        height: hp(143),
        flexDirection: 'row',
        backgroundColor: Colors.white,
        paddingVertical: hp(20),
        paddingHorizontal: wp(20),
        marginStart: wp(20),
        borderRadius: hp(10),
        borderColor: Colors.primaryBrand,
        borderWidth: 1,
    },
    noAddressContainer: {
        // backgroundColor:'white',
        // width:wp(335),
        paddingVertical: hp(20),
        // borderRadius:hp(10),
        height: hp(143),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: hp(10)
    },
    title: {
        fontSize: hp(20),
        color: TextColors.primaryText,
        fontWeight: 'bold'
    },
    addTitle: {
        marginStart: wp(5),
        fontSize: hp(16),
        color: Colors.primaryBrand,
        fontWeight: '700'
    },
    edit: {
        position: 'absolute',
        top: hp(20),
        end: wp(20)
    },
    emptyContainer: {
        height: hp(140),
        marginTop: hp(16),
        borderRadius: hp(10),
        marginStart: wp(20),
        backgroundColor: 'white',
        width: wp(325),
        shadowColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.1,
        elevation: 4,
        marginBottom: hp(16),
        paddingHorizontal: wp(16)
    },
})