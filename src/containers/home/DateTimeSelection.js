import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'
import { Keyboard, StyleSheet, Text, View, Image, TouchableOpacity, Dimensions } from 'react-native'
import { CalendarList, LocaleConfig } from 'react-native-calendars'
import { useSelector } from 'react-redux'
import { HorizontalSeperator } from '../../components/HorizontalSeperator'
import { FloatingTextInput } from '../../components/inputs/FloatingTextInput'
import { Styles } from '../../styles/Styles'
import { getFormatedDate } from '../../utilities/helpers'
import moment from 'moment'
import localization from 'moment/locale/tr'
import { hp, wp } from '../../styles/Dimens'
import { Colors, TextColors } from '../../styles/Colors'
import { FilledButton } from '../../components/buttons/FilledButton'
import { useNavigation } from '@react-navigation/native'
import { TimeSelectionModal } from '../modals/TimeSelectionModal'
import RBSheet from 'react-native-raw-bottom-sheet'
moment.updateLocale('tr', localization)

LocaleConfig.locales['tr'] = {
    monthNames: ['Ocak','Şubat','Mart','Nisan','Mayıs','Haziran','Temmuz','Ağustos','Eylül','Ekim','Kasım','Aralık'],
    monthNamesShort: ['Ocak','Şub.','Mart','Nis.','May.','Haz.','Tem.','Ağu.','Eyl.','Ekim','Kas.','Ara.'],
    dayNames: ['Pazar','Pazartesi','Salı','Çarşamba','Perşembe','Cuma','Cumartesi'],
    dayNamesShort: ['Paz','Pts','Sal','Çar','Per','Cum','Cts'],
    today: 'Bugün'
  };

  LocaleConfig.defaultLocale = 'tr';

const PICKUP_TIME_SELECTION = "PICKUP_TIME_SELECTION"
const DROPOFF_TIME_SELECTION = "DROPOFF_TIME_SELECTION"

export const DateTimeSelection = () => {
    const reservation = useSelector(state => state.reservationReducer.reservation)
    const workingHours = useSelector(state => state.masterReducer.workingHours)

    const [pickupDate, setPickupDate] = useState(reservation.pickupDateTime ? reservation.pickupDateTime.clone() : undefined)
    const [dropoffDate, setDropoffDate] = useState(reservation.dropoffDateTime ? reservation.dropoffDateTime.clone() : undefined)
    const [pickupTime, setPickupTime] = useState(reservation.pickupDateTime ? reservation.pickupDateTime.clone() : undefined)
    const [dropoffTime, setDropoffTime] = useState(reservation.dropoffDateTime ? reservation.dropoffDateTime.clone() : undefined)
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const [visibleMonths, setVisibleMonths] = useState([])

    const navigation = useNavigation()
    const refRBSheet = useRef();
    const refRBSheet2 = useRef();
    let pickerTag = null
    let markedDates = {};

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: null,
        })
    }, [navigation]);

    useEffect(() => {
        setContinueButtonState()
    }, [pickupDate, dropoffDate, pickupTime, dropoffTime])

    const setContinueButtonState = () => {
        setButtonDisabled(false)
        if (pickupDate &&
            dropoffDate &&
            pickupTime &&
            dropoffTime) {
            setButtonDisabled(false)
        }
        else {
            setButtonDisabled(true)
        }
    }

    function getMarkedDates() {
        // let markedDates = {};
        if (!pickupDate && !dropoffDate) {
            return markedDates
        }

        var currDate = pickupDate.startOf('day');
        var now = currDate.clone(), dates = [];

        if (dropoffDate == null) {
            dates.push(now.format('yyyy-MM-DD'));
        }
        else {
            var lastDate = dropoffDate.startOf('day');
            while (now.isSameOrBefore(lastDate)) {
                dates.push(now.format('yyyy-MM-DD'));
                now.add(1, 'days');
            }
        }

        dates.forEach((day, index) => {
            const firstItem = index == 0
            const lastItem = index == dates.length - 1
            if (firstItem) {
                markedDates[day] = { startingDay: true, color: '#ff5000', textColor: 'white' }
            } else if (lastItem) {
                markedDates[day] = { endingDay: true, color: '#ff5000', textColor: 'white' }
            } else {
                markedDates[day] = { color: '#ffdccc', textColor: '#333333' }
            }
        });

        return markedDates;
    }

    const renderDateItem = (title, date, active) => {
        return (
            <View style={{ flexGrow: 1, marginEnd: wp(20) }}>
                <Text style={styles.dateTitle}>{title}</Text>
                <View style={{ flexDirection: 'row', marginVertical: hp(11), alignItems: 'center' }}>
                    <Image style={styles.icon} source={require('../../assets/icons/ic_date.png')} resizeMode={'contain'} />
                    <Text style={date == null ? styles.emptyDate : active ? styles.activeDate : styles.date}>{date ? getFormatedDate(date, 'DD.MM.YYYY') : title}</Text>
                </View>
                <HorizontalSeperator style={{ backgroundColor: active ? Colors.primaryBrand : Colors.seperatorColor }} />
            </View>
        )
    }

    const handleOnContinueButton = () => {
        reservation.pickupDateTime = moment(getFormatedDate(pickupDate, 'yyyy-MM-DD') + ' ' + getFormatedDate(pickupTime, 'HH:mm'));
        reservation.dropoffDateTime = moment(getFormatedDate(dropoffDate, 'yyyy-MM-DD') + ' ' + getFormatedDate(dropoffTime, 'HH:mm'));
        navigation.goBack()
    }

    const handleOnTimeSelection = (tag) => {
        pickerTag = tag
        if (pickerTag === PICKUP_TIME_SELECTION) {
            refRBSheet.current.open()
        } else {
            refRBSheet2.current.open()
        }

    }

    const timeSelected = (item) => {
        if (pickerTag === PICKUP_TIME_SELECTION) {
            setPickupTime(moment(item, 'HH:mm'))
            if (!dropoffTime) {
                setDropoffTime(moment(item, 'HH:mm'))
            }

            refRBSheet.current.close()
        }
        if (pickerTag === DROPOFF_TIME_SELECTION) {
            setDropoffTime(moment(item, 'HH:mm'))
            refRBSheet2.current.close()
        }
    }

    const renderTimeItem = (title, time, tag) => {
        return (
            <TouchableOpacity
                onPress={() => handleOnTimeSelection(tag)}
                disabled={!(pickupDate && dropoffDate)}
                style={{ flexGrow: 1, marginEnd: wp(20), opacity: (pickupDate && dropoffDate) ? 1 : 0.5 }}>
                <Text style={styles.dateTitle}>{title}</Text>
                <View style={{ flexDirection: 'row', marginVertical: hp(11), alignItems: 'center' }}>
                    <Image style={styles.icon} source={require('../../assets/icons/ic_alarm.png')} resizeMode={'contain'} />
                    <Text style={time ? styles.date : styles.emptyDate}>{time ? getFormatedDate(time, 'HH:mm') : title}</Text>
                </View>
                <HorizontalSeperator />
            </TouchableOpacity>
        )
    }

    const onDayPress = (date) => {
        if (pickupDate != null && dropoffDate == null) {
            setDropoffDate(moment(date.dateString))
            // pickerTag = PICKUP_TIME_SELECTION
            // refRBSheet.current.open()
        } else {
            setPickupDate(moment(date.dateString))
            setDropoffDate(null)
        }
    }

    return (
        <View style={{ backgroundColor: 'white', flex: 1 }}>
            <View style={{ flexDirection: 'row', marginStart: wp(20), marginVertical: hp(12) }}>
                {renderDateItem('Alış Tarihi', pickupDate, dropoffDate !== null)}
                {renderDateItem('Teslim Tarihi', dropoffDate, dropoffDate === null)}
            </View>
            <View style={{ flexDirection: 'row', marginStart: wp(20), marginVertical: hp(12) }}>
                {renderTimeItem('Alış Saati', pickupTime, PICKUP_TIME_SELECTION)}
                {renderTimeItem('Teslim Saati', dropoffTime, DROPOFF_TIME_SELECTION)}
            </View>

            <HorizontalSeperator />
            <CalendarList
                firstDay={1}
                minDate={getFormatedDate(moment(), 'yyyy-MM-DD')}
                theme={{
                    textMonthFontSize: hp(20),
                    textMonthFontFamily: 'NunitoSans-Bold',
                    textDayFontSize: hp(16),
                    textDayfontWeight: '700',
                    textSectionTitleColor: '#828282',
                }}
                onVisibleMonthsChange={(months) => { setVisibleMonths(months) }}
                onDayPress={(date) => onDayPress(date)}
                markingType={'period'}
                current={pickupDate && pickupDate.toDate()}
                markedDates={getMarkedDates()} />

            <View style={[Styles.shadowBox, { position: 'absolute' }]} >
                <FilledButton
                    disabled={buttonDisabled}
                    style={Styles.continueButton}
                    onPress={handleOnContinueButton}
                    title={'Onayla'} />
            </View>

            <RBSheet
                height={Dimensions.get('screen').height * 0.5}
                ref={refRBSheet}
                closeOnDragDown={false}
                customStyles={{
                    draggableIcon: Styles.draggableIcon,
                    container: Styles.modalContainer
                }}>
                <TimeSelectionModal
                    branchId={reservation.pickupBranch.branchId}
                    title={'Alış Saati'}
                    date={pickupDate}
                    time={pickupTime}
                    onPress={timeSelected} />
            </RBSheet>

            <RBSheet
                height={hp(349)}
                ref={refRBSheet2}
                closeOnDragDown={false}
                >
                <TimeSelectionModal
                    // isPickupTime={pickerTag === PICKUP_TIME_SELECTION}
                    branchId={reservation.dropoffBranch.branchId}
                    title={'Teslim Saati'}
                    date={dropoffDate}
                    time={dropoffTime}
                    onPress={timeSelected} />
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    dateTitle: {
        fontSize: hp(12),
        color: TextColors.primaryTextV7,
        fontWeight: '700',
    },
    icon: {
        tintColor: Colors.primaryBrand,
        width: hp(20),
        height: hp(20),
        marginEnd: wp(10)
    },
    date: {
        fontSize: hp(16),
        color: TextColors.primaryText,
    },
    emptyDate: {
        fontSize: hp(16),
        color: TextColors.primaryTextV3,
    },
    activeDate: {
        fontSize: hp(16),
        color: Colors.primaryBrand,
        fontFamily: 'NunitoSans-Bold',
    },
    continueButton: {
        width: wp(335),
        marginVertical: hp(40),
        marginHorizontal: wp(20),
        marginTop: hp(16),
    },
})

