import 'react-native-gesture-handler';
import React from 'react'
import { Provider } from 'react-redux';
import { StatusBar, Platform } from 'react-native';
import KeyboardManager from 'react-native-keyboard-manager'
import { Container } from './src/navigation/Container'
import { PersistGate } from 'redux-persist/integration/react'
import { store } from './src/redux/Store';
import { Adjust, AdjustEvent, AdjustConfig } from 'react-native-adjust';
import { ADJUST_APP_TOKEN, ADJUST_ENVIRONMENT } from './src/api/constants';

if (Platform.OS === 'ios') {
  KeyboardManager.setEnable(true);
  KeyboardManager.setEnableDebugging(false);
  KeyboardManager.setKeyboardDistanceFromTextField(10);
  KeyboardManager.setEnableAutoToolbar(true);
  KeyboardManager.setToolbarDoneBarButtonItemText("Bitti");
  KeyboardManager.setToolbarPreviousNextButtonEnable(false);
  KeyboardManager.setShouldShowToolbarPlaceholder(false);
  KeyboardManager.setOverrideKeyboardAppearance(false);
  KeyboardManager.setShouldResignOnTouchOutside(true);
  KeyboardManager.setKeyboardAppearance("default");
  KeyboardManager.resignFirstResponder();
}

export default function App() {
  if (Platform.OS === "android") {
    StatusBar.setBackgroundColor("rgba(0,0,0,0)");
    StatusBar.setTranslucent(true);
    StatusBar.setBarStyle("dark-content")
  }

  const adjustConfig = new AdjustConfig(ADJUST_APP_TOKEN, ADJUST_ENVIRONMENT);
  Adjust.create(adjustConfig)

  React.useEffect(() => {
    return () => {
      Adjust.componentWillUnmount()
    }
  }, [])

  return (
    <>
      <StatusBar barStyle="light-content" translucent backgroundColor="transparent" />
      <Provider store={store}>
        {/* <PersistGate loading={null} persistor={persistor}> */}
        <Container />
        {/* </PersistGate> */}
      </Provider>
    </>

  );
}