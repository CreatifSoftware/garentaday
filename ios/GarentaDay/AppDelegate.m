#import "AppDelegate.h"

#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKCoreKit/FBSDKSettings.h>
#import "RNBootSplash.h"
#import <AppTrackingTransparency/AppTrackingTransparency.h>

@import TrustKit;
@import Firebase;

#ifdef FB_SONARKIT_ENABLED
#import <FlipperKit/FlipperClient.h>
#import <FlipperKitLayoutPlugin/FlipperKitLayoutPlugin.h>
#import <FlipperKitUserDefaultsPlugin/FKUserDefaultsPlugin.h>
#import <FlipperKitNetworkPlugin/FlipperKitNetworkPlugin.h>
#import <SKIOSNetworkPlugin/SKIOSNetworkAdapter.h>
#import <FlipperKitReactPlugin/FlipperKitReactPlugin.h>


static void InitializeFlipper(UIApplication *application) {
  FlipperClient *client = [FlipperClient sharedClient];
  SKDescriptorMapper *layoutDescriptorMapper = [[SKDescriptorMapper alloc] initWithDefaults];
  [client addPlugin:[[FlipperKitLayoutPlugin alloc] initWithRootNode:application withDescriptorMapper:layoutDescriptorMapper]];
  [client addPlugin:[[FKUserDefaultsPlugin alloc] initWithSuiteName:nil]];
  [client addPlugin:[FlipperKitReactPlugin new]];
  [client addPlugin:[[FlipperKitNetworkPlugin alloc] initWithNetworkAdapter:[SKIOSNetworkAdapter new]]];
  [client start];
}
#endif

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#ifdef FB_SONARKIT_ENABLED
  InitializeFlipper(application);
#endif
  
  
  // Override TrustKit's logger method, useful for local debugging
  void (^loggerBlock)(NSString *) = ^void(NSString *message)
  {
    NSLog(@"TrustKit log: %@", message);
  };
  [TrustKit setLoggerBlock:loggerBlock];
  
  NSDictionary *trustKitConfig =
  @{
    // Swizzling because we can't access the NSURLSession instance used in React Native's fetch method
    kTSKSwizzleNetworkDelegates: @YES,
    kTSKPinnedDomains: @{
        @"busdue.com" : @{
            kTSKIncludeSubdomains: @YES, // Pin all subdomains
            kTSKEnforcePinning: @YES, // Block connections if pinning validation failed
            kTSKDisableDefaultReportUri: @YES,
            kTSKPublicKeyHashes : @[
                @"mH0AeOjZJbA28s4NdVbrZsjQlquHT6hbR3FCcp7C3hc=",
                @"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB=", // Fake backup key but we need to provide 2 pins
            ],
        },
    }};
  [TrustKit initSharedInstanceWithConfiguration:trustKitConfig];
  [TrustKit sharedInstance].pinningValidatorCallback = ^(TSKPinningValidatorResult *result, NSString *notedHostname, TKSDomainPinningPolicy *policy) {
    if (result.finalTrustDecision == TSKTrustEvaluationFailedNoMatchingPin) {
      NSLog(@"TrustKit certificate matching failed");
      // Add more logging here. i.e. Sentry, BugSnag etc
    }
  };
  
  if ([FIRApp defaultApp] == nil) {
    [FIRApp configure];
  }
  
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"GarentaDay"
                                            initialProperties:nil];
  
  if (@available(iOS 13.0, *)) {
    rootView.backgroundColor = [UIColor systemBackgroundColor];
  } else {
    rootView.backgroundColor = [UIColor whiteColor];
  }
  
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  
  [[FBSDKApplicationDelegate sharedInstance] application:application
                           didFinishLaunchingWithOptions:launchOptions];
  
  [RNBootSplash initWithStoryboard:@"BootSplash" rootView:rootView];
  
  return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  if (@available(iOS 14.0, *))
  {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
      [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
        switch (status) {
          case ATTrackingManagerAuthorizationStatusAuthorized:
            // The user authorizes access to app-related data that can be used for tracking the user or the device.
            [FBSDKSettings setAdvertiserTrackingEnabled:YES];
            break;
          case ATTrackingManagerAuthorizationStatusNotDetermined:
            // The user has not yet received an authorization request to authorize access to app-related data that can be used for tracking the user or the device.
            //[self requestTrackingAccess];
            [FBSDKSettings setAdvertiserTrackingEnabled:NO];
            break;
          case ATTrackingManagerAuthorizationStatusDenied:
            [FBSDKSettings setAdvertiserTrackingEnabled:NO];
            // The user denies authorization to access app-related data that can be used for tracking the user or the device.
          case ATTrackingManagerAuthorizationStatusRestricted:
            // The authorization to access app-related data that can be used for tracking the user or the device is restricted.
            [FBSDKSettings setAdvertiserTrackingEnabled:NO];
            break;
        }
      }];
    });
  }
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}

@end
